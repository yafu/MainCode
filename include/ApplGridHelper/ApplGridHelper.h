#ifndef ApplGridHelper_ApplGridHelper_h
#define ApplGridHelper_ApplGridHelper_h

#include <iostream>
#include <string>
#include <vector>

#ifdef USE_APPLGRID
#include "appl_grid/appl_grid.h"
#include "appl_grid/appl_timer.h"
#endif

#include "RootCommon.h"
#include "Tools/Tools.h"
#include "Uncertainty/PDFUncertainty.h"

// lhapdf routines
#ifdef USE_LHAPDF
#include "LHAPDF/LHAPDF.h"
extern "C" void evolvepdf_(const double& , const double& , double* );
extern "C" double alphaspdf_(const double& Q);

#ifdef ApplGridHelper_ApplGridHelper_cxx
void (*_pdf)(const double& x, const double& Q, double* f) = 0;
extern "C" void evolvepdfpbar_(const double& x, const double& Q, double* f){
    double xf[13];
    _pdf( x, Q, xf );
    for ( int i = 0 ; i < 13 ; i++ ) f[i] = xf[12 - i];
}
#endif

#endif

using namespace std;

class ApplGridHelper
{
 public:

#ifdef USE_APPLGRID
 map<TString, appl::grid*> ApplGridMap;
#endif

 map<TString, bool> isInitial;

 TString PDFName;
 int iSet;

 ApplGridHelper();
 virtual void InitialPDF(TString PDFName, int iSet);
 virtual void DefaultConvolute(TString GridName);
 virtual void DefaultConvoluteppbar(TString GridName);
 virtual void InitialGrid(TString GridName);
 virtual TH1D* Convolute(TString GridName, TString KFactorName = "");
 virtual TH1D* Convoluteppbar(TString GridName, TString KFactorName = "");
 virtual TH1D* ConvoluteSubProc(TString GridName, int SubProc, TString KFactorName = "");
 virtual TH1D* ConvoluteppbarSubProc(TString GridName, int SubProc, TString KFactorName = "");

 map<TString, vector<double>> KFactorMap;
 virtual void ReadKFactor(TString FileName);

 map<TString, TH1D*> OneMinusTwoD_uu;
 map<TString, TH1D*> OneMinusTwoD_dd;
 map<TString, TH1D*> RelativeCrossSection_uu;
 map<TString, TH1D*> RelativeCrossSection_dd;

 virtual void CalcP0(TString GridNameYP, TString GridNameYM, TString subName, double& P0u, double& P0d, vector<TString> KFactors = {});
 virtual void CalcP0ppbar(TString GridName, TString subName, double& P0u, double& P0d, vector<TString> KFactors = {});
 virtual double GetP0(TH1D* right, TH1D* wrong, TH1D* total);
 virtual void CalcP0PDFUnc(TString GridNameYP, TString GridNameYM, TString subName, TString PDFName, int nSet, vector<TString> KFactors = {});

 bool isLinkFile = false;
 TFile* hf;
 virtual void openFile(const char* name){hf = new TFile(name, "RECREATE"); isLinkFile = true;}
 virtual void LinkFile(TFile* file){hf = file; isLinkFile = true;}
 virtual void SaveFile(){hf->Write(); hf->Close();}

 virtual void GenerateKFactor(TString FileName, TString GridName, TH1D* HighOrder, vector<TString> subProcs = {});
 virtual void GenerateKFactorppbar(TString FileName, TString GridName, TH1D* HighOrder, vector<TString> subProcs = {});

 map<int, TString> SubProcName;
 map<TString, int> SubProcIndex;
 virtual void InitialSubProcName(){
   SubProcName[0] = "bbarbbar";
   SubProcName[1] = "bbarcbar";
   SubProcName[2] = "bbarsbar";
   SubProcName[3] = "bbarubar";
   SubProcName[4] = "bbardbar";
   SubProcName[5] = "bbargluon";
   SubProcName[6] = "bbard";
   SubProcName[7] = "bbaru";
   SubProcName[8] = "bbars";
   SubProcName[9] = "bbarc";
   SubProcName[10] = "bbarb";
   SubProcName[11] = "cbarbbar";
   SubProcName[12] = "cbarcbar";
   SubProcName[13] = "cbarsbar";
   SubProcName[14] = "cbarubar";
   SubProcName[15] = "cbardbar";
   SubProcName[16] = "cbargluon";
   SubProcName[17] = "cbard";
   SubProcName[18] = "cbaru";
   SubProcName[19] = "cbars";
   SubProcName[20] = "cbarc";
   SubProcName[21] = "cbarb";
   SubProcName[22] = "sbarbbar";
   SubProcName[23] = "sbarcbar";
   SubProcName[24] = "sbarsbar";
   SubProcName[25] = "sbarubar";
   SubProcName[26] = "sbardbar";
   SubProcName[27] = "sbargluon";
   SubProcName[28] = "sbard";
   SubProcName[29] = "sbaru";
   SubProcName[30] = "sbars";
   SubProcName[31] = "sbarc";
   SubProcName[32] = "sbarb";
   SubProcName[33] = "ubarbbar";
   SubProcName[34] = "ubarcbar";
   SubProcName[35] = "ubarsbar";
   SubProcName[36] = "ubarubar";
   SubProcName[37] = "ubardbar";
   SubProcName[38] = "ubargluon";
   SubProcName[39] = "ubard";
   SubProcName[40] = "ubaru";
   SubProcName[41] = "ubars";
   SubProcName[42] = "ubarc";
   SubProcName[43] = "ubarb";
   SubProcName[44] = "dbarbbar";
   SubProcName[45] = "dbarcbar";
   SubProcName[46] = "dbarsbar";
   SubProcName[47] = "dbarubar";
   SubProcName[48] = "dbardbar";
   SubProcName[49] = "dbargluon";
   SubProcName[50] = "dbard";
   SubProcName[51] = "dbaru";
   SubProcName[52] = "dbars";
   SubProcName[53] = "dbarc";
   SubProcName[54] = "dbarb";
   SubProcName[55] = "gluonbbar";
   SubProcName[56] = "gluoncbar";
   SubProcName[57] = "gluonsbar";
   SubProcName[58] = "gluonubar";
   SubProcName[59] = "gluondbar";
   SubProcName[60] = "gluongluon";
   SubProcName[61] = "gluond";
   SubProcName[62] = "gluonu";
   SubProcName[63] = "gluons";
   SubProcName[64] = "gluonc";
   SubProcName[65] = "gluonb";
   SubProcName[66] = "dbbar";
   SubProcName[67] = "dcbar";
   SubProcName[68] = "dsbar";
   SubProcName[69] = "dubar";
   SubProcName[70] = "ddbar";
   SubProcName[71] = "dgluon";
   SubProcName[72] = "dd";
   SubProcName[73] = "du";
   SubProcName[74] = "ds";
   SubProcName[75] = "dc";
   SubProcName[76] = "db";
   SubProcName[77] = "ubbar";
   SubProcName[78] = "ucbar";
   SubProcName[79] = "usbar";
   SubProcName[80] = "uubar";
   SubProcName[81] = "udbar";
   SubProcName[82] = "ugluon";
   SubProcName[83] = "ud";
   SubProcName[84] = "uu";
   SubProcName[85] = "us";
   SubProcName[86] = "uc";
   SubProcName[87] = "ub";
   SubProcName[88] = "sbbar";
   SubProcName[89] = "scbar";
   SubProcName[90] = "ssbar";
   SubProcName[91] = "subar";
   SubProcName[92] = "sdbar";
   SubProcName[93] = "sgluon";
   SubProcName[94] = "sd";
   SubProcName[95] = "su";
   SubProcName[96] = "ss";
   SubProcName[97] = "sc";
   SubProcName[98] = "sb";
   SubProcName[99] = "cbbar";
   SubProcName[100] = "ccbar";
   SubProcName[101] = "csbar";
   SubProcName[102] = "cubar";
   SubProcName[103] = "cdbar";
   SubProcName[104] = "cgluon";
   SubProcName[105] = "cd";
   SubProcName[106] = "cu";
   SubProcName[107] = "cs";
   SubProcName[108] = "cc";
   SubProcName[109] = "cb";
   SubProcName[110] = "bbbar";
   SubProcName[111] = "bcbar";
   SubProcName[112] = "bsbar";
   SubProcName[113] = "bubar";
   SubProcName[114] = "bdbar";
   SubProcName[115] = "bgluon";
   SubProcName[116] = "bd";
   SubProcName[117] = "bu";
   SubProcName[118] = "bs";
   SubProcName[119] = "bc";
   SubProcName[120] = "bb";

   for(auto iter = SubProcName.begin(); iter != SubProcName.end(); iter++){
     SubProcIndex[iter->second] = iter->first;
   }
 }

};

#endif
