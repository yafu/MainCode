#ifndef CTEQHelper_dtaReader_h
#define CTEQHelper_dtaReader_h

#include "RootCommon.h"
#include "Tools/Tools.h"

using namespace std;

class dtaReader
{
 public:

 ifstream data;
 TFile* hf;

 string FileLine;

 int DataID = -999;
 double NormFac = 0.0;
 int Npt = 0;
 double Chi2 = 0.0;
 double Spartyness = 0.0;
 double Chi2N = 0.0;
 double R2 = 0.0;
 vector<double> rk;

 map<int, TH1D *> DataNormFac;
 map<int, TH1D *> DataChi2;
 map<int, TH1D *> DataSpartyness;
 map<int, TH1D *> DataChi2N;

 map<int, TH1D *> Y;
 map<int, TH1D *> Q;
 map<int, TH1D *> Rs;
 map<int, TH1D *> Exp;
 map<int, TH1D *> Theory;
 map<int, TH1D *> TotErr;
 map<int, TH1D *> ExpOverFit;
 map<int, TH1D *> ErrOverFit;
 map<int, TH1D *> ChiSq;
 map<int, TH1D *> Shift;
 map<int, TH1D *> ShiftedData;
 map<int, TH1D *> UnCorErr;
 map<int, TH1D *> ReducedChi2;
 map<int, TH1D *> lob;

 map<int, TH1D *> DataR2;
 map<int, TH1D *> Datark;
 map<int, bool> HasR2;

 dtaReader(TString FileName);
 virtual void ReadFile();
 virtual void FillData();
 virtual void openFile(TString name){hf = new TFile(name, "RECREATE");hf->cd();};
 virtual void SaveFile(){hf->Write(); hf->Close();};

};

#endif
