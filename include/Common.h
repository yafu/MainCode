#ifndef Common_h
#define Common_h

#include <iostream>
#include <fstream>
#include "TString.h"
#include <pthread.h>

#include "loopMC.h"
#include "Wasymmetry/loopWasym.h"
#include "loopResBos.h"
#include "loopResBosW.h"
#include "loopResBosWTev.h"
#include "loopPythia.h"
#include "loopPythiaW.h"
#include "loopPythiaWW.h"
#include "loopHerwig.h"
#include "loopFwdReco.h"
#include "WZPolarization/loopWZPolarization.h"
#include "loopCustom.h"
#include "loopCellInfo.h"
#include "loopSherpaWW.h"
#include "loopResBosCMS.h"
#include "loopResBosD0.h"
#include "loopFlavorAsym.h"
#include "loopResBosHighMassZ.h"
#include "loopMadGraph.h"

#include "Externs.h"

extern pthread_mutex_t mtx;

void InitialExterns();
void* StartThread(void* arg);


#endif
