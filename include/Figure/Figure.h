#include <iostream>
#include <TH1D.h>
#include <TROOT.h>
#include <TFile.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TF1.h>
#include "TH2D.h"
#include "THStack.h"
#include "TColor.h"
#include "TGaxis.h"
#include "TLatex.h"

using namespace std;

class Figure
{
 public:

 TCanvas *MyN;
 TGaxis *axis1;

 TString myFigureName;
 TString myXTitle;
 TString myYTitle;

 TFile* file[100];
 TFile* PDFfile[100];
 TH1D* h1[100];
 TH2D* h2[100];
 TH1D* PDF1[100];
 TText* text[100];

 TLegend *legend1;
 TLegend *legend2;
 TLegend *legend3;

 TString myLegendName[100];
 TString myPDFLegendName[100];
 TString myText[100];

 double legendxmin, legendxmax, legendymin, legendymax;
 double TextX, TextY;
 double XRangeMin, XRangeMax, YRangeMin, YRangeMax;
 double BottomMargin, TopMargin, LeftMargin, RightMargin;
 double wmin, wmax;
 double ymin, ymax;

 double NumerPoint, DenomPoint, NumerError, DenomError;
 double DataPoint, DataError;

 double LegendTextSize;

 int ndiv;

 bool isSingle;
 bool isSingleRatio;
 bool isSimple;
 bool isRatio;
 bool isDelta;
 bool isDeltaSigma;
 bool isPDF;
 bool is2D;

 bool isInput;
 bool isInputPDFError;

 bool withPDFError;
 int Process;

 bool isHIST;
 bool isE0;

 bool isAddGaxis;

 bool isNorm;
 bool isLogX;
 bool isEfficiency;

 bool isSetXRange;
 bool isSetYRange;
 bool isSetYRange0;

 bool isWsample;

 bool isRebin;
 int RebinType;
 double* rebin;
 int ngroup;

 bool DoSave;

 int FigureCount;
 int PDFFigureCount;
 int TextCount;

 Figure(const char* FigureName, const char* XTitle, const char* YTitle);
 virtual ~Figure();
 virtual void SetMode(const char* ModeName);
 virtual void SetPlot(const char* PlotMode);
 virtual void SetNorm();
 virtual void SetLogX();
 virtual void SetEfficiency();
 virtual void SetWsample();
 virtual void Input(int Number, const char* rootName, const char* histName, const char* legendName = "");
 virtual void InputPDFError(int Number, const char* rootName, const char* histName, const char* legendName = "");
 virtual void AddText(int Number, const char* TextContent);
 virtual void SetProcess(int Process);
 virtual void SetPlotFormat(int Number, Color_t Color);
 virtual void SetPDFPlotFormat(int Number, Color_t Color);
 virtual void SetXRange(double xmin, double xmax);
 virtual void SetYRange(double ymin, double ymax, int iPlot = 1);
 virtual void SetBaseLine(int Number);
 virtual void Divide(int numer, int denom);
 virtual void Output();
 virtual void Draw();
 virtual void DrawCorrelation(const char* fileName, const char* histName);
 virtual void AddGaxis(double wmin, double wmax, int ndiv);
 virtual void SaveFigure();
 virtual void Rebin(int Nbin, double* xbins);
 virtual void Rebin(int Nbin);
 virtual void SetLegendPosition(double xmin, double ymin, double xmax, double ymax);
 virtual void SetLegendTextSize(double LegendTextSize);
};

Figure::Figure(const char* FigureName, const char* XTitle, const char* YTitle)
{
 FigureCount = 0;
 PDFFigureCount = 0;
 TextCount = 0;

 isSingle = false;
 isSingleRatio = false;
 isSimple = false;
 isRatio = false;
 isDelta = false;
 isDeltaSigma = false;
 isPDF = false;
 is2D = false;

 isInput = false;
 isInputPDFError = false;

 withPDFError = false;
 Process = 0;

 isHIST = false;
 isE0 = false;

 isAddGaxis = false;

 isNorm = false;
 isLogX = false;
 isEfficiency = false;

 isSetXRange = false;
 isSetYRange = false;
 isSetYRange0 = false;

 isWsample = false;

 isRebin = false;
 RebinType = 0;

 DoSave = false;

 LegendTextSize = 0.05;

 myFigureName = FigureName;
 myXTitle = XTitle;
 myYTitle = YTitle;
}

Figure::~Figure()
{
}

void Figure::SetMode(const char* ModeName)
{
 TString myMode = ModeName;
 if(myMode == "SINGLE") isSingle = true;
 if(myMode == "SINGLERATIO") isSingleRatio = true;
 if(myMode == "SIMPLE") isSimple = true;
 if(myMode == "RATIO") isRatio = true;
 if(myMode == "DELTA") isDelta = true;
 if(myMode == "DELTASIGMA") isDeltaSigma = true;
 if(myMode == "PDF") isPDF = true;
 if(myMode == "2DFIGURE") is2D = true;
}

void Figure::SetPlot(const char* PlotMode)
{
 TString myPlotMode = PlotMode;
 if(myPlotMode == "HIST") isHIST = true;
 if(myPlotMode == "E0") isE0 = true;
}

void Figure::SetNorm()
{
 isNorm = true;
}

void Figure::SetLogX()
{
 isLogX = true;
}

void Figure::SetEfficiency()
{
 isEfficiency = true;
}

void Figure::SetWsample()
{
 isWsample = true;
}

void Figure::SaveFigure()
{
 DoSave = true;
}

void Figure::Rebin(int Nbin)
{
 isRebin = true;
 RebinType = 1;
 ngroup = Nbin;
}

void Figure::Rebin(int Nbin, double* xbins)
{
 isRebin = true;
 RebinType = 2;
 rebin = xbins;
 ngroup = Nbin;
}

void Figure::SetLegendPosition(double xmin, double ymin, double xmax, double ymax)
{
 legendxmin = xmin;
 legendxmax = xmax;
 legendymin = ymin;
 legendymax = ymax;
}

void Figure::SetLegendTextSize(double LegendTextSize)
{
 this->LegendTextSize = LegendTextSize;
}

void Figure::Output()
{
 if(!isInput) {cout<<"There is no input file."<<endl; return;}

 for(int i = 1; i < FigureCount + 1; i++)
 {
  for(int ibin = 0; ibin < h1[i]->GetNbinsX(); ibin++)
  {
   if(isE0) cout<<(h1[i]->GetBinCenter(ibin+1) - h1[i]->GetBinWidth(ibin+1) / 2)<<"~"<<(h1[i]->GetBinCenter(ibin+1) + h1[i]->GetBinWidth(ibin+1) / 2)<<"  "<<h1[i]->GetBinContent(ibin+1)<<"  "<<h1[i]->GetBinError(ibin+1)<<endl;

   if(isHIST) cout<<(h1[i]->GetBinCenter(ibin+1) - h1[i]->GetBinWidth(ibin+1) / 2)<<"~"<<(h1[i]->GetBinCenter(ibin+1) + h1[i]->GetBinWidth(ibin+1) / 2)<<"  "<<h1[i]->GetBinContent(ibin+1)<<endl;
  }
 }

}

void Figure::Input(int Number, const char* rootName, const char* histName, const char* legendName)
{
 isInput = true;

 FigureCount++;

 file[Number] = new TFile(rootName);
 h1[Number] = (TH1D *)file[Number]->Get(histName);
 if(is2D) h2[Number] = (TH2D *)file[Number]->Get(histName);

 myLegendName[Number] = legendName;

 if(isRebin)
 {
  cout<<"Histogram has been rebinned."<<endl;
  if(RebinType == 1) h1[Number]->Rebin(ngroup);
  if(RebinType == 2) h1[Number] = (TH1D *)h1[Number]->Rebin(ngroup, histName, rebin);
  if(is2D) {cout<<"Error: 2D histogram cannot be rebined"<<endl; return;}
 }
}

void Figure::InputPDFError(int Number, const char* rootName, const char* histName, const char* legendName)
{
 isInputPDFError = true;

 withPDFError = true;

 PDFFigureCount++;

 PDFfile[Number] = new TFile(rootName);
 PDF1[Number] = (TH1D *)PDFfile[Number]->Get(histName);
 myPDFLegendName[Number] = legendName;

 if(isRebin) cout<<"WARNING: PDF uncertainty cannot be rebined!"<<endl;
}

void Figure::AddText(int Number, const char* TextContent)
{
 if(Number == 0) {cout<<"WARNING!! Please add text from No.1"<<endl;}
 myText[Number] = TextContent;

 TextCount++;
}

void Figure::SetProcess(int Process)
{
 this->Process = Process;
 if(Process == 1) cout<<"Scale Uncertainty"<<endl;
 if(Process == 2) cout<<"PDF Uncertainty"<<endl;
}

void Figure::SetPlotFormat(int Number, Color_t Color)
{
 if(!isInput) {cout<<"There is no input file."<<endl; return;}

 if(!is2D){
   if(isSingle || isSingleRatio)
   {
    h1[Number]->SetMarkerStyle(7);
    h1[Number]->SetMarkerColor(Color);
    h1[Number]->SetMarkerSize(0.5);
    h1[Number]->SetMarkerStyle(21);
    h1[Number]->SetLineWidth(4);
    h1[Number]->SetLineColor(Color);

//   legend1->AddEntry(h1[Number], myLegendName[Number],"lpfe");
   }
 }
}

void Figure::SetPDFPlotFormat(int Number, Color_t Color)
{
 if(!isInputPDFError) {cout<<"There is no input file."<<endl; return;}

 if(isSingle || isSingleRatio)
 {
  PDF1[Number]->SetMarkerStyle(7);
  PDF1[Number]->SetMarkerColor(Color);
  PDF1[Number]->SetMarkerSize(0.5);
  PDF1[Number]->SetMarkerStyle(21);
  PDF1[Number]->SetLineWidth(4);
  PDF1[Number]->SetLineColor(Color);

// legend1->AddEntry(h1[Number], myLegendName[Number],"lpfe");
 }
}

void Figure::SetXRange(double xmin, double xmax)
{
 if(!isInput) {cout<<"There is no input file."<<endl; return;}

 if(!is2D){
   h1[1]->GetXaxis()->SetRangeUser(xmin, xmax);
   if(withPDFError) PDF1[1]->GetXaxis()->SetRangeUser(xmin, xmax);

   this->XRangeMin = xmin;
   this->XRangeMax = xmax;
 }

 isSetXRange = true;
}

void Figure::SetYRange(double ymin, double ymax, int iPlot)
{
 if(is2D){
  cout<<"SetYRange is only for single plot."<<endl;
  cout<<"Other plot type please use other tools."<<endl;
  return;
 }

 isSetYRange = true;

 if(!isInput) {cout<<"There is no input file."<<endl; return;}

 if(!is2D){
   if(iPlot == 0){
     isSetYRange0 = true;
     isSetYRange = false;
     this->ymin = ymin;
     this->ymax = ymax;
     return;
   }
   h1[iPlot]->GetYaxis()->SetRangeUser(ymin, ymax);
 }
}

void Figure::SetBaseLine(int Number)
{
 if(!isInput) {cout<<"There is no input file."<<endl; return;}

 if(!is2D){
   h1[0] = (TH1D *)h1[Number]->Clone("h1");

   for(int i = 1; i < FigureCount + 1; i++)
   {
    h1[i]->Divide(h1[0]);
   }
 }
}

void Figure::Divide(int numer, int denom)
{
 if(!isInput) {cout<<"There is no input file."<<endl; return;}

// h1[0] = (TH1D *)h1[denom]->Clone("h1");

 if(!is2D)
 {
  if(isEfficiency){
    for(int ibin = 1; ibin <= h1[numer]->GetNbinsX(); ibin++){
        NumerPoint = h1[numer]->GetBinContent(ibin);
        DenomPoint = h1[denom]->GetBinContent(ibin);
        NumerError = h1[numer]->GetBinError(ibin);
        DenomError = h1[denom]->GetBinError(ibin);

        DataPoint = NumerPoint / DenomPoint;
        DataError = sqrt(((DenomPoint - NumerPoint) * (DenomPoint - NumerPoint) * NumerError * NumerError / (DenomPoint * DenomPoint)) + (NumerPoint * NumerPoint * (NumerError * NumerError + DenomError * DenomError) / (DenomPoint * DenomPoint))) / DenomPoint;

        if(DenomPoint < 0.00000001) DataPoint = 0;
        if(DenomError < 0.00000001) DataError = 0;
//        cout<<" "<<NumerPoint<<" "<<NumerError<<" "<<DenomPoint<<" "<<DenomError<<" "<<DataPoint<<" "<<DataError<<endl;
        h1[numer]->SetBinContent(ibin, DataPoint);
        h1[numer]->SetBinError(ibin, DataError);
    }
  }

  if(!isEfficiency){
    h1[numer]->Divide(h1[denom]);
  }

  delete h1[denom];
 }
 if(is2D)
 {
  if(isEfficiency){
    for(int ibinx = 1; ibinx <= h2[numer]->GetNbinsX(); ibinx++){
       for(int ibiny = 1; ibiny <= h2[numer]->GetNbinsY(); ibiny++){
          NumerPoint = h2[numer]->GetBinContent(ibinx, ibiny);
          DenomPoint = h2[denom]->GetBinContent(ibinx, ibiny);
          NumerError = h2[numer]->GetBinError(ibinx, ibiny);
          DenomError = h2[denom]->GetBinError(ibinx, ibiny);

          DataPoint = NumerPoint / DenomPoint;
          DataError = sqrt(((DenomPoint - NumerPoint) * (DenomPoint - NumerPoint) * NumerError * NumerError / (DenomPoint * DenomPoint)) + (NumerPoint * NumerPoint * (NumerError * NumerError + DenomError * DenomError) / (DenomPoint * DenomPoint))) / DenomPoint;

          if(DenomPoint < 0.00000001) DataPoint = 0;
          if(DenomError < 0.00000001) DataError = 0;
          h2[numer]->SetBinContent(ibinx, ibiny, DataPoint);
          h2[numer]->SetBinError(ibinx, ibiny, DataError);
       }
    }
  }

  if(!isEfficiency){
    h2[numer]->Divide(h2[denom]);
  }
  
  delete h2[denom];
 }
 FigureCount--;
}

void Figure::AddGaxis(double wmin, double wmax, int ndiv)
{
 isAddGaxis = true;
 this->wmin = wmin;
 this->wmax = wmax;
 this->ndiv = ndiv;
}

void Figure::Draw()
{
 if(!isInput) {cout<<"There is no input file."<<endl; return;}

 if(isPDF) isSingle = false;

 TPad *pad1;
 TPad *pad2;
 TPad *pad3;

 if(isSingle)
 {
/*************************/
/*******Single Plot*******/
/*************************/
  MyN = new TCanvas("MyN","MyN",1000,1000);
//  MyN->Range(0.0 ,0.0, 1.0, 1.0);
  MyN->cd();

  if(isLogX) gPad->SetLogx();

  if(!withPDFError)
  {
   /*********************************/
   /***********Single Plot***********/
   /*******Set Pad Information*******/
   /*********************************/
   pad1 = new TPad("pad1", "pad1", 0, 0, 1.0, 1.0);

   BottomMargin = 0.1;
   TopMargin = 0.1;
   LeftMargin = 0.15;
   RightMargin = 0.05;
   pad1->SetBottomMargin(BottomMargin);
   pad1->SetTopMargin(TopMargin);
   pad1->SetLeftMargin(LeftMargin);
   pad1->SetRightMargin(RightMargin);
  }
  if(withPDFError)
  {
   /****************************************/
   /*******Single Plot with PDF Error*******/
   /**********Set Pad Information***********/
   /****************************************/
   pad1 = new TPad("pad1", "pad1", 0, 0.3, 1.0, 1.0);

   BottomMargin = 0;
   TopMargin = 0.15;
   LeftMargin = 0.15;
   RightMargin = 0.05;
   pad1->SetBottomMargin(BottomMargin);
   pad1->SetTopMargin(TopMargin);
   pad1->SetLeftMargin(LeftMargin);
   pad1->SetRightMargin(RightMargin);
  }
  pad1->SetGridx();
  pad1->SetGridy();
  pad1->Draw();
  pad1->cd();

  if(!is2D)
  {
   /*********************************/
   /***********Single Plot***********/
   /*******Set Plot Information******/
   /*********************************/
   h1[1]->SetTitle("");
   h1[1]->SetStats(0);

   h1[1]->GetXaxis()->SetTitle(myXTitle);
   h1[1]->GetXaxis()->SetTitleSize(0.04);
   h1[1]->GetXaxis()->SetTitleFont(72);
   h1[1]->GetXaxis()->SetTitleOffset(1);

   h1[1]->GetXaxis()->SetLabelSize(0.04);
   h1[1]->GetXaxis()->SetLabelFont(72);

   h1[1]->GetYaxis()->SetTitle(myYTitle);
   h1[1]->GetYaxis()->SetTitleSize(0.04);
   h1[1]->GetYaxis()->SetTitleFont(72);
   h1[1]->GetYaxis()->SetTitleOffset(1);

   h1[1]->GetYaxis()->SetLabelSize(0.03);
   h1[1]->GetYaxis()->SetLabelFont(72);

   h1[1]->SetMarkerStyle(7);
   h1[1]->SetMarkerSize(0.5);
   h1[1]->SetMarkerStyle(21);
   h1[1]->SetMarkerColor(kBlue+1);
   h1[1]->SetLineWidth(4);
   h1[1]->SetLineColor(kBlue+1);

   if(isE0) h1[1]->Draw("E0");
   if(isHIST) h1[1]->Draw("HIST");

   YRangeMax = h1[1]->GetMaximum();
   YRangeMin = h1[1]->GetMinimum();
   if(!isSetXRange){
     XRangeMax = h1[1]->GetXaxis()->GetBinUpEdge(h1[1]->GetNbinsX());
     XRangeMin = h1[1]->GetXaxis()->GetBinLowEdge(1);
   }

   int iPlot = 2;
   while(iPlot <= FigureCount)
   {
    if(isE0) h1[iPlot]->Draw("E0 same");
    if(isHIST) h1[iPlot]->Draw("HIST same");
    if(YRangeMax < h1[iPlot]->GetMaximum()) YRangeMax = h1[iPlot]->GetMaximum();
    if(YRangeMin > h1[iPlot]->GetMinimum()) YRangeMin = h1[iPlot]->GetMinimum();
    if(!isSetXRange){
      if(XRangeMax < h1[iPlot]->GetXaxis()->GetBinUpEdge(h1[iPlot]->GetNbinsX())) XRangeMax = h1[iPlot]->GetXaxis()->GetBinUpEdge(h1[iPlot]->GetNbinsX());
      if(XRangeMin > h1[iPlot]->GetXaxis()->GetBinLowEdge(1)) XRangeMin = h1[iPlot]->GetXaxis()->GetBinLowEdge(1);
    }
    iPlot++;
   }

   if(isLogX) gPad->SetLogx();

   if(!isSetYRange){
     if(YRangeMin > 0) h1[1]->GetYaxis()->SetRangeUser(YRangeMin / 1.1, YRangeMax * 1.1);
     if(YRangeMin < 0) h1[1]->GetYaxis()->SetRangeUser(YRangeMin - (YRangeMax * 0.1), YRangeMax * 1.1); 
   }

   legend1 = new TLegend(legendxmin * (1 - LeftMargin - RightMargin) + LeftMargin, legendymin * (1 - TopMargin - BottomMargin) + BottomMargin, legendxmax * (1 - LeftMargin - RightMargin) + LeftMargin, legendymax * (1 - TopMargin - BottomMargin) + BottomMargin);

   for(int i = 1; i < FigureCount + 1; i++)
   {
    if(!((TString)myLegendName[i] == "")) legend1->AddEntry(h1[i], myLegendName[i],"lpfe");
   }
   legend1->Draw("same");
   legend1->SetFillColor(0);
   legend1->SetFillStyle(0);
   legend1->SetLineColor(0);
   legend1->SetLineWidth(0);
   legend1->SetTextSize(LegendTextSize);
   legend1->SetTextFont(72);
  }

  if(is2D)
  {
   /**************************/
   /*********2D Plot**********/
   /**************************/
   pad1->SetRightMargin(0.15);
   h2[1]->SetTitle("");
   h2[1]->SetStats(0);

   h2[1]->GetXaxis()->SetTitle(myXTitle);
   h2[1]->GetXaxis()->SetTitleSize(0.04);
   h2[1]->GetXaxis()->SetTitleFont(72);
   h2[1]->GetXaxis()->SetTitleOffset(1);

   h2[1]->GetXaxis()->SetLabelSize(0.04);
   h2[1]->GetXaxis()->SetLabelFont(72);

   h2[1]->GetYaxis()->SetTitle(myYTitle);
   h2[1]->GetYaxis()->SetTitleSize(0.04);
   h2[1]->GetYaxis()->SetTitleFont(72);
   h2[1]->GetYaxis()->SetTitleOffset(1);

   h2[1]->GetYaxis()->SetLabelSize(0.03);
   h2[1]->GetYaxis()->SetLabelFont(72);

   gStyle->SetTextFont(72);
//   h2[1]->SetMaximum(1.0);
//   h2[1]->SetMinimum(0.8);
   h2[1]->Draw("COLZ2");
  }

  if(withPDFError)
  {
   /****************************************/
   /*******Single Plot with PDF Error*******/
   /**********Set Plot Information**********/
   /****************************************/
   MyN->cd();
   pad2 = new TPad("pad2", "pad2", 0, 0, 1, 0.3);

   BottomMargin = 0.3;
   TopMargin = 0;
   LeftMargin = 0.15;
   RightMargin = 0.05;
   pad2->SetBottomMargin(BottomMargin);
   pad2->SetTopMargin(TopMargin);
   pad2->SetLeftMargin(LeftMargin);
   pad2->SetRightMargin(RightMargin);

   pad2->SetGridx();
   pad2->SetGridy();
   pad2->Draw();
   pad2->cd();

   int iPlot = 1;

   PDF1[1]->SetTitle("");
   PDF1[1]->SetStats(0);

   PDF1[1]->SetMarkerStyle(7);
   PDF1[1]->SetMarkerSize(0.5);
   PDF1[1]->SetMarkerStyle(21);
   PDF1[1]->SetMarkerColor(kBlue+1);
   PDF1[1]->SetLineWidth(4);
   PDF1[1]->SetLineColor(kBlue+1);

   while(iPlot <= PDFFigureCount)
   {
    PDF1[iPlot]->Draw("HIST same");
    iPlot++;
   }
   legend2 = new TLegend(legendxmin, legendymin, legendxmax, legendymax);
   for(int i = 1; i < FigureCount; i++)
   {
    legend2->AddEntry(PDF1[i], myLegendName[i],"lpfe");
   }

   PDF1[1]->GetYaxis()->SetNdivisions(505);

   if(Process == 1) PDF1[1]->GetYaxis()->SetTitle("Scale Uncertainty");
   if(Process == 2) PDF1[1]->GetYaxis()->SetTitle("PDF Uncertainty");

   PDF1[1]->GetYaxis()->SetRangeUser(PDF1[1]->GetMinimum() / 1.01, PDF1[1]->GetMaximum() * 1.2);
   PDF1[1]->GetYaxis()->SetTitleSize(0.1);
   PDF1[1]->GetYaxis()->SetTitleFont(72);
   PDF1[1]->GetYaxis()->SetTitleOffset(0.4);

   PDF1[1]->GetYaxis()->SetLabelFont(73);// Absolute font size in pixel (precision 3)
   PDF1[1]->GetYaxis()->SetLabelSize(20);

   PDF1[1]->SetLineWidth(2);

   PDF1[1]->GetXaxis()->SetTitle(myXTitle);
   PDF1[1]->GetXaxis()->SetTitleSize(0.1);
   PDF1[1]->GetXaxis()->SetTitleFont(72);
   PDF1[1]->GetXaxis()->SetTitleOffset(1);

   PDF1[1]->GetXaxis()->SetLabelSize(0.1);
   PDF1[1]->GetXaxis()->SetLabelFont(72);

   if(isLogX) gPad->SetLogx();

   if(isAddGaxis){
//      axis1 = new TGaxis(LeftMargin, BottomMargin, 1 - RightMargin, BottomMargin, wmin, wmax, ndiv, "G");
//      axis1->Draw();
   }
  }
 }

 if((isRatio || isDelta || isDeltaSigma) && !isPDF)
 {
  /****************************************/
  /*********Ratio/Delta/Pull Plot**********/
  /****************************************/
  double sum1 = h1[1]->Integral();
  double sum2 = h1[2]->Integral();
  if(isNorm) h1[1]->Scale(sum2 / sum1);

  MyN = new TCanvas("MyN","MyN",700,700);
  gStyle->SetGridWidth(1);
  MyN->cd();
  pad1 = new TPad("pad1", "pad1", 0, 0.25, 1.0, 1.0);
  BottomMargin = 0;
  TopMargin = 0.15;
  LeftMargin = 0.15;
  RightMargin = 0.05;
  pad1->SetBottomMargin(BottomMargin);
  pad1->SetTopMargin(TopMargin);
  pad1->SetLeftMargin(LeftMargin);
  pad1->SetRightMargin(RightMargin);
//  pad1->SetGridx();
//  pad1->SetGridy();
  pad1->Draw();
  pad1->cd();

  h1[1]->SetTitle("");
  h1[1]->SetStats(0);

  if(isE0)
  {
   h1[1]->Draw("E0");
   h1[2]->Draw("E0 same");
  }
  if(isHIST)
  {
   h1[1]->Draw("HIST");
   h1[2]->Draw("HIST same");
  }

//  h1[1]->GetXaxis()->SetTitle(myXTitle);
//  h1[1]->GetXaxis()->SetTitleSize(75);
//  h1[1]->GetXaxis()->SetTitleFont(72);
//  h1[1]->GetXaxis()->SetTitleOffset(1);

//  h1[1]->GetXaxis()->SetLabelSize(50);
//  h1[1]->GetXaxis()->SetLabelFont(72);

  h1[1]->GetYaxis()->SetTitle(myYTitle);
  h1[1]->GetYaxis()->SetTitleSize(0.06);
  h1[1]->GetYaxis()->SetTitleFont(42);
  h1[1]->GetYaxis()->SetTitleOffset(0.7);

  h1[1]->GetYaxis()->SetLabelSize(0.04);
  h1[1]->GetYaxis()->SetLabelFont(42);

  h1[1]->SetMarkerStyle(7);
  h1[1]->SetMarkerColor(kBlue+1);
  h1[1]->SetLineWidth(3);
  h1[1]->SetLineColor(kBlue+1);

  h1[2]->SetMarkerStyle(7);
  h1[2]->SetMarkerColor(kRed+1);
  h1[2]->SetLineWidth(3);
  h1[2]->SetLineColor(kRed+1);

  YRangeMax = h1[1]->GetMaximum();
  YRangeMin = h1[1]->GetMinimum();
  if(!isSetXRange){
    XRangeMax = h1[1]->GetXaxis()->GetBinUpEdge(h1[1]->GetNbinsX());
    XRangeMin = h1[1]->GetXaxis()->GetBinLowEdge(1);
  }

  int iPlot = 2;
  while(iPlot <= FigureCount)
  {
   if(YRangeMax < h1[iPlot]->GetMaximum()) YRangeMax = h1[iPlot]->GetMaximum();
   if(YRangeMin > h1[iPlot]->GetMinimum()) YRangeMin = h1[iPlot]->GetMinimum();

   if(!isSetXRange){
     if(XRangeMax < h1[iPlot]->GetXaxis()->GetBinUpEdge(h1[iPlot]->GetNbinsX())) XRangeMax = h1[iPlot]->GetXaxis()->GetBinUpEdge(h1[iPlot]->GetNbinsX());
     if(XRangeMin > h1[iPlot]->GetXaxis()->GetBinLowEdge(1)) XRangeMin = h1[iPlot]->GetXaxis()->GetBinLowEdge(1);
   }
   iPlot++;
  }

  if(isLogX) gPad->SetLogx();

//   if(!isSetYRange){
//     if(YRangeMin > 0) h1[1]->GetYaxis()->SetRangeUser(YRangeMin / 1.1, YRangeMax * 1.1);
//     if(YRangeMin < 0) h1[1]->GetYaxis()->SetRangeUser(YRangeMin - (YRangeMax * 0.1), YRangeMax * 1.1);
//   }

  if(!isSetYRange) h1[1]->GetYaxis()->SetRangeUser(YRangeMin - fabs(YRangeMin) * 0.1, YRangeMax + fabs(YRangeMax) * 0.1);

  double textlength = (legendymax - legendymin) / 2;
  int iText = 0;
  while(iText<=TextCount){
    iText++;
    text[iText] = new TLatex();

    TextX = XRangeMin + (XRangeMax - XRangeMin) * legendxmin;
    TextY = (YRangeMax + fabs(YRangeMax) * 0.1) - (YRangeMax + fabs(YRangeMax) * 0.1 - YRangeMin + fabs(YRangeMin) * 0.1) * ((1 - legendymax) + iText * textlength);
    text[iText]->SetText(TextX, TextY, myText[iText]);
    text[iText]->SetTextSize(0.04);
    text[iText]->SetTextFont(42);
    text[iText]->Draw("same");
  }

  double steplength = legendymax - legendymin;
  legendymax = legendymax - (TextCount + 0.5) * 0.5 * steplength;
  legendymin = legendymin - (TextCount + 0.5) * 0.5 * steplength;

  legend1 = new TLegend(legendxmin * (1 - LeftMargin - RightMargin) + LeftMargin, legendymin * (1 - TopMargin - BottomMargin) + BottomMargin, legendxmax * (1 - LeftMargin - RightMargin) + LeftMargin, legendymax * (1 - TopMargin - BottomMargin) + BottomMargin);

  legend1->AddEntry(h1[1], myLegendName[1],"lpfe");
  legend1->AddEntry(h1[2], myLegendName[2],"lpfe");
  legend1->SetNColumns(1);
  legend1->Draw("same");
  legend1->SetFillColor(0);
  legend1->SetFillStyle(0);
  legend1->SetLineColor(0);
  legend1->SetLineWidth(0);
  legend1->SetTextSize(0.04);
  legend1->SetTextFont(42);

  MyN->cd();
  pad2 = new TPad("pad2", "pad2", 0, 0, 1, 0.25);
  BottomMargin = 0.3;
  TopMargin = 0;
  LeftMargin = 0.15;
  RightMargin = 0.05;
  pad2->SetBottomMargin(BottomMargin);
  pad2->SetTopMargin(TopMargin);
  pad2->SetLeftMargin(LeftMargin);
  pad2->SetRightMargin(RightMargin);
  pad2->SetGridx();
  pad2->SetGridy();
  pad2->Draw();
  pad2->cd();

  h1[0] = (TH1D*)h1[1]->Clone("h0");

  if(isRatio) h1[0]->Divide(h1[2]);
  if(isDelta) h1[0]->Add(h1[2], -1);
  if(isDeltaSigma)
  {
   for(int ibin = 1; ibin < h1[1]->GetNbinsX() + 1; ibin++)
   {
    h1[0]->SetBinContent(ibin, (h1[1]->GetBinContent(ibin) - h1[2]->GetBinContent(ibin)) / sqrt(h1[1]->GetBinError(ibin) * h1[1]->GetBinError(ibin) + h1[2]->GetBinError(ibin) * h1[2]->GetBinError(ibin)));
   }
  }

  h1[0]->SetTitle("");
  h1[0]->SetStats(0);
  if(isE0) h1[0]->Draw("E0");
  if(isHIST) h1[0]->Draw("HIST");

  h1[0]->GetYaxis()->SetNdivisions(505);

  if(isRatio) h1[0]->GetYaxis()->SetTitle("#frac{" + myLegendName[1] + "}{" + myLegendName[2] + "}");
  if(isDelta) h1[0]->GetYaxis()->SetTitle("#Delta" + myYTitle);
  if(isDeltaSigma) h1[0]->GetYaxis()->SetTitle("#frac{#Delta}{#sigma}");

  if(!isSetYRange0) h1[0]->GetYaxis()->SetRangeUser(h1[0]->GetMinimum() / 1.01, h1[0]->GetMaximum() * 1.01);
  else h1[0]->GetYaxis()->SetRangeUser(ymin, ymax);

  h1[0]->GetYaxis()->SetTitleSize(0.1);
  h1[0]->GetYaxis()->SetTitleFont(42);
  h1[0]->GetYaxis()->SetTitleOffset(0.4);

  h1[0]->GetYaxis()->SetLabelFont(42);// Absolute font size in pixel (precision 3)
  h1[0]->GetYaxis()->SetLabelSize(0.1);

  h1[0]->SetLineWidth(2);

  h1[0]->GetXaxis()->SetTitle(myXTitle);
  h1[0]->GetXaxis()->SetTitleSize(0.12);
  h1[0]->GetXaxis()->SetTitleFont(42);
  h1[0]->GetXaxis()->SetTitleOffset(1);

  h1[0]->GetXaxis()->SetLabelSize(0.12);
  h1[0]->GetXaxis()->SetLabelFont(42);

  if(isLogX) gPad->SetLogx();

 }

 if(isPDF)
 {
  /********************************************/
  /*********Ratio Plot with PDF Error**********/
  /********************************************/
  double sum1 = h1[1]->Integral();
  double sum2 = h1[2]->Integral();
  if(isNorm) h1[1]->Scale(sum2 / sum1);

  MyN = new TCanvas("MyN","MyN",700,700);
  gStyle->SetGridWidth(1);
  MyN->cd();
  pad1 = new TPad("pad1", "pad1", 0, 0.3, 1.0, 1.0);
  BottomMargin = 0;
  TopMargin = 0.15;
  LeftMargin = 0.15;
  RightMargin = 0.05;
  pad1->SetBottomMargin(BottomMargin);
  pad1->SetTopMargin(TopMargin);
  pad1->SetLeftMargin(LeftMargin);
  pad1->SetRightMargin(RightMargin);
//  pad1->SetGridx();
//  pad1->SetGridy();
  pad1->Draw();
  pad1->cd();

  h1[1]->SetTitle("");
  h1[1]->SetStats(0);

  if(isE0)
  {
   h1[1]->Draw("E0");
   h1[2]->Draw("E0 same");
  }
  if(isHIST)
  {
   h1[1]->Draw("HIST");
   h1[2]->Draw("HIST same");
  }

  h1[1]->GetYaxis()->SetTitle(myYTitle);
  h1[1]->GetYaxis()->SetTitleSize(0.06);
  h1[1]->GetYaxis()->SetTitleFont(42);
  h1[1]->GetYaxis()->SetTitleOffset(0.8);

  h1[1]->GetYaxis()->SetLabelSize(0.04);
  h1[1]->GetYaxis()->SetLabelFont(42);

  if(isLogX) gPad->SetLogx();

  YRangeMin = h1[1]->GetMinimum();
  YRangeMax = h1[1]->GetMaximum();
  if(!isSetXRange){
    XRangeMax = h1[1]->GetXaxis()->GetBinUpEdge(h1[1]->GetNbinsX());
    XRangeMin = h1[1]->GetXaxis()->GetBinLowEdge(1);
  }

  if(!isSetYRange) h1[1]->GetYaxis()->SetRangeUser(YRangeMin - fabs(YRangeMin) * 0.1, YRangeMax + fabs(YRangeMax) * 0.1);

  h1[1]->SetLineColor(kBlue+1);
  h1[1]->SetLineWidth(2);
  h1[1]->SetMarkerStyle(7);
  h1[1]->SetMarkerColor(4);

  h1[2]->SetLineColor(kRed);
  h1[2]->SetLineWidth(2);
  h1[2]->SetMarkerStyle(7);
  h1[2]->SetMarkerColor(2);

/*  TText *text1 = new TText();
  text1->SetText(XRangeMin + (XRangeMax - XRangeMin) * (legendxmin - 0.15) / 0.8, (YRangeMin - fabs(YRangeMin) * 0.1) + (YRangeMax + fabs(YRangeMax) * 0.1 - YRangeMin + fabs(YRangeMin) * 0.1) * (legendymin - (legendymax - legendymin) * 0.5) / 0.85, "PDF: If1363.00");
  text1->SetTextSize(0.04);
  text1->SetTextFont(42);
  text1->Draw("same");

  if(!isWsample){
    TText *text2 = new TText();
    text2->SetText(XRangeMin + (XRangeMax - XRangeMin) * (legendxmin - 0.15) / 0.8, (YRangeMin - fabs(YRangeMin) * 0.1) + (YRangeMax + fabs(YRangeMax) * 0.1 - YRangeMin + fabs(YRangeMin) * 0.1) * (legendymin - (legendymax - legendymin) * 1) / 0.85, "STW: 0.2345");
    text2->SetTextSize(0.04);
    text2->SetTextFont(42);
    text2->Draw("same");
    TText *text3 = new TText();
    text3->SetText(XRangeMin + (XRangeMax - XRangeMin) * (legendxmin - 0.15) / 0.8, (YRangeMin - fabs(YRangeMin) * 0.1) + (YRangeMax + fabs(YRangeMax) * 0.1 - YRangeMin + fabs(YRangeMin) * 0.1) * (legendymin - (legendymax - legendymin) * 1.5) / 0.85, "Eta region: CF");
    text3->SetTextSize(0.04);
    text3->SetTextFont(42);
    text3->Draw("same");
  }
*/
  double textlength = (legendymax - legendymin) / 2;
  int iText = 0;
  while(iText<=TextCount){
    iText++;
    text[iText] = new TLatex();

    TextX = XRangeMin + (XRangeMax - XRangeMin) * legendxmin;
    TextY = (YRangeMax + fabs(YRangeMax) * 0.1) - (YRangeMax + fabs(YRangeMax) * 0.1 - YRangeMin + fabs(YRangeMin) * 0.1) * ((1 - legendymax) + iText * textlength);
    text[iText]->SetText(TextX, TextY, myText[iText]);
    text[iText]->SetTextSize(0.04);
    text[iText]->SetTextFont(42);
    text[iText]->Draw("same");
  }

  double steplength = legendymax - legendymin;
  legendymax = legendymax - (TextCount + 0.5) * 0.5 * steplength;
  legendymin = legendymin - (TextCount + 0.5) * 0.5 * steplength;

  legend1 = new TLegend(legendxmin * (1 - LeftMargin - RightMargin) + LeftMargin, legendymin * (1 - TopMargin - BottomMargin) + BottomMargin, legendxmax * (1 - LeftMargin - RightMargin) + LeftMargin, legendymax * (1 - TopMargin - BottomMargin) + BottomMargin);
  legend1->SetNColumns(1);
  legend1->AddEntry(h1[1], myLegendName[1],"lpfe");
  legend1->AddEntry(h1[2], myLegendName[2],"lpfe");
  legend1->SetFillColor(0);
  legend1->SetFillStyle(0);
  legend1->SetLineColor(0);
  legend1->Draw("same");
  legend1->SetTextSize(0.04);
  legend1->SetTextFont(42);

  MyN->cd(); 
  pad2 = new TPad("pad2", "pad2", 0, 0.18, 1, 0.3);
  pad2->SetTopMargin(0);
  pad2->SetBottomMargin(0);
  pad2->SetLeftMargin(0.15);
  pad2->SetRightMargin(0.05);
  pad2->SetGridx();
  pad2->SetGridy();
  pad2->Draw();
  pad2->cd();

  h1[0] = (TH1D*)h1[1]->Clone("h0");

  if(isRatio) h1[0]->Divide(h1[2]);
  if(isDelta) h1[0]->Add(h1[2], -1);
  if(isDeltaSigma)
  {
   for(int ibin = 1; ibin < h1[1]->GetNbinsX() + 1; ibin++)
   {
    h1[0]->SetBinContent(ibin, (h1[1]->GetBinContent(ibin) - h1[2]->GetBinContent(ibin)) / sqrt(h1[1]->GetBinError(ibin) * h1[1]->GetBinError(ibin) + h1[2]->GetBinError(ibin) * h1[2]->GetBinError(ibin)));
   }
  }

  h1[0]->SetTitle("");
  h1[0]->SetStats(0);
  if(isE0) h1[0]->Draw("E0");
  if(isHIST) h1[0]->Draw("HIST");

  h1[0]->GetYaxis()->SetNdivisions(505);

  if(isRatio) h1[0]->GetYaxis()->SetTitle("#frac{" + myLegendName[1] + "}{" + myLegendName[2] + "}");
  if(isDelta) h1[0]->GetYaxis()->SetTitle("#Delta" + myYTitle);
  if(isDeltaSigma) h1[0]->GetYaxis()->SetTitle("#frac{#Delta}{#sigma}");

  if(!isSetYRange0){
    if(isRatio) h1[0]->GetYaxis()->SetRangeUser(h1[0]->GetMinimum() / 1.01, h1[0]->GetMaximum() * 1.01);
    if(isDelta || isDeltaSigma) h1[0]->GetYaxis()->SetRangeUser(h1[0]->GetMinimum() - 1, h1[0]->GetMaximum() + 1);
  }
  else h1[0]->GetYaxis()->SetRangeUser(ymin, ymax);

  h1[0]->GetYaxis()->SetTitleSize(0.2);
  h1[0]->GetYaxis()->SetTitleFont(42);
  h1[0]->GetYaxis()->SetTitleOffset(0.2);

  h1[0]->GetYaxis()->SetLabelFont(42);
  h1[0]->GetYaxis()->SetLabelSize(0.2);

  h1[0]->SetLineWidth(2);

  if(isLogX) gPad->SetLogx();

  MyN->cd();
  pad3 = new TPad("pad3", "pad3", 0, 0, 1, 0.18);
  pad3->SetTopMargin(0);
  pad3->SetBottomMargin(0.35);
  pad3->SetLeftMargin(0.15);
  pad3->SetRightMargin(0.05);
//  pad3->SetGridx();
//  pad3->SetGridy();
  pad3->Draw();
  pad3->cd();

  h1[3]->SetStats(0);
  h1[3]->SetTitle("");

  h1[3]->GetYaxis()->SetTitle("PDF Unc.");
  h1[3]->GetYaxis()->SetTitleSize(0.14);
  h1[3]->GetYaxis()->SetTitleFont(42);
  h1[3]->GetYaxis()->SetTitleOffset(0.31);

  h1[3]->GetYaxis()->SetNdivisions(505);
  h1[3]->GetYaxis()->SetLabelFont(42);
  h1[3]->GetYaxis()->SetLabelSize(0.08);

  h1[3]->GetXaxis()->SetTitle(myXTitle);
  h1[3]->GetXaxis()->SetTitleSize(0.15);
  h1[3]->GetXaxis()->SetTitleFont(42);
  h1[3]->GetXaxis()->SetTitleOffset(1);

  h1[3]->GetXaxis()->SetLabelFont(42);
  h1[3]->GetXaxis()->SetLabelSize(0.15);

  h1[3]->SetLineWidth(2);
  h1[3]->SetLineColor(kBlue);
  h1[4]->SetLineWidth(2);
  h1[4]->SetLineColor(kRed+1);

  h1[3]->Draw("HIST");
  h1[4]->Draw("HIST same");

  if(isLogX) gPad->SetLogx();

  legend2 = new TLegend(0.19, 0.65, 0.49, 0.93);
  legend2->SetNColumns(1);
//  legend2->AddEntry(h1[3], "","lpfe");
//  legend2->AddEntry(h1[4], "","lpfe");
  legend2->SetFillColor(0);
  legend2->SetFillStyle(0);
  legend2->SetLineColor(0);
  legend2->Draw("same");
  legend2->SetTextSize(0.13);
  legend2->SetTextFont(42);
 }

 if(DoSave) MyN->SaveAs(myFigureName);
// delete MyN;
}

void Figure::DrawCorrelation(const char* fileName, const char* histName)
{
 TFile* file = new TFile(fileName);
 TH2D *h1 = (TH2D *)file->Get(histName);

 TPad *pad1;

 MyN = new TCanvas("MyN","MyN",2500,2000);
 MyN->cd();
 pad1 = new TPad("pad1", "pad1", 0, 0, 1.0, 1.0);
 pad1->SetBottomMargin(0.1);
 pad1->SetTopMargin(0.1);
 pad1->SetLeftMargin(0.15);
 pad1->SetRightMargin(0.1);
 pad1->SetGridx();
 pad1->SetGridy();
 pad1->Draw();
 pad1->cd();

 h1->SetTitle("");
 h1->SetStats(0);

 h1->GetXaxis()->SetTitle(myXTitle);
 h1->GetXaxis()->SetTitleSize(0.04);
 h1->GetXaxis()->SetTitleFont(72);
 h1->GetXaxis()->SetTitleOffset(1);

 h1->GetXaxis()->SetLabelSize(0.04);
 h1->GetXaxis()->SetLabelFont(72);

 h1->GetYaxis()->SetTitle(myYTitle);
 h1->GetYaxis()->SetTitleSize(0.04);
 h1->GetYaxis()->SetTitleFont(72);
 h1->GetYaxis()->SetTitleOffset(1);

 h1->GetYaxis()->SetLabelSize(0.03);
 h1->GetYaxis()->SetLabelFont(72);

 gStyle->SetTextFont(72);
 h1->Draw("COLZ");

 if(DoSave) MyN->SaveAs(myFigureName);
 delete MyN;
}
