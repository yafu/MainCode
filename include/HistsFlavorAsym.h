#ifndef __HistsFlavorAsym_H_
#define __HistsFlavorAsym_H_
#include <iostream>
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TFile.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TMath.h"
#include "TTree.h"
#include <vector>
#include "makeHists.h"

using namespace std;

class HistsFlavorAsym : public makeHists
{
 public:

 vector<TH1D *> v_ZRapidity;
 TH1D *ZRapidity[100];

 vector<TH1D *> v_ChangePDF_ZRapidity;
 TH1D *ChangePDF_ZRapidity[100];

 //Forward and backward vs Mass ZY QT
 vector<TH3D *> v_FZMass_ZY_QT;
 TH3D *FZMass_ZY_QT[100];

 vector<TH3D *> v_BZMass_ZY_QT;
 TH3D *BZMass_ZY_QT[100];

 //ZMass ZY QT 3D plot
 vector<TH3D *> v_ZMass_ZY_QT;
 TH3D *ZMass_ZY_QT[100];

 vector<TH3D *> v_ChangePDF_ZMass_ZY_QT;
 TH3D *ChangePDF_ZMass_ZY_QT[100];

 vector<TH3D *> v_FZMass_ZY_QT_uu;
 TH3D *FZMass_ZY_QT_uu[100];

 vector<TH3D *> v_BZMass_ZY_QT_uu;
 TH3D *BZMass_ZY_QT_uu[100];

 vector<TH3D *> v_FZMass_ZY_QT_dd;
 TH3D *FZMass_ZY_QT_dd[100];

 vector<TH3D *> v_BZMass_ZY_QT_dd;
 TH3D *BZMass_ZY_QT_dd[100];

 vector<TH3D *> v_ChangePDF_FZMass_ZY_QT_uu;
 TH3D *ChangePDF_FZMass_ZY_QT_uu[100];

 vector<TH3D *> v_ChangePDF_BZMass_ZY_QT_uu;
 TH3D *ChangePDF_BZMass_ZY_QT_uu[100];

 vector<TH3D *> v_ChangePDF_FZMass_ZY_QT_dd;
 TH3D *ChangePDF_FZMass_ZY_QT_dd[100];

 vector<TH3D *> v_ChangePDF_BZMass_ZY_QT_dd;
 TH3D *ChangePDF_BZMass_ZY_QT_dd[100];

 vector<TH3D *> v_ZMass_ZY_QT_uu;
 TH3D *ZMass_ZY_QT_uu[100];

 vector<TH3D *> v_ZMass_ZY_QT_dd;
 TH3D *ZMass_ZY_QT_dd[100];

 vector<TH3D *> v_ZMass_ZY_QT_ss;
 TH3D *ZMass_ZY_QT_ss[100];

 vector<TH3D *> v_ZMass_ZY_QT_cc;
 TH3D *ZMass_ZY_QT_cc[100];

 vector<TH3D *> v_ZMass_ZY_QT_bb;
 TH3D *ZMass_ZY_QT_bb[100];

 /////////////////////
 //  dilution plot  //
 /////////////////////

 vector<TH3D *> v_ZMass_ZY_QT_uu_wrong;
 TH3D *ZMass_ZY_QT_uu_wrong[100];
 vector<TH3D *> v_ZMass_ZY_QT_uu_total;
 TH3D *ZMass_ZY_QT_uu_total[100];
 TH3D *Dilution_ZMass_ZY_QT_uu;

 vector<TH3D *> v_ZMass_ZY_QT_dd_wrong;
 TH3D *ZMass_ZY_QT_dd_wrong[100];
 vector<TH3D *> v_ZMass_ZY_QT_dd_total;
 TH3D *ZMass_ZY_QT_dd_total[100];
 TH3D *Dilution_ZMass_ZY_QT_dd;

 vector<TH3D *> v_ChangePDF_ZMass_ZY_QT_uu_wrong;
 TH3D *ChangePDF_ZMass_ZY_QT_uu_wrong[100];
 vector<TH3D *> v_ChangePDF_ZMass_ZY_QT_uu_total;
 TH3D *ChangePDF_ZMass_ZY_QT_uu_total[100];
 TH3D *Dilution_ChangePDF_ZMass_ZY_QT_uu;

 vector<TH3D *> v_ChangePDF_ZMass_ZY_QT_dd_wrong;
 TH3D *ChangePDF_ZMass_ZY_QT_dd_wrong[100];
 vector<TH3D *> v_ChangePDF_ZMass_ZY_QT_dd_total;
 TH3D *ChangePDF_ZMass_ZY_QT_dd_total[100];
 TH3D *Dilution_ChangePDF_ZMass_ZY_QT_dd;


 vector<AngularFunction* > v_A0_Mass_ZY_QT_uu;
 AngularFunction *A0_Mass_ZY_QT_uu[100];

 vector<AngularFunction* > v_A0_Mass_ZY_QT_dd;
 AngularFunction *A0_Mass_ZY_QT_dd[100];

 vector<AngularFunction* > v_A0_Mass_ZY_QT_ss;
 AngularFunction *A0_Mass_ZY_QT_ss[100];

 vector<AngularFunction* > v_A0_Mass_ZY_QT_cc;
 AngularFunction *A0_Mass_ZY_QT_cc[100];

 vector<AngularFunction* > v_A0_Mass_ZY_QT_bb;
 AngularFunction *A0_Mass_ZY_QT_bb[100];

 TH3D *AFB_Mass_ZY_ZPt;

 TH3D *AFB_Mass_ZY_ZPt_uu;
 TH3D *AFB_Mass_ZY_ZPt_dd;
 TH3D *CoefficientDilution_ZMass_ZY_QT_uu;
 TH3D *CoefficientDilution_ZMass_ZY_QT_dd;
 TH3D *ResidualDilution_ZMass_ZY_QT_uu;
 TH3D *ResidualDilution_ZMass_ZY_QT_dd;
 TH3D *DilutionAverage_ZMass_ZY_QT_uu;
 TH3D *DilutionAverage_ZMass_ZY_QT_dd;

 TH3D *AFB_ChangePDF_Mass_ZY_ZPt_uu;
 TH3D *AFB_ChangePDF_Mass_ZY_ZPt_dd;
 TH3D *CoefficientDilution_ChangePDF_ZMass_ZY_QT_uu;
 TH3D *CoefficientDilution_ChangePDF_ZMass_ZY_QT_dd;
 TH3D *ResidualDilution_ChangePDF_ZMass_ZY_QT_uu;
 TH3D *ResidualDilution_ChangePDF_ZMass_ZY_QT_dd;
 TH3D *DilutionAverage_ChangePDF_ZMass_ZY_QT_uu;
 TH3D *DilutionAverage_ChangePDF_ZMass_ZY_QT_dd;



   /*************************/
   /*  W related histogram  */
   /*************************/

 vector<TH1D *> v_WY_udbar;
 TH1D *WY_udbar[100];

 vector<TH1D *> v_WY_usbar;
 TH1D *WY_usbar[100];

 vector<TH1D *> v_WY_ubbar;
 TH1D *WY_ubbar[100];

 vector<TH1D *> v_WY_cdbar;
 TH1D *WY_cdbar[100];

 vector<TH1D *> v_WY_csbar;
 TH1D *WY_csbar[100];

 vector<TH1D *> v_WY_cbbar;
 TH1D *WY_cbbar[100];

 vector<TH1D *> v_WY_dbaru;
 TH1D *WY_dbaru[100];

 vector<TH1D *> v_WY_sbaru;
 TH1D *WY_sbaru[100];

 vector<TH1D *> v_WY_bbaru;
 TH1D *WY_bbaru[100];

 vector<TH1D *> v_WY_dbarc;
 TH1D *WY_dbarc[100];

 vector<TH1D *> v_WY_sbarc;
 TH1D *WY_sbarc[100];

 vector<TH1D *> v_WY_bbarc;
 TH1D *WY_bbarc[100];

 vector<TH1D *> v_WY_dubar;
 TH1D *WY_dubar[100];

 vector<TH1D *> v_WY_subar;
 TH1D *WY_subar[100];

 vector<TH1D *> v_WY_bubar;
 TH1D *WY_bubar[100];

 vector<TH1D *> v_WY_dcbar;
 TH1D *WY_dcbar[100];

 vector<TH1D *> v_WY_scbar;
 TH1D *WY_scbar[100];

 vector<TH1D *> v_WY_bcbar;
 TH1D *WY_bcbar[100];

 vector<TH1D *> v_WY_ubard;
 TH1D *WY_ubard[100];

 vector<TH1D *> v_WY_ubars;
 TH1D *WY_ubars[100];

 vector<TH1D *> v_WY_ubarb;
 TH1D *WY_ubarb[100];

 vector<TH1D *> v_WY_cbard;
 TH1D *WY_cbard[100];

 vector<TH1D *> v_WY_cbars;
 TH1D *WY_cbars[100];

 vector<TH1D *> v_WY_cbarb;
 TH1D *WY_cbarb[100];

 vector<TH1D *> v_WY_udbar_total;
 TH1D *WY_udbar_total[100];

 vector<TH1D *> v_WY_csbar_total;
 TH1D *WY_csbar_total[100];

 vector<TH1D *> v_WY_dubar_total;
 TH1D *WY_dubar_total[100];

 vector<TH1D *> v_WY_scbar_total;
 TH1D *WY_scbar_total[100];

 vector<TH1D *> v_WPlusY;
 TH1D *WPlusY[100];

 vector<TH1D *> v_WMinusY;
 TH1D *WMinusY[100];

 vector<TH1D *> v_LeptonEta;
 TH1D *LeptonEta[100];

 vector<TH1D *> v_AntiLeptonEta;
 TH1D *AntiLeptonEta[100];

 vector<TH1D *> v_ChangePDF_LeptonEta;
 TH1D *ChangePDF_LeptonEta[100];

 vector<TH1D *> v_ChangePDF_AntiLeptonEta;
 TH1D *ChangePDF_AntiLeptonEta[100];

//W+/W- A4(AFB)
 vector<TH1D *> v_FWPlusY;
 TH1D *FWPlusY[100];

 vector<TH1D *> v_BWPlusY;
 TH1D *BWPlusY[100];

 vector<TH1D *> v_FWMinusY;
 TH1D *FWMinusY[100];
 
 vector<TH1D *> v_BWMinusY;
 TH1D *BWMinusY[100];

 vector<TH1D *> v_FWPlusY_reco_80385;
 TH1D *FWPlusY_reco_80385[100];

 vector<TH1D *> v_BWPlusY_reco_80385;
 TH1D *BWPlusY_reco_80385[100];

 vector<TH1D *> v_FWMinusY_reco_80385;
 TH1D *FWMinusY_reco_80385[100];

 vector<TH1D *> v_BWMinusY_reco_80385;
 TH1D *BWMinusY_reco_80385[100];

 vector<TH1D *> v_FWPlusY_reco_80395;
 TH1D *FWPlusY_reco_80395[100];

 vector<TH1D *> v_BWPlusY_reco_80395;
 TH1D *BWPlusY_reco_80395[100];

 vector<TH1D *> v_FWMinusY_reco_80395;
 TH1D *FWMinusY_reco_80395[100];

 vector<TH1D *> v_BWMinusY_reco_80395;
 TH1D *BWMinusY_reco_80395[100];

 vector<TH1D *> v_FWPlusY_reco_80375;
 TH1D *FWPlusY_reco_80375[100];

 vector<TH1D *> v_BWPlusY_reco_80375;
 TH1D *BWPlusY_reco_80375[100];

 vector<TH1D *> v_FWMinusY_reco_80375;
 TH1D *FWMinusY_reco_80375[100];

 vector<TH1D *> v_BWMinusY_reco_80375;
 TH1D *BWMinusY_reco_80375[100];

 vector<TH1D *> v_FWPlusMass;
 TH1D *FWPlusMass[100];

 vector<TH1D *> v_BWPlusMass;
 TH1D *BWPlusMass[100];

 vector<TH1D *> v_FWMinusMass;
 TH1D *FWMinusMass[100];

 vector<TH1D *> v_BWMinusMass;
 TH1D *BWMinusMass[100];

 vector<TH1D *> v_WPlusY_wrong;
 TH1D *WPlusY_wrong[100];
 vector<TH1D *> v_WPlusY_total;
 TH1D *WPlusY_total[100];
 TH1D *Dilution_WPlusY;
 TH1D *CoefficientDilution_WPlusY;

 vector<TH1D *> v_WMinusY_wrong;
 TH1D *WMinusY_wrong[100];
 vector<TH1D *> v_WMinusY_total;
 TH1D *WMinusY_total[100];
 TH1D *Dilution_WMinusY;
 TH1D *CoefficientDilution_WMinusY;

 TH1D* AFB_WPlusY;
 TH1D* AFB_WMinusY;
 TH1D* AFB_WPlusY_reco_80385;
 TH1D* AFB_WMinusY_reco_80385;
 TH1D* AFB_WPlusY_reco_80395;
 TH1D* AFB_WMinusY_reco_80395;
 TH1D* AFB_WPlusY_reco_80375;
 TH1D* AFB_WMinusY_reco_80375;
 TH1D* AFB_WPlusMass;
 TH1D* AFB_WMinusMass;
 TH1D* OriginAFB_WPlusY;
 TH1D* OriginAFB_WMinusY;



 TH1D* Wasymmetry;
 TH1D* ChangePDF_Wasymmetry;



//member function
 vector<TString> sysName;
 HistsFlavorAsym(){};
 HistsFlavorAsym(TString RootType){this->RootType = RootType;};
 virtual void InitialSysName();
 virtual void bookHists(int TotalThread);
 virtual void outputInformation();
 virtual void Save();
 virtual void Reset();

// template<class T>

};
#endif
