#ifndef __HISTS_FWDRECO_H_
#define __HISTS_FWDRECO_H_
#include <iostream>
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TFile.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TMath.h"
#include "TTree.h"
#include <vector>
#include "makeHists.h"

using namespace std;

class HistsFwdReco : public makeHists
{
 public:
 vector<TH1D *> v_FwdElectronPt;
 TH1D *FwdElectronPt[100];

 vector<TH1D *> v_FwdElectronEta;
 TH1D *FwdElectronEta[100];

 vector<TH1D *> v_FwdElectronPhi;
 TH1D *FwdElectronPhi[100];

 vector<TH1D *> v_FwdElectronEt;
 TH1D *FwdElectronEt[100];

 vector<TH1D *> v_TruthElectronPt;
 TH1D *TruthElectronPt[100];

 vector<TH1D *> v_TruthElectronEta;
 TH1D *TruthElectronEta[100];
 
 vector<TH1D *> v_TruthElectronPhi;
 TH1D *TruthElectronPhi[100];
 
 vector<TH1D *> v_TruthElectronEt;
 TH1D *TruthElectronEt[100];

 vector<TH1D *> v_TruthElectronEtaAbs;
 TH1D *TruthElectronEtaAbs[100];

 vector<TH1D *> v_MatchedElectronPt;
 TH1D *MatchedElectronPt[100];

 vector<TH1D *> v_MatchedElectronEta;
 TH1D *MatchedElectronEta[100];

 vector<TH1D *> v_MatchedElectronPhi;
 TH1D *MatchedElectronPhi[100];

 vector<TH1D *> v_MatchedElectronEt;
 TH1D *MatchedElectronEt[100];

 vector<TH1D *> v_MatchedElectronEtaAbs;
 TH1D *MatchedElectronEtaAbs[100];

 vector<TH1D *> v_AverageMu;
 TH1D *AverageMu[100];

 vector<TH1D *> v_TruthSize;
 TH1D *TruthSize[100];

 vector<TH1D *> v_RecoSize;
 TH1D *RecoSize[100];

 vector<TH2D *> v_TruthElectronEtEta;
 TH2D *TruthElectronEtEta[100];

 vector<TH2D *> v_TruthElectronEtaPhi;
 TH2D *TruthElectronEtaPhi[100];

 vector<TH2D *> v_MatchedElectronEtEta;
 TH2D *MatchedElectronEtEta[100];
 
 vector<TH2D *> v_MatchedElectronEtaPhi;
 TH2D *MatchedElectronEtaPhi[100];


 vector<ResolutionHist *> v_EResolutionEta;
 ResolutionHist *EResolutionEta[100];

 vector<ResolutionHist *> v_PtResolutionEta;
 ResolutionHist *PtResolutionEta[100];

 vector<ResolutionHist *> v_EtResolutionEta;
 ResolutionHist *EtResolutionEta[100];

 vector<ResolutionHist *> v_PtResolutionEta_LowMu;
 ResolutionHist *PtResolutionEta_LowMu[100];

 vector<ResolutionHist *> v_PtResolutionEta_HighMu;
 ResolutionHist *PtResolutionEta_HighMu[100];

 vector<ResolutionHist *> v_PtResolutionPt;
 ResolutionHist *PtResolutionPt[100];

//member function
 virtual void bookHists(int TotalThread);

// template<class T>

};
#endif
