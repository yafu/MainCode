#ifndef __HISTS_HERWIG_H_
#define __HISTS_HERWIG_H_
#include <iostream>
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TFile.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TMath.h"
#include "TTree.h"
#include <vector>
#include "makeHists.h"

using namespace std;

class HistsHerwig : public makeHists
{
 public:
 vector<TH1D *> v_FZmass_CC;
 TH1D *FZmass_CC[100];

 vector<TH1D *> v_BZmass_CC;
 TH1D *BZmass_CC[100];

 vector<TH1D *> v_FZmass_CF;
 TH1D *FZmass_CF[100];

 vector<TH1D *> v_BZmass_CF;
 TH1D *BZmass_CF[100];

 vector<TH1D *> v_ZPt;
 TH1D *ZPt[100];

 vector<TH1D *> v_ZRapidity;
 TH1D *ZRapidity[100];

 vector<TH1D *> v_ZMass;
 TH1D *ZMass[100];

 vector<TH1D *> v_FZmass;
 TH1D *FZmass[100];

 vector<TH1D *> v_BZmass;
 TH1D *BZmass[100];

 vector<TH1D *> v_CosTheta;
 TH1D *CosTheta[100];

 vector<TH1D *> v_CollinsPhi;
 TH1D *CollinsPhi[100];

 vector<TH1D *> v_plot_phi_eta;
 TH1D *plot_phi_eta[100];

 vector<TH1D *> v_leptonPt;
 TH1D *leptonPt[100];

 vector<TH1D *> v_leptonEta;
 TH1D *leptonEta[100];

 vector<AngularFunction *> v_A0_ZPt;
 AngularFunction *A0_ZPt[100];

 vector<AngularFunction *> v_A1_ZPt;
 AngularFunction *A1_ZPt[100];

 vector<AngularFunction *> v_A2_ZPt;
 AngularFunction *A2_ZPt[100];

 vector<AngularFunction *> v_A3_ZPt;
 AngularFunction *A3_ZPt[100];

 vector<AngularFunction *> v_A4_ZPt;
 AngularFunction *A4_ZPt[100];

 vector<AngularFunction *> v_L0_ZPt;
 AngularFunction *L0_ZPt[100];

//member function
 vector<TString> sysName;
 virtual void InitialSysName();
 virtual void bookHists(int TotalThread);

// template<class T>

};
#endif
