#ifndef __HistsMadGraph_H_
#define __HistsMadGraph_H_
#include <iostream>
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TFile.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TMath.h"
#include "TTree.h"
#include <vector>
#include "makeHists.h"

using namespace std;

class HistsMadGraph : public makeHists
{
 public:

 vector<TH1D *> v_ZMass;
 TH1D* ZMass[100];

 vector<TH1D *> v_ZRapidity;
 TH1D* ZRapidity[100];

 vector<TH1D *> v_ZPt;
 TH1D* ZPt[100];

 vector<TH1D *> v_LeptonPt;
 TH1D* LeptonPt[100];

 vector<TH1D *> v_LeptonEta;
 TH1D* LeptonEta[100];

 vector<TH1D *> v_FZMass;
 TH1D* FZMass[100];

 vector<TH1D *> v_BZMass;
 TH1D* BZMass[100];

 vector<TH1D *> v_ZHighMass;
 TH1D* ZHighMass[100];

 vector<TH1D *> v_FZMass_HighMass;
 TH1D* FZMass_HighMass[100];

 vector<TH1D *> v_BZMass_HighMass;
 TH1D* BZMass_HighMass[100];

 vector<TH1D *> v_CrossSection;
 TH1D* CrossSection[100];

 TH1D* AFB_ZMass;
 TH1D* AFB_ZHighMass;

//member function
 vector<TString> sysName;
 virtual void InitialSysName();
 virtual void bookHists(int TotalThread);
 virtual void outputInformation();

// template<class T>

};
#endif
