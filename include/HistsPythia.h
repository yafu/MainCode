#ifndef __HISTS_PYTHIA_H_
#define __HISTS_PYTHIA_H_
#include <iostream>
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TFile.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TMath.h"
#include "TTree.h"
#include <vector>
#include "makeHists.h"

using namespace std;

class HistsPythia : public makeHists
{
 public:

 /////////////////////////
 //  hadron level plot  //
 /////////////////////////


 vector<TH1D *> v_ZMass;
 TH1D *ZMass[100];

 vector<TH1D *> v_ZMass_CC;
 TH1D *ZMass_CC[100];

 vector<TH1D *> v_ZMass_CF;
 TH1D *ZMass_CF[100];

 vector<TH1D *> v_FZmass;
 TH1D *FZmass[100];

 vector<TH1D *> v_BZmass;
 TH1D *BZmass[100];

 vector<TH1D *> v_FZmass_Hadron_uu;
 TH1D *FZmass_Hadron_uu[100];

 vector<TH1D *> v_BZmass_Hadron_uu;
 TH1D *BZmass_Hadron_uu[100];

 vector<TH1D *> v_FZmass_Hadron_dd;
 TH1D *FZmass_Hadron_dd[100];

 vector<TH1D *> v_BZmass_Hadron_dd;
 TH1D *BZmass_Hadron_dd[100];

 vector<TH1D *> v_FZmass_Hadron_utype;
 TH1D *FZmass_Hadron_utype[100];

 vector<TH1D *> v_BZmass_Hadron_utype;
 TH1D *BZmass_Hadron_utype[100];

 vector<TH1D *> v_FZmass_Hadron_dtype;
 TH1D *FZmass_Hadron_dtype[100];

 vector<TH1D *> v_BZmass_Hadron_dtype;
 TH1D *BZmass_Hadron_dtype[100];

 vector<TH1D *> v_FZmass_LepPtCut;
 TH1D *FZmass_LepPtCut[100];

 vector<TH1D *> v_BZmass_LepPtCut;
 TH1D *BZmass_LepPtCut[100];

 vector<TH1D *> v_FZmass_NoLepPtCut;
 TH1D *FZmass_NoLepPtCut[100];

 vector<TH1D *> v_BZmass_NoLepPtCut;
 TH1D *BZmass_NoLepPtCut[100];

 vector<TH1D *> v_FZmass_CC;
 TH1D *FZmass_CC[100];

 vector<TH1D *> v_BZmass_CC;
 TH1D *BZmass_CC[100];

 vector<TH1D *> v_FZmass_CF;
 TH1D *FZmass_CF[100];

 vector<TH1D *> v_BZmass_CF;
 TH1D *BZmass_CF[100];

 vector<TH1D *> v_ZPt;
 TH1D *ZPt[100];

 vector<TH1D *> v_ZRapidity;
 TH1D *ZRapidity[100];

 vector<TH1D *> v_ZPt_LepPtCut;
 TH1D *ZPt_LepPtCut[100];

 vector<TH1D *> v_ZRapidity_LepPtCut;
 TH1D *ZRapidity_LepPtCut[100];

 vector<TH1D *> v_ZPt_NoLepPtCut;
 TH1D *ZPt_NoLepPtCut[100];

 vector<TH1D *> v_ZRapidity_NoLepPtCut;
 TH1D *ZRapidity_NoLepPtCut[100];

 vector<TH1D *> v_ZRapidityAbs;
 TH1D *ZRapidityAbs[100];

 vector<TH1D *> v_ZPzPtBalance;
 TH1D *ZPzPtBalance[100];

 vector<TH1D *> v_ZPzPtBalance_CC;
 TH1D *ZPzPtBalance_CC[100];

 vector<TH1D *> v_ZPzPtBalance_CF;
 TH1D *ZPzPtBalance_CF[100];

 vector<TH1D *> v_QuarkPzZPzBalance;
 TH1D *QuarkPzZPzBalance[100];

 vector<TH1D *> v_QuarkPzZPzBalance_CC;
 TH1D *QuarkPzZPzBalance_CC[100];

 vector<TH1D *> v_QuarkPzZPzBalance_CF;
 TH1D *QuarkPzZPzBalance_CF[100];

 vector<TH1D *> v_Quark1Pz;
 TH1D *Quark1Pz[100];

 vector<TH1D *> v_Quark2Pz;
 TH1D *Quark2Pz[100];

 //ZMass ZY 2D plot
 vector<TH2D *> v_ZMass_ZY;
 TH2D *ZMass_ZY[100];

 vector<TH2D *> v_ZMass_ZY_CC;
 TH2D *ZMass_ZY_CC[100];

 vector<TH2D *> v_ZMass_ZY_CF;
 TH2D *ZMass_ZY_CF[100];

 vector<TH2D *> v_FZMass_ZY;
 TH2D *FZMass_ZY[100];

 vector<TH2D *> v_BZMass_ZY;
 TH2D *BZMass_ZY[100];

 vector<TH2D *> v_FZMass_ZY_CC;
 TH2D *FZMass_ZY_CC[100];

 vector<TH2D *> v_BZMass_ZY_CC;
 TH2D *BZMass_ZY_CC[100];

 vector<TH2D *> v_FZMass_ZY_CF;
 TH2D *FZMass_ZY_CF[100];

 vector<TH2D *> v_BZMass_ZY_CF;
 TH2D *BZMass_ZY_CF[100];

 //ZMass ZPt 2D plot
 vector<TH2D *> v_ZMass_ZPt;
 TH2D *ZMass_ZPt[100];

 vector<TH2D *> v_ZMass_ZPt_CC;
 TH2D *ZMass_ZPt_CC[100];

 vector<TH2D *> v_ZMass_ZPt_CF;
 TH2D *ZMass_ZPt_CF[100];

 vector<TH2D *> v_FZMass_ZPt;
 TH2D *FZMass_ZPt[100];

 vector<TH2D *> v_BZMass_ZPt;
 TH2D *BZMass_ZPt[100];

 vector<TH2D *> v_FZMass_ZPt_CC;
 TH2D *FZMass_ZPt_CC[100];

 vector<TH2D *> v_BZMass_ZPt_CC;
 TH2D *BZMass_ZPt_CC[100];

 vector<TH2D *> v_FZMass_ZPt_CF;
 TH2D *FZMass_ZPt_CF[100];

 vector<TH2D *> v_BZMass_ZPt_CF;
 TH2D *BZMass_ZPt_CF[100];


 //ZMass ZY MultiBin plot
 vector<MultiBinHist *> v_ZMass_ZY_multi;
 MultiBinHist *ZMass_ZY_multi[100];

 vector<MultiBinHist *> v_ZMass_ZY_CC_multi;
 MultiBinHist *ZMass_ZY_CC_multi[100];

 vector<MultiBinHist *> v_ZMass_ZY_CF_multi;
 MultiBinHist *ZMass_ZY_CF_multi[100];

 vector<MultiBinHist *> v_FZMass_ZY_multi;
 MultiBinHist *FZMass_ZY_multi[100];

 vector<MultiBinHist *> v_BZMass_ZY_multi;
 MultiBinHist *BZMass_ZY_multi[100];

 vector<MultiBinHist *> v_FZMass_ZY_CC_multi;
 MultiBinHist *FZMass_ZY_CC_multi[100];

 vector<MultiBinHist *> v_BZMass_ZY_CC_multi;
 MultiBinHist *BZMass_ZY_CC_multi[100];

 vector<MultiBinHist *> v_FZMass_ZY_CF_multi;
 MultiBinHist *FZMass_ZY_CF_multi[100];

 vector<MultiBinHist *> v_BZMass_ZY_CF_multi;
 MultiBinHist *BZMass_ZY_CF_multi[100];


 //ZMass ZY QT 3D plot
 vector<TH3D *> v_ZMass_ZY_QT;
 TH3D *ZMass_ZY_QT[100];

 vector<TH3D *> v_FZMass_ZY_QT;
 TH3D *FZMass_ZY_QT[100];

 vector<TH3D *> v_BZMass_ZY_QT;
 TH3D *BZMass_ZY_QT[100];

 vector<TH1D *> v_CosTheta;
 TH1D *CosTheta[100];

 vector<TH1D *> v_CosTheta_LepPtCut;
 TH1D *CosTheta_LepPtCut[100];

 vector<TH1D *> v_CosTheta_NoLepPtCut;
 TH1D *CosTheta_NoLepPtCut[100];

 vector<TH1D *> v_CosThetaQ;
 TH1D *CosThetaQ[100];

 vector<TH1D *> v_CollinsPhi;
 TH1D *CollinsPhi[100];

 vector<TH1D *> v_plot_phi_eta;
 TH1D *plot_phi_eta[100];

 vector<TH1D *> v_leptonPt;
 TH1D *leptonPt[100];

 vector<TH1D *> v_leptonEta;
 TH1D *leptonEta[100];

 //Lepton Antilepton eta abs
 vector<TH1D *> v_LepEtaAbs;
 TH1D *LepEtaAbs[100];

 vector<TH1D *> v_AntiLepEtaAbs;
 TH1D *AntiLepEtaAbs[100];

 vector<TH1D *> v_LepEtaAbs_CC;
 TH1D *LepEtaAbs_CC[100];

 vector<TH1D *> v_AntiLepEtaAbs_CC;
 TH1D *AntiLepEtaAbs_CC[100];

 vector<TH1D *> v_LepEtaAbs_CF;
 TH1D *LepEtaAbs_CF[100];

 vector<TH1D *> v_AntiLepEtaAbs_CF;
 TH1D *AntiLepEtaAbs_CF[100];

 //Lepton Antilepton eta
 vector<TH1D *> v_LepEta;
 TH1D *LepEta[100];

 vector<TH1D *> v_AntiLepEta;
 TH1D *AntiLepEta[100];

 vector<TH1D *> v_LepEta_CC;
 TH1D *LepEta_CC[100];

 vector<TH1D *> v_AntiLepEta_CC;
 TH1D *AntiLepEta_CC[100];

 vector<TH1D *> v_LepEta_CF;
 TH1D *LepEta_CF[100];

 vector<TH1D *> v_AntiLepEta_CF;
 TH1D *AntiLepEta_CF[100];

 //Forward and backward rapidity
 vector<TH1D *> v_FZRapidity;
 TH1D *FZRapidity[100];

 vector<TH1D *> v_BZRapidity;
 TH1D *BZRapidity[100];

 vector<TH1D *> v_FZRapidity_LepPtCut;
 TH1D *FZRapidity_LepPtCut[100];

 vector<TH1D *> v_BZRapidity_LepPtCut;
 TH1D *BZRapidity_LepPtCut[100];

 vector<TH1D *> v_FZRapidity_NoLepPtCut;
 TH1D *FZRapidity_NoLepPtCut[100];

 vector<TH1D *> v_BZRapidity_NoLepPtCut;
 TH1D *BZRapidity_NoLepPtCut[100];

 vector<TH1D *> v_FZRapidity_CC;
 TH1D *FZRapidity_CC[100];

 vector<TH1D *> v_BZRapidity_CC;
 TH1D *BZRapidity_CC[100];

 vector<TH1D *> v_FZRapidity_CF;
 TH1D *FZRapidity_CF[100];

 vector<TH1D *> v_BZRapidity_CF;
 TH1D *BZRapidity_CF[100];

 vector<TH1D *> v_FZPt;
 TH1D *FZPt[100];

 vector<TH1D *> v_BZPt;
 TH1D *BZPt[100];

 vector<TH1D *> v_FZPt_LepPtCut;
 TH1D *FZPt_LepPtCut[100];

 vector<TH1D *> v_BZPt_LepPtCut;
 TH1D *BZPt_LepPtCut[100];

 vector<TH1D *> v_FZPt_NoLepPtCut;
 TH1D *FZPt_NoLepPtCut[100];

 vector<TH1D *> v_BZPt_NoLepPtCut;
 TH1D *BZPt_NoLepPtCut[100];

 vector<TH1D *> v_AverageZPt_ZY_numer;
 TH1D *AverageZPt_ZY_numer[100];
 vector<TH1D *> v_AverageZPt_ZY_denom;
 TH1D *AverageZPt_ZY_denom[100];
 TH1D *AverageZPt_ZY;

 vector<TH1D *> v_AverageZPt_lnQ_numer;
 TH1D *AverageZPt_lnQ_numer[100];
 vector<TH1D *> v_AverageZPt_lnQ_denom;
 TH1D *AverageZPt_lnQ_denom[100];
 TH1D *AverageZPt_lnQ;

 vector<TH1D *> v_AverageZY_ZY_numer;
 TH1D *AverageZY_ZY_numer[100];
 vector<TH1D *> v_AverageZY_ZY_denom;
 TH1D *AverageZY_ZY_denom[100];
 TH1D *AverageZY_ZY;



 ////////////////////////
 //  quark level plot  //
 ////////////////////////


 //Forward and backward ZMass
 vector<TH1D *> v_FZmass_CC_uu;
 TH1D *FZmass_CC_uu[100];

 vector<TH1D *> v_BZmass_CC_uu;
 TH1D *BZmass_CC_uu[100];

 vector<TH1D *> v_FZmass_CF_uu;
 TH1D *FZmass_CF_uu[100];

 vector<TH1D *> v_BZmass_CF_uu;
 TH1D *BZmass_CF_uu[100];

 vector<TH1D *> v_FZmass_CC_dd;
 TH1D *FZmass_CC_dd[100];

 vector<TH1D *> v_BZmass_CC_dd;
 TH1D *BZmass_CC_dd[100];

 vector<TH1D *> v_FZmass_CF_dd;
 TH1D *FZmass_CF_dd[100];

 vector<TH1D *> v_BZmass_CF_dd;
 TH1D *BZmass_CF_dd[100];

 vector<TH1D *> v_FZmass_CC_gg;
 TH1D *FZmass_CC_gg[100];

 vector<TH1D *> v_BZmass_CC_gg;
 TH1D *BZmass_CC_gg[100];

 vector<TH1D *> v_FZmass_CF_gg;
 TH1D *FZmass_CF_gg[100];

 vector<TH1D *> v_BZmass_CF_gg;
 TH1D *BZmass_CF_gg[100];

 vector<TH1D *> v_FZmass_uu;
 TH1D *FZmass_uu[100];

 vector<TH1D *> v_BZmass_uu;
 TH1D *BZmass_uu[100];

 vector<TH1D *> v_FZmass_dd;
 TH1D *FZmass_dd[100];

 vector<TH1D *> v_BZmass_dd;
 TH1D *BZmass_dd[100];

 vector<TH1D *> v_FZmass_gg;
 TH1D *FZmass_gg[100];

 vector<TH1D *> v_BZmass_gg;
 TH1D *BZmass_gg[100];

 vector<TH1D *> v_ZMass_CC_uu;
 TH1D *ZMass_CC_uu[100];

 vector<TH1D *> v_ZMass_CC_dd;
 TH1D *ZMass_CC_dd[100];

 vector<TH1D *> v_ZMass_CC_gg;
 TH1D *ZMass_CC_gg[100];

 vector<TH1D *> v_ZMass_CC_ss;
 TH1D *ZMass_CC_ss[100];

 vector<TH1D *> v_ZMass_CC_cc;
 TH1D *ZMass_CC_cc[100];

 vector<TH1D *> v_ZMass_CC_bb;
 TH1D *ZMass_CC_bb[100];

 vector<TH1D *> v_ZMass_CC_utype;
 TH1D *ZMass_CC_utype[100];

 vector<TH1D *> v_ZMass_CC_dtype;
 TH1D *ZMass_CC_dtype[100];

 vector<TH1D *> v_ZMass_CF_uu;
 TH1D *ZMass_CF_uu[100];

 vector<TH1D *> v_ZMass_CF_dd;
 TH1D *ZMass_CF_dd[100];

 vector<TH1D *> v_ZMass_CF_gg;
 TH1D *ZMass_CF_gg[100];

 vector<TH1D *> v_ZMass_CF_ss;
 TH1D *ZMass_CF_ss[100];

 vector<TH1D *> v_ZMass_CF_cc;
 TH1D *ZMass_CF_cc[100];

 vector<TH1D *> v_ZMass_CF_bb;
 TH1D *ZMass_CF_bb[100];

 vector<TH1D *> v_ZMass_CF_utype;
 TH1D *ZMass_CF_utype[100];

 vector<TH1D *> v_ZMass_CF_dtype;
 TH1D *ZMass_CF_dtype[100];

 vector<TH1D *> v_ZMass_uu;
 TH1D *ZMass_uu[100];

 vector<TH1D *> v_ZMass_dd;
 TH1D *ZMass_dd[100];

 vector<TH1D *> v_ZMass_gg;
 TH1D *ZMass_gg[100];

 vector<TH1D *> v_ZMass_ss;
 TH1D *ZMass_ss[100];

 vector<TH1D *> v_ZMass_cc;
 TH1D *ZMass_cc[100];

 vector<TH1D *> v_ZMass_bb;
 TH1D *ZMass_bb[100];

 vector<TH1D *> v_ZMass_utype;
 TH1D *ZMass_utype[100];

 vector<TH1D *> v_ZMass_dtype;
 TH1D *ZMass_dtype[100];

 //AFB vs ZPt
 vector<TH1D *> v_FZPt_uu;
 TH1D *FZPt_uu[100];

 vector<TH1D *> v_BZPt_uu;
 TH1D *BZPt_uu[100];

 vector<TH1D *> v_FZPt_dd;
 TH1D *FZPt_dd[100];

 vector<TH1D *> v_BZPt_dd;
 TH1D *BZPt_dd[100];

 //Forward and backward ZMass ZY 2D plot
 vector<TH2D *> v_ZMass_ZY_uu;
 TH2D *ZMass_ZY_uu[100];

 vector<TH2D *> v_ZMass_ZY_dd;
 TH2D *ZMass_ZY_dd[100];

 vector<TH2D *> v_ZMass_ZY_ss;
 TH2D *ZMass_ZY_ss[100];

 vector<TH2D *> v_ZMass_ZY_cc;
 TH2D *ZMass_ZY_cc[100];

 vector<TH2D *> v_ZMass_ZY_bb;
 TH2D *ZMass_ZY_bb[100];

 vector<TH2D *> v_ZMass_ZY_CC_uu;
 TH2D *ZMass_ZY_CC_uu[100];

 vector<TH2D *> v_ZMass_ZY_CC_dd;
 TH2D *ZMass_ZY_CC_dd[100];

 vector<TH2D *> v_ZMass_ZY_CC_ss;
 TH2D *ZMass_ZY_CC_ss[100];

 vector<TH2D *> v_ZMass_ZY_CC_cc;
 TH2D *ZMass_ZY_CC_cc[100];

 vector<TH2D *> v_ZMass_ZY_CC_bb;
 TH2D *ZMass_ZY_CC_bb[100];

 vector<TH2D *> v_ZMass_ZY_CF_uu;
 TH2D *ZMass_ZY_CF_uu[100];

 vector<TH2D *> v_ZMass_ZY_CF_dd;
 TH2D *ZMass_ZY_CF_dd[100];

 vector<TH2D *> v_ZMass_ZY_CF_ss;
 TH2D *ZMass_ZY_CF_ss[100];

 vector<TH2D *> v_ZMass_ZY_CF_cc;
 TH2D *ZMass_ZY_CF_cc[100];

 vector<TH2D *> v_ZMass_ZY_CF_bb;
 TH2D *ZMass_ZY_CF_bb[100];

 vector<TH2D *> v_FZmass_ZY_uu;
 TH2D *FZmass_ZY_uu[100];

 vector<TH2D *> v_BZmass_ZY_uu;
 TH2D *BZmass_ZY_uu[100];

 vector<TH2D *> v_FZmass_ZY_dd;
 TH2D *FZmass_ZY_dd[100];

 vector<TH2D *> v_BZmass_ZY_dd;
 TH2D *BZmass_ZY_dd[100];

 vector<TH2D *> v_FZmass_ZY_gg;
 TH2D *FZmass_ZY_gg[100];

 vector<TH2D *> v_BZmass_ZY_gg;
 TH2D *BZmass_ZY_gg[100];

 vector<TH2D *> v_FZmass_ZY_CC_uu;
 TH2D *FZmass_ZY_CC_uu[100];

 vector<TH2D *> v_BZmass_ZY_CC_uu;
 TH2D *BZmass_ZY_CC_uu[100];

 vector<TH2D *> v_FZmass_ZY_CC_dd;
 TH2D *FZmass_ZY_CC_dd[100];

 vector<TH2D *> v_BZmass_ZY_CC_dd;
 TH2D *BZmass_ZY_CC_dd[100];

 vector<TH2D *> v_FZmass_ZY_CC_gg;
 TH2D *FZmass_ZY_CC_gg[100];

 vector<TH2D *> v_BZmass_ZY_CC_gg;
 TH2D *BZmass_ZY_CC_gg[100];

 vector<TH2D *> v_FZmass_ZY_CF_uu;
 TH2D *FZmass_ZY_CF_uu[100];

 vector<TH2D *> v_BZmass_ZY_CF_uu;
 TH2D *BZmass_ZY_CF_uu[100];

 vector<TH2D *> v_FZmass_ZY_CF_dd;
 TH2D *FZmass_ZY_CF_dd[100];

 vector<TH2D *> v_BZmass_ZY_CF_dd;
 TH2D *BZmass_ZY_CF_dd[100];

 vector<TH2D *> v_FZmass_ZY_CF_gg;
 TH2D *FZmass_ZY_CF_gg[100];

 vector<TH2D *> v_BZmass_ZY_CF_gg;
 TH2D *BZmass_ZY_CF_gg[100];

 //Forward and backward ZMass ZPt 2D plot
 vector<TH2D *> v_ZMass_ZPt_uu;
 TH2D *ZMass_ZPt_uu[100];

 vector<TH2D *> v_ZMass_ZPt_dd;
 TH2D *ZMass_ZPt_dd[100];

 vector<TH2D *> v_ZMass_ZPt_ss;
 TH2D *ZMass_ZPt_ss[100];

 vector<TH2D *> v_ZMass_ZPt_cc;
 TH2D *ZMass_ZPt_cc[100];

 vector<TH2D *> v_ZMass_ZPt_bb;
 TH2D *ZMass_ZPt_bb[100];

 vector<TH2D *> v_ZMass_ZPt_CC_uu;
 TH2D *ZMass_ZPt_CC_uu[100];

 vector<TH2D *> v_ZMass_ZPt_CC_dd;
 TH2D *ZMass_ZPt_CC_dd[100];

 vector<TH2D *> v_ZMass_ZPt_CC_ss;
 TH2D *ZMass_ZPt_CC_ss[100];

 vector<TH2D *> v_ZMass_ZPt_CC_cc;
 TH2D *ZMass_ZPt_CC_cc[100];

 vector<TH2D *> v_ZMass_ZPt_CC_bb;
 TH2D *ZMass_ZPt_CC_bb[100];

 vector<TH2D *> v_ZMass_ZPt_CF_uu;
 TH2D *ZMass_ZPt_CF_uu[100];

 vector<TH2D *> v_ZMass_ZPt_CF_dd;
 TH2D *ZMass_ZPt_CF_dd[100];

 vector<TH2D *> v_ZMass_ZPt_CF_ss;
 TH2D *ZMass_ZPt_CF_ss[100];

 vector<TH2D *> v_ZMass_ZPt_CF_cc;
 TH2D *ZMass_ZPt_CF_cc[100];

 vector<TH2D *> v_ZMass_ZPt_CF_bb;
 TH2D *ZMass_ZPt_CF_bb[100];

 vector<TH2D *> v_FZmass_ZPt_uu;
 TH2D *FZmass_ZPt_uu[100];

 vector<TH2D *> v_BZmass_ZPt_uu;
 TH2D *BZmass_ZPt_uu[100];

 vector<TH2D *> v_FZmass_ZPt_dd;
 TH2D *FZmass_ZPt_dd[100];

 vector<TH2D *> v_BZmass_ZPt_dd;
 TH2D *BZmass_ZPt_dd[100];

 vector<TH2D *> v_FZmass_ZPt_gg;
 TH2D *FZmass_ZPt_gg[100];

 vector<TH2D *> v_BZmass_ZPt_gg;
 TH2D *BZmass_ZPt_gg[100];

 vector<TH2D *> v_FZmass_ZPt_CC_uu;
 TH2D *FZmass_ZPt_CC_uu[100];

 vector<TH2D *> v_BZmass_ZPt_CC_uu;
 TH2D *BZmass_ZPt_CC_uu[100];

 vector<TH2D *> v_FZmass_ZPt_CC_dd;
 TH2D *FZmass_ZPt_CC_dd[100];

 vector<TH2D *> v_BZmass_ZPt_CC_dd;
 TH2D *BZmass_ZPt_CC_dd[100];

 vector<TH2D *> v_FZmass_ZPt_CC_gg;
 TH2D *FZmass_ZPt_CC_gg[100];

 vector<TH2D *> v_BZmass_ZPt_CC_gg;
 TH2D *BZmass_ZPt_CC_gg[100];

 vector<TH2D *> v_FZmass_ZPt_CF_uu;
 TH2D *FZmass_ZPt_CF_uu[100];

 vector<TH2D *> v_BZmass_ZPt_CF_uu;
 TH2D *BZmass_ZPt_CF_uu[100];

 vector<TH2D *> v_FZmass_ZPt_CF_dd;
 TH2D *FZmass_ZPt_CF_dd[100];

 vector<TH2D *> v_BZmass_ZPt_CF_dd;
 TH2D *BZmass_ZPt_CF_dd[100];

 vector<TH2D *> v_FZmass_ZPt_CF_gg;
 TH2D *FZmass_ZPt_CF_gg[100];

 vector<TH2D *> v_BZmass_ZPt_CF_gg;
 TH2D *BZmass_ZPt_CF_gg[100];

 //Forward and backward ZMass ZY MultiBin plot
 vector<MultiBinHist *> v_FZmass_ZY_uu_multi;
 MultiBinHist *FZmass_ZY_uu_multi[100];

 vector<MultiBinHist *> v_BZmass_ZY_uu_multi;
 MultiBinHist *BZmass_ZY_uu_multi[100];

 vector<MultiBinHist *> v_FZmass_ZY_dd_multi;
 MultiBinHist *FZmass_ZY_dd_multi[100];

 vector<MultiBinHist *> v_BZmass_ZY_dd_multi;
 MultiBinHist *BZmass_ZY_dd_multi[100];

 vector<MultiBinHist *> v_FZmass_ZY_gg_multi;
 MultiBinHist *FZmass_ZY_gg_multi[100];

 vector<MultiBinHist *> v_BZmass_ZY_gg_multi;
 MultiBinHist *BZmass_ZY_gg_multi[100];

 vector<MultiBinHist *> v_FZmass_ZY_CC_uu_multi;
 MultiBinHist *FZmass_ZY_CC_uu_multi[100];

 vector<MultiBinHist *> v_BZmass_ZY_CC_uu_multi;
 MultiBinHist *BZmass_ZY_CC_uu_multi[100];

 vector<MultiBinHist *> v_FZmass_ZY_CC_dd_multi;
 MultiBinHist *FZmass_ZY_CC_dd_multi[100];

 vector<MultiBinHist *> v_BZmass_ZY_CC_dd_multi;
 MultiBinHist *BZmass_ZY_CC_dd_multi[100];

 vector<MultiBinHist *> v_FZmass_ZY_CC_gg_multi;
 MultiBinHist *FZmass_ZY_CC_gg_multi[100];

 vector<MultiBinHist *> v_BZmass_ZY_CC_gg_multi;
 MultiBinHist *BZmass_ZY_CC_gg_multi[100];

 vector<MultiBinHist *> v_FZmass_ZY_CF_uu_multi;
 MultiBinHist *FZmass_ZY_CF_uu_multi[100];

 vector<MultiBinHist *> v_BZmass_ZY_CF_uu_multi;
 MultiBinHist *BZmass_ZY_CF_uu_multi[100];

 vector<MultiBinHist *> v_FZmass_ZY_CF_dd_multi;
 MultiBinHist *FZmass_ZY_CF_dd_multi[100];

 vector<MultiBinHist *> v_BZmass_ZY_CF_dd_multi;
 MultiBinHist *BZmass_ZY_CF_dd_multi[100];

 vector<MultiBinHist *> v_FZmass_ZY_CF_gg_multi;
 MultiBinHist *FZmass_ZY_CF_gg_multi[100];

 vector<MultiBinHist *> v_BZmass_ZY_CF_gg_multi;
 MultiBinHist *BZmass_ZY_CF_gg_multi[100];

 //Forward and backward ZY
 vector<TH1D *> v_FZRapidity_uu;
 TH1D *FZRapidity_uu[100];

 vector<TH1D *> v_BZRapidity_uu;
 TH1D *BZRapidity_uu[100];

 vector<TH1D *> v_FZRapidity_CC_uu;
 TH1D *FZRapidity_CC_uu[100];

 vector<TH1D *> v_BZRapidity_CC_uu;
 TH1D *BZRapidity_CC_uu[100];

 vector<TH1D *> v_FZRapidity_CF_uu;
 TH1D *FZRapidity_CF_uu[100];

 vector<TH1D *> v_BZRapidity_CF_uu;
 TH1D *BZRapidity_CF_uu[100];

 vector<TH1D *> v_FZRapidity_dd;
 TH1D *FZRapidity_dd[100];

 vector<TH1D *> v_BZRapidity_dd;
 TH1D *BZRapidity_dd[100];

 vector<TH1D *> v_FZRapidity_CC_dd;
 TH1D *FZRapidity_CC_dd[100];

 vector<TH1D *> v_BZRapidity_CC_dd;
 TH1D *BZRapidity_CC_dd[100];

 vector<TH1D *> v_FZRapidity_CF_dd;
 TH1D *FZRapidity_CF_dd[100];

 vector<TH1D *> v_BZRapidity_CF_dd;
 TH1D *BZRapidity_CF_dd[100];

 vector<TH1D *> v_FZRapidity_gg;
 TH1D *FZRapidity_gg[100];

 vector<TH1D *> v_BZRapidity_gg;
 TH1D *BZRapidity_gg[100];

 vector<TH1D *> v_FZRapidity_CC_gg;
 TH1D *FZRapidity_CC_gg[100];

 vector<TH1D *> v_BZRapidity_CC_gg;
 TH1D *BZRapidity_CC_gg[100];

 vector<TH1D *> v_FZRapidity_CF_gg;
 TH1D *FZRapidity_CF_gg[100];

 vector<TH1D *> v_BZRapidity_CF_gg;
 TH1D *BZRapidity_CF_gg[100];

 //quark level ZPt ZY and ZMass
 vector<TH1D *> v_ZPt_uub;
 TH1D *ZPt_uub[100];

 vector<TH1D *> v_ZRapidity_uub;
 TH1D *ZRapidity_uub[100];

 vector<TH1D *> v_ZMass_uub;
 TH1D *ZMass_uub[100];

 vector<TH1D *> v_ZPt_ddb;
 TH1D *ZPt_ddb[100];

 vector<TH1D *> v_ZRapidity_ddb;
 TH1D *ZRapidity_ddb[100];

 vector<TH1D *> v_ZMass_ddb;
 TH1D *ZMass_ddb[100];

 vector<TH1D *> v_ZPt_ssb;
 TH1D *ZPt_ssb[100];

 vector<TH1D *> v_ZRapidity_ssb;
 TH1D *ZRapidity_ssb[100];

 vector<TH1D *> v_ZMass_ssb;
 TH1D *ZMass_ssb[100];


 /////////////////////
 //  dilution plot  //
 /////////////////////

 //ZMass dilution
 vector<TH1D *> v_ZMass_CC_total;
 TH1D *ZMass_CC_total[100];
 vector<TH1D *> v_ZMass_CC_wrong;
 TH1D *ZMass_CC_wrong[100];
 TH1D *Dilution_ZMass_CC;

 vector<TH1D *> v_ZMass_CF_total;
 TH1D *ZMass_CF_total[100];
 vector<TH1D *> v_ZMass_CF_wrong;
 TH1D *ZMass_CF_wrong[100];
 TH1D *Dilution_ZMass_CF;

 vector<TH1D *> v_ZMass_Full_total;
 TH1D *ZMass_Full_total[100];
 vector<TH1D *> v_ZMass_Full_wrong;
 TH1D *ZMass_Full_wrong[100];
 TH1D *Dilution_ZMass_Full;

 //quark ZMass dilution
 vector<TH1D *> v_ZMass_CC_uu_total;
 TH1D *ZMass_CC_uu_total[100];
 vector<TH1D *> v_ZMass_CC_uu_wrong;
 TH1D *ZMass_CC_uu_wrong[100];
 TH1D *Dilution_ZMass_CC_uu;

 vector<TH1D *> v_ZMass_CC_dd_total;
 TH1D *ZMass_CC_dd_total[100];
 vector<TH1D *> v_ZMass_CC_dd_wrong;
 TH1D *ZMass_CC_dd_wrong[100];
 TH1D *Dilution_ZMass_CC_dd;

 vector<TH1D *> v_ZMass_CF_uu_total;
 TH1D *ZMass_CF_uu_total[100];
 vector<TH1D *> v_ZMass_CF_uu_wrong;
 TH1D *ZMass_CF_uu_wrong[100];
 TH1D *Dilution_ZMass_CF_uu;

 vector<TH1D *> v_ZMass_CF_dd_total;
 TH1D *ZMass_CF_dd_total[100];
 vector<TH1D *> v_ZMass_CF_dd_wrong;
 TH1D *ZMass_CF_dd_wrong[100];
 TH1D *Dilution_ZMass_CF_dd;

 vector<TH1D *> v_ZMass_Full_uu_total;
 TH1D *ZMass_Full_uu_total[100];
 vector<TH1D *> v_ZMass_Full_uu_wrong;
 TH1D *ZMass_Full_uu_wrong[100];
 TH1D *Dilution_ZMass_Full_uu;

 vector<TH1D *> v_ZMass_Full_dd_total;
 TH1D *ZMass_Full_dd_total[100];
 vector<TH1D *> v_ZMass_Full_dd_wrong;
 TH1D *ZMass_Full_dd_wrong[100];
 TH1D *Dilution_ZMass_Full_dd;

 vector<TH1D *> v_ZMass_Full_ss_total;
 TH1D *ZMass_Full_ss_total[100];
 vector<TH1D *> v_ZMass_Full_ss_wrong;
 TH1D *ZMass_Full_ss_wrong[100];
 TH1D *Dilution_ZMass_Full_ss;

 vector<TH1D *> v_ZMass_Full_cc_total;
 TH1D *ZMass_Full_cc_total[100];
 vector<TH1D *> v_ZMass_Full_cc_wrong;
 TH1D *ZMass_Full_cc_wrong[100];
 TH1D *Dilution_ZMass_Full_cc;

 vector<TH1D *> v_ZMass_Full_bb_total;
 TH1D *ZMass_Full_bb_total[100];
 vector<TH1D *> v_ZMass_Full_bb_wrong;
 TH1D *ZMass_Full_bb_wrong[100];
 TH1D *Dilution_ZMass_Full_bb;

 //Forward and backward dilution
 vector<TH1D *> v_FZmass_CC_uu_total;
 TH1D *FZmass_CC_uu_total[100];
 vector<TH1D *> v_FZmass_CC_uu_wrong;
 TH1D *FZmass_CC_uu_wrong[100];
 TH1D *Dilution_FZmass_CC_uu;

 vector<TH1D *> v_BZmass_CC_uu_total;
 TH1D *BZmass_CC_uu_total[100];
 vector<TH1D *> v_BZmass_CC_uu_wrong;
 TH1D *BZmass_CC_uu_wrong[100];
 TH1D *Dilution_BZmass_CC_uu;

 vector<TH1D *> v_FZmass_CC_dd_total;
 TH1D *FZmass_CC_dd_total[100];
 vector<TH1D *> v_FZmass_CC_dd_wrong;
 TH1D *FZmass_CC_dd_wrong[100];
 TH1D *Dilution_FZmass_CC_dd;

 vector<TH1D *> v_BZmass_CC_dd_total;
 TH1D *BZmass_CC_dd_total[100];
 vector<TH1D *> v_BZmass_CC_dd_wrong;
 TH1D *BZmass_CC_dd_wrong[100];
 TH1D *Dilution_BZmass_CC_dd;

 vector<TH1D *> v_FZmass_CF_uu_total;
 TH1D *FZmass_CF_uu_total[100];
 vector<TH1D *> v_FZmass_CF_uu_wrong;
 TH1D *FZmass_CF_uu_wrong[100];
 TH1D *Dilution_FZmass_CF_uu;

 vector<TH1D *> v_BZmass_CF_uu_total;
 TH1D *BZmass_CF_uu_total[100];
 vector<TH1D *> v_BZmass_CF_uu_wrong;
 TH1D *BZmass_CF_uu_wrong[100];
 TH1D *Dilution_BZmass_CF_uu;

 vector<TH1D *> v_FZmass_CF_dd_total;
 TH1D *FZmass_CF_dd_total[100];
 vector<TH1D *> v_FZmass_CF_dd_wrong;
 TH1D *FZmass_CF_dd_wrong[100];
 TH1D *Dilution_FZmass_CF_dd;

 vector<TH1D *> v_BZmass_CF_dd_total;
 TH1D *BZmass_CF_dd_total[100];
 vector<TH1D *> v_BZmass_CF_dd_wrong;
 TH1D *BZmass_CF_dd_wrong[100];
 TH1D *Dilution_BZmass_CF_dd;

 vector<TH1D *> v_FZmass_Full_uu_total;
 TH1D *FZmass_Full_uu_total[100];
 vector<TH1D *> v_FZmass_Full_uu_wrong;
 TH1D *FZmass_Full_uu_wrong[100];
 TH1D *Dilution_FZmass_Full_uu;

 vector<TH1D *> v_BZmass_Full_uu_total;
 TH1D *BZmass_Full_uu_total[100];
 vector<TH1D *> v_BZmass_Full_uu_wrong;
 TH1D *BZmass_Full_uu_wrong[100];
 TH1D *Dilution_BZmass_Full_uu;

 vector<TH1D *> v_FZmass_Full_dd_total;
 TH1D *FZmass_Full_dd_total[100];
 vector<TH1D *> v_FZmass_Full_dd_wrong;
 TH1D *FZmass_Full_dd_wrong[100];
 TH1D *Dilution_FZmass_Full_dd;

 vector<TH1D *> v_BZmass_Full_dd_total;
 TH1D *BZmass_Full_dd_total[100];
 vector<TH1D *> v_BZmass_Full_dd_wrong;
 TH1D *BZmass_Full_dd_wrong[100];
 TH1D *Dilution_BZmass_Full_dd;


 //AFB Dilution Mass ZY 2D
 vector<TH2D *> v_ZMass_ZY_total;
 TH2D *ZMass_ZY_total[100];
 vector<TH2D *> v_ZMass_ZY_wrong;
 TH2D *ZMass_ZY_wrong[100];
 TH2D *Dilution_ZMass_ZY;

 vector<TH2D *> v_ZMass_ZY_CC_total;
 TH2D *ZMass_ZY_CC_total[100];
 vector<TH2D *> v_ZMass_ZY_CC_wrong;
 TH2D *ZMass_ZY_CC_wrong[100];
 TH2D *Dilution_ZMass_ZY_CC;

 vector<TH2D *> v_ZMass_ZY_CF_total;
 TH2D *ZMass_ZY_CF_total[100];
 vector<TH2D *> v_ZMass_ZY_CF_wrong;
 TH2D *ZMass_ZY_CF_wrong[100];
 TH2D *Dilution_ZMass_ZY_CF;

 vector<TH2D *> v_ZMass_ZY_uu_total;
 TH2D *ZMass_ZY_uu_total[100];
 vector<TH2D *> v_ZMass_ZY_uu_wrong;
 TH2D *ZMass_ZY_uu_wrong[100];
 TH2D *Dilution_ZMass_ZY_uu;

 vector<TH2D *> v_ZMass_ZY_CC_uu_total;
 TH2D *ZMass_ZY_CC_uu_total[100];
 vector<TH2D *> v_ZMass_ZY_CC_uu_wrong;
 TH2D *ZMass_ZY_CC_uu_wrong[100];
 TH2D *Dilution_ZMass_ZY_CC_uu;

 vector<TH2D *> v_ZMass_ZY_CF_uu_total;
 TH2D *ZMass_ZY_CF_uu_total[100];
 vector<TH2D *> v_ZMass_ZY_CF_uu_wrong;
 TH2D *ZMass_ZY_CF_uu_wrong[100];
 TH2D *Dilution_ZMass_ZY_CF_uu;

 vector<TH2D *> v_ZMass_ZY_dd_total;
 TH2D *ZMass_ZY_dd_total[100];
 vector<TH2D *> v_ZMass_ZY_dd_wrong;
 TH2D *ZMass_ZY_dd_wrong[100];
 TH2D *Dilution_ZMass_ZY_dd;

 vector<TH2D *> v_ZMass_ZY_CC_dd_total;
 TH2D *ZMass_ZY_CC_dd_total[100];
 vector<TH2D *> v_ZMass_ZY_CC_dd_wrong;
 TH2D *ZMass_ZY_CC_dd_wrong[100];
 TH2D *Dilution_ZMass_ZY_CC_dd;

 vector<TH2D *> v_ZMass_ZY_CF_dd_total;
 TH2D *ZMass_ZY_CF_dd_total[100];
 vector<TH2D *> v_ZMass_ZY_CF_dd_wrong;
 TH2D *ZMass_ZY_CF_dd_wrong[100];
 TH2D *Dilution_ZMass_ZY_CF_dd;

 vector<TH2D *> v_FZMass_ZY_total;
 TH2D *FZMass_ZY_total[100];
 vector<TH2D *> v_FZMass_ZY_wrong;
 TH2D *FZMass_ZY_wrong[100];
 TH2D *Dilution_FZMass_ZY;

 vector<TH2D *> v_BZMass_ZY_total;
 TH2D *BZMass_ZY_total[100];
 vector<TH2D *> v_BZMass_ZY_wrong;
 TH2D *BZMass_ZY_wrong[100];
 TH2D *Dilution_BZMass_ZY;

 //AFB Dilution Mass ZPt 2D
 vector<TH2D *> v_ZMass_ZPt_total;
 TH2D *ZMass_ZPt_total[100];
 vector<TH2D *> v_ZMass_ZPt_wrong;
 TH2D *ZMass_ZPt_wrong[100];
 TH2D *Dilution_ZMass_ZPt;

 vector<TH2D *> v_ZMass_ZPt_CC_total;
 TH2D *ZMass_ZPt_CC_total[100];
 vector<TH2D *> v_ZMass_ZPt_CC_wrong;
 TH2D *ZMass_ZPt_CC_wrong[100];
 TH2D *Dilution_ZMass_ZPt_CC;

 vector<TH2D *> v_ZMass_ZPt_CF_total;
 TH2D *ZMass_ZPt_CF_total[100];
 vector<TH2D *> v_ZMass_ZPt_CF_wrong;
 TH2D *ZMass_ZPt_CF_wrong[100];
 TH2D *Dilution_ZMass_ZPt_CF;

 vector<TH2D *> v_ZMass_ZPt_uu_total;
 TH2D *ZMass_ZPt_uu_total[100];
 vector<TH2D *> v_ZMass_ZPt_uu_wrong;
 TH2D *ZMass_ZPt_uu_wrong[100];
 TH2D *Dilution_ZMass_ZPt_uu;

 vector<TH2D *> v_ZMass_ZPt_CC_uu_total;
 TH2D *ZMass_ZPt_CC_uu_total[100];
 vector<TH2D *> v_ZMass_ZPt_CC_uu_wrong;
 TH2D *ZMass_ZPt_CC_uu_wrong[100];
 TH2D *Dilution_ZMass_ZPt_CC_uu;

 vector<TH2D *> v_ZMass_ZPt_CF_uu_total;
 TH2D *ZMass_ZPt_CF_uu_total[100];
 vector<TH2D *> v_ZMass_ZPt_CF_uu_wrong;
 TH2D *ZMass_ZPt_CF_uu_wrong[100];
 TH2D *Dilution_ZMass_ZPt_CF_uu;

 vector<TH2D *> v_ZMass_ZPt_dd_total;
 TH2D *ZMass_ZPt_dd_total[100];
 vector<TH2D *> v_ZMass_ZPt_dd_wrong;
 TH2D *ZMass_ZPt_dd_wrong[100];
 TH2D *Dilution_ZMass_ZPt_dd;

 vector<TH2D *> v_ZMass_ZPt_CC_dd_total;
 TH2D *ZMass_ZPt_CC_dd_total[100];
 vector<TH2D *> v_ZMass_ZPt_CC_dd_wrong;
 TH2D *ZMass_ZPt_CC_dd_wrong[100];
 TH2D *Dilution_ZMass_ZPt_CC_dd;

 vector<TH2D *> v_ZMass_ZPt_CF_dd_total;
 TH2D *ZMass_ZPt_CF_dd_total[100];
 vector<TH2D *> v_ZMass_ZPt_CF_dd_wrong;
 TH2D *ZMass_ZPt_CF_dd_wrong[100];
 TH2D *Dilution_ZMass_ZPt_CF_dd;

 //AFB Dilution MultiBin
 vector<MultiBinHist *> v_ZMass_ZY_total_multi;
 MultiBinHist *ZMass_ZY_total_multi[100];
 vector<MultiBinHist *> v_ZMass_ZY_wrong_multi;
 MultiBinHist *ZMass_ZY_wrong_multi[100];
 MultiBinHist *Dilution_ZMass_ZY_multi;

 vector<MultiBinHist *> v_ZMass_ZY_CC_total_multi;
 MultiBinHist *ZMass_ZY_CC_total_multi[100];
 vector<MultiBinHist *> v_ZMass_ZY_CC_wrong_multi;
 MultiBinHist *ZMass_ZY_CC_wrong_multi[100];
 MultiBinHist *Dilution_ZMass_ZY_CC_multi;

 vector<MultiBinHist *> v_ZMass_ZY_CF_total_multi;
 MultiBinHist *ZMass_ZY_CF_total_multi[100];
 vector<MultiBinHist *> v_ZMass_ZY_CF_wrong_multi;
 MultiBinHist *ZMass_ZY_CF_wrong_multi[100];
 MultiBinHist *Dilution_ZMass_ZY_CF_multi;

 vector<MultiBinHist *> v_ZMass_ZY_uu_total_multi;
 MultiBinHist *ZMass_ZY_uu_total_multi[100];
 vector<MultiBinHist *> v_ZMass_ZY_uu_wrong_multi;
 MultiBinHist *ZMass_ZY_uu_wrong_multi[100];
 MultiBinHist *Dilution_ZMass_ZY_uu_multi;

 vector<MultiBinHist *> v_ZMass_ZY_CC_uu_total_multi;
 MultiBinHist *ZMass_ZY_CC_uu_total_multi[100];
 vector<MultiBinHist *> v_ZMass_ZY_CC_uu_wrong_multi;
 MultiBinHist *ZMass_ZY_CC_uu_wrong_multi[100];
 MultiBinHist *Dilution_ZMass_ZY_CC_uu_multi;

 vector<MultiBinHist *> v_ZMass_ZY_CF_uu_total_multi;
 MultiBinHist *ZMass_ZY_CF_uu_total_multi[100];
 vector<MultiBinHist *> v_ZMass_ZY_CF_uu_wrong_multi;
 MultiBinHist *ZMass_ZY_CF_uu_wrong_multi[100];
 MultiBinHist *Dilution_ZMass_ZY_CF_uu_multi;

 vector<MultiBinHist *> v_ZMass_ZY_dd_total_multi;
 MultiBinHist *ZMass_ZY_dd_total_multi[100];
 vector<MultiBinHist *> v_ZMass_ZY_dd_wrong_multi;
 MultiBinHist *ZMass_ZY_dd_wrong_multi[100];
 MultiBinHist *Dilution_ZMass_ZY_dd_multi;

 vector<MultiBinHist *> v_ZMass_ZY_CC_dd_total_multi;
 MultiBinHist *ZMass_ZY_CC_dd_total_multi[100];
 vector<MultiBinHist *> v_ZMass_ZY_CC_dd_wrong_multi;
 MultiBinHist *ZMass_ZY_CC_dd_wrong_multi[100];
 MultiBinHist *Dilution_ZMass_ZY_CC_dd_multi;

 vector<MultiBinHist *> v_ZMass_ZY_CF_dd_total_multi;
 MultiBinHist *ZMass_ZY_CF_dd_total_multi[100];
 vector<MultiBinHist *> v_ZMass_ZY_CF_dd_wrong_multi;
 MultiBinHist *ZMass_ZY_CF_dd_wrong_multi[100];
 MultiBinHist *Dilution_ZMass_ZY_CF_dd_multi;


 //AFB Dilution 3D
 vector<TH3D *> v_ZMass_ZY_QT_total;
 TH3D *ZMass_ZY_QT_total[100];
 vector<TH3D *> v_ZMass_ZY_QT_wrong;
 TH3D *ZMass_ZY_QT_wrong[100];
 TH3D *Dilution_ZMass_ZY_QT;

 vector<TH3D *> v_ZMass_ZY_QT_CC_total;
 TH3D *ZMass_ZY_QT_CC_total[100];
 vector<TH3D *> v_ZMass_ZY_QT_CC_wrong;
 TH3D *ZMass_ZY_QT_CC_wrong[100];
 TH3D *Dilution_ZMass_ZY_QT_CC;

 vector<TH3D *> v_ZMass_ZY_QT_CF_total;
 TH3D *ZMass_ZY_QT_CF_total[100];
 vector<TH3D *> v_ZMass_ZY_QT_CF_wrong;
 TH3D *ZMass_ZY_QT_CF_wrong[100];
 TH3D *Dilution_ZMass_ZY_QT_CF;

 //Dilution vs ZPt ZY CosThetaQ
 vector<TH1D *> v_ZPt_total;
 TH1D *ZPt_total[100];
 vector<TH1D *> v_ZPt_wrong;
 TH1D *ZPt_wrong[100];
 TH1D *Dilution_ZPt;

 vector<TH1D *> v_ZPt_LepPtCut_total;
 TH1D *ZPt_LepPtCut_total[100];
 vector<TH1D *> v_ZPt_LepPtCut_wrong;
 TH1D *ZPt_LepPtCut_wrong[100];
 TH1D *Dilution_ZPt_LepPtCut;

 vector<TH1D *> v_ZPt_NoLepPtCut_total;
 TH1D *ZPt_NoLepPtCut_total[100];
 vector<TH1D *> v_ZPt_NoLepPtCut_wrong;
 TH1D *ZPt_NoLepPtCut_wrong[100];
 TH1D *Dilution_ZPt_NoLepPtCut;

 vector<TH1D *> v_ZRapidity_total;
 TH1D *ZRapidity_total[100];
 vector<TH1D *> v_ZRapidity_wrong;
 TH1D *ZRapidity_wrong[100];
 TH1D *Dilution_ZRapidity;

 vector<TH1D *> v_ZRapidity_LepPtCut_total;
 TH1D *ZRapidity_LepPtCut_total[100];
 vector<TH1D *> v_ZRapidity_LepPtCut_wrong;
 TH1D *ZRapidity_LepPtCut_wrong[100];
 TH1D *Dilution_ZRapidity_LepPtCut;

 vector<TH1D *> v_ZRapidity_NoLepPtCut_total;
 TH1D *ZRapidity_NoLepPtCut_total[100];
 vector<TH1D *> v_ZRapidity_NoLepPtCut_wrong;
 TH1D *ZRapidity_NoLepPtCut_wrong[100];
 TH1D *Dilution_ZRapidity_NoLepPtCut;

 vector<TH1D *> v_ZRapidity_uu_total;
 TH1D *ZRapidity_uu_total[100];
 vector<TH1D *> v_ZRapidity_uu_wrong;
 TH1D *ZRapidity_uu_wrong[100];
 TH1D *Dilution_ZRapidity_uu;

 vector<TH1D *> v_ZRapidity_dd_total;
 TH1D *ZRapidity_dd_total[100];
 vector<TH1D *> v_ZRapidity_dd_wrong;
 TH1D *ZRapidity_dd_wrong[100];
 TH1D *Dilution_ZRapidity_dd;

 vector<TH1D *> v_ZRapidityAbs_total;
 TH1D *ZRapidityAbs_total[100];
 vector<TH1D *> v_ZRapidityAbs_wrong;
 TH1D *ZRapidityAbs_wrong[100];
 TH1D *Dilution_ZRapidityAbs;

 vector<TH1D *> v_CosThetaQ_total;
 TH1D *CosThetaQ_total[100];
 vector<TH1D *> v_CosThetaQ_wrong;
 TH1D *CosThetaQ_wrong[100];
 TH1D *Dilution_CosThetaQ;

 vector<AngularFunction *> v_A0_ZPt;
 AngularFunction *A0_ZPt[100];

 vector<AngularFunction *> v_A1_ZPt;
 AngularFunction *A1_ZPt[100];

 vector<AngularFunction *> v_A2_ZPt;
 AngularFunction *A2_ZPt[100];

 vector<AngularFunction *> v_A3_ZPt;
 AngularFunction *A3_ZPt[100];

 vector<AngularFunction *> v_A4_ZPt;
 AngularFunction *A4_ZPt[100];

 vector<AngularFunction *> v_L0_ZPt;
 AngularFunction *L0_ZPt[100];

 int ZY_bin = 5; double ZY_left = 0.0; double ZY_right = 5.0;
 int ZY_CC_bin = 3; double ZY_CC_left = 0.0; double ZY_CC_right = 2.5;
 int ZY_CF_bin = 3; double ZY_CF_left = 1.0; double ZY_CF_right = 4.0;
// double rangeZY[ZY_bin + 1];
// double rangeZYCC[ZY_CC_bin + 1];
// double rangeZYCF[ZY_CF_bin + 1];

 int ZPt_bin = 5; double ZPt_left = 0.0; double ZPt_right = 100.0;
 int ZPt_CC_bin = 5; double ZPt_CC_left = 0.0; double ZPt_CC_right = 100.0;
 int ZPt_CF_bin = 5; double ZPt_CF_left = 0.0; double ZPt_CF_right = 100.0;
// double RangeZPt[ZPt_bin + 1];
// double RangeZPtCC[ZPt_CC_bin + 1];
// double RangeZPtCF[ZPt_CF_bin + 1];

 TH1D *AFB_CC;
 TH1D *AFB_CF;
 TH1D *AFB_Full;
 TH1D *AFB_LepPtCut;
 TH1D *AFB_NoLepPtCut;

 TH2D *AFB_Mass_ZY_CC;
 TH2D *AFB_Mass_ZY_CF;
 TH2D *AFB_Mass_ZY_Full;

 TH2D *AFB_Mass_ZPt_CC;
 TH2D *AFB_Mass_ZPt_CF;
 TH2D *AFB_Mass_ZPt_Full;

 MultiBinHist *AFB_Mass_ZY_CC_multi;
 MultiBinHist *AFB_Mass_ZY_CF_multi;
 MultiBinHist *AFB_Mass_ZY_Full_multi;

 TH1D *AFBSlope_ZY_CC;
 TH1D *AFBSlope_ZY_CF;
 TH1D *AFBSlope_ZY_Full;
 TH1D *AFBOffset_ZY_CC;
 TH1D *AFBOffset_ZY_CF;
 TH1D *AFBOffset_ZY_Full;

 TH1D *AFBSlope_ZPt_CC;
 TH1D *AFBSlope_ZPt_CF;
 TH1D *AFBSlope_ZPt_Full;
 TH1D *AFBOffset_ZPt_CC;
 TH1D *AFBOffset_ZPt_CF;
 TH1D *AFBOffset_ZPt_Full;

 TH1D *AFBSlope_CC;
 TH1D *AFBSlope_CF;
 TH1D *AFBSlope_Full;

 TH1D *AFBSlope_82_98_CC;
 TH1D *AFBSlope_82_98_CF;
 TH1D *AFBSlope_82_98_Full;

 TH1D *AFBSlope_84_96_CC;
 TH1D *AFBSlope_84_96_CF;
 TH1D *AFBSlope_84_96_Full;

 TH1D *AFBOffset_CC;
 TH1D *AFBOffset_CF;
 TH1D *AFBOffset_Full;

 TH1D *AFBOffset_82_98_CC;
 TH1D *AFBOffset_82_98_CF;
 TH1D *AFBOffset_82_98_Full;

 TH1D *AFBOffset_84_96_CC;
 TH1D *AFBOffset_84_96_CF;
 TH1D *AFBOffset_84_96_Full;

 TH2D *AFBQuark_Mass_ZY_CC;
 TH2D *AFBQuark_Mass_ZY_CF;
 TH2D *AFBQuark_Mass_ZY_Full;

 TH2D *AFBQuark_Mass_ZPt_CC;
 TH2D *AFBQuark_Mass_ZPt_CF;
 TH2D *AFBQuark_Mass_ZPt_Full;

 TH1D *AFBQuarkSlope_ZY_CC;
 TH1D *AFBQuarkSlope_ZY_CF;
 TH1D *AFBQuarkSlope_ZY_Full;
 TH1D *AFBQuarkOffset_ZY_CC;
 TH1D *AFBQuarkOffset_ZY_CF;
 TH1D *AFBQuarkOffset_ZY_Full;

 TH1D *AFBQuarkSlope_ZPt_CC;
 TH1D *AFBQuarkSlope_ZPt_CF;
 TH1D *AFBQuarkSlope_ZPt_Full;
 TH1D *AFBQuarkOffset_ZPt_CC;
 TH1D *AFBQuarkOffset_ZPt_CF;
 TH1D *AFBQuarkOffset_ZPt_Full;

 TH1D *AFBQuarkSlope_CC;
 TH1D *AFBQuarkSlope_CF;
 TH1D *AFBQuarkSlope_Full;

 TH1D *AFBQuarkSlope_82_98_CC;
 TH1D *AFBQuarkSlope_82_98_CF;
 TH1D *AFBQuarkSlope_82_98_Full;

 TH1D *AFBQuarkSlope_84_96_CC;
 TH1D *AFBQuarkSlope_84_96_CF;
 TH1D *AFBQuarkSlope_84_96_Full;

 TH1D *AFBQuarkOffset_CC;
 TH1D *AFBQuarkOffset_CF;
 TH1D *AFBQuarkOffset_Full;

 TH1D *AFBQuarkOffset_82_98_CC;
 TH1D *AFBQuarkOffset_82_98_CF;
 TH1D *AFBQuarkOffset_82_98_Full;

 TH1D *AFBQuarkOffset_84_96_CC;
 TH1D *AFBQuarkOffset_84_96_CF;
 TH1D *AFBQuarkOffset_84_96_Full;

 TH1D *AFBQuarkDiff_CC;
 TH1D *AFBQuarkDiff_CF;
 TH1D *AFBQuarkDiff_Full;

 TH1D *AFBQuarkDiff_CC_Side;
 TH1D *AFBQuarkDiff_CF_Side;
 TH1D *AFBQuarkDiff_Full_Side;

 TH1D *AFBQuarkDiff_CC_ZPole;
 TH1D *AFBQuarkDiff_CF_ZPole;
 TH1D *AFBQuarkDiff_Full_ZPole;

 TH1D *AFBQuarkFraction_CC;
 TH1D *AFBQuarkFraction_CF;
 TH1D *AFBQuarkFraction_Full;

 TH1D *AFBS0Fraction_CC;
 TH1D *AFBS0Fraction_CF;
 TH1D *AFBS0Fraction_Full;

 TH2D *AFBS0Fraction_ZY_CC;
 TH2D *AFBS0Fraction_ZY_CF;
 TH2D *AFBS0Fraction_ZY_Full;

 TH2D *AFBS0Fraction_ZPt_CC;
 TH2D *AFBS0Fraction_ZPt_CF;
 TH2D *AFBS0Fraction_ZPt_Full;

 TH1D *AFBDiff_CC;
 TH1D *AFBDiff_CF;
 TH1D *AFBDiff_Full;

 TH1D *AFBDiff_CC_ZPole;
 TH1D *AFBDiff_CF_ZPole;
 TH1D *AFBDiff_Full_ZPole;

 TH1D *AFBDiff_CC_Side;
 TH1D *AFBDiff_CF_Side;
 TH1D *AFBDiff_Full_Side;

 TH2D *AFBDiff_Mass_ZY_CC;
 TH2D *AFBDiff_Mass_ZY_CF;
 TH2D *AFBDiff_Mass_ZY_Full;

 MultiBinHist *AFBDiff_Mass_ZY_CC_multi;
 MultiBinHist *AFBDiff_Mass_ZY_CF_multi;
 MultiBinHist *AFBDiff_Mass_ZY_Full_multi;

 TH1D *AFBGradient_CC;
 TH1D *AFBGradient_CF;
 TH1D *AFBGradient_Full;

 TH1D *AFBQuarkGradient_CC;
 TH1D *AFBQuarkGradient_CF;
 TH1D *AFBQuarkGradient_Full;

 TH1D *DilutionGradient_CC;
 TH1D *DilutionGradient_CF;
 TH1D *DilutionGradient_Full;

 TH1D *DilutionAFB_CC;
 TH1D *DilutionAFB_CF;
 TH1D *DilutionAFB_Full;

 TH1D *AveAFB_CC;
 TH1D *AveAFB_CF;
 TH1D *AveAFB_Full;

 TH1D *QuarkAveAFB_CC;
 TH1D *QuarkAveAFB_CF;
 TH1D *QuarkAveAFB_Full;

 TH1D *AFB_CC_uu;
 TH1D *AFB_CF_uu;
 TH1D *AFB_Full_uu;

 TH1D *AFB_CC_dd;
 TH1D *AFB_CF_dd;
 TH1D *AFB_Full_dd;

 TH1D *AFB_CC_qq;
 TH1D *AFB_CF_qq;
 TH1D *AFB_Full_qq;

 TH1D *AFB_Full_uudd;
 TH1D *AFB_Full_gluon;

 TH1D *AFB_ZPt_uu;
 TH1D *AFB_ZPt_dd;

 TH2D *AFB_Mass_ZPt_Full_uu;
 TH2D *AFB_Mass_ZPt_Full_dd;

 TH1D *AFB_Hadron_Full_uu;
 TH1D *AFB_Hadron_Full_dd;
 TH1D *AFB_Hadron_Full_uudd;

 TH1D *AFB_Hadron_Full_utype;
 TH1D *AFB_Hadron_Full_dtype;

 TH1D *AveAFB_Full_uu;
 TH1D *AveAFB_Full_dd;
 TH1D *AveAFB_Full_uudd;

 TH1D *AveAFB_Full_utype;
 TH1D *AveAFB_Full_dtype;

 TH1D *QuarkAveAFB_Full_uu;
 TH1D *QuarkAveAFB_Full_dd;
 TH1D *QuarkAveAFB_Full_uudd;
 TH1D *QuarkAveAFB_Full_gluon;

 TH1D *AFB_qq_ApplyDilution;
 TH1D *AFB_qq_ApplyDilution_CC;
 TH1D *AFB_qq_ApplyDilution_CF;

 TH1D *FZRapidity_CC_qq;
 TH1D *BZRapidity_CC_qq;

 TH1D *FZRapidity_CF_qq;
 TH1D *BZRapidity_CF_qq;

 TH1D *FZRapidity_qq;
 TH1D *BZRapidity_qq;

 TH1D *LepAsym;
 TH1D *LepAsym_CC;
 TH1D *LepAsym_CF;

 TH1D *AFB_ZY;
 TH1D *AFB_ZY_LepPtCut;
 TH1D *AFB_ZY_NoLepPtCut;
 TH1D *AFB_ZY_CC;
 TH1D *AFB_ZY_CF;

 TH1D *AFB_ZPt;
 TH1D *AFB_ZPt_LepPtCut;
 TH1D *AFB_ZPt_NoLepPtCut;

 TH1D *AFB_ZY_qq;
 TH1D *AFB_ZY_CC_qq;
 TH1D *AFB_ZY_CF_qq;

 TH1D *LepWidthDiff;
 TH1D *LepWidthDiff_CC;
 TH1D *LepWidthDiff_CF;

 TH1D *ReweightFactor_FZRapidity;
 TH1D *ReweightFactor_BZRapidity;
 TH1D *ReweightFactor_FZRapidity_CC;
 TH1D *ReweightFactor_BZRapidity_CC;
 TH1D *ReweightFactor_FZRapidity_CF;
 TH1D *ReweightFactor_BZRapidity_CF;

 TH1D *Dilution_Linear;
 double Dilution_k;
 double Dilution_b;

//member function
 vector<TString> sysName;
 HistsPythia();
 virtual void InitialSysName();
 virtual void bookHists(int TotalThread);
 virtual void GetReweightFactor();
 virtual void RetrieveWeight(double ZY, double &FZY, double &BZY, double &FZY_CC, double &BZY_CC, double &FZY_CF, double &BZY_CF);
 virtual void LinkClass(HistsPythia *hist);
 virtual void InputParameter(double k, double b);
 virtual void GetPartOfResults();
 virtual void outputInformation();

// template<class T>

};

#ifdef HistsPythia_cxx
HistsPythia* m_HistsPythia;
double MinChi2 = 1000000.0;
#endif
void DilutionFitting(Int_t &npbar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag);

#endif
