#ifndef __HISTS_PYTHIAW_H_
#define __HISTS_PYTHIAW_H_
#include <iostream>
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TFile.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TMath.h"
#include "TTree.h"
#include <vector>
#include "makeHists.h"

using namespace std;

class HistsPythiaW : public makeHists
{
 public:
 vector<TH1D *> v_FZmass_CC;
 TH1D *FZmass_CC[100];

 vector<TH1D *> v_BZmass_CC;
 TH1D *BZmass_CC[100];

 vector<TH1D *> v_FZmass_CF;
 TH1D *FZmass_CF[100];

 vector<TH1D *> v_BZmass_CF;
 TH1D *BZmass_CF[100];

 vector<TH1D *> v_ZPt;
 TH1D *ZPt[100];

 vector<TH1D *> v_ZRapidity;
 TH1D *ZRapidity[100];

 vector<TH1D *> v_ZMass;
 TH1D *ZMass[100];

 vector<TH1D *> v_FZmass;
 TH1D *FZmass[100];

 vector<TH1D *> v_BZmass;
 TH1D *BZmass[100];

 vector<TH1D *> v_CosTheta;
 TH1D *CosTheta[100];

 vector<TH1D *> v_CollinsPhi;
 TH1D *CollinsPhi[100];

 vector<TH1D *> v_plot_phi_eta;
 TH1D *plot_phi_eta[100];

 vector<TH1D *> v_leptonPt;
 TH1D *leptonPt[100];

 vector<TH1D *> v_leptonEta;
 TH1D *leptonEta[100];

 vector<TH1D *> v_WPt_ud;
 TH1D *WPt_ud[100];

 vector<TH1D *> v_WRapidity_ud;
 TH1D *WRapidity_ud[100];

 vector<TH1D *> v_WMass_ud;
 TH1D *WMass_ud[100];

 vector<TH1D *> v_LeptonEta_ud;
 TH1D *LeptonEta_ud[100];

 vector<TH1D *> v_LeptonEta_LHCb_ud;
 TH1D *LeptonEta_LHCb_ud[100];

 vector<TH1D *> v_WPt_cs;
 TH1D *WPt_cs[100];

 vector<TH1D *> v_WRapidity_cs;
 TH1D *WRapidity_cs[100];

 vector<TH1D *> v_WMass_cs;
 TH1D *WMass_cs[100];

 vector<TH1D *> v_LeptonEta_cs;
 TH1D *LeptonEta_cs[100];

 vector<TH1D *> v_LeptonEta_LHCb_cs;
 TH1D *LeptonEta_LHCb_cs[100];

 vector<TH1D *> v_WPt_us;
 TH1D *WPt_us[100];

 vector<TH1D *> v_WRapidity_us;
 TH1D *WRapidity_us[100];

 vector<TH1D *> v_WMass_us;
 TH1D *WMass_us[100];

 vector<TH1D *> v_LeptonEta_us;
 TH1D *LeptonEta_us[100];

 vector<TH1D *> v_LeptonEta_LHCb_us;
 TH1D *LeptonEta_LHCb_us[100];

 vector<AngularFunction *> v_A0_ZPt;
 AngularFunction *A0_ZPt[100];

 vector<AngularFunction *> v_A1_ZPt;
 AngularFunction *A1_ZPt[100];

 vector<AngularFunction *> v_A2_ZPt;
 AngularFunction *A2_ZPt[100];

 vector<AngularFunction *> v_A3_ZPt;
 AngularFunction *A3_ZPt[100];

 vector<AngularFunction *> v_A4_ZPt;
 AngularFunction *A4_ZPt[100];

 vector<AngularFunction *> v_L0_ZPt;
 AngularFunction *L0_ZPt[100];

//member function
 vector<TString> sysName;
 virtual void InitialSysName();
 virtual void bookHists(int TotalThread);

// template<class T>

};
#endif
