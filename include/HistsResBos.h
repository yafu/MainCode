#ifndef __HISTS_RESBOS_H_
#define __HISTS_RESBOS_H_
#include <iostream>
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TFile.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TMath.h"
#include "TTree.h"
#include <vector>
#include "makeHists.h"

using namespace std;

class HistsResBos : public makeHists
{
 public:

 /////////////////////////
 //  hadron level plot  //
 /////////////////////////


 vector<TH1D *> v_ZMass;
 TH1D *ZMass[100];

 vector<TH1D *> v_ZMass_CC;
 TH1D *ZMass_CC[100];

 vector<TH1D *> v_ZMass_CF;
 TH1D *ZMass_CF[100];

 vector<TH1D *> v_FZmass;
 TH1D *FZmass[100];

 vector<TH1D *> v_BZmass;
 TH1D *BZmass[100];

 vector<TH1D *> v_FZmass_CC;
 TH1D *FZmass_CC[100];

 vector<TH1D *> v_BZmass_CC;
 TH1D *BZmass_CC[100];

 vector<TH1D *> v_FZmass_CF;
 TH1D *FZmass_CF[100];

 vector<TH1D *> v_BZmass_CF;
 TH1D *BZmass_CF[100];

 vector<TH1D *> v_FZmass_LHCb;
 TH1D *FZmass_LHCb[100];

 vector<TH1D *> v_BZmass_LHCb;
 TH1D *BZmass_LHCb[100];

 vector<TH1D *> v_ZPt;
 TH1D *ZPt[100];

 vector<TH1D *> v_ZRapidity;
 TH1D *ZRapidity[100];

 vector<TH1D *> v_ZE;
 TH1D *ZE[100];

 vector<TH2D *> v_ZYE;
 TH2D *ZYE[100];

 vector<TH1D *> v_ZRapidity_LHCb;
 TH1D *ZRapidity_LHCb[100];

 vector<TH1D *> v_ZPt_LHCb;
 TH1D *ZPt_LHCb[100];

 vector<TH1D *> v_phi_eta_LHCb;
 TH1D *phi_eta_LHCb[100];

 vector<TH1D *> v_ZRapidityAbs;
 TH1D *ZRapidityAbs[100];

 vector<TH1D *> v_ZPzPtBalance;
 TH1D *ZPzPtBalance[100];

 vector<TH1D *> v_ZPzPtBalance_CC;
 TH1D *ZPzPtBalance_CC[100];

 vector<TH1D *> v_ZPzPtBalance_CF;
 TH1D *ZPzPtBalance_CF[100];

 vector<TH1D *> v_CosTheta;
 TH1D *CosTheta[100];

 vector<TH1D *> v_CosThetaQ;
 TH1D *CosThetaQ[100];

 vector<TH1D *> v_CosTheta_q;
 TH1D *CosTheta_q[100];

 vector<TH1D *> v_CosTheta_h;
 TH1D *CosTheta_h[100];

 vector<TH1D *> v_ZRapidity_qqbar;
 TH1D *ZRapidity_qqbar[100];

 vector<TH1D *> v_ZRapidity_qbarq;
 TH1D *ZRapidity_qbarq[100];

 vector<TH1D *> v_CollinsPhi;
 TH1D *CollinsPhi[100];

 vector<TH1D *> v_plot_phi_eta;
 TH1D *plot_phi_eta[100];

 vector<TH1D *> v_leptonPt;
 TH1D *leptonPt[100];

 vector<TH1D *> v_leptonEta;
 TH1D *leptonEta[100];

 //Lepton Antilepton eta abs
 vector<TH1D *> v_LepEtaAbs;
 TH1D *LepEtaAbs[100];

 vector<TH1D *> v_AntiLepEtaAbs;
 TH1D *AntiLepEtaAbs[100];

 vector<TH1D *> v_LepEtaAbs_CC;
 TH1D *LepEtaAbs_CC[100];

 vector<TH1D *> v_AntiLepEtaAbs_CC;
 TH1D *AntiLepEtaAbs_CC[100];

 vector<TH1D *> v_LepEtaAbs_CF;
 TH1D *LepEtaAbs_CF[100];

 vector<TH1D *> v_AntiLepEtaAbs_CF;
 TH1D *AntiLepEtaAbs_CF[100];

 vector<TH1D *> v_LepEtaAbs_LHCb;
 TH1D *LepEtaAbs_LHCb[100];

 vector<TH1D *> v_AntiLepEtaAbs_LHCb;
 TH1D *AntiLepEtaAbs_LHCb[100];

 //Lepton Antilepton eta
 vector<TH1D *> v_LepEta;
 TH1D *LepEta[100];

 vector<TH1D *> v_AntiLepEta;
 TH1D *AntiLepEta[100];

 vector<TH1D *> v_LepEta_CC;
 TH1D *LepEta_CC[100];

 vector<TH1D *> v_AntiLepEta_CC;
 TH1D *AntiLepEta_CC[100];

 vector<TH1D *> v_LepEta_CF;
 TH1D *LepEta_CF[100];

 vector<TH1D *> v_AntiLepEta_CF;
 TH1D *AntiLepEta_CF[100];

 vector<TH1D *> v_LepEta_LHCb;
 TH1D *LepEta_LHCb[100];

 vector<TH1D *> v_AntiLepEta_LHCb;
 TH1D *AntiLepEta_LHCb[100];

 //Forward and backward rapidity
 vector<TH1D *> v_FZRapidity;
 TH1D *FZRapidity[100];

 vector<TH1D *> v_BZRapidity;
 TH1D *BZRapidity[100];

 vector<TH1D *> v_FZRapidity_CC;
 TH1D *FZRapidity_CC[100];

 vector<TH1D *> v_BZRapidity_CC;
 TH1D *BZRapidity_CC[100];

 vector<TH1D *> v_FZRapidity_CF;
 TH1D *FZRapidity_CF[100];

 vector<TH1D *> v_BZRapidity_CF;
 TH1D *BZRapidity_CF[100];

 vector<TH1D *> v_FZRapidity_LHCb;
 TH1D *FZRapidity_LHCb[100];

 vector<TH1D *> v_BZRapidity_LHCb;
 TH1D *BZRapidity_LHCb[100];

 //Average Forward and backward vs Mass ZY QT
 vector<TH2D *> v_FAveZMass_ZY_QT;
 TH2D *FAveZMass_ZY_QT[100];

 vector<TH2D *> v_BAveZMass_ZY_QT;
 TH2D *BAveZMass_ZY_QT[100];

 //Forward and backward vs Mass ZY QT
 vector<TH3D *> v_FZMass_ZY_QT;
 TH3D *FZMass_ZY_QT[100];

 vector<TH3D *> v_BZMass_ZY_QT;
 TH3D *BZMass_ZY_QT[100];

 vector<TH3D *> v_CMS_FZMass_ZY_QT;
 TH3D *CMS_FZMass_ZY_QT[100];

 vector<TH3D *> v_CMS_BZMass_ZY_QT;
 TH3D *CMS_BZMass_ZY_QT[100];

 vector<TH1D *> v_AverageZPt_ZY_numer;
 TH1D *AverageZPt_ZY_numer[100];
 vector<TH1D *> v_AverageZPt_ZY_denom;
 TH1D *AverageZPt_ZY_denom[100];
 TH1D *AverageZPt_ZY;

 vector<TH1D *> v_AverageZPt_lnQ_numer;
 TH1D *AverageZPt_lnQ_numer[100];
 vector<TH1D *> v_AverageZPt_lnQ_denom;
 TH1D *AverageZPt_lnQ_denom[100];
 TH1D *AverageZPt_lnQ;

 vector<TH1D *> v_AverageLepE_ZY_numer;
 TH1D *AverageLepE_ZY_numer[100];
 vector<TH1D *> v_AverageLepE_ZY_denom;
 TH1D *AverageLepE_ZY_denom[100];
 TH1D *AverageLepE_ZY;

 vector<TH1D *> v_AverageAntiLepE_ZY_numer;
 TH1D *AverageAntiLepE_ZY_numer[100];
 vector<TH1D *> v_AverageAntiLepE_ZY_denom;
 TH1D *AverageAntiLepE_ZY_denom[100];
 TH1D *AverageAntiLepE_ZY;

 TH1D *RatioLepE_ZY;

 //Forward and backward ZMass ZPt 2D plot
 vector<TH2D *> v_FZmass_ZPt_uu;
 TH2D *FZmass_ZPt_uu[100];

 vector<TH2D *> v_BZmass_ZPt_uu;
 TH2D *BZmass_ZPt_uu[100];

 vector<TH2D *> v_FZmass_ZPt_dd;
 TH2D *FZmass_ZPt_dd[100];

 vector<TH2D *> v_BZmass_ZPt_dd;
 TH2D *BZmass_ZPt_dd[100];


 ////////////////////////
 //  quark level plot  //
 ////////////////////////


 //Forward and backward ZMass
 vector<TH1D *> v_FZmass_CC_uu;
 TH1D *FZmass_CC_uu[100];

 vector<TH1D *> v_BZmass_CC_uu;
 TH1D *BZmass_CC_uu[100];

 vector<TH1D *> v_FZmass_CF_uu;
 TH1D *FZmass_CF_uu[100];

 vector<TH1D *> v_BZmass_CF_uu;
 TH1D *BZmass_CF_uu[100];

 vector<TH1D *> v_FZmass_CC_dd;
 TH1D *FZmass_CC_dd[100];

 vector<TH1D *> v_BZmass_CC_dd;
 TH1D *BZmass_CC_dd[100];

 vector<TH1D *> v_FZmass_CF_dd;
 TH1D *FZmass_CF_dd[100];

 vector<TH1D *> v_BZmass_CF_dd;
 TH1D *BZmass_CF_dd[100];

 vector<TH1D *> v_FZmass_CC_gg;
 TH1D *FZmass_CC_gg[100];

 vector<TH1D *> v_BZmass_CC_gg;
 TH1D *BZmass_CC_gg[100];

 vector<TH1D *> v_FZmass_CF_gg;
 TH1D *FZmass_CF_gg[100];

 vector<TH1D *> v_BZmass_CF_gg;
 TH1D *BZmass_CF_gg[100];

 vector<TH1D *> v_FZmass_uu;
 TH1D *FZmass_uu[100];

 vector<TH1D *> v_BZmass_uu;
 TH1D *BZmass_uu[100];

 vector<TH1D *> v_FZmass_dd;
 TH1D *FZmass_dd[100];

 vector<TH1D *> v_BZmass_dd;
 TH1D *BZmass_dd[100];

 vector<TH1D *> v_FZmass_gg;
 TH1D *FZmass_gg[100];

 vector<TH1D *> v_BZmass_gg;
 TH1D *BZmass_gg[100];

 vector<TH1D *> v_ZMass_CC_uu;
 TH1D *ZMass_CC_uu[100];

 vector<TH1D *> v_ZMass_CC_dd;
 TH1D *ZMass_CC_dd[100];

 vector<TH1D *> v_ZMass_CC_gg;
 TH1D *ZMass_CC_gg[100];

 vector<TH1D *> v_ZMass_CC_ss;
 TH1D *ZMass_CC_ss[100];

 vector<TH1D *> v_ZMass_CC_cc;
 TH1D *ZMass_CC_cc[100];

 vector<TH1D *> v_ZMass_CC_bb;
 TH1D *ZMass_CC_bb[100];

 vector<TH1D *> v_ZMass_CC_utype;
 TH1D *ZMass_CC_utype[100];

 vector<TH1D *> v_ZMass_CC_dtype;
 TH1D *ZMass_CC_dtype[100];

 vector<TH1D *> v_ZMass_CF_uu;
 TH1D *ZMass_CF_uu[100];

 vector<TH1D *> v_ZMass_CF_dd;
 TH1D *ZMass_CF_dd[100];

 vector<TH1D *> v_ZMass_CF_gg;
 TH1D *ZMass_CF_gg[100];

 vector<TH1D *> v_ZMass_CF_ss;
 TH1D *ZMass_CF_ss[100];

 vector<TH1D *> v_ZMass_CF_cc;
 TH1D *ZMass_CF_cc[100];

 vector<TH1D *> v_ZMass_CF_bb;
 TH1D *ZMass_CF_bb[100];

 vector<TH1D *> v_ZMass_CF_utype;
 TH1D *ZMass_CF_utype[100];

 vector<TH1D *> v_ZMass_CF_dtype;
 TH1D *ZMass_CF_dtype[100];

 vector<TH1D *> v_ZMass_uu;
 TH1D *ZMass_uu[100];

 vector<TH1D *> v_ZMass_dd;
 TH1D *ZMass_dd[100];

 vector<TH1D *> v_ZMass_gg;
 TH1D *ZMass_gg[100];

 vector<TH1D *> v_ZMass_ss;
 TH1D *ZMass_ss[100];

 vector<TH1D *> v_ZMass_cc;
 TH1D *ZMass_cc[100];

 vector<TH1D *> v_ZMass_bb;
 TH1D *ZMass_bb[100];

 vector<TH1D *> v_ZMass_utype;
 TH1D *ZMass_utype[100];

 vector<TH1D *> v_ZMass_dtype;
 TH1D *ZMass_dtype[100];

 //AFB vs ZPt
 vector<TH1D *> v_FZPt_uu;
 TH1D *FZPt_uu[100];

 vector<TH1D *> v_BZPt_uu;
 TH1D *BZPt_uu[100];

 vector<TH1D *> v_FZPt_dd;
 TH1D *FZPt_dd[100];

 vector<TH1D *> v_BZPt_dd;
 TH1D *BZPt_dd[100];

 //quark level ZPt ZY and ZMass
 vector<TH1D *> v_ZPt_uub;
 TH1D *ZPt_uub[100];

 vector<TH1D *> v_ZRapidity_uub;
 TH1D *ZRapidity_uub[100];

 vector<TH1D *> v_ZMass_uub;
 TH1D *ZMass_uub[100];

 vector<TH1D *> v_ZPt_ddb;
 TH1D *ZPt_ddb[100];

 vector<TH1D *> v_ZRapidity_ddb;
 TH1D *ZRapidity_ddb[100];

 vector<TH1D *> v_ZMass_ddb;
 TH1D *ZMass_ddb[100];

 vector<TH1D *> v_ZPt_ssb;
 TH1D *ZPt_ssb[100];

 vector<TH1D *> v_ZRapidity_ssb;
 TH1D *ZRapidity_ssb[100];

 vector<TH1D *> v_ZMass_ssb;
 TH1D *ZMass_ssb[100];

 //ZMass ZY QT 3D plot
 vector<TH3D *> v_ZMass_ZY_QT;
 TH3D *ZMass_ZY_QT[100];

 vector<TH3D *> v_FZMass_ZY_QT_uu;
 TH3D *FZMass_ZY_QT_uu[100];

 vector<TH3D *> v_BZMass_ZY_QT_uu;
 TH3D *BZMass_ZY_QT_uu[100];

 vector<TH3D *> v_FZMass_ZY_QT_dd;
 TH3D *FZMass_ZY_QT_dd[100];
 
 vector<TH3D *> v_BZMass_ZY_QT_dd;
 TH3D *BZMass_ZY_QT_dd[100];

 vector<TH3D *> v_ZMass_ZY_QT_uu;
 TH3D *ZMass_ZY_QT_uu[100];

 vector<TH3D *> v_ZMass_ZY_QT_dd;
 TH3D *ZMass_ZY_QT_dd[100];

 vector<TH3D *> v_ZMass_ZY_QT_ss;
 TH3D *ZMass_ZY_QT_ss[100];

 vector<TH3D *> v_ZMass_ZY_QT_cc;
 TH3D *ZMass_ZY_QT_cc[100];

 vector<TH3D *> v_ZMass_ZY_QT_bb;
 TH3D *ZMass_ZY_QT_bb[100];

 vector<TH3D *> v_CMS_ZMass_ZY_QT;
 TH3D *CMS_ZMass_ZY_QT[100];

 vector<TH3D *> v_CMS_FZMass_ZY_QT_uu;
 TH3D *CMS_FZMass_ZY_QT_uu[100];

 vector<TH3D *> v_CMS_BZMass_ZY_QT_uu;
 TH3D *CMS_BZMass_ZY_QT_uu[100];

 vector<TH3D *> v_CMS_FZMass_ZY_QT_dd;
 TH3D *CMS_FZMass_ZY_QT_dd[100];

 vector<TH3D *> v_CMS_BZMass_ZY_QT_dd;
 TH3D *CMS_BZMass_ZY_QT_dd[100];

 /////////////////////
 //  dilution plot  //
 /////////////////////

 //ZMass dilution
 vector<TH1D *> v_ZMass_CC_total;
 TH1D *ZMass_CC_total[100];
 vector<TH1D *> v_ZMass_CC_wrong;
 TH1D *ZMass_CC_wrong[100];
 TH1D *Dilution_ZMass_CC;

 vector<TH1D *> v_ZMass_CF_total;
 TH1D *ZMass_CF_total[100];
 vector<TH1D *> v_ZMass_CF_wrong;
 TH1D *ZMass_CF_wrong[100];
 TH1D *Dilution_ZMass_CF;

 vector<TH1D *> v_ZMass_Full_total;
 TH1D *ZMass_Full_total[100];
 vector<TH1D *> v_ZMass_Full_wrong;
 TH1D *ZMass_Full_wrong[100];
 TH1D *Dilution_ZMass_Full;

 //quark ZMass dilution
 vector<TH1D *> v_ZMass_CC_uu_total;
 TH1D *ZMass_CC_uu_total[100];
 vector<TH1D *> v_ZMass_CC_uu_wrong;
 TH1D *ZMass_CC_uu_wrong[100];
 TH1D *Dilution_ZMass_CC_uu;

 vector<TH1D *> v_ZMass_CC_dd_total;
 TH1D *ZMass_CC_dd_total[100];
 vector<TH1D *> v_ZMass_CC_dd_wrong;
 TH1D *ZMass_CC_dd_wrong[100];
 TH1D *Dilution_ZMass_CC_dd;

 vector<TH1D *> v_ZMass_CF_uu_total;
 TH1D *ZMass_CF_uu_total[100];
 vector<TH1D *> v_ZMass_CF_uu_wrong;
 TH1D *ZMass_CF_uu_wrong[100];
 TH1D *Dilution_ZMass_CF_uu;

 vector<TH1D *> v_ZMass_CF_dd_total;
 TH1D *ZMass_CF_dd_total[100];
 vector<TH1D *> v_ZMass_CF_dd_wrong;
 TH1D *ZMass_CF_dd_wrong[100];
 TH1D *Dilution_ZMass_CF_dd;

 vector<TH1D *> v_ZMass_Full_uu_total;
 TH1D *ZMass_Full_uu_total[100];
 vector<TH1D *> v_ZMass_Full_uu_wrong;
 TH1D *ZMass_Full_uu_wrong[100];
 TH1D *Dilution_ZMass_Full_uu;

 vector<TH1D *> v_ZMass_Full_dd_total;
 TH1D *ZMass_Full_dd_total[100];
 vector<TH1D *> v_ZMass_Full_dd_wrong;
 TH1D *ZMass_Full_dd_wrong[100];
 TH1D *Dilution_ZMass_Full_dd;

 vector<TH1D *> v_ZMass_Full_ss_total;
 TH1D *ZMass_Full_ss_total[100];
 vector<TH1D *> v_ZMass_Full_ss_wrong;
 TH1D *ZMass_Full_ss_wrong[100];
 TH1D *Dilution_ZMass_Full_ss;

 vector<TH1D *> v_ZMass_Full_cc_total;
 TH1D *ZMass_Full_cc_total[100];
 vector<TH1D *> v_ZMass_Full_cc_wrong;
 TH1D *ZMass_Full_cc_wrong[100];
 TH1D *Dilution_ZMass_Full_cc;

 vector<TH1D *> v_ZMass_Full_bb_total;
 TH1D *ZMass_Full_bb_total[100];
 vector<TH1D *> v_ZMass_Full_bb_wrong;
 TH1D *ZMass_Full_bb_wrong[100];
 TH1D *Dilution_ZMass_Full_bb;

 //Dilution vs ZPt ZY CosThetaQ
 vector<TH1D *> v_ZPt_total;
 TH1D *ZPt_total[100];
 vector<TH1D *> v_ZPt_wrong;
 TH1D *ZPt_wrong[100];
 TH1D *Dilution_ZPt;

 vector<TH1D *> v_ZRapidity_total;
 TH1D *ZRapidity_total[100];
 vector<TH1D *> v_ZRapidity_wrong;
 TH1D *ZRapidity_wrong[100];
 TH1D *Dilution_ZRapidity;

 vector<TH3D *> v_ZMass_ZY_QT_uu_wrong;
 TH3D *ZMass_ZY_QT_uu_wrong[100];
 vector<TH3D *> v_ZMass_ZY_QT_uu_total;
 TH3D *ZMass_ZY_QT_uu_total[100];
 TH3D *Dilution_ZMass_ZY_QT_uu;

 vector<TH3D *> v_ZMass_ZY_QT_dd_wrong;
 TH3D *ZMass_ZY_QT_dd_wrong[100];
 vector<TH3D *> v_ZMass_ZY_QT_dd_total;
 TH3D *ZMass_ZY_QT_dd_total[100];
 TH3D *Dilution_ZMass_ZY_QT_dd;

 vector<TH3D *> v_CMS_ZMass_ZY_QT_uu_wrong;
 TH3D *CMS_ZMass_ZY_QT_uu_wrong[100];
 vector<TH3D *> v_CMS_ZMass_ZY_QT_uu_total;
 TH3D *CMS_ZMass_ZY_QT_uu_total[100];
 TH3D *Dilution_CMS_ZMass_ZY_QT_uu;

 vector<TH3D *> v_CMS_ZMass_ZY_QT_dd_wrong;
 TH3D *CMS_ZMass_ZY_QT_dd_wrong[100];
 vector<TH3D *> v_CMS_ZMass_ZY_QT_dd_total;
 TH3D *CMS_ZMass_ZY_QT_dd_total[100];
 TH3D *Dilution_CMS_ZMass_ZY_QT_dd;

 vector<TH3D *> v_XSection_M_Y_CosTheta;
 TH3D *XSection_M_Y_CosTheta[100];

 vector<TH1D *> v_ZPt_y1_Total;
 TH1D *ZPt_y1_Total[100];

 vector<TH1D *> v_ZPt_y1_Fiduc;
 TH1D *ZPt_y1_Fiduc[100];

 vector<TH1D *> v_ZPt_y2_Total;
 TH1D *ZPt_y2_Total[100];

 vector<TH1D *> v_ZPt_y2_Fiduc;
 TH1D *ZPt_y2_Fiduc[100];

 vector<TH1D *> v_ZPt_y3_Total;
 TH1D *ZPt_y3_Total[100];

 vector<TH1D *> v_ZPt_y3_Fiduc;
 TH1D *ZPt_y3_Fiduc[100];

 vector<TH1D *> v_ZPt_y4_Total;
 TH1D *ZPt_y4_Total[100];

 vector<TH1D *> v_ZPt_y4_Fiduc;
 TH1D *ZPt_y4_Fiduc[100];

 vector<TH1D *> v_ZPt_y5_Total;
 TH1D *ZPt_y5_Total[100];

 vector<TH1D *> v_ZPt_y5_Fiduc;
 TH1D *ZPt_y5_Fiduc[100];

 vector<TH1D *> v_ZPt_y6_Total;
 TH1D *ZPt_y6_Total[100];

 vector<TH1D *> v_ZPt_y6_Fiduc;
 TH1D *ZPt_y6_Fiduc[100];

 vector<TH1D *> v_ATLASZPt_Fiduc;
 TH1D *ATLASZPt_Fiduc[100];

 vector<TH1D *> v_ATLASZPt_Total;
 TH1D *ATLASZPt_Total[100];

 vector<TH2D *> v_ATLASZPt_2D_Fiduc;
 TH2D *ATLASZPt_2D_Fiduc[100];

 vector<TH2D *> v_ATLASZPt_2D_Total;
 TH2D *ATLASZPt_2D_Total[100];

 vector<TH2D *> v_ATLASZPt_2D_Total_uu;
 TH2D *ATLASZPt_2D_Total_uu[100];

 vector<TH2D *> v_ATLASZPt_2D_Total_dd;
 TH2D *ATLASZPt_2D_Total_dd[100];

 vector<TH2D *> v_ATLASZPt_2D_Total_MassWindow;
 TH2D *ATLASZPt_2D_Total_MassWindow[100];

 vector<TH1D *> v_ZPt_Threshold;
 TH1D *ZPt_Threshold[100];

 vector<AngularFunction *> v_A0_ZPt;
 AngularFunction *A0_ZPt[100];

 vector<AngularFunction *> v_A1_ZPt;
 AngularFunction *A1_ZPt[100];

 vector<AngularFunction *> v_A2_ZPt;
 AngularFunction *A2_ZPt[100];

 vector<AngularFunction *> v_A3_ZPt;
 AngularFunction *A3_ZPt[100];

 vector<AngularFunction *> v_A4_ZPt;
 AngularFunction *A4_ZPt[100];

 vector<AngularFunction *> v_L0_ZPt;
 AngularFunction *L0_ZPt[100];

 vector<AngularFunction *> v_A0_ZPt_LHCb;
 AngularFunction *A0_ZPt_LHCb[100];

 vector<AngularFunction *> v_A1_ZPt_LHCb;
 AngularFunction *A1_ZPt_LHCb[100];

 vector<AngularFunction *> v_A2_ZPt_LHCb;
 AngularFunction *A2_ZPt_LHCb[100];

 vector<AngularFunction *> v_A3_ZPt_LHCb;
 AngularFunction *A3_ZPt_LHCb[100];

 vector<AngularFunction *> v_A4_ZPt_LHCb;
 AngularFunction *A4_ZPt_LHCb[100];

 vector<AngularFunction *> v_A02_ZPt_LHCb;
 AngularFunction *A02_ZPt_LHCb[100];

 vector<AngularFunction *> v_A0_ZY_LHCb;
 AngularFunction *A0_ZY_LHCb[100];
 
 vector<AngularFunction *> v_A1_ZY_LHCb;
 AngularFunction *A1_ZY_LHCb[100];
 
 vector<AngularFunction *> v_A2_ZY_LHCb;
 AngularFunction *A2_ZY_LHCb[100];
 
 vector<AngularFunction *> v_A3_ZY_LHCb;
 AngularFunction *A3_ZY_LHCb[100];

 vector<AngularFunction *> v_A4_ZY_LHCb;
 AngularFunction *A4_ZY_LHCb[100];

 vector<AngularFunction *> v_A02_ZY_LHCb;
 AngularFunction *A02_ZY_LHCb[100];

 vector<AngularFunction *> v_QuarkA4_ZPt_uu;
 AngularFunction *QuarkA4_ZPt_uu[100];

 vector<AngularFunction *> v_QuarkA4_ZY_uu;
 AngularFunction *QuarkA4_ZY_uu[100];

 vector<AngularFunction *> v_QuarkA4_ZPt_dd;
 AngularFunction *QuarkA4_ZPt_dd[100];

 vector<AngularFunction *> v_QuarkA4_ZY_dd;
 AngularFunction *QuarkA4_ZY_dd[100];

 vector<AngularFunction *> v_QuarkL0_ZPt_uu;
 AngularFunction *QuarkL0_ZPt_uu[100];

 vector<AngularFunction *> v_QuarkL0_ZY_uu;
 AngularFunction *QuarkL0_ZY_uu[100];

 vector<AngularFunction *> v_QuarkL0_ZPt_dd;
 AngularFunction *QuarkL0_ZPt_dd[100];

 vector<AngularFunction *> v_QuarkL0_ZY_dd;
 AngularFunction *QuarkL0_ZY_dd[100];

 vector<AngularFunction* > v_A0_Mass_ZY_QT_uu;
 AngularFunction *A0_Mass_ZY_QT_uu[100];

 vector<AngularFunction* > v_A0_Mass_ZY_QT_dd;
 AngularFunction *A0_Mass_ZY_QT_dd[100];

 vector<AngularFunction* > v_A0_Mass_ZY_QT_ss;
 AngularFunction *A0_Mass_ZY_QT_ss[100];

 vector<AngularFunction* > v_A0_Mass_ZY_QT_cc;
 AngularFunction *A0_Mass_ZY_QT_cc[100];

 vector<AngularFunction* > v_A0_Mass_ZY_QT_bb;
 AngularFunction *A0_Mass_ZY_QT_bb[100];


// For study energy scale //
 vector<CovHist *> v_FZmass_CC_Cov;
 CovHist *FZmass_CC_Cov[100];

 vector<CovHist *> v_BZmass_CC_Cov;
 CovHist *BZmass_CC_Cov[100];

 vector<CovHist *> v_FZmass_CF_Cov;
 CovHist *FZmass_CF_Cov[100];

 vector<CovHist *> v_BZmass_CF_Cov;
 CovHist *BZmass_CF_Cov[100];
/////////////////////////////////////////

 int ZY_bin = 5; double ZY_left = 0.0; double ZY_right = 5.0;
 int ZY_CC_bin = 3; double ZY_CC_left = 0.0; double ZY_CC_right = 2.5;
 int ZY_CF_bin = 3; double ZY_CF_left = 1.0; double ZY_CF_right = 4.0;
// double rangeZY[ZY_bin + 1];
// double rangeZYCC[ZY_CC_bin + 1];
// double rangeZYCF[ZY_CF_bin + 1];

 int ZPt_bin = 5; double ZPt_left = 0.0; double ZPt_right = 100.0;
 int ZPt_CC_bin = 5; double ZPt_CC_left = 0.0; double ZPt_CC_right = 100.0;
 int ZPt_CF_bin = 5; double ZPt_CF_left = 0.0; double ZPt_CF_right = 100.0;
// double RangeZPt[ZPt_bin + 1];
// double RangeZPtCC[ZPt_CC_bin + 1];
// double RangeZPtCF[ZPt_CF_bin + 1];


 TH1D *AFB_CC;
 TH1D *AFB_CF;
 TH1D *AFB_Full;
 TH1D *AFB_LHCb;

 TH2D *AFB_Mass_ZY_CC;
 TH2D *AFB_Mass_ZY_CF;

 TH1D *AFB_CC_80_100;
 TH1D *AFB_CF_80_100;
 TH1D *AFB_Full_80_100;
 TH1D *AFB_LHCb_80_100;

 TH2D *AveAFB_ZY_ZPt;
 TH3D *AFB_Mass_ZY_ZPt;
 TH3D *AFB_CMS_Mass_ZY_ZPt;


 TH1D *AFBSlope_CC;
 TH1D *AFBSlope_CF;
 TH1D *AFBSlope_Full;
 TH1D *AFBSlope_LHCb;

 TH1D *AFBSlope_82_98_CC;
 TH1D *AFBSlope_82_98_CF;
 TH1D *AFBSlope_82_98_Full;
 TH1D *AFBSlope_82_98_LHCb;

 TH1D *AFBSlope_84_96_CC;
 TH1D *AFBSlope_84_96_CF;
 TH1D *AFBSlope_84_96_Full;
 TH1D *AFBSlope_84_96_LHCb;

 TH1D *AFBOffset_CC;
 TH1D *AFBOffset_CF;
 TH1D *AFBOffset_Full;
 TH1D *AFBOffset_LHCb;

 TH1D *AFBOffset_82_98_CC;
 TH1D *AFBOffset_82_98_CF;
 TH1D *AFBOffset_82_98_Full;
 TH1D *AFBOffset_82_98_LHCb;

 TH1D *AFBOffset_84_96_CC;
 TH1D *AFBOffset_84_96_CF;
 TH1D *AFBOffset_84_96_Full;
 TH1D *AFBOffset_84_96_LHCb;

 TH1D *AFBQuarkSlope_CC;
 TH1D *AFBQuarkSlope_CF;
 TH1D *AFBQuarkSlope_Full;
 TH1D *AFBQuarkSlope_LHCb;

 TH1D *AFBQuarkSlope_82_98_CC;
 TH1D *AFBQuarkSlope_82_98_CF;
 TH1D *AFBQuarkSlope_82_98_Full;

 TH1D *AFBQuarkSlope_84_96_CC;
 TH1D *AFBQuarkSlope_84_96_CF;
 TH1D *AFBQuarkSlope_84_96_Full;

 TH1D *AFBQuarkOffset_CC;
 TH1D *AFBQuarkOffset_CF;
 TH1D *AFBQuarkOffset_Full;
 TH1D *AFBQuarkOffset_LHCb;

 TH1D *AFBQuarkOffset_82_98_CC;
 TH1D *AFBQuarkOffset_82_98_CF;
 TH1D *AFBQuarkOffset_82_98_Full;

 TH1D *AFBQuarkOffset_84_96_CC;
 TH1D *AFBQuarkOffset_84_96_CF;
 TH1D *AFBQuarkOffset_84_96_Full;

 TH1D *AFBDiff_CC;
 TH1D *AFBDiff_CF;
 TH1D *AFBDiff_Full;
 TH1D *AFBDiff_LHCb;

 TH1D *AFBDiff_CC_ZPole;
 TH1D *AFBDiff_CF_ZPole;
 TH1D *AFBDiff_Full_ZPole;
 TH1D *AFBDiff_LHCb_ZPole;

 TH1D *AFBDiff_CC_Side;
 TH1D *AFBDiff_CF_Side;
 TH1D *AFBDiff_Full_Side;
 TH1D *AFBDiff_LHCb_Side;

 TH1D *AFBGradient_CC;
 TH1D *AFBGradient_CF;
 TH1D *AFBGradient_Full;
 TH1D *AFBGradient_LHCb;

 TH1D *AFBQuarkGradient_CC;
 TH1D *AFBQuarkGradient_CF;
 TH1D *AFBQuarkGradient_Full;

 TH1D *DilutionGradient_CC;
 TH1D *DilutionGradient_CF;
 TH1D *DilutionGradient_Full;

 TH1D *AFBQuarkDiff_CC;
 TH1D *AFBQuarkDiff_CF;
 TH1D *AFBQuarkDiff_Full;

 TH1D *AFBQuarkDiff_CC_Side;
 TH1D *AFBQuarkDiff_CF_Side;
 TH1D *AFBQuarkDiff_Full_Side;

 TH1D *AFBQuarkDiff_CC_ZPole;
 TH1D *AFBQuarkDiff_CF_ZPole;
 TH1D *AFBQuarkDiff_Full_ZPole;

 TH1D *AFBQuarkFraction_CC;
 TH1D *AFBQuarkFraction_CF;
 TH1D *AFBQuarkFraction_Full;

 TH1D *AFBS0Fraction_CC;
 TH1D *AFBS0Fraction_CF;
 TH1D *AFBS0Fraction_Full;

 TH1D *AFB_CC_uu;
 TH1D *AFB_CF_uu;
 TH1D *AFB_Full_uu;
 TH1D *AFB_LHCb_uu;

 TH1D *AFB_CC_dd;
 TH1D *AFB_CF_dd;
 TH1D *AFB_Full_dd;
 TH1D *AFB_LHCb_dd;

 TH1D *AFB_CC_qq;
 TH1D *AFB_CF_qq;
 TH1D *AFB_Full_qq;
 TH1D *AFB_LHCb_qq;

 TH1D *AFB_ZPt_uu;
 TH1D *AFB_ZPt_dd;

 TH2D *AFB_Mass_ZPt_Full_uu;
 TH2D *AFB_Mass_ZPt_Full_dd;

 TH3D *AFB_Mass_ZY_ZPt_uu;
 TH3D *AFB_Mass_ZY_ZPt_dd;

 TH3D *AFB_CMS_Mass_ZY_ZPt_uu;
 TH3D *AFB_CMS_Mass_ZY_ZPt_dd;

 TH1D *XsecFraction_CC_uu;
 TH1D *XsecFraction_CC_dd;
 TH1D *XsecFraction_CF_uu;
 TH1D *XsecFraction_CF_dd;
 TH1D *XsecFraction_Full_uu;
 TH1D *XsecFraction_Full_dd;

 TH1D *CoefficientDilution_CC_uu;
 TH1D *CoefficientDilution_CC_dd;
 TH1D *CoefficientDilution_CF_uu;
 TH1D *CoefficientDilution_CF_dd;
 TH1D *CoefficientDilution_Full_uu;
 TH1D *CoefficientDilution_Full_dd;

 TH1D *ResidualDilution_CC_uu;
 TH1D *ResidualDilution_CC_dd;
 TH1D *ResidualDilution_CF_uu;
 TH1D *ResidualDilution_CF_dd;
 TH1D *ResidualDilution_Full_uu;
 TH1D *ResidualDilution_Full_dd;

 TH1D *DilutionAverage_CC_uu;
 TH1D *DilutionAverage_CC_dd;
 TH1D *DilutionAverage_CF_uu;
 TH1D *DilutionAverage_CF_dd;
 TH1D *DilutionAverage_Full_uu;
 TH1D *DilutionAverage_Full_dd;

 TH3D *CoefficientDilution_ZMass_ZY_QT_uu;
 TH3D *CoefficientDilution_ZMass_ZY_QT_dd;
 TH3D *ResidualDilution_ZMass_ZY_QT_uu;
 TH3D *ResidualDilution_ZMass_ZY_QT_dd;
 TH3D *DilutionAverage_ZMass_ZY_QT_uu;
 TH3D *DilutionAverage_ZMass_ZY_QT_dd;

 TH3D *CoefficientDilution_CMS_ZMass_ZY_QT_uu;
 TH3D *CoefficientDilution_CMS_ZMass_ZY_QT_dd;
 TH3D *ResidualDilution_CMS_ZMass_ZY_QT_uu;
 TH3D *ResidualDilution_CMS_ZMass_ZY_QT_dd;
 TH3D *DilutionAverage_CMS_ZMass_ZY_QT_uu;
 TH3D *DilutionAverage_CMS_ZMass_ZY_QT_dd;

 TH1D *AveAFB_CC;
 TH1D *AveAFB_CF;
 TH1D *AveAFB_Full;
 TH1D *AveAFB_LHCb;

 TH1D *QuarkAveAFB_CC;
 TH1D *QuarkAveAFB_CF;
 TH1D *QuarkAveAFB_Full;

 TH1D *LepAsym;
 TH1D *LepAsym_CC;
 TH1D *LepAsym_CF;
 TH1D *LepAsym_LHCb;

 TH1D *AFB_ZY;
 TH1D *AFB_ZY_CC;
 TH1D *AFB_ZY_CF;
 TH1D *AFB_ZY_LHCb;

 TH1D *LepWidthDiff;
 TH1D *LepWidthDiff_CC;
 TH1D *LepWidthDiff_CF;
 TH1D *LepWidthDiff_LHCb;

 TH1D *CutEff_ZPt_y1;
 TH1D *CutEff_ZPt_y2;
 TH1D *CutEff_ZPt_y3;
 TH1D *CutEff_ZPt_y4;
 TH1D *CutEff_ZPt_y5;
 TH1D *CutEff_ZPt_y6;
 TH2D *ATLASZPt_2D_Fiduc_Final;
 TH2D *ATLASZPt_2D_Total_Final;
 TH2D *ATLASZPt_2D_Total_uu_Final;
 TH2D *ATLASZPt_2D_Total_dd_Final;
 TH2D *ATLASZPt_2D_Total_MassWindow_Final;

 TH1D *QuarkA4Ratio_ZPt_uu;
 TH1D *QuarkA4Ratio_ZY_uu;
 TH1D *QuarkA4Ratio_ZPt_dd;
 TH1D *QuarkA4Ratio_ZY_dd;

 virtual void bookHists(int TotalThread);
 virtual void bookCovHists(int TotalThread);
 virtual void outputInformation();

// template<class T>

 TFile *DataFile;
 TH1D *DataHist;

 virtual void InputData(TString FileName, TString HistName);


};
#endif
