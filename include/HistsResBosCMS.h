#ifndef __HistsResBosCMS_H_
#define __HistsResBosCMS_H_
#include <iostream>
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TFile.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TMath.h"
#include "TTree.h"
#include <vector>
#include "makeHists.h"

#include "ReadInRoot/ResBosZPt.h"

class ResBosZPt;

using namespace std;

class HistsResBosCMS : public makeHists
{
 public:

 vector<TH1D *> v_ZY_x1;
 TH1D *ZY_x1[100];

 vector<TH1D *> v_ZY_x2;
 TH1D *ZY_x2[100];

 vector<TH1D *> v_ZY_x3;
 TH1D *ZY_x3[100];

 //Forward and backward vs Mass ZY QT
 vector<TH3D *> v_FZMass_ZY_QT;
 TH3D *FZMass_ZY_QT[100];

 vector<TH3D *> v_BZMass_ZY_QT;
 TH3D *BZMass_ZY_QT[100];

 //ZMass ZY QT 3D plot
 vector<TH3D *> v_ZMass_ZY_QT;
 TH3D *ZMass_ZY_QT[100];

 vector<TH3D *> v_FZMass_ZY_QT_uu;
 TH3D *FZMass_ZY_QT_uu[100];

 vector<TH3D *> v_BZMass_ZY_QT_uu;
 TH3D *BZMass_ZY_QT_uu[100];

 vector<TH3D *> v_FZMass_ZY_QT_dd;
 TH3D *FZMass_ZY_QT_dd[100];

 vector<TH3D *> v_BZMass_ZY_QT_dd;
 TH3D *BZMass_ZY_QT_dd[100];

 vector<TH3D *> v_ZMass_ZY_QT_uu;
 TH3D *ZMass_ZY_QT_uu[100];

 vector<TH3D *> v_ZMass_ZY_QT_dd;
 TH3D *ZMass_ZY_QT_dd[100];

 vector<TH3D *> v_ZMass_ZY_QT_ss;
 TH3D *ZMass_ZY_QT_ss[100];

 vector<TH3D *> v_ZMass_ZY_QT_cc;
 TH3D *ZMass_ZY_QT_cc[100];

 vector<TH3D *> v_ZMass_ZY_QT_bb;
 TH3D *ZMass_ZY_QT_bb[100];

 /////////////////////
 //  dilution plot  //
 /////////////////////

 vector<TH3D *> v_ZMass_ZY_QT_uu_wrong;
 TH3D *ZMass_ZY_QT_uu_wrong[100];
 vector<TH3D *> v_ZMass_ZY_QT_uu_total;
 TH3D *ZMass_ZY_QT_uu_total[100];
 TH3D *Dilution_ZMass_ZY_QT_uu;

 vector<TH3D *> v_ZMass_ZY_QT_dd_wrong;
 TH3D *ZMass_ZY_QT_dd_wrong[100];
 vector<TH3D *> v_ZMass_ZY_QT_dd_total;
 TH3D *ZMass_ZY_QT_dd_total[100];
 TH3D *Dilution_ZMass_ZY_QT_dd;

 vector<TH3D *> v_ZMass_ZY_QT_uu_YP_wrong;
 TH3D *ZMass_ZY_QT_uu_YP_wrong[100];
 vector<TH3D *> v_ZMass_ZY_QT_uu_YP_right;
 TH3D *ZMass_ZY_QT_uu_YP_right[100];
 vector<TH3D *> v_ZMass_ZY_QT_uu_YM_wrong;
 TH3D *ZMass_ZY_QT_uu_YM_wrong[100];
 vector<TH3D *> v_ZMass_ZY_QT_uu_YM_right;
 TH3D *ZMass_ZY_QT_uu_YM_right[100];

 vector<TH3D *> v_ZMass_ZY_QT_dd_YP_wrong;
 TH3D *ZMass_ZY_QT_dd_YP_wrong[100];
 vector<TH3D *> v_ZMass_ZY_QT_dd_YP_right;
 TH3D *ZMass_ZY_QT_dd_YP_right[100];
 vector<TH3D *> v_ZMass_ZY_QT_dd_YM_wrong;
 TH3D *ZMass_ZY_QT_dd_YM_wrong[100];
 vector<TH3D *> v_ZMass_ZY_QT_dd_YM_right;
 TH3D *ZMass_ZY_QT_dd_YM_right[100];

 vector<TH3D *> v_ZMass_ZY_QT_YP;
 TH3D *ZMass_ZY_QT_YP[100];
 vector<TH3D *> v_ZMass_ZY_QT_YM;
 TH3D *ZMass_ZY_QT_YM[100];

 vector<AngularFunction* > v_A0_Mass_ZY_QT_uu;
 AngularFunction *A0_Mass_ZY_QT_uu[100];

 vector<AngularFunction* > v_A0_Mass_ZY_QT_dd;
 AngularFunction *A0_Mass_ZY_QT_dd[100];

 vector<AngularFunction* > v_A0_Mass_ZY_QT_ss;
 AngularFunction *A0_Mass_ZY_QT_ss[100];

 vector<AngularFunction* > v_A0_Mass_ZY_QT_cc;
 AngularFunction *A0_Mass_ZY_QT_cc[100];

 vector<AngularFunction* > v_A0_Mass_ZY_QT_bb;
 AngularFunction *A0_Mass_ZY_QT_bb[100];

 /////////////////////////
 //  Theory prediction  //
 /////////////////////////

 vector<TH1D *> v_ATLAS8TeV_pT_Mass12_20_Theory;
 TH1D* ATLAS8TeV_pT_Mass12_20_Theory[100];

 vector<TH1D *> v_ATLAS8TeV_pT_Mass20_30_Theory;
 TH1D* ATLAS8TeV_pT_Mass20_30_Theory[100];

 vector<TH1D *> v_ATLAS8TeV_pT_Mass30_46_Theory;
 TH1D* ATLAS8TeV_pT_Mass30_46_Theory[100];

 vector<TH1D *> v_ATLAS8TeV_pT_Mass46_66_Theory;
 TH1D* ATLAS8TeV_pT_Mass46_66_Theory[100];

 vector<TH1D *> v_ATLAS8TeV_pT_Mass66_116_Theory;
 TH1D* ATLAS8TeV_pT_Mass66_116_Theory[100];

 vector<TH1D *> v_ATLAS8TeV_pT_Mass116_150_Theory;
 TH1D* ATLAS8TeV_pT_Mass116_150_Theory[100];

 vector<TH1D *> v_ATLAS8TeV_Phi_Y1_Theory;
 TH1D* ATLAS8TeV_Phi_Y1_Theory[100];

 vector<TH1D *> v_ATLAS8TeV_Phi_Y2_Theory;
 TH1D* ATLAS8TeV_Phi_Y2_Theory[100];

 vector<TH1D *> v_ATLAS8TeV_Phi_Y3_Theory;
 TH1D* ATLAS8TeV_Phi_Y3_Theory[100];

 vector<TH1D *> v_ATLAS8TeV_Phi_Y4_Theory;
 TH1D* ATLAS8TeV_Phi_Y4_Theory[100];

 vector<TH1D *> v_ATLAS8TeV_Phi_Y5_Theory;
 TH1D* ATLAS8TeV_Phi_Y5_Theory[100];

 vector<TH1D *> v_ATLAS8TeV_Phi_Y6_Theory;
 TH1D* ATLAS8TeV_Phi_Y6_Theory[100];

 vector<TH1D *> v_ATLAS13TeV_pT_Theory;
 TH1D* ATLAS13TeV_pT_Theory[100];

 vector<TH1D *> v_ATLAS13TeV_Phi_Theory;
 TH1D* ATLAS13TeV_Phi_Theory[100];

 vector<TH1D *> v_CMS13TeV_pT_Theory;
 TH1D* CMS13TeV_pT_Theory[100];

 vector<TH1D *> v_CMS13TeV_Phi_Theory;
 TH1D* CMS13TeV_Phi_Theory[100];

 vector<TH1D *> v_LHCb13TeV_pT_Theory;
 TH1D* LHCb13TeV_pT_Theory[100];

 vector<TH1D *> v_LHCb13TeV_Phi_Theory;
 TH1D* LHCb13TeV_Phi_Theory[100];

 TH3D *AFB_Mass_ZY_ZPt;

 TH3D *AFB_Mass_ZY_ZPt_uu;
 TH3D *AFB_Mass_ZY_ZPt_dd;

 TH3D *CoefficientDilution_ZMass_ZY_QT_uu;
 TH3D *CoefficientDilution_ZMass_ZY_QT_dd;
 TH3D *ResidualDilution_ZMass_ZY_QT_uu;
 TH3D *ResidualDilution_ZMass_ZY_QT_dd;
 TH3D *DilutionAverage_ZMass_ZY_QT_uu;
 TH3D *DilutionAverage_ZMass_ZY_QT_dd;

 TH3D *OneMinusTwoD_uu;
 TH3D *OneMinusTwoD_dd;
 TH3D *RelativeCrossSection_uu;
 TH3D *RelativeCrossSection_dd;



 //////////////////////
 //  Published Data  //
 //////////////////////

 TH1D* ATLAS8TeV_pT_Mass12_20_Data;
 TH1D* ATLAS8TeV_pT_Mass20_30_Data;
 TH1D* ATLAS8TeV_pT_Mass30_46_Data;
 TH1D* ATLAS8TeV_pT_Mass46_66_Data;
 TH1D* ATLAS8TeV_pT_Mass66_116_Data;
 TH1D* ATLAS8TeV_pT_Mass116_150_Data;
 TH1D* ATLAS8TeV_Phi_Y1_Data;
 TH1D* ATLAS8TeV_Phi_Y2_Data;
 TH1D* ATLAS8TeV_Phi_Y3_Data;
 TH1D* ATLAS8TeV_Phi_Y4_Data;
 TH1D* ATLAS8TeV_Phi_Y5_Data;
 TH1D* ATLAS8TeV_Phi_Y6_Data;
 TH1D* ATLAS13TeV_pT_Data;
 TH1D* ATLAS13TeV_Phi_Data;
 TH1D* CMS13TeV_pT_Data;
 TH1D* CMS13TeV_Phi_Data;
 TH1D* LHCb13TeV_pT_Data;
 TH1D* LHCb13TeV_Phi_Data;

 TH1D* ATLAS8TeV_pT_Mass12_20_Theory_Final;
 TH1D* ATLAS8TeV_pT_Mass20_30_Theory_Final;
 TH1D* ATLAS8TeV_pT_Mass30_46_Theory_Final;
 TH1D* ATLAS8TeV_pT_Mass46_66_Theory_Final;
 TH1D* ATLAS8TeV_pT_Mass66_116_Theory_Final;
 TH1D* ATLAS8TeV_pT_Mass116_150_Theory_Final;
 TH1D* ATLAS8TeV_Phi_Y1_Theory_Final;
 TH1D* ATLAS8TeV_Phi_Y2_Theory_Final;
 TH1D* ATLAS8TeV_Phi_Y3_Theory_Final;
 TH1D* ATLAS8TeV_Phi_Y4_Theory_Final;
 TH1D* ATLAS8TeV_Phi_Y5_Theory_Final;
 TH1D* ATLAS8TeV_Phi_Y6_Theory_Final;
 TH1D* ATLAS13TeV_pT_Theory_Final;
 TH1D* ATLAS13TeV_Phi_Theory_Final;
 TH1D* CMS13TeV_pT_Theory_Final;
 TH1D* CMS13TeV_Phi_Theory_Final;
 TH1D* LHCb13TeV_pT_Theory_Final;
 TH1D* LHCb13TeV_Phi_Theory_Final;

 vector<TString> DataList;

//member function
 vector<TString> sysName;
 HistsResBosCMS(){};
 HistsResBosCMS(TString RootType){this->RootType = RootType;};
 virtual void InitialSysName();
 virtual void bookHists(int TotalThread);
 virtual void outputInformation();
 virtual void Save();
 virtual void Reset();
 virtual void InputData(vector<TString> DataList){this->DataList = DataList;};
 virtual void ReadData();

// template<class T>

};
#endif
