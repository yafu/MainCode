#ifndef __HistsResBosD0_H_
#define __HistsResBosD0_H_
#include <iostream>
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TFile.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TMath.h"
#include "TTree.h"
#include <vector>
#include "makeHists.h"

using namespace std;

class HistsResBosD0 : public makeHists
{
 public:

 //Forward and backward vs Mass ZY QT
 vector<TH3D *> v_FZMass_ZY_QT;
 TH3D *FZMass_ZY_QT[100];

 vector<TH3D *> v_BZMass_ZY_QT;
 TH3D *BZMass_ZY_QT[100];

 //ZMass ZY QT 3D plot
 vector<TH3D *> v_ZMass_ZY_QT;
 TH3D *ZMass_ZY_QT[100];

 vector<TH3D *> v_FZMass_ZY_QT_uu;
 TH3D *FZMass_ZY_QT_uu[100];

 vector<TH3D *> v_BZMass_ZY_QT_uu;
 TH3D *BZMass_ZY_QT_uu[100];

 vector<TH3D *> v_FZMass_ZY_QT_dd;
 TH3D *FZMass_ZY_QT_dd[100];

 vector<TH3D *> v_BZMass_ZY_QT_dd;
 TH3D *BZMass_ZY_QT_dd[100];

 vector<TH3D *> v_ZMass_ZY_QT_uu;
 TH3D *ZMass_ZY_QT_uu[100];

 vector<TH3D *> v_ZMass_ZY_QT_dd;
 TH3D *ZMass_ZY_QT_dd[100];

 vector<TH3D *> v_ZMass_ZY_QT_ss;
 TH3D *ZMass_ZY_QT_ss[100];

 vector<TH3D *> v_ZMass_ZY_QT_cc;
 TH3D *ZMass_ZY_QT_cc[100];

 vector<TH3D *> v_ZMass_ZY_QT_bb;
 TH3D *ZMass_ZY_QT_bb[100];

 /////////////////////
 //  dilution plot  //
 /////////////////////

 vector<TH3D *> v_ZMass_ZY_QT_uu_wrong;
 TH3D *ZMass_ZY_QT_uu_wrong[100];
 vector<TH3D *> v_ZMass_ZY_QT_uu_total;
 TH3D *ZMass_ZY_QT_uu_total[100];
 TH3D *Dilution_ZMass_ZY_QT_uu;

 vector<TH3D *> v_ZMass_ZY_QT_dd_wrong;
 TH3D *ZMass_ZY_QT_dd_wrong[100];
 vector<TH3D *> v_ZMass_ZY_QT_dd_total;
 TH3D *ZMass_ZY_QT_dd_total[100];
 TH3D *Dilution_ZMass_ZY_QT_dd;

 vector<TH3D *> v_ZMass_ZY_QT_uu_right;
 TH3D *ZMass_ZY_QT_uu_right[100];

 vector<TH3D *> v_ZMass_ZY_QT_dd_right;
 TH3D *ZMass_ZY_QT_dd_right[100];

 TH3D *AFB_Mass_ZY_ZPt;

 TH3D *AFB_Mass_ZY_ZPt_uu;
 TH3D *AFB_Mass_ZY_ZPt_dd;

 TH3D *CoefficientDilution_ZMass_ZY_QT_uu;
 TH3D *CoefficientDilution_ZMass_ZY_QT_dd;
 TH3D *ResidualDilution_ZMass_ZY_QT_uu;
 TH3D *ResidualDilution_ZMass_ZY_QT_dd;
 TH3D *DilutionAverage_ZMass_ZY_QT_uu;
 TH3D *DilutionAverage_ZMass_ZY_QT_dd;

 TH3D *OneMinusTwoD_uu;
 TH3D *OneMinusTwoD_dd;
 TH3D *RelativeCrossSection_uu;
 TH3D *RelativeCrossSection_dd;


//member function
 vector<TString> sysName;
 virtual void InitialSysName();
 virtual void bookHists(int TotalThread);
 virtual void outputInformation();

// template<class T>

};
#endif
