#ifndef __HistsResBosHighMassZ_H_
#define __HistsResBosHighMassZ_H_
#include <iostream>
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TFile.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TMath.h"
#include "TTree.h"
#include <vector>
#include "makeHists.h"

using namespace std;

class HistsResBosHighMassZ : public makeHists
{
 public:

 //////////////////////////
 //  Z Event Histograms  //
 //////////////////////////

 vector<TH1D *> v_ZMass_uu;
 TH1D* ZMass_uu[100];

 vector<TH1D *> v_ZMass_dd;
 TH1D* ZMass_dd[100];

 vector<TH1D *> v_ZMass_ss;
 TH1D* ZMass_ss[100];

 vector<TH1D *> v_ZMass_cc;
 TH1D* ZMass_cc[100];

 vector<TH1D *> v_ZMass_bb;
 TH1D* ZMass_bb[100];

 vector<TH1D *> v_ZMass_utype;
 TH1D* ZMass_utype[100];

 vector<TH1D *> v_ZMass_dtype;
 TH1D* ZMass_dtype[100];

 vector<TH3D *> v_Xsec_3D_CC;
 TH3D* Xsec_3D_CC[100];

 vector<TH3D *> v_Xsec_3D_CF;
 TH3D* Xsec_3D_CF[100];

 vector<TH2D *> v_Xsec_2D_CC;
 TH2D* Xsec_2D_CC[100];

 vector<TH2D *> v_Xsec_2D_CF;
 TH2D* Xsec_2D_CF[100];

 vector<TH1D *> v_ZRapidity_CC;
 TH1D* ZRapidity_CC[100];

 vector<TH1D *> v_ZRapidity_CF;
 TH1D* ZRapidity_CF[100];

 vector<TH1D *> v_ZRapidity_CF_Low;
 TH1D* ZRapidity_CF_Low[100];

 vector<TH1D *> v_ZPt;
 TH1D* ZPt[100];

 vector<TH1D *> v_FZMass_CC;
 TH1D* FZMass_CC[100];

 vector<TH1D *> v_BZMass_CC;
 TH1D* BZMass_CC[100];

 vector<TH1D *> v_FZMass_CF;
 TH1D* FZMass_CF[100];

 vector<TH1D *> v_BZMass_CF;
 TH1D* BZMass_CF[100];

 vector<TH1D *> v_FZMass_LowRegion_CC;
 TH1D* FZMass_LowRegion_CC[100];

 vector<TH1D *> v_BZMass_LowRegion_CC;
 TH1D* BZMass_LowRegion_CC[100];

 vector<TH1D *> v_FZMass_LowRegion_CF;
 TH1D* FZMass_LowRegion_CF[100];

 vector<TH1D *> v_BZMass_LowRegion_CF;
 TH1D* BZMass_LowRegion_CF[100];

 vector<TH1D *> v_FZMass_HighRegion_CC;
 TH1D* FZMass_HighRegion_CC[100];

 vector<TH1D *> v_BZMass_HighRegion_CC;
 TH1D* BZMass_HighRegion_CC[100];

 vector<TH1D *> v_FZMass_HighRegion_CF;
 TH1D* FZMass_HighRegion_CF[100];
 
 vector<TH1D *> v_BZMass_HighRegion_CF;
 TH1D* BZMass_HighRegion_CF[100];

 vector<TH1D *> v_FZMass_HighMass_uu;
 TH1D* FZMass_HighMass_uu[100];

 vector<TH1D *> v_BZMass_HighMass_uu;
 TH1D* BZMass_HighMass_uu[100];

 vector<TH1D *> v_FZMass_HighMass_dd;
 TH1D* FZMass_HighMass_dd[100];

 vector<TH1D *> v_BZMass_HighMass_dd;
 TH1D* BZMass_HighMass_dd[100];

 vector<TH1D *> v_ZHighMass_CC;
 TH1D* ZHighMass_CC[100];

 vector<TH1D *> v_ZHighMass_CF;
 TH1D* ZHighMass_CF[100];

 vector<TH2D *> v_FZMass_ZY_CC;
 TH2D* FZMass_ZY_CC[100];

 vector<TH2D *> v_BZMass_ZY_CC;
 TH2D* BZMass_ZY_CC[100];

 vector<TH2D *> v_FZMass_ZY_CF;
 TH2D* FZMass_ZY_CF[100];

 vector<TH2D *> v_BZMass_ZY_CF;
 TH2D* BZMass_ZY_CF[100];

 /////////////////////
 //  Dilution plot  //
 /////////////////////

 //quark ZMass dilution
 vector<TH1D *> v_ZHighMass_uu_total;
 TH1D *ZHighMass_uu_total[100];
 vector<TH1D *> v_ZHighMass_uu_wrong;
 TH1D *ZHighMass_uu_wrong[100];
 TH1D *Dilution_ZHighMass_uu;

 vector<TH1D *> v_ZHighMass_dd_total;
 TH1D *ZHighMass_dd_total[100];
 vector<TH1D *> v_ZHighMass_dd_wrong;
 TH1D *ZHighMass_dd_wrong[100];
 TH1D *Dilution_ZHighMass_dd;

 //////////////////////
 //  ATLAS 8TeV ZPT  //
 //////////////////////

 vector<TH1D *> v_ATLASZPt_Fiduc;
 TH1D *ATLASZPt_Fiduc[100];

 vector<TH1D *> v_ATLASZPt_Total;
 TH1D *ATLASZPt_Total[100];

 vector<TH2D *> v_ATLASZPt_2D_Fiduc;
 TH2D *ATLASZPt_2D_Fiduc[100];

 vector<TH2D *> v_ATLASZPt_2D_Total;
 TH2D *ATLASZPt_2D_Total[100];

 vector<TH2D *> v_ATLASZPt_2D_Total_uu;
 TH2D *ATLASZPt_2D_Total_uu[100];

 vector<TH2D *> v_ATLASZPt_2D_Total_dd;
 TH2D *ATLASZPt_2D_Total_dd[100];

 vector<TH2D *> v_ATLASZPt_2D_Total_MassWindow;
 TH2D *ATLASZPt_2D_Total_MassWindow[100];

 //////////////////////////
 //  W Event Histograms  //
 //////////////////////////

 vector<TH1D *> v_PositiveLeptonEta;
 TH1D* PositiveLeptonEta[100];

 vector<TH1D *> v_NegativeLeptonEta;
 TH1D* NegativeLeptonEta[100];

 vector<TH1D *> v_PositiveLeptonPt;
 TH1D* PositiveLeptonPt[100];

 vector<TH1D *> v_NegativeLeptonPt;
 TH1D* NegativeLeptonPt[100];

 vector<TH2D *> v_PositiveLeptonPtEta;
 TH2D* PositiveLeptonPtEta[100];

 vector<TH2D *> v_NegativeLeptonPtEta;
 TH2D* NegativeLeptonPtEta[100];

 vector<TH1D *> v_WLeptonPt;
 TH1D* WLeptonPt[100];

 vector<TH1D *> v_WPlusNeutrinoPt;
 TH1D* WPlusNeutrinoPt[100];

 vector<TH1D *> v_WMinusNeutrinoPt;
 TH1D* WMinusNeutrinoPt[100];

 vector<TH1D *> v_NeutrinoPt;
 TH1D* NeutrinoPt[100];

 vector<TH1D *> v_WPlusMT;
 TH1D* WPlusMT[100];

 vector<TH1D *> v_WMinusMT;
 TH1D* WMinusMT[100];

 vector<TH1D *> v_WBosonMT;
 TH1D* WBosonMT[100];

 vector<TH1D *> v_WPlusXsec;
 TH1D* WPlusXsec[100];

 vector<TH1D *> v_WMinusXsec;
 TH1D* WMinusXsec[100];

 vector<TH1D *> v_WHighMassMT;
 TH1D* WHighMassMT[100];

 vector<TH1D *> v_WPlusHighMassMT;
 TH1D* WPlusHighMassMT[100];

 vector<TH1D *> v_WMinusHighMassMT;
 TH1D* WMinusHighMassMT[100];

/////////////////////////////////////////////////////////////

 TH1D* AFB_CC;
 TH1D* AFB_CF;
 TH1D* AFB_LowRegion_CC;
 TH1D* AFB_LowRegion_CF;
 TH1D* AFB_HighRegion_CC;
 TH1D* AFB_HighRegion_CF;
 TH1D* AFB_Delta_CC;
 TH1D* AFB_Delta_CF;
 TH2D* AFB_Mass_ZY_CC;
 TH2D* AFB_Mass_ZY_CF;
 TH1D* AFB_HighMass_uu;
 TH1D* AFB_HighMass_dd;


 TH1D* Corr_AFB_HighMassZ_CC;
 TH1D* Corr_AFB_HighMassZ_CF;

 TH1D* ZMass_utype_Pseudodata;
 TH1D* ZMass_dtype_Pseudodata;

 TH3D* Xsec_3D_CC_Pseudodata;
 TH3D* Xsec_3D_CF_Pseudodata;
 TH2D* Xsec_2D_CC_Pseudodata;
 TH2D* Xsec_2D_CF_Pseudodata;
 TH1D* ZRapidity_CC_Pseudodata;
 TH1D* ZRapidity_CF_Pseudodata;
 TH1D* ZRapidity_CF_Low_Pseudodata;
 TH1D* ZHighMass_CC_Pseudodata;
 TH1D* ZHighMass_CF_Pseudodata;

 TH1D* FZMass_CC_Pseudodata;
 TH1D* BZMass_CC_Pseudodata;
 TH1D* FZMass_CF_Pseudodata;
 TH1D* BZMass_CF_Pseudodata;

 TH2D* FZMass_ZY_CC_Pseudodata;
 TH2D* BZMass_ZY_CC_Pseudodata;
 TH2D* FZMass_ZY_CF_Pseudodata;
 TH2D* BZMass_ZY_CF_Pseudodata;

 //////////////////////
 //  ATLAS 8TeV ZPT  //
 //////////////////////

 TH2D *ATLASZPt_2D_Fiduc_Final;
 TH2D *ATLASZPt_2D_Total_Final;
 TH2D *ATLASZPt_2D_Total_uu_Final;
 TH2D *ATLASZPt_2D_Total_dd_Final;
 TH2D *ATLASZPt_2D_Total_MassWindow_Final;

/////////////////////////////////////////////////////////////

 TH1D* PositiveLeptonEta_Pseudodata;
 TH1D* NegativeLeptonEta_Pseudodata;
 TH1D* PositiveLeptonPt_Pseudodata;
 TH1D* NegativeLeptonPt_Pseudodata;
 TH2D* PositiveLeptonPtEta_Pseudodata;
 TH2D* NegativeLeptonPtEta_Pseudodata;
 TH1D* WLeptonPt_Pseudodata;
 TH1D* WPlusNeutrinoPt_Pseudodata;
 TH1D* WMinusNeutrinoPt_Pseudodata;
 TH1D* NeutrinoPt_Pseudodata;
 TH1D* WPlusMT_Pseudodata;
 TH1D* WMinusMT_Pseudodata;
 TH1D* WBosonMT_Pseudodata;
 TH1D* WPlusXsec_Pseudodata;
 TH1D* WMinusXsec_Pseudodata;
 TH1D* WHighMassMT_Pseudodata;
 TH1D* WPlusHighMassMT_Pseudodata;
 TH1D* WMinusHighMassMT_Pseudodata;

 TH1D* WLeptonEtaAsymmetry;
 TH1D* WLeptonPtAsymmetry;
 TH1D* NeutrinoPtAsymmetry;
 TH1D* WBosonMTAsymmetry;
 TH1D* WBosonXsecAsymmetry;

//member function
 vector<TString> sysName;
 virtual void InitialSysName();
 virtual void bookHists(int TotalThread);
 virtual void outputInformation();

// template<class T>

};
#endif
