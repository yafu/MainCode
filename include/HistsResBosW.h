#ifndef __HISTS_RESBOSW_H_
#define __HISTS_RESBOSW_H_
#include <iostream>
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TFile.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TMath.h"
#include "TTree.h"
#include <vector>
#include "makeHists.h"

using namespace std;

class HistsResBosW : public makeHists
{
 public:

 vector<TH1D *> v_LeptonEtaAbs;
 TH1D *LeptonEtaAbs[100];
 vector<TH1D *> v_AntiLeptonEtaAbs;
 TH1D *AntiLeptonEtaAbs[100];

 vector<TH1D *> v_LeptonEtaAbs_pt35;
 TH1D *LeptonEtaAbs_pt35[100];
 vector<TH1D *> v_AntiLeptonEtaAbs_pt35;
 TH1D *AntiLeptonEtaAbs_pt35[100];

 vector<TH1D *> v_LeptonEtaAbs_pt25_35;
 TH1D *LeptonEtaAbs_pt25_35[100];
 vector<TH1D *> v_AntiLeptonEtaAbs_pt25_35;
 TH1D *AntiLeptonEtaAbs_pt25_35[100];

 vector<TH2D *> v_LeptonEtaAbs2D;
 TH2D *LeptonEtaAbs2D[100];
 vector<TH2D *> v_AntiLeptonEtaAbs2D;
 TH2D *AntiLeptonEtaAbs2D[100];

 vector<TH2D *> v_LeptonEtaAbs_MT2D;
 TH2D *LeptonEtaAbs_MT2D[100];
 vector<TH2D *> v_AntiLeptonEtaAbs_MT2D;
 TH2D *AntiLeptonEtaAbs_MT2D[100];

 vector<TH2D *> v_LeptonEtaAbs_ut2D;
 TH2D *LeptonEtaAbs_ut2D[100];
 vector<TH2D *> v_AntiLeptonEtaAbs_ut2D;
 TH2D *AntiLeptonEtaAbs_ut2D[100];

 vector<TH3D *> v_LeptonEtaAbs3D_cut30;
 TH3D *LeptonEtaAbs3D_cut30[100];
 vector<TH3D *> v_AntiLeptonEtaAbs3D_cut30;
 TH3D *AntiLeptonEtaAbs3D_cut30[100];

 vector<TH3D *> v_LeptonEtaAbs3D_cut35;
 TH3D *LeptonEtaAbs3D_cut35[100];
 vector<TH3D *> v_AntiLeptonEtaAbs3D_cut35;
 TH3D *AntiLeptonEtaAbs3D_cut35[100];

 vector<TH3D *> v_LeptonEtaAbs3D_cut40;
 TH3D *LeptonEtaAbs3D_cut40[100];
 vector<TH3D *> v_AntiLeptonEtaAbs3D_cut40;
 TH3D *AntiLeptonEtaAbs3D_cut40[100];

 vector<TH1D *> v_LeptonEtaAbs_LHCb;
 TH1D *LeptonEtaAbs_LHCb[100];
 vector<TH1D *> v_AntiLeptonEtaAbs_LHCb;
 TH1D *AntiLeptonEtaAbs_LHCb[100];

 vector<TH1D *> v_WPlusRapidity;
 TH1D *WPlusRapidity[100];
 vector<TH1D *> v_WMinusRapidity;
 TH1D *WMinusRapidity[100];

 vector<TH1D *> v_WPlusPt;
 TH1D *WPlusPt[100];
 vector<TH1D *> v_WMinusPt;
 TH1D *WMinusPt[100];

 vector<TH1D *> v_WPlusE;
 TH1D *WPlusE[100];
 vector<TH1D *> v_WMinusE;
 TH1D *WMinusE[100];

 vector<TH2D *> v_WPlusYPt;
 TH2D *WPlusYPt[100];
 vector<TH2D *> v_WMinusYPt;
 TH2D *WMinusYPt[100];

 vector<TH2D *> v_WPlusYE;
 TH2D *WPlusYE[100];
 vector<TH2D *> v_WMinusYE;
 TH2D *WMinusYE[100];


 vector<TH1D *> v_LeptonEta;
 TH1D *LeptonEta[100];

 vector<TH1D *> v_AntiLeptonEta;
 TH1D *AntiLeptonEta[100];

 vector<TH2D *> v_LeptonEtaMET;
 TH2D *LeptonEtaMET[100];
 vector<TH2D *> v_AntiLeptonEtaMET;
 TH2D *AntiLeptonEtaMET[100];

 vector<TH1D *> v_WPlusMET;
 TH1D *WPlusMET[100];

 vector<TH1D *> v_WMinusMET;
 TH1D *WMinusMET[100];

 vector<TH1D *> v_WPlusMET_LowEta;
 TH1D *WPlusMET_LowEta[100];

 vector<TH1D *> v_WMinusMET_LowEta;
 TH1D *WMinusMET_LowEta[100];

 vector<TH1D *> v_WPlusMET_HighEta;
 TH1D *WPlusMET_HighEta[100];

 vector<TH1D *> v_WMinusMET_HighEta;
 TH1D *WMinusMET_HighEta[100];

 vector<TH1D *> v_LeptonPt;
 TH1D *LeptonPt[100];

 vector<TH1D *> v_AntiLeptonPt;
 TH1D *AntiLeptonPt[100];

 vector<TH1D *> v_LeptonPt_LowEta;
 TH1D *LeptonPt_LowEta[100];

 vector<TH1D *> v_AntiLeptonPt_LowEta;
 TH1D *AntiLeptonPt_LowEta[100];

 vector<TH1D *> v_LeptonPt_HighEta;
 TH1D *LeptonPt_HighEta[100];

 vector<TH1D *> v_AntiLeptonPt_HighEta;
 TH1D *AntiLeptonPt_HighEta[100];

 vector<TH2D *> v_LeptonEtaPt;
 TH2D *LeptonEtaPt[100];
 vector<TH2D *> v_AntiLeptonEtaPt;
 TH2D *AntiLeptonEtaPt[100];

 virtual void bookHists(int TotalThread);

// template<class T>

};
#endif
