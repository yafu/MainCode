#ifndef __HISTS_RESBOSWTEV_H_
#define __HISTS_RESBOSWTEV_H_
#include <iostream>
#include <fstream>
#include <string>
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TFile.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TMath.h"
#include "TTree.h"
#include <vector>
#include "makeHists.h"

using namespace std;

class HistsResBosWTev : public makeHists
{
 public:

 ifstream infile;

 vector<TH1D *> v_LeptonEta_w225;
 TH1D *LeptonEta_w225[100];
 vector<TH1D *> v_AntiLeptonEta_w225;
 TH1D *AntiLeptonEta_w225[100];

 vector<TH1D *> v_LeptonEta_w227;
 TH1D *LeptonEta_w227[100];
 vector<TH1D *> v_AntiLeptonEta_w227;
 TH1D *AntiLeptonEta_w227[100];

 vector<TH1D *> v_LeptonEta_w234;
 TH1D *LeptonEta_w234[100];
 vector<TH1D *> v_AntiLeptonEta_w234;
 TH1D *AntiLeptonEta_w234[100];

 vector<TH1D *> v_LeptonEta_w281;
 TH1D *LeptonEta_w281[100];
 vector<TH1D *> v_AntiLeptonEta_w281;
 TH1D *AntiLeptonEta_w281[100];

 vector<TH1D *> v_WPlusRapidity;
 TH1D *WPlusRapidity[100];

 vector<TH1D *> v_WPlusPt;
 TH1D *WPlusPt[100];

 vector<TH1D *> v_WMinusRapidity;
 TH1D *WMinusRapidity[100];

 vector<TH1D *> v_WMinusPt;
 TH1D *WMinusPt[100];

 vector<TH1D *> v_LeptonEta;
 TH1D *LeptonEta[100];

 vector<TH1D *> v_AntiLeptonEta;
 TH1D *AntiLeptonEta[100];

 vector<TH1D *> v_LeptonPt;
 TH1D *LeptonPt[100];

 vector<TH1D *> v_AntiLeptonPt;
 TH1D *AntiLeptonPt[100];

 vector<TH1D *> v_MtW_CDF;
 TH1D *MtW_CDF[100];

 vector<TH1D *> v_LeptonPt_CDF;
 TH1D *LeptonPt_CDF[100];

 vector<TH1D *> v_NuPt_CDF;
 TH1D *NuPt_CDF[100];

 vector<TH1D *> v_LeptonPt_WPlus_CDF;
 TH1D *LeptonPt_WPlus_CDF[100];

 vector<TH1D *> v_NuPt_WPlus_CDF;
 TH1D *NuPt_WPlus_CDF[100];

 vector<TH1D *> v_LeptonPt_WMinus_CDF;
 TH1D *LeptonPt_WMinus_CDF[100];

 vector<TH1D *> v_NuPt_WMinus_CDF;
 TH1D *NuPt_WMinus_CDF[100];

 vector<TH1D *> v_DeltaPhi_CDF;
 TH1D *DeltaPhi_CDF[100];

 vector<TH1D *> v_WPt_CDF;
 TH1D *WPt_CDF[100];

 vector<TH1D *> v_ZPt_CDF;
 TH1D *ZPt_CDF[100];

 vector<TH1D *> v_WPt_CDF_FineBin;
 TH1D *WPt_CDF_FineBin[100];

 vector<TH1D *> v_ZPt_CDF_FineBin;
 TH1D *ZPt_CDF_FineBin[100];

 vector<TH1D *> v_WPt_CDF_ScaleUp;
 TH1D *WPt_CDF_ScaleUp[100];

 vector<TH1D *> v_WPt_CDF_ScaleDown;
 TH1D *WPt_CDF_ScaleDown[100];

 vector<TH1D *> v_WPt_CDF_ScaleVari;
 TH1D *WPt_CDF_ScaleVari[100];

 vector<TH1D *> v_WPt_Inclusive_LargeRange;
 TH1D *WPt_Inclusive_LargeRange[100];

 vector<TH1D *> v_ZPt_Inclusive_LargeRange;
 TH1D *ZPt_Inclusive_LargeRange[100];

 vector<TH1D *> v_MtW_CDF_Smear;
 TH1D *MtW_CDF_Smear[100];

 vector<TH1D *> v_LeptonPt_CDF_Smear;
 TH1D *LeptonPt_CDF_Smear[100];

 vector<TH1D *> v_NuPt_CDF_Smear;
 TH1D *NuPt_CDF_Smear[100];

 vector<TH1D *> v_DeltaPhi_CDF_Smear;
 TH1D *DeltaPhi_CDF_Smear[100];

 vector<TH1D *> v_WPt_CDF_Smear;
 TH1D *WPt_CDF_Smear[100];

 vector<TH1D *> v_ZPt_CDF_Smear;
 TH1D *ZPt_CDF_Smear[100];

 vector<TH1D *> v_MtW_Inclusive;
 TH1D *MtW_Inclusive[100];

 vector<TH1D *> v_LeptonPt_Inclusive;
 TH1D *LeptonPt_Inclusive[100];

 vector<TH1D *> v_NuPt_Inclusive;
 TH1D *NuPt_Inclusive[100];

 vector<TH1D *> v_LeptonPt_WPlus_Inclusive;
 TH1D *LeptonPt_WPlus_Inclusive[100];

 vector<TH1D *> v_NuPt_WPlus_Inclusive;
 TH1D *NuPt_WPlus_Inclusive[100];

 vector<TH1D *> v_LeptonPt_WMinus_Inclusive;
 TH1D *LeptonPt_WMinus_Inclusive[100];

 vector<TH1D *> v_NuPt_WMinus_Inclusive;
 TH1D *NuPt_WMinus_Inclusive[100];

 vector<TH1D *> v_LeptonEta_WPlus_Inclusive;
 TH1D *LeptonEta_WPlus_Inclusive[100];

 vector<TH1D *> v_NuEta_WPlus_Inclusive;
 TH1D *NuEta_WPlus_Inclusive[100];

 vector<TH1D *> v_LeptonEta_WMinus_Inclusive;
 TH1D *LeptonEta_WMinus_Inclusive[100];

 vector<TH1D *> v_NuEta_WMinus_Inclusive;
 TH1D *NuEta_WMinus_Inclusive[100];

 vector<TH1D *> v_DeltaPhi_Inclusive;
 TH1D *DeltaPhi_Inclusive[100];

 vector<TH1D *> v_WPt_Inclusive;
 TH1D *WPt_Inclusive[100];

 vector<TH1D *> v_ZPt_Inclusive;
 TH1D *ZPt_Inclusive[100];

 vector<TH1D *> v_MtW_EventCount;
 TH1D *MtW_EventCount[100];

 vector<TH1D *> v_MtW_Smear_EventCount;
 TH1D *MtW_Smear_EventCount[100];

 TH1D *Prediction_w225;
 TH1D *Data_w225;

 TH1D *Prediction_w227;
 TH1D *Data_w227;

 TH1D *Prediction_w234;
 TH1D *Data_w234;

 TH1D *Prediction_w281;
 TH1D *Data_w281;

 TH1D *RatioWZ_CDF;
 TH1D *RatioWZ_Inclusive;
 TH1D *RatioWZ_ScaleEnvelope;
 TH1D *NormalizedWPt_CDF;
 TH1D *NormalizedWPt_Inclusive;
 TH1D *NormalizedZPt_CDF;
 TH1D *NormalizedZPt_Inclusive;

 TH1D *MtW_CDF_Pseudodata;
 TH1D *LeptonPt_CDF_Pseudodata;
 TH1D *NuPt_CDF_Pseudodata;
 TH1D *DeltaPhi_CDF_Pseudodata;
 TH1D *WPt_CDF_Pseudodata;
 TH1D *WPt_CDF_Pseudodata_NoFluc;
 TH1D *WPt_CDF_ScaleUp_NoFluc;
 TH1D *WPt_CDF_ScaleDown_NoFluc;
 TH1D *WPt_CDF_ScaleVari_NoFluc;
 TH1D *ZPt_CDF_Pseudodata;

 TH1D *MtW_CDF_LargeStat;
 TH1D *LeptonPt_CDF_LargeStat;
 TH1D *NuPt_CDF_LargeStat;
 TH1D *DeltaPhi_CDF_LargeStat;
 TH1D *WPt_CDF_LargeStat;
 TH1D *ZPt_CDF_LargeStat;
 TH1D *WPt_CDF_FineBin_LargeStat;
 TH1D *ZPt_CDF_FineBin_LargeStat;

 TH1D *MtW_CDF_Smear_Pseudodata;
 TH1D *LeptonPt_CDF_Smear_Pseudodata;
 TH1D *NuPt_CDF_Smear_Pseudodata;
 TH1D *DeltaPhi_CDF_Smear_Pseudodata;
 TH1D *WPt_CDF_Smear_Pseudodata;
 TH1D *ZPt_CDF_Smear_Pseudodata;

 TH1D *MtW_CDF_Smear_LargeStat;
 TH1D *LeptonPt_CDF_Smear_LargeStat;
 TH1D *NuPt_CDF_Smear_LargeStat;
 TH1D *DeltaPhi_CDF_Smear_LargeStat;
 TH1D *WPt_CDF_Smear_LargeStat;
 TH1D *ZPt_CDF_Smear_LargeStat;

 TH1D *FiducialCrossSection_W;
 TH1D *FiducialCrossSection_Z;

 TH1D *FiducialCrossSection_Smear_W;
 TH1D *FiducialCrossSection_Smear_Z;

 TH1D *MtW_CDF_Data;
 TH1D *LeptonPt_CDF_Data;
 TH1D *NuPt_CDF_Data;

 TH1D *MtW_CDF_Electron;
 TH1D *LeptonPt_CDF_Electron;
 TH1D *NuPt_CDF_Electron;

 vector<AngularFunction *> v_A0_ZPt;
 AngularFunction *A0_ZPt[100];

 vector<AngularFunction *> v_A1_ZPt;
 AngularFunction *A1_ZPt[100];

 vector<AngularFunction *> v_A2_ZPt;
 AngularFunction *A2_ZPt[100];

 vector<AngularFunction *> v_A3_ZPt;
 AngularFunction *A3_ZPt[100];

 vector<AngularFunction *> v_A4_ZPt;
 AngularFunction *A4_ZPt[100];

 vector<AngularFunction *> v_L0_ZPt;
 AngularFunction *L0_ZPt[100];

 vector<AngularFunction *> v_A0_ZY;
 AngularFunction *A0_ZY[100];

 vector<AngularFunction *> v_A1_ZY;
 AngularFunction *A1_ZY[100];

 vector<AngularFunction *> v_A2_ZY;
 AngularFunction *A2_ZY[100];

 vector<AngularFunction *> v_A3_ZY;
 AngularFunction *A3_ZY[100];

 vector<AngularFunction *> v_A4_ZY;
 AngularFunction *A4_ZY[100];

 double DataPoint;
 double StaErr;
 double SysErr;
 double TotErr;
 vector<TString> DataList;
 string FileLine;

 HistsResBosWTev(){};
 HistsResBosWTev(TString RootType){this->RootType = RootType;};
 virtual void bookHists(int TotalThread);
 virtual void outputInformation();
 virtual void ReadData();
 virtual void InputData(vector<TString> DataList);
 virtual void InitialCDFData();
// template<class T>

};
#endif
