#ifndef __HistsSherpaWW_H_
#define __HistsSherpaWW_H_
#include <iostream>
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TFile.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TMath.h"
#include "TTree.h"
#include <vector>
#include "makeHists.h"

using namespace std;

class HistsSherpaWW : public makeHists
{
 public:

 vector<TH1D *> v_Wrong_uu;
 TH1D *Wrong_uu[100];

 vector<TH1D *> v_Right_uu;
 TH1D *Right_uu[100];

 vector<TH1D *> v_Total_uu;
 TH1D *Total_uu[100];

 vector<TH1D *> v_Wrong_dd;
 TH1D *Wrong_dd[100];

 vector<TH1D *> v_Right_dd;
 TH1D *Right_dd[100];

 vector<TH1D *> v_Total_dd;
 TH1D *Total_dd[100];

 vector<TH1D *> v_Wrong_ss;
 TH1D *Wrong_ss[100];

 vector<TH1D *> v_Right_ss;
 TH1D *Right_ss[100];

 vector<TH1D *> v_Total_ss;
 TH1D *Total_ss[100];

 vector<TH1D *> v_Wrong_cc;
 TH1D *Wrong_cc[100];

 vector<TH1D *> v_Right_cc;
 TH1D *Right_cc[100];

 vector<TH1D *> v_Total_cc;
 TH1D *Total_cc[100];

 vector<TH1D *> v_Wrong_bb;
 TH1D *Wrong_bb[100];

 vector<TH1D *> v_Right_bb;
 TH1D *Right_bb[100];

 vector<TH1D *> v_Total_bb;
 TH1D *Total_bb[100];

 vector<TH1D *> v_Wrong_AllFlavor;
 TH1D *Wrong_AllFlavor[100];

 vector<TH1D *> v_Right_AllFlavor;
 TH1D *Right_AllFlavor[100];

 vector<TH1D *> v_Total_AllFlavor;
 TH1D *Total_AllFlavor[100];

 vector<TH1D *> v_WrongW_uu;
 TH1D *WrongW_uu[100];

 vector<TH1D *> v_RightW_uu;
 TH1D *RightW_uu[100];

 vector<TH1D *> v_TotalW_uu;
 TH1D *TotalW_uu[100];

 vector<TH1D *> v_WrongW_dd;
 TH1D *WrongW_dd[100];

 vector<TH1D *> v_RightW_dd;
 TH1D *RightW_dd[100];

 vector<TH1D *> v_TotalW_dd;
 TH1D *TotalW_dd[100];

 vector<TH1D *> v_WrongW_ss;
 TH1D *WrongW_ss[100];

 vector<TH1D *> v_RightW_ss;
 TH1D *RightW_ss[100];

 vector<TH1D *> v_TotalW_ss;
 TH1D *TotalW_ss[100];

 vector<TH1D *> v_WrongW_cc;
 TH1D *WrongW_cc[100];

 vector<TH1D *> v_RightW_cc;
 TH1D *RightW_cc[100];

 vector<TH1D *> v_TotalW_cc;
 TH1D *TotalW_cc[100];

 vector<TH1D *> v_WrongW_bb;
 TH1D *WrongW_bb[100];

 vector<TH1D *> v_RightW_bb;
 TH1D *RightW_bb[100];

 vector<TH1D *> v_TotalW_bb;
 TH1D *TotalW_bb[100];

 vector<TH1D *> v_WrongW_AllFlavor;
 TH1D *WrongW_AllFlavor[100];

 vector<TH1D *> v_RightW_AllFlavor;
 TH1D *RightW_AllFlavor[100];

 vector<TH1D *> v_TotalW_AllFlavor;
 TH1D *TotalW_AllFlavor[100];

 vector<TH1D *> v_LeptonEta;
 TH1D *LeptonEta[100];

 vector<TH1D *> v_AntiLeptonEta;
 TH1D *AntiLeptonEta[100];

 vector<TH1D *> v_WPlusRapidity;
 TH1D *WPlusRapidity[100];

 vector<TH1D *> v_WMinusRapidity;
 TH1D *WMinusRapidity[100];

 vector<TH1D *> v_WWMass;
 TH1D *WWMass[100];

 vector<TH1D *> v_WWMass_uu;
 TH1D *WWMass_uu[100];

 vector<TH1D *> v_WWMass_dd;
 TH1D *WWMass_dd[100];

 vector<TH1D *> v_EtaDiff_uu;
 TH1D *EtaDiff_uu[100];

 vector<TH1D *> v_EtaDiff_dd;
 TH1D *EtaDiff_dd[100];

 vector<TH1D *> v_CosThetaPlusDiff;
 TH1D *CosThetaPlusDiff[100];

 vector<TH1D *> v_CosThetaMinusDiff;
 TH1D *CosThetaMinusDiff[100];

 int Eta_bin = 1;  double Eta_left = 0.0;  double Eta_right = 10.0;
 int WY_bin = 1;   double WY_left = 0.0;   double WY_right = 10.0;

 TH1D* Ratio_uu;
 TH1D* Ratio_dd;
 TH1D* Ratio_ss;
 TH1D* Ratio_cc;
 TH1D* Ratio_bb;
 TH1D* Ratio_AllFlavor;
 TH1D* Ratio_AllFlavor_Overall;

 TH1D* RatioW_uu;
 TH1D* RatioW_dd;
 TH1D* RatioW_ss;
 TH1D* RatioW_cc;
 TH1D* RatioW_bb;
 TH1D* RatioW_AllFlavor;
 TH1D* RatioW_AllFlavor_Overall;

 TH1D* BoostAsymmetry_uu;
 TH1D* BoostAsymmetry_dd;
 TH1D* BoostAsymmetry_ss;
 TH1D* BoostAsymmetry_cc;
 TH1D* BoostAsymmetry_bb;
 TH1D* BoostAsymmetry_AllFlavor;

 TH1D* BoostAsymmetryW_uu;
 TH1D* BoostAsymmetryW_dd;
 TH1D* BoostAsymmetryW_ss;
 TH1D* BoostAsymmetryW_cc;
 TH1D* BoostAsymmetryW_bb;
 TH1D* BoostAsymmetryW_AllFlavor;

//member function
 vector<TString> sysName;
 virtual void InitialSysName();
 virtual void bookHists(int TotalThread);
 virtual void outputInformation();

// template<class T>

};
#endif
