#ifndef __HISTS_WZPolarization_H_
#define __HISTS_WZPolarization_H_
#include <iostream>
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TFile.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TMath.h"
#include "TTree.h"
#include <vector>
#include "makeHists.h"
#include "TreeForWZPolarization.h"
#include "WZPolarization/loopWZPolarization.h"

using namespace std;

class ProcessHist;
class HistsWZPolarization;
class EventWZPolarization;
class TreeForWZPolarization;
class loopWZPolarization;

class ProcessHist
{
 friend class HistsWZPolarization;

 public:

//WZQCD and Polarized
 vector<BkgHist *> v_InclusivePolarized_bkg;
 BkgHist *InclusivePolarized_bkg[100];

 vector<BkgHist *> v_SignalPolarized_bkg;
 BkgHist *SignalPolarized_bkg[100];

 vector<BkgHist *> v_SignalPtWZPolarized_bkg;
 BkgHist *SignalPtWZPolarized_bkg[100];

 vector<BkgHist *> v_ZZCRPolarized_bkg;
 BkgHist *ZZCRPolarized_bkg[100];

 vector<BkgHist *> v_TopCRPolarized_bkg;
 BkgHist *TopCRPolarized_bkg[100];

 vector<BkgHist *> v_HighPtWZCRPolarized_bkg;
 BkgHist *HighPtWZCRPolarized_bkg[100];

 vector<BkgHist *> v_LowPtWZCRPolarized_bkg;
 BkgHist *LowPtWZCRPolarized_bkg[100];

 vector<BkgHist *> v_HighR21CRPolarized_bkg;
 BkgHist *HighR21CRPolarized_bkg[100];

 vector<BkgHist *> v_LowR21CRPolarized_bkg;
 BkgHist *LowR21CRPolarized_bkg[100];

 vector<BkgHist *> v_LowPtZCRPolarized_bkg;
 BkgHist *LowPtZCRPolarized_bkg[100];

 vector<BkgHist *> v_HighPtZCRPolarized_bkg;
 BkgHist *HighPtZCRPolarized_bkg[100];

 vector<BkgHist *> v_PtWZ10CRPolarized_bkg;
 BkgHist *PtWZ10CRPolarized_bkg[100];

 vector<BkgHist *> v_PtWZ20CRPolarized_bkg;
 BkgHist *PtWZ20CRPolarized_bkg[100];

 vector<BkgHist *> v_PtWZ40CRPolarized_bkg;
 BkgHist *PtWZ40CRPolarized_bkg[100];


//Truth
 vector<BkgHist *> v_TruthInclusive_bkg;
 BkgHist *TruthInclusive_bkg[100];
 
 vector<BkgHist *> v_TruthSignal_bkg;
 BkgHist *TruthSignal_bkg[100];
 
 vector<BkgHist *> v_TruthZZCRPolarized_bkg;
 BkgHist *TruthZZCRPolarized_bkg[100];
 
 vector<BkgHist *> v_TruthTopCRPolarized_bkg;
 BkgHist *TruthTopCRPolarized_bkg[100];
 
 vector<BkgHist *> v_TruthHighPtWZCRPolarized_bkg;
 BkgHist *TruthHighPtWZCRPolarized_bkg[100];
 
 vector<BkgHist *> v_TruthLowPtZCRPolarized_bkg;
 BkgHist *TruthLowPtZCRPolarized_bkg[100];

//Data
 vector<TH1D *> v_Inclusive_Data;
 TH1D *Inclusive_Data[100];
 
 vector<TH1D *> v_Signal_Data;
 TH1D *Signal_Data[100];

 vector<TH1D *> v_SignalPtWZ_Data;
 TH1D *SignalPtWZ_Data[100];
 
 vector<TH1D *> v_ZZCRPolarized_Data;
 TH1D *ZZCRPolarized_Data[100];
 
 vector<TH1D *> v_TopCRPolarized_Data;
 TH1D *TopCRPolarized_Data[100];
 
 vector<TH1D *> v_HighPtWZCRPolarized_Data;
 TH1D *HighPtWZCRPolarized_Data[100];

 vector<TH1D *> v_LowPtWZCRPolarized_Data;
 TH1D *LowPtWZCRPolarized_Data[100];

 vector<TH1D *> v_HighR21CRPolarized_Data;
 TH1D *HighR21CRPolarized_Data[100];

 vector<TH1D *> v_LowR21CRPolarized_Data;
 TH1D *LowR21CRPolarized_Data[100];

 vector<TH1D *> v_LowPtZCRPolarized_Data;
 TH1D *LowPtZCRPolarized_Data[100];

 vector<TH1D *> v_HighPtZCRPolarized_Data;
 TH1D *HighPtZCRPolarized_Data[100];

 vector<TH1D *> v_PtWZ10CRPolarized_Data;
 TH1D *PtWZ10CRPolarized_Data[100];

 vector<TH1D *> v_PtWZ20CRPolarized_Data;
 TH1D *PtWZ20CRPolarized_Data[100];

 vector<TH1D *> v_PtWZ40CRPolarized_Data;
 TH1D *PtWZ40CRPolarized_Data[100];

 vector<TH1D *> v_TopEnrichCR_Data;
 TH1D *TopEnrichCR_Data[100];

 vector<TH1D *> v_ZjetEnrichCR_Data;
 TH1D *ZjetEnrichCR_Data[100];

 vector<TH1D *> v_ZFakeElCR_Data;
 TH1D *ZFakeElCR_Data[100];

 vector<TH1D *> v_ZFakeMuCR_Data;
 TH1D *ZFakeMuCR_Data[100];

 vector<TH1D *> v_TopElCR_Data;
 TH1D *TopElCR_Data[100];

 vector<TH1D *> v_TopMuCR_Data;
 TH1D *TopMuCR_Data[100];

//Final Data(Data - Bkg)
 TH1D* Inclusive_FinalData;
 TH1D* Signal_FinalData;
 TH1D* HighPtWZCR_FinalData;
 TH1D* LowPtZCR_FinalData;

//Final TT(Data - Bkg - LL - LT - TL)
 TH1D* PtWZ10CR_FinalTT;
 TH1D* PtWZ20CR_FinalTT;
 TH1D* PtWZ40CR_FinalTT;
 TH1D* LowPtWZCR_FinalTT;

//Fake
 vector<FakeHist *> v_Inclusive_Fake;
 FakeHist *Inclusive_Fake[100];

 vector<FakeHist *> v_Signal_Fake;
 FakeHist *Signal_Fake[100];

 vector<FakeHist *> v_SignalPtWZ_Fake;
 FakeHist *SignalPtWZ_Fake[100];

 vector<FakeHist *> v_ZZCR_Fake;
 FakeHist *ZZCR_Fake[100];

 vector<FakeHist *> v_TopCR_Fake;
 FakeHist *TopCR_Fake[100];

 vector<FakeHist *> v_HighPtWZCR_Fake;
 FakeHist *HighPtWZCR_Fake[100];

 vector<FakeHist *> v_LowPtWZCR_Fake;
 FakeHist *LowPtWZCR_Fake[100];

 vector<FakeHist *> v_HighR21CR_Fake;
 FakeHist *HighR21CR_Fake[100];

 vector<FakeHist *> v_LowR21CR_Fake;
 FakeHist *LowR21CR_Fake[100];

 vector<FakeHist *> v_LowPtZCR_Fake;
 FakeHist *LowPtZCR_Fake[100];

 vector<FakeHist *> v_HighPtZCR_Fake;
 FakeHist *HighPtZCR_Fake[100];

 vector<FakeHist *> v_PtWZ10CR_Fake;
 FakeHist *PtWZ10CR_Fake[100];

 vector<FakeHist *> v_PtWZ20CR_Fake;
 FakeHist *PtWZ20CR_Fake[100];

 vector<FakeHist *> v_PtWZ40CR_Fake;
 FakeHist *PtWZ40CR_Fake[100];

 vector<FakeHist *> v_WZInclusiveNoMETCR_Fake;
 FakeHist *WZInclusiveNoMETCR_Fake[100];

 vector<FakeHist *> v_ZFakeElCR_Fake;
 FakeHist *ZFakeElCR_Fake[100];

 vector<FakeHist *> v_ZFakeMuCR_Fake;
 FakeHist *ZFakeMuCR_Fake[100];

 vector<FakeHist *> v_TopEnrichCR_Fake;
 FakeHist *TopEnrichCR_Fake[100];

 vector<FakeHist *> v_ZjetEnrichCR_Fake;
 FakeHist *ZjetEnrichCR_Fake[100];

 vector<FakeHist *> v_TopElCR_Fake;
 FakeHist *TopElCR_Fake[100];

 vector<FakeHist *> v_TopMuCR_Fake;
 FakeHist *TopMuCR_Fake[100];

//WZQCD systematic
 vector<SysHist *> v_InclusivePolarized_sys;
 SysHist *InclusivePolarized_sys[100];

 vector<SysHist *> v_SignalPolarized_sys;
 SysHist *SignalPolarized_sys[100];

 vector<SysHist *> v_ZZCRPolarized_sys;
 SysHist *ZZCRPolarized_sys[100];

 vector<SysHist *> v_TopCRPolarized_sys;
 SysHist *TopCRPolarized_sys[100];

 vector<SysHist *> v_HighPtWZCRPolarized_sys;
 SysHist *HighPtWZCRPolarized_sys[100];

 vector<SysHist *> v_LowPtZCRPolarized_sys;
 SysHist *LowPtZCRPolarized_sys[100];

//All Bkg
 vector<BkgHist *> v_Inclusive_bkg;
 BkgHist *Inclusive_bkg[100];

 vector<BkgHist *> v_Signal_bkg;
 BkgHist *Signal_bkg[100];

 vector<BkgHist *> v_SignalPtWZ_bkg;
 BkgHist *SignalPtWZ_bkg[100];

 vector<BkgHist *> v_ZZCR_bkg;
 BkgHist *ZZCR_bkg[100];

 vector<BkgHist *> v_TopCR_bkg;
 BkgHist *TopCR_bkg[100];

 vector<BkgHist *> v_HighPtWZCR_bkg;
 BkgHist *HighPtWZCR_bkg[100];

 vector<BkgHist *> v_LowPtWZCR_bkg;
 BkgHist *LowPtWZCR_bkg[100];

 vector<BkgHist *> v_HighR21CR_bkg;
 BkgHist *HighR21CR_bkg[100];

 vector<BkgHist *> v_LowR21CR_bkg;
 BkgHist *LowR21CR_bkg[100];

 vector<BkgHist *> v_LowPtZCR_bkg;
 BkgHist *LowPtZCR_bkg[100];

 vector<BkgHist *> v_HighPtZCR_bkg;
 BkgHist *HighPtZCR_bkg[100];

 vector<BkgHist *> v_PtWZ10CR_bkg;
 BkgHist *PtWZ10CR_bkg[100];

 vector<BkgHist *> v_PtWZ20CR_bkg;
 BkgHist *PtWZ20CR_bkg[100];

 vector<BkgHist *> v_PtWZ40CR_bkg;
 BkgHist *PtWZ40CR_bkg[100];

 vector<BkgHist *> v_WZInclusiveNoMETCR_bkg;
 BkgHist *WZInclusiveNoMETCR_bkg[100];

 vector<BkgHist *> v_ZFakeElCR_bkg;
 BkgHist *ZFakeElCR_bkg[100];

 vector<BkgHist *> v_ZFakeMuCR_bkg;
 BkgHist *ZFakeMuCR_bkg[100];

 vector<BkgHist *> v_TopEnrichCR_bkg;
 BkgHist *TopEnrichCR_bkg[100];

 vector<BkgHist *> v_ZjetEnrichCR_bkg;
 BkgHist *ZjetEnrichCR_bkg[100];

 vector<BkgHist *> v_TopElCR_bkg;
 BkgHist *TopElCR_bkg[100];

 vector<BkgHist *> v_TopMuCR_bkg;
 BkgHist *TopMuCR_bkg[100];

//All Bkg with systematic
 vector<BkgHist *> v_InclusivePolarizedSys_bkg;
 BkgHist *InclusivePolarizedSys_bkg[100];

 vector<BkgHist *> v_SignalPolarizedSys_bkg;
 BkgHist *SignalPolarizedSys_bkg[100];

 vector<BkgHist *> v_ZZCRPolarizedSys_bkg;
 BkgHist *ZZCRPolarizedSys_bkg[100];

 vector<BkgHist *> v_TopCRPolarizedSys_bkg;
 BkgHist *TopCRPolarizedSys_bkg[100];

 vector<BkgHist *> v_HighPtWZCRPolarizedSys_bkg;
 BkgHist *HighPtWZCRPolarizedSys_bkg[100];

 vector<BkgHist *> v_LowPtZCRPolarizedSys_bkg;
 BkgHist *LowPtZCRPolarizedSys_bkg[100];


 vector<BkgHist *> v_InclusiveSys_bkg;
 BkgHist *InclusiveSys_bkg[100];

 vector<BkgHist *> v_SignalSys_bkg;
 BkgHist *SignalSys_bkg[100];

 vector<BkgHist *> v_ZZCRSys_bkg;
 BkgHist *ZZCRSys_bkg[100];

 vector<BkgHist *> v_TopCRSys_bkg;
 BkgHist *TopCRSys_bkg[100];

 vector<BkgHist *> v_HighPtWZCRSys_bkg;
 BkgHist *HighPtWZCRSys_bkg[100];

 vector<BkgHist *> v_LowPtZCRSys_bkg;
 BkgHist *LowPtZCRSys_bkg[100];


 ProcessHist();
 virtual void Initial(int iThread, int MCType);
 virtual void Fill(int iThread, double par, double weight, EventWZPolarization ProcessEvent);
 virtual void FillFake(int iThread, double par, double weight, EventWZPolarization ProcessEvent);
 virtual void FillTruth(int iThread, double par, double weight, EventWZPolarization ProcessEvent);
 virtual void InputReweightingFactor(int iThread, double weight);
 virtual void ResetHist();
 virtual void GetFinalData();

 bool OnlyNominal[100] = {true};
 bool doBkg[100] = {false};
 bool doControlRegion[100] = {false};
 int doFakeRate[100] = {0};

 HistsWZPolarization *fHist;
 int TotalThread;

 int MCType[100] = {0};
 bool isData[100] = {false};
 bool isSignal[100] = {false};

 TString HistName = "";
 TString SystematicName = "";

 double ReweightingFactor[100] = {1.0};

 double NLOKFactor = 1.0;
 double NLOKFactor_ZZCR = 1.0;
 double NLOKFactor_HighPtZCR = 1.0;
 double NLOKFactor_Signal = 1.0;
 double NLOKFactor_SignalPtWZ = 1.0;
 double NLOKFactor_HighPtWZCR = 1.0;
 double NLOKFactor_LowPtWZCR = 1.0;
 double NLOKFactor_HighR21CR = 1.0;
 double NLOKFactor_LowR21CR = 1.0;

 double NLOKFactorError = 0.0;
 double NLOKFactorError_ZZCR = 0.0;
 double NLOKFactorError_HighPtZCR = 0.0;
 double NLOKFactorError_Signal = 0.0;
 double NLOKFactorError_SignalPtWZ = 0.0;
 double NLOKFactorError_HighPtWZCR = 0.0;
 double NLOKFactorError_LowPtWZCR = 0.0;
 double NLOKFactorError_HighR21CR = 0.0;
 double NLOKFactorError_LowR21CR = 0.0;


 virtual void InputNLOKFactor(double Inclusive, double ZZ, double HighPtZ, double Signal, double SignalPtWZ, double HighPtWZ, double LowPtWZ, double HighR21, double LowR21)
 {
   this->NLOKFactor = Inclusive;
   this->NLOKFactor_ZZCR = ZZ;
   this->NLOKFactor_HighPtZCR = HighPtZ;
   this->NLOKFactor_Signal = Signal;
   this->NLOKFactor_SignalPtWZ = SignalPtWZ;
   this->NLOKFactor_HighPtWZCR = HighPtWZ;
   this->NLOKFactor_LowPtWZCR = LowPtWZ;
   this->NLOKFactor_HighR21CR = HighR21;
   this->NLOKFactor_LowR21CR = LowR21;
 }

};


class HistsWZPolarization : public makeHists
{
 friend class ProcessHist;

 TreeForWZPolarization *fTree;

 public:

 ProcessHist *M_WZ;
 ProcessHist *M_3l;
 ProcessHist *Mt_WZ;
 ProcessHist *M_Z;
 ProcessHist *Mt_W;
 ProcessHist *Met;
 ProcessHist *Njets;
 ProcessHist *NBjets;
 ProcessHist *Lep1Pt;
 ProcessHist *Lep2Pt;
 ProcessHist *Lep3Pt;
 ProcessHist *Lep1Eta;
 ProcessHist *Lep2Eta;
 ProcessHist *Lep3Eta;
 ProcessHist *Lep1Phi;
 ProcessHist *Lep2Phi;
 ProcessHist *Lep3Phi;
 ProcessHist *LepPt;
 ProcessHist *LepEta;
 ProcessHist *LepPhi;
 ProcessHist *Lep2Pt_mmm;
 ProcessHist *Lep2Pt_mme;
 ProcessHist *Lep2Pt_eem;
 ProcessHist *Lep2Pt_eee;
 ProcessHist *Lep3Pt_mmm;
 ProcessHist *Lep3Pt_mme;
 ProcessHist *Lep3Pt_eem;
 ProcessHist *Lep3Pt_eee;
 ProcessHist *Lep3Eta_mmm;
 ProcessHist *Lep3Eta_mme;
 ProcessHist *Lep3Eta_eem;
 ProcessHist *Lep3Eta_eee;
 ProcessHist *Mt_W_mmm;
 ProcessHist *Mt_W_mme;
 ProcessHist *Mt_W_eem;
 ProcessHist *Mt_W_eee;
 ProcessHist *Met_mmm;
 ProcessHist *Met_mme;
 ProcessHist *Met_eem;
 ProcessHist *Met_eee;
 //ProcessHist *Mpx;
 //ProcessHist *Mpy;
 //ProcessHist *Mpz;
 ProcessHist *LWTNNNuPz;
 ProcessHist *TruthNuPz;
// ProcessHist *MpzDiff;
// ProcessHist *LWTNNNuPzDiff;
 ProcessHist *LWTNNpLL;
 ProcessHist *LWTNNpLT;
 ProcessHist *LWTNNpTL;
 ProcessHist *LWTNNpTT;
 ProcessHist *Pt_W;
 ProcessHist *Pt_Z;
 ProcessHist *Pt_Z_mmm;
 ProcessHist *Pt_Z_mme;
 ProcessHist *Pt_Z_eem;
 ProcessHist *Pt_Z_eee;
 ProcessHist *Pt_WZ;
 ProcessHist *SumJetPt;
 ProcessHist *CosThetaV;
 ProcessHist *CosThetaVAbs;
 ProcessHist *CosThetaV_WLZL;
 ProcessHist *CosThetaV_WLZH;
 ProcessHist *CosThetaV_WHZL;
 ProcessHist *CosThetaV_WHZH;
 ProcessHist *CosThetaLepW;
 ProcessHist *CosThetaLepZ;
 ProcessHist *DY_WZ;
 ProcessHist *DY_3Z;
 ProcessHist *DY_3N;
 ProcessHist *DY_WPlusZ;
 ProcessHist *DY_WMinusZ;
 ProcessHist *DY_3PlusZ;
 ProcessHist *DY_3MinusZ;
 ProcessHist *DeltaPhiLepWLepZ;
 ProcessHist *DeltaPhiLepWLepZ_WLZL;
 ProcessHist *DeltaPhiLepWLepZ_WLZH;
 ProcessHist *DeltaPhiLepWLepZ_WHZL;
 ProcessHist *DeltaPhiLepWLepZ_WHZH;
 ProcessHist *DeltaPhiLepWLepZWZFrame;
 ProcessHist *DeltaPhiLepWLepZWZFrame_WLZL;
 ProcessHist *DeltaPhiLepWLepZWZFrame_WLZH;
 ProcessHist *DeltaPhiLepWLepZWZFrame_WHZL;
 ProcessHist *DeltaPhiLepWLepZWZFrame_WHZH;
 ProcessHist *R21;
 //ProcessHist *R21_WLZL;
 //ProcessHist *R21_WLZH;
 //ProcessHist *R21_WHZL;
 //ProcessHist *R21_WHZH;
 ProcessHist *Channel;
 ProcessHist *BDTScore;
 ProcessHist *BDTScore_WLZL;
 ProcessHist *BDTScore_WLZH;
 ProcessHist *BDTScore_WHZL;
 ProcessHist *BDTScore_WHZH;
 ProcessHist *BDTScoreSR;
 ProcessHist *BDTScoreSR_WLZL;
 ProcessHist *BDTScoreSR_WLZH;
 ProcessHist *BDTScoreSR_WHZL;
 ProcessHist *BDTScoreSR_WHZH;
 ProcessHist *BDTScoreSR100;
 ProcessHist *RightZYWLepEta;
 ProcessHist *WrongZYWLepEta;
 ProcessHist *WDecayStatus;
 ProcessHist *ZDecayStatus;

 vector<TH1D *> v_MpzDiff;
 TH1D* MpzDiff[100];

 vector<TH1D *> v_LWTNNNuPzDiff;
 TH1D* LWTNNNuPzDiff[100];


 vector<TH1D *> v_Event_CutFlow;
 TH1D* Event_CutFlow[100];

 vector<BkgHist *> v_Yield_CutFlow_bkg;
 BkgHist *Yield_CutFlow_bkg[100];

 vector<BkgHist *> v_NormalizedYield_CutFlow_bkg;
 BkgHist *NormalizedYield_CutFlow_bkg[100];

 vector<BkgHist *> v_Polarized_CutFlow_bkg;
 BkgHist *Polarized_CutFlow_bkg[100];


 double NLOKFactor = 1.0;
 double NLOKFactor_ZZCR = 1.0;
 double NLOKFactor_HighPtZCR = 1.0;
 double NLOKFactor_Signal = 1.0;
 double NLOKFactor_SignalPtWZ = 1.0;
 double NLOKFactor_HighPtWZCR = 1.0;
 double NLOKFactor_LowPtWZCR = 1.0;
 double NLOKFactor_HighR21CR = 1.0;
 double NLOKFactor_LowR21CR = 1.0;

 double NLOKFactorError = 0.0;
 double NLOKFactorError_ZZCR = 0.0;
 double NLOKFactorError_HighPtZCR = 0.0;
 double NLOKFactorError_Signal = 0.0;
 double NLOKFactorError_SignalPtWZ = 0.0;
 double NLOKFactorError_HighPtWZCR = 0.0;
 double NLOKFactorError_LowPtWZCR = 0.0;
 double NLOKFactorError_HighR21CR = 0.0;
 double NLOKFactorError_LowR21CR = 0.0;


 TH1D* ReweightFactor;
 TH1D* ReweightFactor_Truth;

 vector<BkgHist *> v_BkgYield_bkg;
 BkgHist *BkgYield_bkg[100];

 vector<BkgHist *> v_PolarizedYield_bkg;
 BkgHist *PolarizedYield_bkg[100];

 vector<BkgHist *> v_PolarizedYield_ZZCR_bkg;
 BkgHist *PolarizedYield_ZZCR_bkg[100];

 vector<BkgHist *> v_PolarizedYield_HighPtZCR_bkg;
 BkgHist *PolarizedYield_HighPtZCR_bkg[100];

 vector<BkgHist *> v_PolarizedYield_Signal_bkg;
 BkgHist *PolarizedYield_Signal_bkg[100];

 vector<BkgHist *> v_PolarizedYield_SignalPtWZ_bkg;
 BkgHist *PolarizedYield_SignalPtWZ_bkg[100];

 vector<BkgHist *> v_PolarizedYield_HighPtWZCR_bkg;
 BkgHist *PolarizedYield_HighPtWZCR_bkg[100];

 vector<BkgHist *> v_PolarizedYield_LowPtWZCR_bkg;
 BkgHist *PolarizedYield_LowPtWZCR_bkg[100];

 vector<BkgHist *> v_PolarizedYield_HighR21CR_bkg;
 BkgHist *PolarizedYield_HighR21CR_bkg[100];

 vector<BkgHist *> v_PolarizedYield_LowR21CR_bkg;
 BkgHist *PolarizedYield_LowR21CR_bkg[100];


 vector<TH1D *> v_LeptonCount;
 TH1D *LeptonCount[100];


 TH1D* Ratio_Inclusive;
 TH1D* Ratio_Signal;
 TH1D* Ratio_HighPtWZCR;
 TH1D* Ratio_LowPtZCR;


//Fake Rate
 vector<TH1D *> v_Lep3Pt_ZFakeEl_PassZ;
 TH1D* Lep3Pt_ZFakeEl_PassZ[100];

 vector<TH1D *> v_Lep3Pt_ZFakeEl_UpSys_PassZ;
 TH1D* Lep3Pt_ZFakeEl_UpSys_PassZ[100];

 vector<TH1D *> v_Lep3Pt_ZFakeEl_DownSys_PassZ;
 TH1D* Lep3Pt_ZFakeEl_DownSys_PassZ[100];

 vector<TH1D *> v_Lep3Pt_ZFakeMu_PassZ;
 TH1D* Lep3Pt_ZFakeMu_PassZ[100];

 vector<TH1D *> v_Lep3Pt_TopEl_PassZ;
 TH1D* Lep3Pt_TopEl_PassZ[100];

 vector<TH1D *> v_Lep3Pt_TopMu_PassZ;
 TH1D* Lep3Pt_TopMu_PassZ[100];

 vector<TH1D *> v_Lep3Pt_ZFakeEl_PassW;
 TH1D* Lep3Pt_ZFakeEl_PassW[100];

 vector<TH1D *> v_Lep3Pt_ZFakeEl_UpSys_PassW;
 TH1D* Lep3Pt_ZFakeEl_UpSys_PassW[100];

 vector<TH1D *> v_Lep3Pt_ZFakeEl_DownSys_PassW;
 TH1D* Lep3Pt_ZFakeEl_DownSys_PassW[100];

 vector<TH1D *> v_Lep3Pt_ZFakeMu_PassW;
 TH1D* Lep3Pt_ZFakeMu_PassW[100];

 vector<TH1D *> v_Lep3Pt_TopEl_PassW;
 TH1D* Lep3Pt_TopEl_PassW[100];

 vector<TH1D *> v_Lep3Pt_TopMu_PassW;
 TH1D* Lep3Pt_TopMu_PassW[100];

 vector<TH1D *> v_Lep3Pt_ZFakeEl_UnPassZ;
 TH1D* Lep3Pt_ZFakeEl_UnPassZ[100];

 vector<TH1D *> v_Lep3Pt_ZFakeEl_UpSys_UnPassZ;
 TH1D* Lep3Pt_ZFakeEl_UpSys_UnPassZ[100];

 vector<TH1D *> v_Lep3Pt_ZFakeEl_DownSys_UnPassZ;
 TH1D* Lep3Pt_ZFakeEl_DownSys_UnPassZ[100];

 vector<TH1D *> v_Lep3Pt_ZFakeMu_UnPassZ;
 TH1D* Lep3Pt_ZFakeMu_UnPassZ[100];

 vector<TH1D *> v_Lep3Pt_TopEl_UnPassZ;
 TH1D* Lep3Pt_TopEl_UnPassZ[100];

 vector<TH1D *> v_Lep3Pt_TopMu_UnPassZ;
 TH1D* Lep3Pt_TopMu_UnPassZ[100];

 vector<TH1D *> v_Lep3Pt_ZFakeEl_UnPassW;
 TH1D* Lep3Pt_ZFakeEl_UnPassW[100];

 vector<TH1D *> v_Lep3Pt_ZFakeEl_UpSys_UnPassW;
 TH1D* Lep3Pt_ZFakeEl_UpSys_UnPassW[100];

 vector<TH1D *> v_Lep3Pt_ZFakeEl_DownSys_UnPassW;
 TH1D* Lep3Pt_ZFakeEl_DownSys_UnPassW[100];

 vector<TH1D *> v_Lep3Pt_ZFakeMu_UnPassW;
 TH1D* Lep3Pt_ZFakeMu_UnPassW[100];

 vector<TH1D *> v_Lep3Pt_TopEl_UnPassW;
 TH1D* Lep3Pt_TopEl_UnPassW[100];

 vector<TH1D *> v_Lep3Pt_TopMu_UnPassW;
 TH1D* Lep3Pt_TopMu_UnPassW[100];

 vector<TH1D *> v_Lep3Pt_ZFakeEl_Loose;
 TH1D* Lep3Pt_ZFakeEl_Loose[100];

 vector<TH1D *> v_Lep3Pt_ZFakeEl_UpSys_Loose;
 TH1D* Lep3Pt_ZFakeEl_UpSys_Loose[100];

 vector<TH1D *> v_Lep3Pt_ZFakeEl_DownSys_Loose;
 TH1D* Lep3Pt_ZFakeEl_DownSys_Loose[100];

 vector<TH1D *> v_Lep3Pt_ZFakeMu_Loose;
 TH1D* Lep3Pt_ZFakeMu_Loose[100];

 vector<TH1D *> v_Lep3Pt_TopEl_Loose;
 TH1D* Lep3Pt_TopEl_Loose[100];

 vector<TH1D *> v_Lep3Pt_TopMu_Loose;
 TH1D* Lep3Pt_TopMu_Loose[100];

 vector<TH1D *> v_Lep3Pt_ZFakeEl_PassMedium;
 TH1D* Lep3Pt_ZFakeEl_PassMedium[100];

 vector<TH1D *> v_Lep3Pt_ZFakeMu_PassMedium;
 TH1D* Lep3Pt_ZFakeMu_PassMedium[100];

 vector<TH1D *> v_Lep3Pt_TopEl_PassMedium;
 TH1D* Lep3Pt_TopEl_PassMedium[100];

 vector<TH1D *> v_Lep3Pt_TopMu_PassMedium;
 TH1D* Lep3Pt_TopMu_PassMedium[100];

 vector<TH1D *> v_Lep3Pt_ZFakeEl_PassTight;
 TH1D* Lep3Pt_ZFakeEl_PassTight[100];

 vector<TH1D *> v_Lep3Pt_ZFakeMu_PassTight;
 TH1D* Lep3Pt_ZFakeMu_PassTight[100];

 vector<TH1D *> v_Lep3Pt_TopEl_PassTight;
 TH1D* Lep3Pt_TopEl_PassTight[100];

 vector<TH1D *> v_Lep3Pt_TopMu_PassTight;
 TH1D* Lep3Pt_TopMu_PassTight[100];

 //Fake rate correction
 vector<TH1D *> v_Lep3Pt_ZFakeEl_LargeMtW_PassW;
 TH1D* Lep3Pt_ZFakeEl_LargeMtW_PassW[100];

 vector<TH1D *> v_Lep3Pt_ZFakeEl_LargeMtW_UnPassW;
 TH1D* Lep3Pt_ZFakeEl_LargeMtW_UnPassW[100];

 vector<TH1D *> v_Lep3Pt_ZFakeMu_LargeMtW_PassW;
 TH1D* Lep3Pt_ZFakeMu_LargeMtW_PassW[100];

 vector<TH1D *> v_Lep3Pt_ZFakeMu_LargeMtW_UnPassW;
 TH1D* Lep3Pt_ZFakeMu_LargeMtW_UnPassW[100];

 vector<TH1D *> v_Lep3Pt_ZFakeEl_LowMtW_PassW;
 TH1D* Lep3Pt_ZFakeEl_LowMtW_PassW[100];

 vector<TH1D *> v_Lep3Pt_ZFakeEl_LowMtW_UnPassW;
 TH1D* Lep3Pt_ZFakeEl_LowMtW_UnPassW[100];

 vector<TH1D *> v_Lep3Pt_ZFakeMu_LowMtW_PassW;
 TH1D* Lep3Pt_ZFakeMu_LowMtW_PassW[100];

 vector<TH1D *> v_Lep3Pt_ZFakeMu_LowMtW_UnPassW;
 TH1D* Lep3Pt_ZFakeMu_LowMtW_UnPassW[100];

 //Real Efficiency
 vector<TH1D *> v_Lep3Pt_RealEl_PassZ;
 TH1D* Lep3Pt_RealEl_PassZ[100];

 vector<TH1D *> v_Lep3Pt_RealEl_UnPassZ;
 TH1D* Lep3Pt_RealEl_UnPassZ[100];

 vector<TH1D *> v_Lep3Pt_RealEl_PassW;
 TH1D* Lep3Pt_RealEl_PassW[100];

 vector<TH1D *> v_Lep3Pt_RealEl_UnPassW;
 TH1D* Lep3Pt_RealEl_UnPassW[100];

 vector<TH1D *> v_Lep3Pt_RealEl_Loose;
 TH1D* Lep3Pt_RealEl_Loose[100];

 vector<TH1D *> v_Lep3Pt_RealMu_PassZ;
 TH1D* Lep3Pt_RealMu_PassZ[100];

 vector<TH1D *> v_Lep3Pt_RealMu_UnPassZ;
 TH1D* Lep3Pt_RealMu_UnPassZ[100];

 vector<TH1D *> v_Lep3Pt_RealMu_PassW;
 TH1D* Lep3Pt_RealMu_PassW[100];

 vector<TH1D *> v_Lep3Pt_RealMu_UnPassW;
 TH1D* Lep3Pt_RealMu_UnPassW[100];

 vector<TH1D *> v_Lep3Pt_RealMu_Loose;
 TH1D* Lep3Pt_RealMu_Loose[100];

 //Lepton2 Fake rate
 vector<TH1D *> v_Lep2Pt_ZFakeEl_PassZ;
 TH1D* Lep2Pt_ZFakeEl_PassZ[100];

 vector<TH1D *> v_Lep2Pt_ZFakeMu_PassZ;
 TH1D* Lep2Pt_ZFakeMu_PassZ[100];

 vector<TH1D *> v_Lep2Pt_TopEl_PassZ;
 TH1D* Lep2Pt_TopEl_PassZ[100];

 vector<TH1D *> v_Lep2Pt_TopMu_PassZ;
 TH1D* Lep2Pt_TopMu_PassZ[100];

 vector<TH1D *> v_Lep2Pt_ZFakeEl_UnPassZ;
 TH1D* Lep2Pt_ZFakeEl_UnPassZ[100];

 vector<TH1D *> v_Lep2Pt_ZFakeMu_UnPassZ;
 TH1D* Lep2Pt_ZFakeMu_UnPassZ[100];

 vector<TH1D *> v_Lep2Pt_TopEl_UnPassZ;
 TH1D* Lep2Pt_TopEl_UnPassZ[100];

 vector<TH1D *> v_Lep2Pt_TopMu_UnPassZ;
 TH1D* Lep2Pt_TopMu_UnPassZ[100];


 //Lepton3 MET distribution
 vector<TH1D *> v_Lep3MET_ZFakeEl_PassW;
 TH1D* Lep3MET_ZFakeEl_PassW[100];

 vector<TH1D *> v_Lep3MET_ZFakeEl_UnPassW;
 TH1D* Lep3MET_ZFakeEl_UnPassW[100];

 vector<TH1D *> v_Lep3MET_ZFakeMu_PassW;
 TH1D* Lep3MET_ZFakeMu_PassW[100];

 vector<TH1D *> v_Lep3MET_ZFakeMu_UnPassW;
 TH1D* Lep3MET_ZFakeMu_UnPassW[100];

 vector<TH2D *> v_Lep3PtMET_ZFakeEl_PassW;
 TH2D* Lep3PtMET_ZFakeEl_PassW[100];

 vector<TH2D *> v_Lep3PtMET_ZFakeEl_UnPassW;
 TH2D* Lep3PtMET_ZFakeEl_UnPassW[100];

 vector<TH2D *> v_Lep3PtMET_ZFakeMu_PassW;
 TH2D* Lep3PtMET_ZFakeMu_PassW[100];

 vector<TH2D *> v_Lep3PtMET_ZFakeMu_UnPassW;
 TH2D* Lep3PtMET_ZFakeMu_UnPassW[100];

 vector<TH1D *> v_AverageLep3Pt_Lep3MET_ZFakeEl_Loose_numer;
 TH1D* AverageLep3Pt_Lep3MET_ZFakeEl_Loose_numer[100];
 vector<TH1D *> v_AverageLep3Pt_Lep3MET_ZFakeEl_Loose_denom;
 TH1D* AverageLep3Pt_Lep3MET_ZFakeEl_Loose_denom[100];
 TH1D* AverageLep3Pt_Lep3MET_ZFakeEl_Loose;

 vector<TH1D *> v_AverageLep3Pt_Lep3MET_ZFakeMu_Loose_numer;
 TH1D* AverageLep3Pt_Lep3MET_ZFakeMu_Loose_numer[100];
 vector<TH1D *> v_AverageLep3Pt_Lep3MET_ZFakeMu_Loose_denom;
 TH1D* AverageLep3Pt_Lep3MET_ZFakeMu_Loose_denom[100];
 TH1D* AverageLep3Pt_Lep3MET_ZFakeMu_Loose;

 vector<TH1D *> v_AverageLep3Pt_Lep3MET_ZFakeEl_PassW_numer;
 TH1D* AverageLep3Pt_Lep3MET_ZFakeEl_PassW_numer[100];
 TH1D* AverageLep3Pt_Lep3MET_ZFakeEl_PassW;
 
 vector<TH1D *> v_AverageLep3Pt_Lep3MET_ZFakeMu_PassW_numer;
 TH1D* AverageLep3Pt_Lep3MET_ZFakeMu_PassW_numer[100];
 TH1D* AverageLep3Pt_Lep3MET_ZFakeMu_PassW;

 vector<TH1D *> v_AverageLep3Pt_Lep3MET_ZFakeEl_UnPassW_numer;
 TH1D* AverageLep3Pt_Lep3MET_ZFakeEl_UnPassW_numer[100];
 TH1D* AverageLep3Pt_Lep3MET_ZFakeEl_UnPassW;

 vector<TH1D *> v_AverageLep3Pt_Lep3MET_ZFakeMu_UnPassW_numer;
 TH1D* AverageLep3Pt_Lep3MET_ZFakeMu_UnPassW_numer[100];
 TH1D* AverageLep3Pt_Lep3MET_ZFakeMu_UnPassW;


 TH1D *ZElFakeRate;
 TH1D *ZMuFakeRate;
 TH1D *WElFakeRate;
 TH1D *WMuFakeRate;

 TH1D *ZElFakeRate_UpSys;
 TH1D *ZMuFakeRate_UpSys;
 TH1D *WElFakeRate_UpSys;
 TH1D *WMuFakeRate_UpSys;

 TH1D *ZElFakeRate_DownSys;
 TH1D *ZMuFakeRate_DownSys;
 TH1D *WElFakeRate_DownSys;
 TH1D *WMuFakeRate_DownSys;

 TH1D *ZElFakeEff;
 TH1D *ZMuFakeEff;
 TH1D *WElFakeEff;
 TH1D *WMuFakeEff;

 TH1D *ZElRealEff;
 TH1D *ZMuRealEff;
 TH1D *WElRealEff;
 TH1D *WMuRealEff;

 TH1D *ZFakeElCR_ZRate;
 TH1D *ZFakeElCR_UpSys_ZRate;
 TH1D *ZFakeElCR_DownSys_ZRate;
 TH1D *ZFakeElCR_WRate;
 TH1D *ZFakeElCR_UpSys_WRate;
 TH1D *ZFakeElCR_DownSys_WRate;
 TH1D *ZFakeMuCR_ZRate;
 TH1D *ZFakeMuCR_WRate;
 TH1D *TopElCR_ZRate;
 TH1D *TopElCR_WRate;
 TH1D *TopMuCR_ZRate;
 TH1D *TopMuCR_WRate;

 TH1D *ZFakeElCR_ZRate2;
 TH1D *ZFakeMuCR_ZRate2;
 TH1D *TopElCR_ZRate2;
 TH1D *TopMuCR_ZRate2;

 TH1D *ZFakeElCR_MET_WRate;
 TH1D *ZFakeMuCR_MET_WRate;

 TH2D *ZFakeElCR_PtMET_WRate;
 TH2D *ZFakeMuCR_PtMET_WRate;

 TH1D *ZFakeEl_LargeMtW;
 TH1D *ZFakeMu_LargeMtW;
 TH1D *ZFakeEl_LowMtW;
 TH1D *ZFakeMu_LowMtW;
 TH1D *ZFakeEl_Correction;
 TH1D *ZFakeMu_Correction;

 TH1D *Deep_DY_WZ_TT;
 TH1D *Deep_DY_3Z_TT;

 TH1D *Deep_DY_WZ_TT_DataSubtracted;
 TH1D *Deep_DY_3Z_TT_DataSubtracted;

 int M_WZ_bin = 20;          double M_WZ_left = 0.0;           double M_WZ_right = 500.0;
 int M_3l_bin = 20;          double M_3l_left = 0.0;           double M_3l_right = 200.0;
 int Mt_WZ_bin = 20;         double Mt_WZ_left = 0.0;          double Mt_WZ_right = 500.0;
 int M_Z_bin = 20;           double M_Z_left = 0.0;            double M_Z_right = 200.0;
 int Mt_W_bin = 20;          double Mt_W_left = 0.0;           double Mt_W_right = 200.0;
 int Met_bin = 40;           double Met_left = 20.0;           double Met_right = 420.0;
 int Njets_bin = 10;         double Njets_left = 0.0;          double Njets_right = 10.0;
 int NBjets_bin = 6;         double NBjets_left = 0.0;         double NBjets_right = 6.0;
 int Lep1Pt_bin = 40;        double Lep1Pt_left = 20.0;        double Lep1Pt_right = 420.0;
 int Lep2Pt_bin = 40;        double Lep2Pt_left = 20.0;        double Lep2Pt_right = 420.0;
 int Lep3Pt_bin = 40;        double Lep3Pt_left = 20.0;        double Lep3Pt_right = 420.0;
 int Lep1Eta_bin = 20;       double Lep1Eta_left = -2.5;       double Lep1Eta_right = 2.5;
 int Lep2Eta_bin = 20;       double Lep2Eta_left = -2.5;       double Lep2Eta_right = 2.5;
 int Lep3Eta_bin = 20;       double Lep3Eta_left = -2.5;       double Lep3Eta_right = 2.5;
 int Lep1Phi_bin = 20;       double Lep1Phi_left = -3.14;      double Lep1Phi_right = 3.14;
 int Lep2Phi_bin = 20;       double Lep2Phi_left = -3.14;      double Lep2Phi_right = 3.14;
 int Lep3Phi_bin = 20;       double Lep3Phi_left = -3.14;      double Lep3Phi_right = 3.14;
 int Mpx_bin = 20;           double Mpx_left = 0.0;            double Mpx_right = 200.0;
 int Mpy_bin = 20;           double Mpy_left = 0.0;            double Mpy_right = 200.0;
 int Mpz_bin = 20;           double Mpz_left = 0.0;            double Mpz_right = 200.0;
 int MpzDiff_bin = 20;       double MpzDiff_left = 0.0;        double MpzDiff_right = 200.0;
 int LWTNNpLL_bin = 20;      double LWTNNpLL_left = 0.0;       double LWTNNpLL_right = 1.0;
 int Pt_W_bin = 60;          double Pt_W_left = 0.0;           double Pt_W_right = 600.0;
 int Pt_Z_bin = 7;          double Pt_Z_left = 0.0;           double Pt_Z_right = 200.0;
 int Pt_WZ_bin = 20;         double Pt_WZ_left = 0.0;          double Pt_WZ_right = 200.0;
 int SumJetPt_bin = 20;      double SumJetPt_left = 0.0;       double SumJetPt_right = 200.0;
 int CosThetaV_bin = 40;     double CosThetaV_left = -1.0;     double CosThetaV_right = 1.0;
 int CosThetaVAbs_bin = 20;  double CosThetaVAbs_left = 0.0;   double CosThetaVAbs_right = 1.0;
 int CosThetaLepW_bin = 20;  double CosThetaLepW_left = -1.0;  double CosThetaLepW_right = 1.0;
 int CosThetaLepZ_bin = 20;  double CosThetaLepZ_left = -1.0;  double CosThetaLepZ_right = 1.0;
 int DY_WZ_bin = 10;         double DY_WZ_left = -6.0;         double DY_WZ_right = 6.0;
 int DY_3Z_bin = 20;         double DY_3Z_left = -6.0;         double DY_3Z_right = 6.0;
 int DY_3N_bin = 20;         double DY_3N_left = -6.0;         double DY_3N_right = 6.0;
 int Truth_Pt_WZ_bin = 20;   double Truth_Pt_WZ_left = 0.0;    double Truth_Pt_WZ_right = 200000.0;
 int DeltaPhi_bin = 20;      double DeltaPhi_left = 0.0;       double DeltaPhi_right = 3.14;
 int R21_bin = 20;           double R21_left = 0.0;            double R21_right = 1.0;
 int Channel_bin = 6;        double Channel_left = 0.0;        double Channel_right = 6.0;
 int BDTScore_bin = 20;      double BDTScore_left = 0.0;       double BDTScore_right = 1.0;
 int EtaDiff_bin = 5;        double EtaDiff_left = 0.0;        double EtaDiff_right = 2.5;

 double Pt_Z_binning[32] = {0, 20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 220, 240, 260, 280, 300, 320, 340, 360, 380, 400, 
                            420, 440, 460, 480, 500, 550, 600, 700, 800, 900, 1000};
// int FakeEl_bin = 5;         double FakeEl_binning[6] = {15, 25, 35, 60, 100, 1000};
// int FakeMu_bin = 5;         double FakeMu_binning[6] = {15, 25, 35, 60, 100, 1000};
// int FakeEl_bin = 9;         double FakeEl_binning[10] = {15, 20, 22, 24, 26, 28, 30, 35, 60, 1000};
// int FakeMu_bin = 9;         double FakeMu_binning[10] = {15, 20, 22, 24, 26, 28, 30, 35, 60, 1000};
// int FakeEl_bin = 4;         double FakeEl_binning[5] = {15, 25, 35, 60, 1000};
// int FakeMu_bin = 4;         double FakeMu_binning[5] = {15, 25, 35, 60, 1000};
// int FakeEl_bin = 1;         double FakeEl_binning[2] = {0, 10000};
// int FakeMu_bin = 1;         double FakeMu_binning[2] = {0, 10000};
 int FakeEl_bin = 4;         double FakeEl_binning[5] = {15, 20, 30, 50, 200};
 int FakeMu_bin = 4;         double FakeMu_binning[5] = {15, 20, 30, 50, 200};
 int FakeEl1_bin = 8;         double FakeEl1_binning[9] = {15, 20, 25, 30, 35, 40, 50, 70, 200};
 int FakeMu1_bin = 8;         double FakeMu1_binning[9] = {15, 20, 25, 30, 35, 40, 50, 70, 200};
// int FakeEl_bin = 5;         double FakeEl_binning[6] = {15, 20, 30, 50, 200, 10000};
// int FakeMu_bin = 5;         double FakeMu_binning[6] = {15, 20, 30, 50, 200, 10000};
// int FakeEl_bin = 8;         double FakeEl_binning[9] = {15, 20, 30, 50, 70, 90, 120, 160, 200};
// int FakeMu_bin = 8;         double FakeMu_binning[9] = {15, 20, 30, 50, 70, 90, 120, 160, 200};


// int FakeEl_bin = 3;         double FakeEl_binning[4] = {15, 60, 100, 10000};
// int FakeMu_bin = 3;         double FakeMu_binning[4] = {15, 60, 100, 10000};

// int FakeMET_bin = 10;       double FakeMET_binning[11] = {0, 5, 10, 25, 35, 45, 55, 70, 80, 100, 200};
// int FakeMET_bin = 2;       double FakeMET_binning[3] = {0, 25, 200};
 int FakeMET_bin = 8;       double FakeMET_binning[9] = {0, 10, 20, 30, 40, 50, 60, 70, 80};
// int FakeMET_bin = 2;       double FakeMET_binning[3] = {0, 30, 80};


 int FakeElCorrection_bin = 4;  double FakeElCorrection_binning[5] = {15, 20, 30, 50, 200};
 int FakeMuCorrection_bin = 4;  double FakeMuCorrection_binning[5] = {15, 20, 30, 50, 200};
// int FakeElCorrection_bin = 1;  double FakeElCorrection_binning[2] = {0, 10000};
// int FakeMuCorrection_bin = 1;  double FakeMuCorrection_binning[2] = {0, 10000};



 vector<ProcessHist *> hist_process;
 vector<TString> HistNames;

//member function
 vector<TString> sysName;
 HistsWZPolarization();
 virtual void bookHists(int TotalThread);
 virtual void bookProcessHists(TString ProcessName, TString subName);
 virtual void GetReweightFactor();
 virtual void GetFakeRate();
 virtual void LinkClass(HistsWZPolarization *hist);
 virtual void outputInformation();
 virtual void bookAllProcessHist(ProcessHist* &hist, TString name, TString title, int nbin, double left, double right);
 virtual void bookAllProcessHist(ProcessHist* &hist, TString name, TString title, int nbin, double* xbins);
 virtual void DefineHist(ProcessHist* &hist, TString name, TString title, int nbin, double left, double right);
 virtual void DefineHist(ProcessHist* &hist, TString name, TString title, int nbin, double* xbins);
 virtual void CustomMerge(int TotalThread, int notDelete = 0){};
 virtual void CustomReset(int TotalThread){};
 virtual void SaveCustomFakePlot();
 virtual void InputCustomFakeRate();
 virtual void outputTeXInfo();

// template<class T>

 vector<TString> SysNameSignalWZ;
 vector<TString> BkgNameSignalWZ;
 map<TString, int> SysNameIndexSignalWZ;
 map<TString, int> BkgNameIndexSignalWZ;

 vector<TString> BkgNameZZControlRegion;
 map<TString, int> BkgNameIndexZZControlRegion;
 vector<TString> BkgNameTopControlRegion;
 map<TString, int> BkgNameIndexTopControlRegion;
 vector<TString> BkgNameWZPolarized;
 map<TString, int> BkgNameIndexWZPolarized;

 TString SystematicName = "";

 void InputSystematicName(TString SystematicName);
 virtual int FindSysIndex(TString SysName, TString ProcessName);
 virtual void DefineSysName(int index, TString Name, TString ProcessName);
 virtual void InitialSysName(TString ProcessName);
 virtual void InitialBkgName(TString ProcessName);

 bool OnlyNominal = true;
 virtual void RunAllSystematic(){OnlyNominal = false;}

 bool doBkg = false;
 virtual void RunBkg(){doBkg = true;}

 bool doControlRegion = false;
 virtual void RunControlRegion(){doControlRegion = true;}

 int doFakeRate = 0;
 virtual void RunFakeRate(int doFakeRate){this->doFakeRate = doFakeRate;}

 bool isHaveFakeRate = false;

 bool isRunReweightingFactor = false;
 bool isHaveReweightingFactor = false;

 map<TString, ofstream> TeXInfo;
};

#ifdef HistsWZPolarization_cxx
HistsWZPolarization* FinalHists;
double Min_Chi2 = 1000000.0;
#endif
void ScaleFitting(Int_t &npbar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag);

#endif
