#ifndef __HISTS_WASYM_H_
#define __HISTS_WASYM_H_
#include <iostream>
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TFile.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TMath.h"
#include "TTree.h"
#include <vector>
#include "makeHists.h"

using namespace std;

class HistsWasym : public makeHists
{
 public:
//W Electron Event Plot 
 vector<TH1D *> v_DataWEvent_Ele_CutFlow;
 TH1D *DataWEvent_Ele_CutFlow[100];
 vector<SysHist *> v_WEvent_Ele_CutFlow_sys;
 SysHist *WEvent_Ele_CutFlow_sys[100];
 vector<BkgHist *> v_WEvent_Ele_CutFlow_bkg;
 BkgHist *WEvent_Ele_CutFlow_bkg[100];

 vector<TH1D *> v_DataWEvent_Posi_CutFlow;
 TH1D *DataWEvent_Posi_CutFlow[100];
 vector<SysHist *> v_WEvent_Posi_CutFlow_sys;
 SysHist *WEvent_Posi_CutFlow_sys[100];
 vector<BkgHist *> v_WEvent_Posi_CutFlow_bkg;
 BkgHist *WEvent_Posi_CutFlow_bkg[100];

 vector<TH1D *> v_TruthWEvent_Ele_eta;
 TH1D *TruthWEvent_Ele_eta[100];
 vector<TH1D *> v_DataWEvent_Ele_eta;
 TH1D *DataWEvent_Ele_eta[100];
 vector<SysHist *> v_WEvent_Ele_eta_sys;
 SysHist *WEvent_Ele_eta_sys[100];
 vector<BkgHist *> v_WEvent_Ele_eta_bkg;
 BkgHist *WEvent_Ele_eta_bkg[100];

 vector<TH1D *> v_TruthWEvent_Posi_eta;
 TH1D *TruthWEvent_Posi_eta[100];
 vector<TH1D *> v_DataWEvent_Posi_eta;
 TH1D *DataWEvent_Posi_eta[100];
 vector<SysHist *> v_WEvent_Posi_eta_sys;
 SysHist *WEvent_Posi_eta_sys[100];
 vector<BkgHist *> v_WEvent_Posi_eta_bkg;
 BkgHist *WEvent_Posi_eta_bkg[100];

 vector<TH1D *> v_DataWEvent_WenuPt_reco;
 TH1D *DataWEvent_WenuPt_reco[100];
 vector<SysHist *> v_WEvent_WenuPt_reco_sys;
 SysHist *WEvent_WenuPt_reco_sys[100];
 vector<BkgHist *> v_WEvent_WenuPt_reco_bkg;
 BkgHist *WEvent_WenuPt_reco_bkg[100];

//W Muon Event Plot
 vector<TH1D *> v_DataWEvent_Muon_CutFlow;
 TH1D *DataWEvent_Muon_CutFlow[100];
 vector<SysHist *> v_WEvent_Muon_CutFlow_sys;
 SysHist *WEvent_Muon_CutFlow_sys[100];
 vector<BkgHist *> v_WEvent_Muon_CutFlow_bkg;
 BkgHist *WEvent_Muon_CutFlow_bkg[100];

 vector<TH1D *> v_DataWEvent_AntiMuon_CutFlow;
 TH1D *DataWEvent_AntiMuon_CutFlow[100];
 vector<SysHist *> v_WEvent_AntiMuon_CutFlow_sys;
 SysHist *WEvent_AntiMuon_CutFlow_sys[100];
 vector<BkgHist *> v_WEvent_AntiMuon_CutFlow_bkg;
 BkgHist *WEvent_AntiMuon_CutFlow_bkg[100];

 vector<TH1D *> v_TruthWEvent_Muon_eta;
 TH1D *TruthWEvent_Muon_eta[100];
 vector<TH1D *> v_DataWEvent_Muon_eta;
 TH1D *DataWEvent_Muon_eta[100];
 vector<SysHist *> v_WEvent_Muon_eta_sys;
 SysHist *WEvent_Muon_eta_sys[100];
 vector<BkgHist *> v_WEvent_Muon_eta_bkg;
 BkgHist *WEvent_Muon_eta_bkg[100];

 vector<TH1D *> v_TruthWEvent_AntiMuon_eta;
 TH1D *TruthWEvent_AntiMuon_eta[100];
 vector<TH1D *> v_DataWEvent_AntiMuon_eta;
 TH1D *DataWEvent_AntiMuon_eta[100];
 vector<SysHist *> v_WEvent_AntiMuon_eta_sys;
 SysHist *WEvent_AntiMuon_eta_sys[100];
 vector<BkgHist *> v_WEvent_AntiMuon_eta_bkg;
 BkgHist *WEvent_AntiMuon_eta_bkg[100];

 vector<TH1D *> v_DataWEvent_WmunuPt_reco;
 TH1D *DataWEvent_WmunuPt_reco[100];
 vector<SysHist *> v_WEvent_WmunuPt_reco_sys;
 SysHist *WEvent_WmunuPt_reco_sys[100];
 vector<BkgHist *> v_WEvent_WmunuPt_reco_bkg;
 BkgHist *WEvent_WmunuPt_reco_bkg[100];

//Z Electron Event Plot
 vector<TH1D *> v_DataZEvent_Ele_CutFlow;
 TH1D *DataZEvent_Ele_CutFlow[100];
 vector<SysHist *> v_ZEvent_Ele_CutFlow_sys;
 SysHist *ZEvent_Ele_CutFlow_sys[100];
 vector<BkgHist *> v_ZEvent_Ele_CutFlow_bkg;
 BkgHist *ZEvent_Ele_CutFlow_bkg[100];

 vector<TH1D *> v_DataZEvent_Posi_CutFlow;
 TH1D *DataZEvent_Posi_CutFlow[100];
 vector<SysHist *> v_ZEvent_Posi_CutFlow_sys;
 SysHist *ZEvent_Posi_CutFlow_sys[100];
 vector<BkgHist *> v_ZEvent_Posi_CutFlow_bkg;
 BkgHist *ZEvent_Posi_CutFlow_bkg[100];

 vector<TH1D *> v_DataZEvent_Ele_eta;
 TH1D *DataZEvent_Ele_eta[100];
 vector<SysHist *> v_ZEvent_Ele_eta_sys;
 SysHist *ZEvent_Ele_eta_sys[100];
 vector<BkgHist *> v_ZEvent_Ele_eta_bkg;
 BkgHist *ZEvent_Ele_eta_bkg[100];

 vector<TH1D *> v_DataZEvent_Posi_eta;
 TH1D *DataZEvent_Posi_eta[100];
 vector<SysHist *> v_ZEvent_Posi_eta_sys;
 SysHist *ZEvent_Posi_eta_sys[100];
 vector<BkgHist *> v_ZEvent_Posi_eta_bkg;
 BkgHist *ZEvent_Posi_eta_bkg[100];

 vector<TH1D *> v_DataZEvent_ZeeEvent_CutFlow;
 TH1D *DataZEvent_ZeeEvent_CutFlow[100];
 vector<SysHist *> v_ZEvent_ZeeEvent_CutFlow_sys;
 SysHist *ZEvent_ZeeEvent_CutFlow_sys[100];
 vector<BkgHist *> v_ZEvent_ZeeEvent_CutFlow_bkg;
 BkgHist *ZEvent_ZeeEvent_CutFlow_bkg[100];

 vector<TH1D *> v_DataZEvent_ZeeMass_reco;
 TH1D *DataZEvent_ZeeMass_reco[100];
 vector<SysHist *> v_ZEvent_ZeeMass_reco_sys;
 SysHist *ZEvent_ZeeMass_reco_sys[100];
 vector<BkgHist *> v_ZEvent_ZeeMass_reco_bkg;
 BkgHist *ZEvent_ZeeMass_reco_bkg[100];

 vector<TH1D *> v_DataZEvent_ZeePt_reco;
 TH1D *DataZEvent_ZeePt_reco[100];
 vector<SysHist *> v_ZEvent_ZeePt_reco_sys;
 SysHist *ZEvent_ZeePt_reco_sys[100];
 vector<BkgHist *> v_ZEvent_ZeePt_reco_bkg;
 BkgHist *ZEvent_ZeePt_reco_bkg[100];

 vector<TH1D *> v_DataZEvent_ZeeRapidity_reco;
 TH1D *DataZEvent_ZeeRapidity_reco[100];
 vector<SysHist *> v_ZEvent_ZeeRapidity_reco_sys;
 SysHist *ZEvent_ZeeRapidity_reco_sys[100];
 vector<BkgHist *> v_ZEvent_ZeeRapidity_reco_bkg;
 BkgHist *ZEvent_ZeeRapidity_reco_bkg[100];

//Z Muon Event Plot
 vector<TH1D *> v_DataZEvent_Muon_CutFlow;
 TH1D *DataZEvent_Muon_CutFlow[100];
 vector<SysHist *> v_ZEvent_Muon_CutFlow_sys;
 SysHist *ZEvent_Muon_CutFlow_sys[100];
 vector<BkgHist *> v_ZEvent_Muon_CutFlow_bkg;
 BkgHist *ZEvent_Muon_CutFlow_bkg[100];

 vector<TH1D *> v_DataZEvent_AntiMuon_CutFlow;
 TH1D *DataZEvent_AntiMuon_CutFlow[100];
 vector<SysHist *> v_ZEvent_AntiMuon_CutFlow_sys;
 SysHist *ZEvent_AntiMuon_CutFlow_sys[100];
 vector<BkgHist *> v_ZEvent_AntiMuon_CutFlow_bkg;
 BkgHist *ZEvent_AntiMuon_CutFlow_bkg[100];

 vector<TH1D *> v_DataZEvent_Muon_eta;
 TH1D *DataZEvent_Muon_eta[100];
 vector<SysHist *> v_ZEvent_Muon_eta_sys;
 SysHist *ZEvent_Muon_eta_sys[100];
 vector<BkgHist *> v_ZEvent_Muon_eta_bkg;
 BkgHist *ZEvent_Muon_eta_bkg[100];

 vector<TH1D *> v_DataZEvent_AntiMuon_eta;
 TH1D *DataZEvent_AntiMuon_eta[100];
 vector<SysHist *> v_ZEvent_AntiMuon_eta_sys;
 SysHist *ZEvent_AntiMuon_eta_sys[100];
 vector<BkgHist *> v_ZEvent_AntiMuon_eta_bkg;
 BkgHist *ZEvent_AntiMuon_eta_bkg[100];

 vector<TH1D *> v_DataZEvent_ZmmEvent_CutFlow;
 TH1D *DataZEvent_ZmmEvent_CutFlow[100];
 vector<SysHist *> v_ZEvent_ZmmEvent_CutFlow_sys;
 SysHist *ZEvent_ZmmEvent_CutFlow_sys[100];
 vector<BkgHist *> v_ZEvent_ZmmEvent_CutFlow_bkg;
 BkgHist *ZEvent_ZmmEvent_CutFlow_bkg[100];

 vector<TH1D *> v_DataZEvent_ZmmMass_reco;
 TH1D *DataZEvent_ZmmMass_reco[100];
 vector<SysHist *> v_ZEvent_ZmmMass_reco_sys;
 SysHist *ZEvent_ZmmMass_reco_sys[100];
 vector<BkgHist *> v_ZEvent_ZmmMass_reco_bkg;
 BkgHist *ZEvent_ZmmMass_reco_bkg[100];

 vector<TH1D *> v_DataZEvent_ZmmPt_reco;
 TH1D *DataZEvent_ZmmPt_reco[100];
 vector<SysHist *> v_ZEvent_ZmmPt_reco_sys;
 SysHist *ZEvent_ZmmPt_reco_sys[100];
 vector<BkgHist *> v_ZEvent_ZmmPt_reco_bkg;
 BkgHist *ZEvent_ZmmPt_reco_bkg[100];

 vector<TH1D *> v_DataZEvent_ZmmRapidity_reco;
 TH1D *DataZEvent_ZmmRapidity_reco[100];
 vector<SysHist *> v_ZEvent_ZmmRapidity_reco_sys;
 SysHist *ZEvent_ZmmRapidity_reco_sys[100];
 vector<BkgHist *> v_ZEvent_ZmmRapidity_reco_bkg;
 BkgHist *ZEvent_ZmmRapidity_reco_bkg[100];


 vector<TString> sysNameWenu;
 vector<TString> sysNameWmunu;
 vector<TString> sysNameZee;
 vector<TString> sysNameZmm;
 map<TString, int> SysNameWenuIndex;
 map<TString, int> SysNameWmunuIndex;
 map<TString, int> SysNameZeeIndex;
 map<TString, int> SysNameZmmIndex;

 vector<TString> bkgNameWenu;
 vector<TString> bkgNameWmunu;
 vector<TString> bkgNameZee;
 vector<TString> bkgNameZmm;
 map<TString, int> NameIndexWenu;
 map<TString, int> NameIndexWmunu;
 map<TString, int> NameIndexZee;
 map<TString, int> NameIndexZmm;

 bool OnlyNominal = true;
 virtual void RunAllSystematic();


 virtual int FindSysIndex(TString SysName, TString ProcessName);
 virtual void DefineSysName(int index, TString Name, TString ProcessName);

//member function
 virtual void InitialBkgName(TString ProcessName);

 virtual void InitialSysName(TString ProcessName);
 virtual void bookHists(int TotalThread);
 virtual void bookProcessHists(TString ProcessName, TString subName);

// template<class T>

};
#endif
