#ifndef LinkDef_h
#define LinkDef_h
#pragma link C++ class vector<float> +;
#pragma link C++ class vector<vector<float> >+;
#pragma link C++ class vector<int> +;
#pragma link C++ class vector<vector<int> >+;
#pragma link C++ class vector<double> +;
#pragma link C++ class vector<vector<double> >+;
#endif
