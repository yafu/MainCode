#ifndef MCFMWWBoost_h
#define MCFMWWBoost_h

#include "ReadInRoot/ReadInRoot.h"
#include "HistsResBos.h"

using namespace std;

class MCFMWWBoost : public ReadInRoot
{
 public:

 HistsResBos* myhists;

 double DataPoint;
 double TheoryPoint;
 double Uncertainty;
 double Residue;

 int TotalBin = 0;
 int DataType;
 double *BinCenter;
 double *Binning;
 double *DeltaBin;

 int iplot = 0;
 TH1D* DataHist[100];
 TH1D* TheoryHist[100];

 int iplot2D = 0;
 TH2D* Data2D[100];
 TH2D* Theory2D[100];

 TH1D* BoostAsymmetry;
 TH1D* BoostAsymmetryW;

 TString subName = "";

 void LinkHist(HistsResBos* &myhists);
 void WriteIn(TString VariableName);
 void GetBinning(const char* name, TString VariableName);
 void GetAdditionalPlot();
 void InputSubName(TString subName){this->subName = subName;};
};

#endif
