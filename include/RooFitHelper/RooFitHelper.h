#ifndef RooFitHelper_h
#define RooFitHelper_h

#include <iostream>
#include "RootCommon.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooGaussian.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include "TAxis.h"
#include "RooFitResult.h"
#include "RooGenericPdf.h"
#include "RooConstVar.h"
#include "RooTFnBinding.h"

using namespace RooFit;
using namespace std;

class RooFitHelper
{
 public:

#ifdef USE_ROOFIT

//RooFit-related variable defined here
 int npar = 0;
 RooRealVar* myVar[100];



#endif


 RooFitHelper();
 virtual ~RooFitHelper(){};

#ifdef USE_ROOFIT
 virtual void FitGaussian(RooDataSet *data);
 virtual void FitGaussian(TH1D *h1);
 virtual void Input(int n, TString name, TString title, double center, double lower, double upper);
 virtual void Input(int n, RooRealVar *var);
 virtual void FitFunction(TString func, RooArgSet var, RooDataSet *data);
 virtual void FitFunction(TString func, RooArgSet var, TH1D *h1);
 virtual void PlotFunction(TF1* f1, double left, double right);

#endif

};
#endif
