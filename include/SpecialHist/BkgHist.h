#ifndef BkgHist_h
#define BkgHist_h

#include "TH1D.h"
#include "TString.h"
#include <vector>
#include <iostream>
#include <map>
#include "SpecialHist/SysHist.h"
#include "Tools/Tools.h"

using namespace std;

class BkgHist
{
 public:

 int MCType;

 vector<TString> bkgName;
 map<TString, int> NameIndex;

 TString RequiredProcessName;
 vector<TString> UsedList;
 map<int, int> ListFlag;

 vector<TString> sysName;

 TString HistName;

 TH1D* myHist[1000];
 SysHist* mySysHist[1000];

 vector<TH1D *> BkgHist_1d;
 vector<SysHist *> BkgHist_sys;

 vector<TString> AdditionalName;
 map<TString, TH1D *> AdditionalHist;

 double KFactor = 1.0;
 double KFactors[1000] = {1.0};

 bool isWplusenuEvent = false;
 bool isWplusmunuEvent = false;
 bool isWplustaonuEvent = false;
 bool isWminusenuEvent = false;
 bool isWminusmunuEvent = false;
 bool isWminustaonuEvent = false;
 bool isZeeEvent = false;
 bool isZmumuEvent = false;
 bool isData = false;

 int iBkg = -1;
 int iWHist = -1;

 bool NoBkgHist = false;

 TString ProcessName;
 TString ShortName;

 BkgHist(TString ProcessName, vector<TString> bkgName, map<TString, int> NameIndex);
 virtual void DefineHist(TString name, TString title, int nbin, double left, double right, int iflag = 1);
 virtual void DefineHist(TString name, TString title, int nbin, double* xbins, int iflag = 1);
 virtual void DefineSysHist(vector<TString> sysName, TString name, TString title, int nbin, double left, double right, int iflag = 1);
 virtual void DefineSysHist(vector<TString> sysName, TString name, TString title, int nbin, double* xbins, int iflag = 1);
 virtual void DefineAdditionalName();
 virtual void DefineAdditionalHist(TString name, TString title, int nbin, double left, double right);
 virtual void DefineAdditionalHist(TString name, TString title, int nbin, double* xbins);
 virtual void Initial(int MCType);
 virtual void GetRequiredList(TString ProcessName);
 virtual void Fill(double par, double weight);
 virtual void FillSys(int isys, double par, double weight);
 virtual void InputKFactor(double KFactor);
 TH1D* GetHistPtr(TString name);
 virtual void ResetHist();
 virtual void DeleteHist();
 virtual void SaveHist();
 virtual void ResetSysHist();
 virtual void DeleteSysHist();
 virtual void SaveSysHist();

};
#endif
