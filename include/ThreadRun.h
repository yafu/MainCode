#ifndef ThreadRun_h
#define ThreadRun_h

#include <iostream>
#include <fstream>
#include <pthread.h>
#include "TLorentzVector.h"
#include "TFile.h"
#include "Tools/Tools.h"
#include "Tools/Log.h"
#include "Tools/SumOfWeightHelper.h"
#include "TMinuitHelper/TMinuitHelper.h"
#include "RootCommon.h"
#include "Calibration/Calibration.h"

#include "control.h"
#include "Common.h"
#include "Externs.h"

#include "Wasymmetry/loopWasym.h"
#include "HistsWasym.h"
#include "TreeForWasym.h"

#include "loopResBos.h"
#include "HistsResBos.h"
#include "loopResBosW.h"
#include "HistsResBosW.h"
#include "loopResBosWTev.h"
#include "HistsResBosWTev.h"
#include "TreeForResBos.h"

#include "loopPythia.h"
#include "HistsPythia.h"
#include "TreeForPythia.h"

#include "loopPythiaW.h"
#include "HistsPythiaW.h"
#include "TreeForPythiaW.h"

#include "loopPythiaWW.h"
#include "HistsPythiaWW.h"
#include "TreeForPythiaWW.h"

#include "loopHerwig.h"
#include "HistsHerwig.h"
#include "TreeForHerwig.h"

#include "loopFwdReco.h"
#include "HistsFwdReco.h"
#include "TreeForFwdReco.h"

#include "WZPolarization/loopWZPolarization.h"
#include "HistsWZPolarization.h"
#include "TreeForWZPolarization.h"
#include "TreeForPreWZPolarization.h"

#include "loopCustom.h"
#include "HistsCustom.h"
#include "TreeForCustom.h"

#include "loopCellInfo.h"
#include "HistsCellInfo.h"
#include "TreeForCellInfo.h"

#include "loopSherpaWW.h"
#include "HistsSherpaWW.h"
#include "TreeForSherpaWW.h"

#include "loopResBosCMS.h"
#include "HistsResBosCMS.h"
#include "TreeForResBosCMS.h"

#include "loopResBosD0.h"
#include "HistsResBosD0.h"
#include "TreeForResBosD0.h"

#include "loopFlavorAsym.h"
#include "HistsFlavorAsym.h"
#include "TreeForFlavorAsym.h"

#include "loopResBosHighMassZ.h"
#include "HistsResBosHighMassZ.h"
#include "TreeForResBosHighMassZ.h"

#include "loopMadGraph.h"
#include "HistsMadGraph.h"
#include "TreeForMadGraph.h"

void ThreadRunWasym(Control process);
void ThreadRunResBos(Control process);
void ThreadRunResBosW(Control process);
void ThreadRunResBosWTev(Control process);
void ThreadRunResBosCMS(Control process);
void ThreadRunResBosD0(Control process);
void ThreadRunPythia(Control process, HistsPythia* myhists, int prerunFlag = 0);
void ThreadRunPythiaW(Control process);
void ThreadRunPythiaWW(Control process);
void ThreadRunHerwig(Control process);
void ThreadRunFwdReco(Control process);
void ThreadRunResBosEnergyScale(Control process);
void ThreadRunWZPolarization(Control process, HistsWZPolarization* myhists);
void ThreadRunPreWZPolarization(Control process, HistsWZPolarization* myhists);
void ThreadRunCustom(Control process);
void ThreadRunCellInfo(Control process);
void ThreadRunSherpaWW(Control process);
void ThreadRunFlavorAsym(Control process);
void ThreadRunResBosHighMassZ(Control process);
void ThreadRunMadGraph(Control process);

void CondorRunWasym(Control process);
void CondorRunResBos(Control process);
void CondorRunResBosW(Control process);
void CondorRunResBosWTev(Control process);
void CondorRunResBosCMS(Control process);
void CondorRunResBosD0(Control process);
void CondorRunPythia(Control process, HistsPythia* myhists, int prerunFlag = 0);
void CondorRunPythiaW(Control process);
void CondorRunPythiaWW(Control process);
void CondorRunHerwig(Control process);
void CondorRunFwdReco(Control process);
void CondorRunResBosEnergyScale(Control process);
void CondorRunWZPolarization(Control process, HistsWZPolarization* myhists);
void CondorRunPreWZPolarization(Control process, HistsWZPolarization* myhists);
void CondorRunCustom(Control process);
void CondorRunCellInfo(Control process);
void CondorRunSherpaWW(Control process);
void CondorRunFlavorAsym(Control process);
void CondorRunResBosHighMassZ(Control process);
void CondorRunMadGraph(Control process);

#endif
