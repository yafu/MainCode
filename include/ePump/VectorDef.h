#ifndef VDHDR
#define VDHDR

using namespace std;

#include <math.h>
#include <vector>

typedef vector<double> d_vec;
typedef vector<d_vec> d_mat;
typedef vector<d_mat> d_mat_list;
typedef vector<int> i_vec;

#endif


