#ifndef ePumpHelper_ePumpHelper_h
#define ePumpHelper_ePumpHelper_h

#include "RootCommon.h"
#include "ePump/ePump.h"

class ePump;

class ePumpHelper
{
 public:

 TString FileName;

 ePump* EU;

 int NEV;
 vector<TString> AllDataNames;

 map<TString, int> DataIndex;
 map<TString, int> DataErrorType;
 map<TString, double> DataWeight;
 map<TString, bool> DataIncluded;

 map<TString, int> DataNPoints;
 map<TString, vector<double>> DataPoint;
 map<TString, vector<double>> DataTotalErr;

 map<TString, vector<vector<double>>> TheoryTemplate;

 map<TString, vector<double>> OldTheory;
 map<TString, vector<double>> NewTheory;
 map<TString, vector<double>> OldPDFError;
 map<TString, vector<double>> NewPDFError;
 map<vector<TString>, vector<vector<double>>> OldPDFCorrelation;
 map<vector<TString>, vector<vector<double>>> NewPDFCorrelation;

 map<TString, double> OldChi2;
 map<TString, double> NewChi2;
 map<TString, double> OldSpartyness;
 map<TString, double> NewSpartyness;

 map<TString, vector<vector<double>>> NewTheoryWeightFunction;
 map<TString, vector<vector<double>>> NewPDFErrorWeightFunction;
 map<TString, vector<double>> NewChi2WeightFunction;
 map<TString, vector<double>> NewChi2NptWeightFunction;
 map<TString, vector<double>> NewSpartynessWeightFunction;
 map<TString, vector<TH1D *>> NewTheoryWeightHist;
 map<TString, vector<TH1D *>> NewPDFErrorWeightHist;
 map<TString, TH1D *> NewChi2WeightHist;
 map<TString, TH1D *> NewChi2NptWeightHist;
 map<TString, TH1D *> NewSpartynessWeightHist;

 ePumpHelper(const char* filename);
 virtual void Initial();
 virtual void Update();

 virtual void ProcessWeightFunction(vector<TString> DataNames);

 inline double GetTheoryTemplate(TString DataName, int i, int iPDF);
 inline double GetOldTheory(TString DataName, int i);
 inline double GetNewTheory(TString DataName, int i);
 inline double GetOldPDFError(TString DataName, int i);
 inline double GetNewPDFError(TString DataName, int i);
 inline double GetOldRelativePDFError(TString DataName, int i);
 inline double GetNewRelativePDFError(TString DataName, int i);
 inline double GetOldPDFCorrelation(TString DataName1, TString DataName2, int i, int j);
 inline double GetNewPDFCorrelation(TString DataName1, TString DataName2, int i, int j);
 inline double GetOldChi2(TString DataName);
 inline double GetNewChi2(TString DataName);

 inline vector<double> GetNewTheoryWeightFunction(TString DataName, int i);
 inline vector<double> GetNewPDFErrorWeightFunction(TString DataName, int i);

 virtual void ConstructDataHist(TString DataName, TH1D* &Data);
 virtual void ConstructTheoryHist(TString DataName, vector<TH1D *> &Theory);
 virtual void ConstructOldPDFErrorHist(TString DataName, TString subName, TH1D* &Hist, TString type = "");
 virtual void ConstructNewPDFErrorHist(TString DataName, TString subName, TH1D* &Hist, TString type = "");

 TH1D* ConstructDataHist(TString DataName);
 vector<TH1D *> ConstructTheoryHist(TString DataName);
 TH1D* ConstructOldPDFErrorHist(TString DataName, TString subName, TString type = "");
 TH1D* ConstructNewPDFErrorHist(TString DataName, TString subName, TString type = "");

 virtual void SaveAllHist();

 virtual void OutputResult(TString DataName);

 virtual void SetDataWeight(TString DataName, double w);
 virtual void ResetWeight();

 virtual void TurnOnData(TString DataName);
 virtual void ResetData();

 virtual void ResetUpdate();

 bool DoSaveHist = false;
 virtual void SaveHist();

 virtual void TurnOffUpdatePDF();

 bool doCorrelationCosine = true;
 virtual void TurnOffCorrelationCosine();

 TFile* hf;
 bool isLinkFile = false;
 virtual void LinkFile(TFile* file){isLinkFile = true;this->hf = file;}
};
#endif
