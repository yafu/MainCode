#ifndef loopMC_h
#define loopMC_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "TMath.h"
#include "TH1F.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include "TLorentzVector.h"
#include "TH3F.h"
#include <TRandom3.h>
#include <TMinuit.h>
#include <TApplication.h>
#include "TEnv.h"
#include <TComplex.h>
#include "TH2D.h"
#include "TH3D.h"
#include "TF1.h"
#include "TLegend.h"
#include <TProfile.h>
#include "TH1D.h"
#include <pthread.h>
#include <mutex>

#include "makeHists.h"

#include "Tools/Tools.h"
#include "PDFHelper/PDFReweight.h"
#include "ElectroweakTheory/ResBosEW.h"

using namespace std;

class loopMC
{
 public:
 Int_t Sta_TotalNumber;

 vector<TString> RootNames;
 TString RootType;

 vector<int> RootTypes;
 vector<double> CrossSections;
 vector<double> SumOfWeights;
 vector<int> Years;
 vector<TString> RootTypesForResBosW;

 map<pair<int, int>, double> SumOfWeightsMap;

 double TotalWeight;

 int TotalThread;
 int iThread;
 bool isThread;

 int NEvents;
 int TotalEntries[1000];

 int InitialEntry;
 int Nentries;

 int FirstFile, EndFile;
 bool DivideFile = false;

 bool isTest = false;

 vector<TString> CutInfo;
 vector<double> CutLeft;
 vector<double> CutRight;

 double PDFReweightFactor = 1.0;
 bool isPDFReweight = false;
 TString oldPDFName = "";
 TString newPDFName = "";
 int oldPDFset = 0;
 int newPDFset = -1;
 PDFReweight *myPDFReweight;

 loopMC();
 virtual void InputThreadInfo(int TotalThread, int iThread);
 virtual void Initial(int ifile);
 virtual void OutputFileInfo();
 virtual void ConfigCut(const char* CutFile);
 virtual void Test() {isTest = true;}
 virtual void InputPDFReweightInfo(const char* oldPDFName, const char* newPDFName, int oldPDFset, int newPDFset);
 virtual void InitialPDFInfo(const char* oldPDFName, int oldPDFset);
 virtual void End();
 virtual ~loopMC();

};
#endif
