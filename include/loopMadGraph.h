#ifndef loopMadGraph_h
#define loopMadGraph_h

#include "loopMC.h"

#include "HistsMadGraph.h"
#include "TreeForMadGraph.h"

using namespace std;

class loopMadGraph : public loopMC
{
 public:

 TLorentzVector Electron;
 TLorentzVector Positron;
 TLorentzVector ZBoson;

 double CrossSection = 0.0;
 int MadGraphNEvent = 0;

 HistsMadGraph *myhists;

 TreeForMadGraph *MadGraphTree;

 bool isCut = false;

 loopMadGraph(TString RootType);
 virtual void InputHist(HistsMadGraph* &myhists);
 virtual void InputTree(TreeForMadGraph *MadGraphTree);
 virtual void ReadTree(int ifile);
 virtual void End(int RootNumber);
 virtual void Finish();
 virtual ~loopMadGraph();

};
#endif
