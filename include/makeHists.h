#ifndef _MAKE_HISTS_H_
#define _MAKE_HISTS_H_
#include <iostream>
#include <fstream>
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TGraph.h"
#include "TFile.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TMath.h"
#include "TTree.h"
#include <vector>
#include <string>
#include <iomanip>
#include <sstream>
#include <map>
#include "Tools/Tools.h"
#include "Tools/Log.h"
#include "SpecialHist/AngularFunction.h"
#include "SpecialHist/ResolutionHist.h"
#include "SpecialHist/SysHist.h"
#include "SpecialHist/BkgHist.h"
#include "SpecialHist/CovHist.h"
#include "SpecialHist/FakeHist.h"
#include "SpecialHist/MultiBinHist.h"

using namespace std;

class makeHists
{
 public:

 TString RootType;

 vector<vector<TH1D *>> hist_1d;
 vector<vector<TH2D *>> hist_2d;
 vector<vector<TH3D *>> hist_3d;

 vector<vector<AngularFunction *>> hist_angular;
 vector<vector<ResolutionHist *>> hist_resolution;

 vector<vector<SysHist *>> SysHist_1d;
 vector<vector<BkgHist *>> BkgHist_1d;
 vector<vector<CovHist *>> CovHist_1d;
 vector<vector<BkgHist *>> BkgSysHist_1d;
 vector<vector<FakeHist *>> FakeHist_1d;
 vector<vector<MultiBinHist *>> MultiBinHist_1d;

 TFile *hf;

 TString subName = "";

 bool HaveMerged;

 bool isSaveCustomFakePlot = false;

 ofstream outputfile;
 Logstream myLog;
 virtual void LinkLog(Logstream log){this->myLog = log;}

 makeHists();
 virtual void openFile(const char* fName);
 virtual void bookHists(int TotalThread);
 virtual void MergeHists(int TotalThread, int notDelete = 0);
 virtual void ResetHists(int TotalThread);
 virtual void SaveAllHists(int TotalThread);
 virtual void saveHists();
 virtual void GetPartOfResults(){};
 virtual void outputInformation(){};
 virtual void CustomMerge(int TotalThread, int notDelete = 0){};
 virtual void CustomReset(int TotalThread){};
 virtual void SaveCustomFakePlot(){};

// template<class T>
 virtual void FillHists(int i, vector<TH1D *> &HistVector, TH1D* &Hist, TString name, TString title, int nbin, double left, double right);
 virtual void FillHists(int i, vector<TH1D *> &HistVector, TH1D* &Hist, TString name, TString title, int nbin, double* xbins);
 virtual void FillHists(int i, vector<TH2D *> &HistVector, TH2D* &Hist, TString name, TString title, int nbinx, double leftx, double rightx, int nbiny, double lefty, double righty);
 virtual void FillHists(int i, vector<TH2D *> &HistVector, TH2D* &Hist, TString name, TString title, int nbinx, double* xbins, int nbiny, double* ybins);
 virtual void FillHists(int i, vector<TH3D *> &HistVector, TH3D* &Hist, TString name, TString title, int nbinx, double leftx, double rightx, int nbiny, double lefty, double righty, int nbinz, double leftz, double rightz);
 virtual void FillHists(int i, vector<TH3D *> &HistVector, TH3D* &Hist, TString name, TString title, int nbinx, double* xbins, int nbiny, double* ybins, int nbinz, double* zbins);
 virtual void FillHists(int i, vector<AngularFunction *> &HistVector, AngularFunction* &Hist, TString name, TString type, int nbin, double left, double right);
 virtual void FillHists(int i, vector<AngularFunction *> &HistVector, AngularFunction* &Hist, TString name, TString type, int nbin, double* xbins);
 virtual void FillHists(int i, vector<AngularFunction *> &HistVector, AngularFunction* &Hist, TString name, TString type, int nbinx, double* xbins, int nbiny, double* ybins);
 virtual void FillHists(int i, vector<AngularFunction *> &HistVector, AngularFunction* &Hist, TString name, TString type, int nbinx, double* xbins, int nbiny, double* ybins, int nbinz, double* zbins);
 virtual void FillHists(int i, vector<ResolutionHist *> &HistVector, ResolutionHist* &Hist, TString name, TString type, int nbin, double left, double right);
 virtual void FillHists(int i, vector<ResolutionHist *> &HistVector, ResolutionHist* &Hist, TString name, TString type, int nbin, double* xbins);
 virtual void FillSysHists(int i, vector<TString> sysName, vector<SysHist *> &HistVector, SysHist* &Hist, TString name, TString title, int nbin, double left, double right);

 virtual void FillSysHists(int i, vector<TString> sysName, vector<SysHist *> &HistVector, SysHist* &Hist, TString name, TString title, int nbin, double* xbins);

 virtual void FillBkgHists(int i, TString ProcessName, vector<TString> bkgName, map<TString, int> NameIndex, vector<BkgHist *> &HistVector, BkgHist* &Hist, TString name, TString title, int nbin, double left, double right, int iflag = 1);
 virtual void FillBkgHists(int i, TString ProcessName, vector<TString> bkgName, map<TString, int> NameIndex, vector<BkgHist *> &HistVector, BkgHist* &Hist, TString name, TString title, int nbin, double* xbins, int iflag = 1);
 virtual void FillBkgSysHists(int i, TString ProcessName, vector<TString> bkgName, map<TString, int> NameIndex, vector<TString> sysName, vector<BkgHist *> &HistVector, BkgHist* &Hist, TString name, TString title, int nbin, double left, double right, int iflag = 1);
 virtual void FillBkgSysHists(int i, TString ProcessName, vector<TString> bkgName, map<TString, int> NameIndex, vector<TString> sysName, vector<BkgHist *> &HistVector, BkgHist* &Hist, TString name, TString title, int nbin, double* xbins, int iflag = 1);
 virtual void FillCovHists(int i, vector<CovHist *> &HistVector, CovHist* &Hist, TString name, TString title, int nbin, double left, double right);
 virtual void FillFakeHists(int i, vector<FakeHist *> &HistVector, FakeHist* &Hist, TString name, TString title, int nbin, double left, double right);
 virtual void FillFakeHists(int i, vector<FakeHist *> &HistVector, FakeHist* &Hist, TString name, TString title, int nbin, double* xbins);

 virtual void FillMultiBinHists(int i, vector<vector<double>> MultiBinVector, vector<MultiBinHist *> &HistVector, MultiBinHist* &Hist, TString name, TString title, int nbin, double* xbins);

 int TotalThread;

};

#define BookHist(i, v_hist, hist, name, title, nbin, left, right)						\
  do														\
  {														\
   FillHists(i, v_hist, hist, (TString)name + subName + tail, (TString)name + subName + tail, nbin, left, right);			\
  } while(0);

#define BookHist1(i, v_hist, hist, name, title, nbin, xbins)                                               \
  do                                                                                                            \
  {                                                                                                             \
   FillHists(i, v_hist, hist, (TString)name + subName + tail, (TString)name + subName + tail, nbin, xbins);                       \
  } while(0);

#define Book2DHist(i, v_hist, hist, name, title, nbinx, leftx, rightx, nbiny, lefty, righty)                    \
  do                                                                                                            \
  {                                                                                                             \
   FillHists(i, v_hist, hist, (TString)name + subName + tail, (TString)name + subName + tail, nbinx, leftx, rightx, nbiny, lefty, righty);\
  } while(0);

#define Book2DHist1(i, v_hist, hist, name, title, nbinx, xbins, nbiny, ybins)                    \
  do                                                                                                            \
  {                                                                                                             \
   FillHists(i, v_hist, hist, (TString)name + subName + tail, (TString)name + subName + tail, nbinx, xbins, nbiny, ybins);\
  } while(0);

#define Book3DHist(i, v_hist, hist, name, title, nbinx, leftx, rightx, nbiny, lefty, righty, nbinz, leftz, rightz)                    \
  do                                                                                                            \
  {                                                                                                             \
   FillHists(i, v_hist, hist, (TString)name + subName + tail, (TString)name + subName + tail, nbinx, leftx, rightx, nbiny, lefty, righty, nbinz, leftz, rightz);\
  } while(0);

#define Book3DHist1(i, v_hist, hist, name, title, nbinx, xbins, nbiny, ybins, nbinz, zbins)                    \
  do                                                                                                            \
  {                                                                                                             \
   FillHists(i, v_hist, hist, (TString)name + subName + tail, (TString)name + subName + tail, nbinx, xbins, nbiny, ybins, nbinz, zbins);\
  } while(0);

#define BookSysHist(i, sysName, v_hist, hist, name, title, nbin, left, right)					\
  do														\
  {														\
   FillSysHists(i, sysName, v_hist, hist, (TString)name + subName + tail, (TString)name + subName + tail, nbin, left, right);	\
  } while(0);

#define BookSysHist1(i, sysName, v_hist, hist, name, title, nbin, xbins)                                   \
  do                                                                                                            \
  {                                                                                                             \
   FillSysHists(i, sysName, v_hist, hist, (TString)name + subName + tail, (TString)name + subName + tail, nbin, xbins);   \
  } while(0);

#define BookBkgHist(i, ProcessName, bkgName, NameIndex, v_hist, hist, name, title, nbin, left, right)                                    \
  do                                                                                                            \
  {                                                                                                             \
   FillBkgHists(i, ProcessName, bkgName, NameIndex, v_hist, hist, (TString)name + subName + tail, (TString)name + subName + tail, nbin, left, right);        \
  } while(0);

#define BookBkgHist1(i, ProcessName, bkgName, NameIndex, v_hist, hist, name, title, nbin, xbins)                                    \
  do                                                                                                            \
  {                                                                                                             \
   FillBkgHists(i, ProcessName, bkgName, NameIndex, v_hist, hist, (TString)name + subName + tail, (TString)name + subName + tail, nbin, xbins);        \
  } while(0);

#define BookBkgSysHist(i, ProcessName, bkgName, NameIndex, sysName, v_hist, hist, name, title, nbin, left, right)                                    \
  do                                                                                                            \
  {                                                                                                             \
   FillBkgSysHists(i, ProcessName, bkgName, NameIndex, sysName, v_hist, hist, (TString)name + subName + tail, (TString)name + subName + tail, nbin, left, right);        \
  } while(0);

#define BookBkgSysHist1(i, ProcessName, bkgName, NameIndex, sysName, v_hist, hist, name, title, nbin, xbins)                                    \
  do                                                                                                            \
  {                                                                                                             \
   FillBkgSysHists(i, ProcessName, bkgName, NameIndex, sysName, v_hist, hist, (TString)name + subName + tail, (TString)name + subName + tail, nbin, xbins);        \
  } while(0);

#define BookCovHist(i, v_hist, hist, name, title, nbin, left, right)                                    \
  do                                                                                                            \
  {                                                                                                             \
   FillCovHists(i, v_hist, hist, (TString)name + subName + tail, (TString)name + subName + tail, nbin, left, right);        \
  } while(0);

#define BookFakeHist(i, v_hist, hist, name, title, nbin, left, right)                                    \
  do                                                                                                            \
  {                                                                                                             \
   FillFakeHists(i, v_hist, hist, (TString)name + subName + tail, (TString)name + subName + tail, nbin, left, right);        \
  } while(0);

#define BookFakeHist1(i, v_hist, hist, name, title, nbin, xbins)                                    \
  do                                                                                                            \
  {                                                                                                             \
   FillFakeHists(i, v_hist, hist, (TString)name + subName + tail, (TString)name + subName + tail, nbin, xbins);        \
  } while(0);

#define BookMultiBinHist(i, MultiBinVector, v_hist, hist, name, title, nbin, xbins)                                    \
  do                                                                                                            \
  {                                                                                                             \
   FillMultiBinHists(i, MultiBinVector, v_hist, hist, (TString)name + subName + tail, (TString)name + subName + tail, nbin, xbins);        \
  } while(0);


#define BookAngularHist(i, v_hist, hist, name, type, nbin, left, right)                                               \
  do                                                                                                            \
  {                                                                                                             \
   FillHists(i, v_hist, hist, (TString)name + subName + tail, (TString)type, nbin, left, right);                       \
  } while(0);

#define BookAngularHist1(i, v_hist, hist, name, type, nbin, xbins)                                               \
  do                                                                                                            \
  {                                                                                                             \
   FillHists(i, v_hist, hist, (TString)name + subName + tail, (TString)type, nbin, xbins);                       \
  } while(0);

#define Book2DAngularHist(i, v_hist, hist, name, type, nbinx, xbins, nbiny, ybins)                                               \
  do                                                                                                            \
  {                                                                                                             \
   FillHists(i, v_hist, hist, (TString)name + subName + tail, (TString)type, nbinx, xbins, nbiny, ybins);                       \
  } while(0);

#define Book3DAngularHist(i, v_hist, hist, name, type, nbinx, xbins, nbiny, ybins, nbinz, zbins)                                               \
  do                                                                                                            \
  {                                                                                                             \
   FillHists(i, v_hist, hist, (TString)name + subName + tail, (TString)type, nbinx, xbins, nbiny, ybins, nbinz, zbins);                       \
  } while(0);


#endif
