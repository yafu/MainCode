#!/bin/bash
lsetup "root 6.14.04-x86_64-slc6-gcc62-opt"
lsetup "cmake 3.11.0"
export PATH=${PATH}:/lustre/AtlUser/yfu/MainCode/build/bin
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/lustre/AtlUser/yfu/MainCode/build/lib

export LHAPDF_ROOT_DIR=/lustre/AtlUser/yfu/LHAPDF/
export LHAPDF_PDF_DIR=${LHAPDF_ROOT_DIR}/share/LHAPDF
export PATH=$PATH:${LHAPDF_ROOT_DIR}/bin
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${LHAPDF_ROOT_DIR}/lib

