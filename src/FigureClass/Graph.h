#include <iostream>
#include <TH1D.h>
#include <TROOT.h>
#include <TFile.h>
#include "TTree.h"
#include <TStyle.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TF1.h>
#include "TH2D.h"
#include "THStack.h"
#include "TColor.h"
#include "TGaxis.h"
#include "TLatex.h"
#include "TMathText.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "png.h"
#include "pngconf.h"

using namespace std;

class Graph
{
 public:

 TCanvas *MyN;

 TString myFigureName;
 TString myXTitle;
 TString myYTitle;

 TFile* file[100];

 TH1D* h1[100];
 TH2D* h2[100];

 TH1D* tmph1[100];

 TText* text[100];
 TMathText* mathtext[100];
 TStyle* myStyle[100];

 TLegend *legend1;
 TLegend *legend2;
 TLegend *legend3;

 TString myLegendName[100];
 TString myCustomLegendName[10][100];
 TString myText[100];
 TString PaintTextStyle[100];

 double legendxmin = 0.1;
 double legendxmax = 0.3;
 double legendymin = 0.6;
 double legendymax = 0.8;
 double legend2xmin = 0.1;
 double legend2xmax = 0.3;
 double legend2ymin = 0.6;
 double legend2ymax = 0.8;
 bool isDrawLegend2 = false;
 double TextX, TextY;
 double XRangeMin, XRangeMax, YRangeMin, YRangeMax;
 double BottomMargin, TopMargin, LeftMargin, RightMargin;
 double wmin, wmax;
 double ymin, ymax;

 bool isInput;

 bool isLogX = false;
 bool isLogY = false;

 bool DoSave = false;

 bool isDefineHist = false;

 int FigureCount;
 int TextCount;

 int TotalPadNumber = 0;
 int CustomFigureCount[10] = {0};
 TGraphErrors* Custom[10][500] = {0};
 TMultiGraph* CustomMultiGraph[10] = {0};
 TH1D* CustomHist[10][500] = {0};
 bool isCustom = false;
 TString myPadName[10];
 TString CustomPlot[10][500];
 bool isSetCustomYRange[10] = {0};
 double CustomYRangeMin[10];
 double CustomYRangeMax[10];
 double CustomLegendSize[10] = {0.04};
 double CustomXTitleSize[10] = {};
 double CustomXTitleOffset[10] = {};
 double CustomXLabelSize[10] = {};
 double CustomYTitleSize[10] = {};
 double CustomYTitleOffset[10] = {};
 double CustomYLabelSize[10] = {};

 int CustomNdivisions[10] = {1};

 double CustomBottomMargin[10] = {0.0};
 double CustomTopMargin[10] = {0.0};
 double CustomLeftMargin[10] = {0.0};
 double CustomRightMargin[10] = {0.0};

 double CustomPadYMax[10] = {0.0};
 double CustomPadYMin[10] = {0.0};

 bool isCustomGrid[10] = {true};

 map<pair<int, int>, Color_t> CustomColor;
 map<int, Color_t> myColor;

 TH1D* tmpHist[500];

 bool isOutput = false;

 bool isSetXRange = false;

 Graph(const char* FigureName, const char* XTitle, const char* YTitle);
 virtual ~Graph();
 virtual void SetMode(const char* ModeName);
 virtual void SetLogX(){isLogX = true;};
 virtual void SetLogY(){isLogY = true;};
 virtual void DefineHist(int Number, TString name, TString title, int nbin, double left, double right);
 virtual void DefineHist(int Number, TString name, TString title, int nbinx, double* xbins);

 virtual void SetCustomPad(int TotalPadNumber);
 virtual void CustomSetting(int PadNumber, int histNumber1, int histNumber2 = 0, TString process = "");
 virtual void SetCustomPlotFormat(int PadNumber, int Number, Color_t Color, Style_t style = 21, bool isFilled = false);
 virtual void SetPadTitle(int PadNumber, const char* PadName);
 virtual void SetCustomYRange(int PadNumber, double ymin, double ymax);
 virtual void SetCustomLegendSize(int PadNumber, double size);
 virtual void SetCustomXTitleSize(int PadNumber, double size);
 virtual void SetCustomXTitleOffset(int PadNumber, double size);
 virtual void SetCustomXLabelSize(int PadNumber, double size);
 virtual void SetCustomYTitleSize(int PadNumber, double size);
 virtual void SetCustomYTitleOffset(int PadNumber, double size);
 virtual void SetCustomYLabelSize(int PadNumber, double size);
 virtual void SetCustomPadYPosition(int PadNumber, double ymin, double ymax);
 virtual void SetCustomNdivisions(int PadNumber, int ndiv);
 virtual void SetCustomGrid(int PadNumber, bool flag);
 virtual void SetCustomBarOffset(int PadNumber, int Number, double size);
 virtual void SetCustomMarkerStyle(int PadNumber, int Number, Style_t style);
 virtual void SetCustomLineStyle(int PadNumber, int Number, Style_t style);
 virtual void SetCustomLineWidth(int PadNumber, int Number, double size);
 virtual void DefineCustomHist(int Number, int histNumber1, int histNumber2 = 0, TString process = "", const char* LegendName = "");
 virtual void CustomNorm(int fig1, int fig2, int BaseOne = 1);
 virtual void SetCustomPlot(int PadNumber, int Number, TString format);

 virtual void Input(int Number, const char* rootName, const char* histName, const char* legendName = "");
 virtual void Input(int Number, vector<double> x, vector<double> y, const char* legendName = "");
 virtual void InputGraph(int Number, const char* rootName, const char* histName, const char* legendName = "");

 virtual void SetXRange(double xmin, double xmax);

 virtual void AddText(int Number, const char* TextContent);
 virtual void Draw();
 virtual void SaveFigure(){DoSave = true;};
 virtual void SetLegendPosition(double xmin, double ymin, double xmax, double ymax);
 virtual void SetLegend2Position(double xmin, double ymin, double xmax, double ymax);

 virtual void Compare(TString Option, TString isNorm = "");

};

Graph::Graph(const char* FigureName, const char* XTitle, const char* YTitle)
{
 FigureCount = 0;
 TextCount = 0;

 isInput = false;

 isLogX = false;

 isSetXRange = false;

 myFigureName = FigureName;
 myXTitle = XTitle;
 myYTitle = YTitle;

 SaveFigure();

 for(int i = 0; i < 10; i++){
   myStyle[i] = new TStyle((TString)"Style" + (int)i, (TString)"Style" + (int)i);
   myStyle[i]->SetTextFont(42);
   myStyle[i]->SetHatchesLineWidth(2);
   myStyle[i]->SetStatStyle(0);
   myStyle[i]->SetStatX(0);
   myStyle[i]->SetStatY(0);
 }

 myColor[1] = (Color_t)kRed;
 myColor[2] = (Color_t)kBlue;
 myColor[3] = (Color_t)kGreen+1;
 myColor[4] = (Color_t)kOrange;
 myColor[5] = (Color_t)kCyan;
 myColor[6] = (Color_t)kPink-1;
 myColor[7] = (Color_t)kMagenta;
 myColor[8] = (Color_t)kAzure;
 myColor[9] = (Color_t)kYellow+1;
 myColor[10] = (Color_t)kSpring+2;
 myColor[11] = (Color_t)kTeal;
 myColor[12] = (Color_t)kAzure+1;
 myColor[13] = (Color_t)kYellow+2;

}

Graph::~Graph()
{
}

void Graph::SetMode(const char* ModeName)
{
 TString myMode = ModeName;
 if(myMode == "CUSTOM") isCustom = true;
}

void Graph::SetLegendPosition(double xmin, double ymin, double xmax, double ymax)
{
 legendxmin = xmin;
 legendxmax = xmax;
 legendymin = ymin;
 legendymax = ymax;
}

void Graph::SetLegend2Position(double xmin, double ymin, double xmax, double ymax)
{
 isDrawLegend2 = true;

 legend2xmin = xmin;
 legend2xmax = xmax;
 legend2ymin = ymin;
 legend2ymax = ymax;
}

void Graph::SetXRange(double xmin, double xmax)
{
 if(!isInput) {cout<<"There is no input file."<<endl; return;}

 this->XRangeMin = xmin;
 this->XRangeMax = xmax;

 isSetXRange = true;
}


void Graph::DefineHist(int Number, TString name, TString title, int nbin, double left, double right)
{
 isDefineHist = true;
 h1[Number] = new TH1D(name, title, nbin, left, right);
}

void Graph::DefineHist(int Number, TString name, TString title, int nbinx, double* xbins)
{
 isDefineHist = true;
 h1[Number] = new TH1D(name, title, nbinx, xbins);
}

void Graph::SetCustomPad(int TotalPadNumber)
{
 if(!isCustom){
   cout<<"Please set mode to CUSTOM"<<endl;
   return;
 }

 this->TotalPadNumber = TotalPadNumber;

 if(TotalPadNumber == 1){
   CustomXTitleSize[1] = 0.04;
   CustomXTitleOffset[1] = 1.2;
   CustomXLabelSize[1] = 0.04;
   CustomYTitleSize[1] = 0.04;
   CustomYTitleOffset[1] = 1.2;
   CustomYLabelSize[1] = 0.04;
   CustomLegendSize[1] = 0.025;
   CustomBottomMargin[1] = 0.1;
   CustomTopMargin[1] = 0.05;
   CustomLeftMargin[1] = 0.15;
   CustomRightMargin[1] = 0.05;
   CustomPadYMax[1] = 1.0;
   CustomPadYMin[1] = 0.01;
 }
 if(TotalPadNumber == 2){
   CustomYTitleSize[1] = 0.045;
   CustomYTitleOffset[1] = 1.0;
   CustomYLabelSize[1] = 0.04;
   CustomLegendSize[1] = 0.035;
   CustomBottomMargin[1] = 0.0;
   CustomTopMargin[1] = 0.15;
   CustomLeftMargin[1] = 0.15;
   CustomRightMargin[1] = 0.05;
   CustomPadYMax[1] = 1.0;
   CustomPadYMin[1] = 0.25;

   CustomXTitleSize[2] = 0.12;
   CustomXTitleOffset[2] = 1;
   CustomXLabelSize[2] = 0.12;
   CustomYTitleSize[2] = 0.08;
   CustomYTitleOffset[2] = 0.55;
   CustomYLabelSize[2] = 0.08;
   CustomLegendSize[2] = 0.025;
   CustomBottomMargin[2] = 0.3;
   CustomTopMargin[2] = 0.0;
   CustomLeftMargin[2] = 0.15;
   CustomRightMargin[2] = 0.05;
   CustomPadYMax[2] = 0.25;
   CustomPadYMin[2] = 0.0;

   CustomNdivisions[2] = 505;

   isCustomGrid[2] = true;
 }
 if(TotalPadNumber == 3){
   CustomYTitleSize[1] = 0.06;
   CustomYTitleOffset[1] = 0.8;
   CustomYLabelSize[1] = 0.04;
   CustomLegendSize[1] = 0.025;
   CustomBottomMargin[1] = 0.0;
   CustomTopMargin[1] = 0.15;
   CustomLeftMargin[1] = 0.15;
   CustomRightMargin[1] = 0.05;
   CustomPadYMax[1] = 1.0;
   CustomPadYMin[1] = 0.3;

   CustomXTitleSize[2] = 0.12;
   CustomXTitleOffset[2] = 1;
   CustomXLabelSize[2] = 0.12;
   CustomYTitleSize[2] = 0.2;
   CustomYTitleOffset[2] = 0.3;
   CustomYLabelSize[2] = 0.2;
   CustomLegendSize[2] = 0.025;
   CustomBottomMargin[2] = 0.0;
   CustomTopMargin[2] = 0.0;
   CustomLeftMargin[2] = 0.15;
   CustomRightMargin[2] = 0.05;
   CustomPadYMax[2] = 0.3;
   CustomPadYMin[2] = 0.18;

   CustomXTitleSize[3] = 0.15;
   CustomXTitleOffset[3] = 1;
   CustomXLabelSize[3] = 0.15;
   CustomYTitleSize[3] = 0.14;
   CustomYTitleOffset[3] = 0.31;
   CustomYLabelSize[3] = 0.08;
   CustomLegendSize[3] = 0.025;
   CustomBottomMargin[3] = 0.35;
   CustomTopMargin[3] = 0.0;
   CustomLeftMargin[3] = 0.15;
   CustomRightMargin[3] = 0.05;
   CustomPadYMax[3] = 0.18;
   CustomPadYMin[3] = 0.0;

 }

}

void Graph::DefineCustomHist(int Number, int histNumber1, int histNumber2, TString process, const char* LegendName)
{
 tmpHist[Number] = (TH1D *)h1[histNumber1]->Clone("tmp");
 if((TString)process == "Plus"){
   tmpHist[Number]->Add(h1[histNumber2]);
 }
 if((TString)process == "Minus"){
   tmpHist[Number]->Add(h1[histNumber2], -1);
 }
 if((TString)process == "Divide"){
   tmpHist[Number]->Divide(h1[histNumber2]);
 }
 if((TString)process == "Pull"){
   for(int ibin = 1; ibin <= h1[histNumber1]->GetNbinsX(); ibin++){
     tmpHist[Number]->SetBinContent(ibin, (h1[histNumber1]->GetBinContent(ibin) - h1[histNumber2]->GetBinContent(ibin)) / sqrt(h1[histNumber1]->GetBinError(ibin) * h1[histNumber1]->GetBinError(ibin) + h1[histNumber2]->GetBinError(ibin) * h1[histNumber2]->GetBinError(ibin)));
   }
 }

 myLegendName[Number] = LegendName;

 if((TString)process == "") return;

 int tmpNumber = Number;
 if(tmpNumber <= FigureCount) return;
 h1[tmpNumber] = (TH1D *)tmpHist[Number]->Clone((TString)"tmp_" + (int)Number);

 FigureCount++;

}

void Graph::CustomSetting(int PadNumber, int histNumber1, int histNumber2, TString process)
{
 if(!isCustom){
   cout<<"Please set mode to CUSTOM"<<endl;
   return;
 }
 if(!isInput){
   cout<<"Please input at first"<<endl;
   return;
 }
 if(PadNumber > TotalPadNumber){
   cout<<"Out of pad range"<<endl;
   return;
 }

 CustomFigureCount[PadNumber]++;

 int Number = CustomFigureCount[PadNumber];

 CustomHist[PadNumber][Number] = (TH1D *)h1[histNumber1]->Clone((TString)"CustomHist_" + (int)PadNumber + (TString)"_" + (int)Number);
 myCustomLegendName[PadNumber][Number] = myLegendName[histNumber1];

 if((TString)process == "Plus"){
   CustomHist[PadNumber][Number]->Add(h1[histNumber2]);
 }
 if((TString)process == "Minus"){
   CustomHist[PadNumber][Number]->Add(h1[histNumber2], -1);
   for(int ibin = 1; ibin <= h1[histNumber1]->GetNbinsX(); ibin++){
   }
 }
 if((TString)process == "Divide"){
   CustomHist[PadNumber][Number]->Divide(h1[histNumber2]);
   if(isOutput){
     for(int ibin = 1; ibin <= CustomHist[PadNumber][Number]->GetNbinsX(); ibin++){
       cout<<"bin "<<ibin<<": "<<CustomHist[PadNumber][Number]->GetBinContent(ibin)<<" +- "<<CustomHist[PadNumber][Number]->GetBinError(ibin)<<endl;
     }
   }
 }
 if((TString)process == "Pull"){
   for(int ibin = 1; ibin <= h1[histNumber1]->GetNbinsX(); ibin++){
     CustomHist[PadNumber][Number]->SetBinContent(ibin, (h1[histNumber1]->GetBinContent(ibin) - h1[histNumber2]->GetBinContent(ibin)) / sqrt(h1[histNumber1]->GetBinError(ibin) * h1[histNumber1]->GetBinError(ibin) + h1[histNumber2]->GetBinError(ibin) * h1[histNumber2]->GetBinError(ibin)));
   }
 }

 cout<<"Integral of "<<CustomHist[PadNumber][Number]->GetName()<<": "<<CustomHist[PadNumber][Number]->Integral()<<endl;

 Custom[PadNumber][Number] = new TGraphErrors(CustomHist[PadNumber][Number]->GetNbinsX());
 Custom[PadNumber][Number]->SetName((TString)"Custom_" + (int)PadNumber + (TString)"_" + (int)Number);
 for(int ibin = 1; ibin <= CustomHist[PadNumber][Number]->GetNbinsX(); ibin++){
   Custom[PadNumber][Number]->SetPointX(ibin, CustomHist[PadNumber][Number]->GetBinCenter(ibin));
   Custom[PadNumber][Number]->SetPointY(ibin, CustomHist[PadNumber][Number]->GetBinContent(ibin));
   Custom[PadNumber][Number]->SetPointError(ibin, 0.0, CustomHist[PadNumber][Number]->GetBinError(ibin));
 }

 Custom[PadNumber][Number]->RemovePoint(0);

 if((TString)process == "") return;

}

void Graph::SetCustomPlotFormat(int PadNumber, int Number, Color_t Color, Style_t style, bool isFilled)
{
 if(!isInput) {cout<<"There is no input file."<<endl; return;}

 //Custom[PadNumber][Number]->SetMarkerStyle(style);
 Custom[PadNumber][Number]->SetMarkerColor(Color);
 //Custom[PadNumber][Number]->SetMarkerSize(1.2);
 Custom[PadNumber][Number]->SetLineWidth(1);
 Custom[PadNumber][Number]->SetLineColor(Color);
 Custom[PadNumber][Number]->SetLineStyle(1);
 if(isFilled){
   Custom[PadNumber][Number]->SetFillColor(Color);
   Custom[PadNumber][Number]->SetFillStyle(3354);
 }

 CustomColor[make_pair(PadNumber, Number)] = Color;
//   legend1->AddEntry(h1[Number], myLegendName[Number],"lpfe");
}

void Graph::SetPadTitle(int PadNumber, const char* PadName)
{
 myPadName[PadNumber] = PadName;
}

void Graph::SetCustomYRange(int PadNumber, double ymin, double ymax)
{
 isSetCustomYRange[PadNumber] = true;

 CustomYRangeMin[PadNumber] = ymin;
 CustomYRangeMax[PadNumber] = ymax;
}

void Graph::SetCustomLegendSize(int PadNumber, double size)
{
 CustomLegendSize[PadNumber] = size;
}

void Graph::SetCustomXTitleSize(int PadNumber, double size)
{
 CustomXTitleSize[PadNumber] = size;
}

void Graph::SetCustomXTitleOffset(int PadNumber, double size)
{
 CustomXTitleOffset[PadNumber] = size;
}

void Graph::SetCustomXLabelSize(int PadNumber, double size)
{
 CustomXLabelSize[PadNumber] = size;
}

void Graph::SetCustomYTitleSize(int PadNumber, double size)
{
 CustomYTitleSize[PadNumber] = size;
}

void Graph::SetCustomYTitleOffset(int PadNumber, double size)
{
 CustomYTitleOffset[PadNumber] = size;
}

void Graph::SetCustomYLabelSize(int PadNumber, double size)
{
 CustomYLabelSize[PadNumber] = size;
}

void Graph::SetCustomPadYPosition(int PadNumber, double ymin, double ymax)
{
 CustomPadYMin[PadNumber] = ymin;
 CustomPadYMax[PadNumber] = ymax;
}

void Graph::SetCustomNdivisions(int PadNumber, int ndiv)
{
 CustomNdivisions[PadNumber] = ndiv;
}

void Graph::SetCustomGrid(int PadNumber, bool flag)
{
 isCustomGrid[PadNumber] = flag;
}

void Graph::SetCustomBarOffset(int PadNumber, int Number, double size)
{
}

void Graph::SetCustomMarkerStyle(int PadNumber, int Number, Style_t style)
{
 Custom[PadNumber][Number]->SetMarkerStyle(style);
}

void Graph::SetCustomLineStyle(int PadNumber, int Number, Style_t style)
{
 Custom[PadNumber][Number]->SetLineStyle(style);
}

void Graph::SetCustomLineWidth(int PadNumber, int Number, double size)
{
 Custom[PadNumber][Number]->SetLineWidth(size);
}

void Graph::CustomNorm(int fig1, int fig2, int BaseOne)
{
 if(!isCustom){
   cout<<"Please set mode to CUSTOM"<<endl;
   return;
 }
 if(!isInput){
   cout<<"Please input at first"<<endl;
   return;
 }

 double figsum1 = h1[fig1]->Integral();
 double figsum2 = h1[fig2]->Integral();

 if(BaseOne == 1) h1[fig2]->Scale(figsum1 / figsum2);
 if(BaseOne == 2) h1[fig1]->Scale(figsum2 / figsum1);
}

void Graph::SetCustomPlot(int PadNumber, int Number, TString format)
{
 CustomPlot[PadNumber][Number] = format;
 if(format == "AP"){
   Custom[PadNumber][Number]->SetMarkerSize(2);
   Custom[PadNumber][Number]->SetLineWidth(1504);
   Custom[PadNumber][Number]->SetLineStyle(1);
   Custom[PadNumber][Number]->SetMarkerStyle(45);
 }
 if(format == "AC"){
   Custom[PadNumber][Number]->SetMarkerSize(0.4);
   Custom[PadNumber][Number]->SetLineWidth(4);
   Custom[PadNumber][Number]->SetLineStyle(1);
   Custom[PadNumber][Number]->SetMarkerStyle(8);
 }

}

void Graph::Input(int Number, const char* rootName, const char* histName, const char* legendName)
{
 isInput = true;

 FigureCount++;

 file[Number] = new TFile(rootName);

 if(!isDefineHist) h1[Number] = (TH1D *)file[Number]->Get(histName);
 if(isDefineHist){
   tmph1[Number] = (TH1D *)file[Number]->Get(histName);
   for(int ibin = 1; ibin <= tmph1[Number]->GetNbinsX(); ibin++){
     h1[Number]->SetBinContent(ibin, tmph1[Number]->GetBinContent(ibin));
     h1[Number]->SetBinError(ibin, tmph1[Number]->GetBinError(ibin));
   }
 }

 myLegendName[Number] = legendName;

 cout<<"Integral of "<<h1[Number]->GetName()<<": "<<h1[Number]->Integral()<<endl;
}

void Graph::Input(int Number, vector<double> x, vector<double> y, const char* legendName)
{
 isInput = true;

 FigureCount++;

 Custom[1][Number] = new TGraphErrors(x.size());

 for(int ibin = 0; ibin < x.size(); ibin++){
   Custom[1][Number]->SetPointX(ibin + 1, x.at(ibin));
   Custom[1][Number]->SetPointY(ibin + 1, y.at(ibin));
   //Custom[1][Number]->SetPointError(ibin + 1, 0.0, 0.0);
 }
 Custom[1][Number]->RemovePoint(0);
}

void Graph::InputGraph(int Number, const char* rootName, const char* histName, const char* legendName)
{
 isInput = true;

 FigureCount++;

 file[Number] = new TFile(rootName);

 Custom[1][Number] = (TGraphErrors *)file[Number]->Get(histName);

 myLegendName[Number] = legendName;
 myCustomLegendName[1][Number] = legendName;

 CustomFigureCount[1]++;

}

void Graph::AddText(int Number, const char* TextContent)
{
 if(Number == 0) {cout<<"WARNING!! Please add text from No.1"<<endl;}
 myText[Number] = TextContent;

 TextCount++;
}

void Graph::Compare(TString Option, TString isNorm)
{
 if(isNorm == "Norm"){
   for(int i = 1; i <= FigureCount; i++){
     CustomNorm(1, i, 1);
   }
 }

 if(Option == "Ratio"){
   SetCustomPad(2);
   CustomSetting(1, 1);
   CustomSetting(1, 2);
   CustomSetting(2, 1, 2, "Divide");
   SetCustomPlotFormat(1, 1, kBlue);
   SetCustomPlotFormat(1, 2, kRed);
   SetCustomPlotFormat(2, 1, kBlack);
   SetCustomYRange(2, 0.8, 1.2);
 }
 if(Option == "Delta"){
   SetCustomPad(2);
   CustomSetting(1, 1);
   CustomSetting(1, 2);
   CustomSetting(2, 1, 2, "Minus");
   SetCustomPlotFormat(1, 1, kBlue);
   SetCustomPlotFormat(1, 2, kRed);
   SetCustomPlotFormat(2, 1, kBlack);
   SetCustomYRange(2, -0.5, 0.5);
 }
 if(Option == "Pull"){
   SetCustomPad(2);
   CustomSetting(1, 1);
   CustomSetting(1, 2);
   CustomSetting(2, 1, 2, "Pull");
   SetCustomPlotFormat(1, 1, kBlue);
   SetCustomPlotFormat(1, 2, kRed);
   SetCustomPlotFormat(2, 1, kBlack);
   SetCustomYRange(2, -0.5, 0.5);
 }
 if(Option == "Simple"){
   SetCustomPad(1);
   CustomSetting(1, 1);
   CustomSetting(1, 2);
   SetCustomPlotFormat(1, 1, kBlue);
   SetCustomPlotFormat(1, 2, kRed);
 }
 if(Option == "MultiPull"){
   SetCustomPad(2);
   for(int i = 1; i <= FigureCount; i++){
     CustomSetting(1, i);
     CustomSetting(2, 1, i, "Pull");
     SetCustomPlotFormat(1, i, myColor[i]);
     SetCustomPlotFormat(2, i, myColor[i]);
   }
   SetCustomYRange(2, -0.5, 0.5);
 }
 if(Option == "MultiDelta"){
   SetCustomPad(2);
   for(int i = 1; i <= FigureCount; i++){
     CustomSetting(1, i);
     CustomSetting(2, 1, i, "Minus");
     SetCustomPlotFormat(1, i, myColor[i]);
     SetCustomPlotFormat(2, i, myColor[i]);
   }
   SetCustomYRange(2, -0.5, 0.5);
 }
 if(Option == "MultiRatio"){
   SetCustomPad(2);
   for(int i = 1; i <= FigureCount; i++){
     CustomSetting(1, i);
     CustomSetting(2, 1, i, "Divide");
     SetCustomPlotFormat(1, i, myColor[i]);
     SetCustomPlotFormat(2, i, myColor[i]);
   }
   SetCustomYRange(2, 0.5, 1.5);
 }
 if(Option == "MultiSimple"){
   SetCustomPad(1);
   for(int i = 1; i <= FigureCount; i++){
     CustomSetting(1, i);
     SetCustomPlotFormat(1, i, myColor[i]);
   }
 }

}

void Graph::Draw()
{
 if(!isInput) {cout<<"There is no input file."<<endl; return;}

 TPad *pad1;
 TPad *pad2;
 TPad *pad3;

 if(isCustom){
   MyN = new TCanvas("MyN","MyN",1000,1000);
   MyN->cd();
   if(isLogX) gPad->SetLogx();
   gStyle->SetGridWidth(1);

   if(TotalPadNumber == 1){
     pad1 = new TPad("pad1", "pad1", 0, CustomPadYMin[1], 1.0, CustomPadYMax[1]);
     CustomMultiGraph[1] = new TMultiGraph();

     BottomMargin = CustomBottomMargin[1];
     TopMargin = CustomTopMargin[1];
     LeftMargin = CustomLeftMargin[1];
     RightMargin = CustomRightMargin[1];
     pad1->SetBottomMargin(BottomMargin);
     pad1->SetTopMargin(TopMargin);
     pad1->SetLeftMargin(LeftMargin);
     pad1->SetRightMargin(RightMargin);
     pad1->Draw();
     pad1->cd();

     if(isLogX) gPad->SetLogx();

     Custom[1][1]->SetTitle("");
     //Custom[1][1]->SetStats(0);

     Custom[1][1]->GetXaxis()->SetTitle(myXTitle);
     Custom[1][1]->GetXaxis()->SetTitleSize(CustomXTitleSize[1]);
     Custom[1][1]->GetXaxis()->SetTitleFont(42);
     Custom[1][1]->GetXaxis()->SetTitleOffset(CustomXTitleOffset[1]);

     Custom[1][1]->GetXaxis()->SetLabelSize(CustomXLabelSize[1]);
     Custom[1][1]->GetXaxis()->SetLabelFont(42);

     Custom[1][1]->GetYaxis()->SetTitle(myYTitle);
     Custom[1][1]->GetYaxis()->SetTitleSize(CustomYTitleSize[1]);
     Custom[1][1]->GetYaxis()->SetTitleFont(42);
     Custom[1][1]->GetYaxis()->SetTitleOffset(CustomYTitleOffset[1]);

     Custom[1][1]->GetYaxis()->SetLabelSize(CustomYLabelSize[1]);
     Custom[1][1]->GetYaxis()->SetLabelFont(42);

     //Custom[1][1]->Draw(CustomPlot[1][1]);
     CustomMultiGraph[1]->Add(Custom[1][1]);

     gStyle->SetHatchesLineWidth(2);

     YRangeMax = Custom[1][1]->GetMaximum();
     YRangeMin = Custom[1][1]->GetMinimum();
     if(!isSetXRange){
       //XRangeMax = h1[1]->GetXaxis()->GetBinUpEdge(Custom[1][1]->GetN());
       //XRangeMin = h1[1]->GetXaxis()->GetBinLowEdge(1);
     }

     if(isLogY){
       Custom[1][1]->SetMinimum();
     }

     int iPlot = 2;
     while(iPlot <= CustomFigureCount[1]){
      if(isLogY) Custom[1][iPlot]->SetMinimum();
      //Custom[1][iPlot]->Draw((CustomPlot[1][iPlot] + " same").Data());
      CustomMultiGraph[1]->Add(Custom[1][iPlot], CustomPlot[1][iPlot]);
      if(YRangeMax < Custom[1][iPlot]->GetMaximum()) YRangeMax = Custom[1][iPlot]->GetMaximum();
      if(YRangeMin > Custom[1][iPlot]->GetMinimum()) YRangeMin = Custom[1][iPlot]->GetMinimum();
      if(isSetXRange) Custom[1][iPlot]->GetXaxis()->SetRangeUser(XRangeMin, XRangeMax);
      iPlot++;
     }
     CustomMultiGraph[1]->Draw(CustomPlot[1][1]);
     CustomMultiGraph[1]->GetYaxis()->SetTitle(myYTitle);
     CustomMultiGraph[1]->GetYaxis()->SetTitleSize(CustomYTitleSize[1]);
     CustomMultiGraph[1]->GetYaxis()->SetTitleFont(42);
     CustomMultiGraph[1]->GetYaxis()->SetTitleOffset(CustomYTitleOffset[1]);
     CustomMultiGraph[1]->GetYaxis()->SetLabelSize(CustomYLabelSize[1]);
     CustomMultiGraph[1]->GetYaxis()->SetLabelFont(42);
     CustomMultiGraph[1]->GetXaxis()->SetRangeUser(XRangeMin, XRangeMax);
     CustomMultiGraph[1]->GetXaxis()->SetTitle(myXTitle);
     CustomMultiGraph[1]->GetXaxis()->SetTitleSize(CustomXTitleSize[1]);
     CustomMultiGraph[1]->GetXaxis()->SetTitleFont(42);
     CustomMultiGraph[1]->GetXaxis()->SetTitleOffset(CustomXTitleOffset[1]);
     CustomMultiGraph[1]->GetXaxis()->SetLabelSize(CustomXLabelSize[1]);
     CustomMultiGraph[1]->GetXaxis()->SetLabelFont(42);
     if(isSetCustomYRange[1]) CustomMultiGraph[1]->GetYaxis()->SetRangeUser(CustomYRangeMin[1], CustomYRangeMax[1]);
     if(isSetXRange) CustomMultiGraph[1]->GetXaxis()->SetRangeUser(XRangeMin, XRangeMax);

     if(isLogY) pad1->SetLogy();
     if(!isSetCustomYRange[1]){
       if(!isLogY) Custom[1][1]->GetYaxis()->SetRangeUser(YRangeMin - fabs(YRangeMin) * 0.3, YRangeMax + fabs(YRangeMax) * 0.3);
//       if(!isLogY){
//         Custom[1][1]->SetMinimum(YRangeMin - fabs(YRangeMin) * 0.3);
//         Custom[1][1]->SetMaximum(YRangeMax + fabs(YRangeMax) * 0.3);
//       }
     }

     if(isSetXRange) Custom[1][1]->GetXaxis()->SetRangeUser(XRangeMin, XRangeMax);
     if(isSetCustomYRange[1]){
       Custom[1][1]->GetYaxis()->SetRangeUser(CustomYRangeMin[1], CustomYRangeMax[1]);
//       Custom[1][1]->SetMinimum(CustomYRangeMin[1]);
//       Custom[1][1]->SetMaximum(CustomYRangeMax[1]);

       if(CustomYRangeMin[1] < 0.0){
         YRangeMin = CustomYRangeMin[1] / 1.3;
       }
       if(CustomYRangeMin[1] >= 0.0){
         YRangeMin = CustomYRangeMin[1] / 0.7;
       }
       if(CustomYRangeMax[1] < 0.0){
         YRangeMax = CustomYRangeMax[1] / 0.7;
       }
       if(CustomYRangeMax[1] >= 0.0){
         YRangeMax = CustomYRangeMax[1] / 1.3;
       }
     }

     cout<<"YRangeMin: "<<YRangeMin<<" YRangeMax: "<<YRangeMax<<endl;
     cout<<"XRangeMin: "<<XRangeMin<<" XRangeMax: "<<XRangeMax<<endl;

     double textlength = (legendymax - legendymin) / CustomFigureCount[1];
     int iText = 1;
     while(iText<=TextCount){
       text[iText] = new TLatex();
//       text[iText] = new TMathText();

       TextX = XRangeMin + (XRangeMax - XRangeMin) * (legendxmin + 0.05);
       if(isLogX) TextX = pow(10, (log10(XRangeMin) + (log10(XRangeMax) - log10(XRangeMin)) * (legendxmin + 0.05)));
       TextY = (YRangeMax + fabs(YRangeMax) * 0.3) - (YRangeMax + fabs(YRangeMax) * 0.3 - YRangeMin + fabs(YRangeMin) * 0.3) * ((1 - legendymax) + iText * textlength);
       text[iText]->SetText(TextX, TextY, myText[iText]);
       text[iText]->SetTextSize(CustomLegendSize[1]);
       text[iText]->SetTextFont(72);
       text[iText]->Draw("same");
       iText++;
       cout<<log10(XRangeMin)<<" "<<(log10(XRangeMax) - log10(XRangeMin))<<" "<<endl;
       cout<<TextX<<" "<<TextY<<endl;
     }

     double steplength = legendymax - legendymin;
     legendymax = legendymax - (TextCount + 0.5) * (1.0 / CustomFigureCount[1]) * steplength;
     legendymin = legendymin - (TextCount + 0.5) * (1.0 / CustomFigureCount[1]) * steplength;

     legend1 = new TLegend(legendxmin * (1 - LeftMargin - RightMargin) + LeftMargin, legendymin * (1 - TopMargin - BottomMargin) + BottomMargin, legendxmax * (1 - LeftMargin - RightMargin) + LeftMargin, legendymax * (1 - TopMargin - BottomMargin) + BottomMargin);

     for(int i = 1; i <= CustomFigureCount[1]; i++){
       if(!((TString)myCustomLegendName[1][i] == "")) legend1->AddEntry(Custom[1][i], myCustomLegendName[1][i],"pl");
       //if((TString)myCustomLegendName[1][i] == "") legend1->AddEntry(Custom[1][i], myCustomLegendName[1][i]);
     }
     legend1->SetNColumns(1);
     legend1->Draw("same");
     legend1->SetFillColor(0);
     legend1->SetFillStyle(0);
     legend1->SetLineColor(0);
     legend1->SetLineWidth(0);
     legend1->SetTextSize(CustomLegendSize[1]);
     legend1->SetTextFont(42);

   }

   if(TotalPadNumber == 2){
     pad1 = new TPad("pad1", "pad1", 0, CustomPadYMin[1], 1.0, CustomPadYMax[1]);
     CustomMultiGraph[1] = new TMultiGraph();

     BottomMargin = CustomBottomMargin[1];
     TopMargin = CustomTopMargin[1];
     LeftMargin = CustomLeftMargin[1];
     RightMargin = CustomRightMargin[1];
     pad1->SetBottomMargin(BottomMargin);
     pad1->SetTopMargin(TopMargin);
     pad1->SetLeftMargin(LeftMargin);
     pad1->SetRightMargin(RightMargin);
     pad1->Draw();
     pad1->cd();

     if(isLogX) gPad->SetLogx();

     Custom[1][1]->SetTitle("");
     //Custom[1][1]->SetStats(0);

     Custom[1][1]->GetYaxis()->SetTitle(myYTitle);
     Custom[1][1]->GetYaxis()->SetTitleSize(CustomYTitleSize[1]);
     Custom[1][1]->GetYaxis()->SetTitleFont(42);
     Custom[1][1]->GetYaxis()->SetTitleOffset(CustomYTitleOffset[1]);

     Custom[1][1]->GetYaxis()->SetLabelSize(CustomYLabelSize[1]);
     Custom[1][1]->GetYaxis()->SetLabelFont(42);

     //Custom[1][1]->Draw(CustomPlot[1][1]);
     CustomMultiGraph[1]->Add(Custom[1][1]);

     gStyle->SetHatchesLineWidth(2);

     YRangeMax = Custom[1][1]->GetMaximum();
     YRangeMin = Custom[1][1]->GetMinimum();
     if(!isSetXRange){
       XRangeMax = h1[1]->GetXaxis()->GetBinUpEdge(Custom[1][1]->GetN());
       XRangeMin = h1[1]->GetXaxis()->GetBinLowEdge(1);
     }

     if(isLogY){
       Custom[1][1]->SetMinimum();
     }

     int iPlot = 2;
     while(iPlot <= CustomFigureCount[1]){
      if(isLogY) Custom[1][iPlot]->SetMinimum();
      //Custom[1][iPlot]->Draw((CustomPlot[1][iPlot] + " same").Data());
      CustomMultiGraph[1]->Add(Custom[1][iPlot]);
      if(YRangeMax < Custom[1][iPlot]->GetMaximum()) YRangeMax = Custom[1][iPlot]->GetMaximum();
      if(YRangeMin > Custom[1][iPlot]->GetMinimum()) YRangeMin = Custom[1][iPlot]->GetMinimum();
      if(!isSetXRange){
        if(XRangeMax < Custom[1][iPlot]->GetXaxis()->GetBinUpEdge(Custom[1][iPlot]->GetN())) XRangeMax = Custom[1][iPlot]->GetXaxis()->GetBinUpEdge(Custom[1][iPlot]->GetN());
        if(XRangeMin > Custom[1][iPlot]->GetXaxis()->GetBinLowEdge(1)) XRangeMin = Custom[1][iPlot]->GetXaxis()->GetBinLowEdge(1);
      }
      iPlot++;
     }
     CustomMultiGraph[1]->Draw(CustomPlot[1][1]);
     CustomMultiGraph[1]->GetXaxis()->SetRangeUser(XRangeMin, XRangeMax);
     CustomMultiGraph[1]->GetYaxis()->SetTitle(myYTitle);
     CustomMultiGraph[1]->GetYaxis()->SetTitleSize(CustomYTitleSize[1]);
     CustomMultiGraph[1]->GetYaxis()->SetTitleFont(42);
     CustomMultiGraph[1]->GetYaxis()->SetTitleOffset(CustomYTitleOffset[1]);
     CustomMultiGraph[1]->GetYaxis()->SetLabelSize(CustomYLabelSize[1]);
     CustomMultiGraph[1]->GetYaxis()->SetLabelFont(42);
     if(isSetCustomYRange[1]) CustomMultiGraph[1]->GetYaxis()->SetRangeUser(CustomYRangeMin[1], CustomYRangeMax[1]);
     if(isSetXRange) CustomMultiGraph[1]->GetXaxis()->SetRangeUser(XRangeMin, XRangeMax);

     if(isLogX) gPad->SetLogx();
     if(isLogY) gPad->SetLogy();

     if(!isSetCustomYRange[1] && !isLogY) Custom[1][1]->GetYaxis()->SetRangeUser(YRangeMin - fabs(YRangeMin) * 0.3, YRangeMax + fabs(YRangeMax) * 0.3);

     if(isSetXRange) Custom[1][1]->GetXaxis()->SetRangeUser(XRangeMin, XRangeMax);
     if(isSetCustomYRange[1]){
       if((!isLogY) || ((CustomYRangeMin[1] > 0) && (CustomYRangeMax[1]) > 0)){
          Custom[1][1]->GetYaxis()->SetRangeUser(CustomYRangeMin[1], CustomYRangeMax[1]);
       }
       if(CustomYRangeMin[1] < 0.0){
         YRangeMin = CustomYRangeMin[1] / 1.3;
       }
       if(CustomYRangeMin[1] >= 0.0){
         YRangeMin = CustomYRangeMin[1] / 0.7;
       }
       if(CustomYRangeMax[1] < 0.0){
         YRangeMax = CustomYRangeMax[1] / 0.7;
       }
       if(CustomYRangeMax[1] >= 0.0){
         YRangeMax = CustomYRangeMax[1] / 1.3;
       }
     }

     double textlength = (legendymax - legendymin) / CustomFigureCount[1];
     int iText = 1;
     while(iText<=TextCount){
       text[iText] = new TLatex();

       TextX = XRangeMin + (XRangeMax - XRangeMin) * (legendxmin);
       if(isLogX) TextX = pow(10, (log10(XRangeMin) + (0 - log10(XRangeMin)) * legendxmin));
       TextY = (YRangeMax + fabs(YRangeMax) * 0.3) - (YRangeMax + fabs(YRangeMax) * 0.3 - YRangeMin + fabs(YRangeMin) * 0.3) * ((1 - legendymax) + iText * textlength);
       text[iText]->SetText(TextX, TextY, myText[iText]);
       text[iText]->SetTextSize(CustomLegendSize[1]);
       text[iText]->SetTextFont(72);
       text[iText]->Draw("same");
       iText++;
     }

     double steplength = legendymax - legendymin;
     legendymax = legendymax - (TextCount + 0.5) * (1.0 / CustomFigureCount[1]) * steplength;
     legendymin = legendymin - (TextCount + 0.5) * (1.0 / CustomFigureCount[1]) * steplength;

     legend1 = new TLegend(legendxmin * (1 - LeftMargin - RightMargin) + LeftMargin, legendymin * (1 - TopMargin - BottomMargin) + BottomMargin, legendxmax * (1 - LeftMargin - RightMargin) + LeftMargin, legendymax * (1 - TopMargin - BottomMargin) + BottomMargin);

     for(int i = 1; i < CustomFigureCount[1] + 1; i++){
       if(!((TString)myCustomLegendName[1][i] == "")) legend1->AddEntry(Custom[1][i], myCustomLegendName[1][i],"pe");
//       if(!((TString)myCustomLegendName[1][i] == "")) legend1->AddEntry(Custom[1][i], myCustomLegendName[1][i],"lpfe");
     }

     legend1->SetNColumns(1);
     legend1->Draw("same");
     legend1->SetFillColor(0);
     legend1->SetFillStyle(0);
     legend1->SetLineColor(0);
     legend1->SetLineWidth(0);
     legend1->SetTextSize(CustomLegendSize[1]);
     legend1->SetTextFont(42);

     MyN->cd();
     pad2 = new TPad("pad2", "pad2", 0, CustomPadYMin[2], 1, CustomPadYMax[2]);
     CustomMultiGraph[2] = new TMultiGraph();

     BottomMargin = CustomBottomMargin[2];
     TopMargin = CustomTopMargin[2];
     LeftMargin = CustomLeftMargin[2];
     RightMargin = CustomRightMargin[2];
     pad2->SetBottomMargin(BottomMargin);
     pad2->SetTopMargin(TopMargin);
     pad2->SetLeftMargin(LeftMargin);
     pad2->SetRightMargin(RightMargin);
     if(isCustomGrid[2]) pad2->SetGridx();
     if(isCustomGrid[2]) pad2->SetGridy();
     pad2->Draw();
     pad2->cd();

     Custom[2][1]->SetTitle("");
     //Custom[2][1]->SetStats(0);

     Custom[2][1]->GetYaxis()->SetNdivisions(CustomNdivisions[2]);

     YRangeMax = Custom[2][1]->GetMaximum();
     YRangeMin = Custom[2][1]->GetMinimum();
     iPlot = 2;
     while(iPlot <= CustomFigureCount[2]){
       if(YRangeMax < Custom[2][iPlot]->GetMaximum()) YRangeMax = Custom[2][iPlot]->GetMaximum();
       if(YRangeMin > Custom[2][iPlot]->GetMinimum()) YRangeMin = Custom[2][iPlot]->GetMinimum();
       iPlot++;
     }

     if(isSetXRange) Custom[2][1]->GetXaxis()->SetRangeUser(XRangeMin, XRangeMax);
     if(!isSetXRange){
       XRangeMax = h1[1]->GetXaxis()->GetBinUpEdge(Custom[2][1]->GetN());
       XRangeMin = h1[1]->GetXaxis()->GetBinLowEdge(1);
     }
     if(!isSetCustomYRange[2]) Custom[2][1]->GetYaxis()->SetRangeUser(YRangeMin * 0.7, YRangeMax * 1.3);
     if(isSetCustomYRange[2]) Custom[2][1]->GetYaxis()->SetRangeUser(CustomYRangeMin[2], CustomYRangeMax[2]);

     Custom[2][1]->GetYaxis()->SetTitle(myPadName[2]);
     Custom[2][1]->GetYaxis()->SetTitleSize(CustomYTitleSize[2]);
     Custom[2][1]->GetYaxis()->SetTitleFont(42);
     Custom[2][1]->GetYaxis()->SetTitleOffset(CustomYTitleOffset[2]);

     Custom[2][1]->GetYaxis()->SetLabelFont(42);// Absolute font size in pixel (precision 3)
     Custom[2][1]->GetYaxis()->SetLabelSize(CustomYLabelSize[2]);

//     Custom[2][1]->SetLineWidth(2);

//     Custom[2][1]->GetXaxis()->SetTitle(myXTitle);
//     Custom[2][1]->GetXaxis()->SetTitleSize(CustomXTitleSize[2]);
//     Custom[2][1]->GetXaxis()->SetTitleFont(42);
//     Custom[2][1]->GetXaxis()->SetTitleOffset(CustomXTitleOffset[2]);

//     Custom[2][1]->GetXaxis()->SetLabelSize(CustomXLabelSize[2]);
//     Custom[2][1]->GetXaxis()->SetLabelFont(42);


     //Custom[2][1]->Draw(CustomPlot[2][1]);
     CustomMultiGraph[2]->Add(Custom[2][1]);

     iPlot = 2;
     while(iPlot <= CustomFigureCount[2]){
      //Custom[2][iPlot]->Draw((CustomPlot[2][iPlot] + " same").Data());
      CustomMultiGraph[2]->Add(Custom[2][iPlot]);
      iPlot++;
     }
     pad2->cd();
     CustomMultiGraph[2]->Draw(CustomPlot[2][1]);
     CustomMultiGraph[2]->GetXaxis()->SetRangeUser(XRangeMin, XRangeMax);
     CustomMultiGraph[2]->GetYaxis()->SetLabelFont(42);// Absolute font size in pixel (precision 3)
     CustomMultiGraph[2]->GetYaxis()->SetLabelSize(CustomYLabelSize[2]);
     CustomMultiGraph[2]->GetYaxis()->SetNdivisions(CustomNdivisions[2]);
     CustomMultiGraph[2]->GetXaxis()->SetTitle(myXTitle);
     CustomMultiGraph[2]->GetXaxis()->SetTitleSize(CustomXTitleSize[2]);
     CustomMultiGraph[2]->GetXaxis()->SetTitleFont(42);
     CustomMultiGraph[2]->GetXaxis()->SetTitleOffset(CustomXTitleOffset[2]);
     CustomMultiGraph[2]->GetXaxis()->SetLabelSize(CustomXLabelSize[2]);
     CustomMultiGraph[2]->GetXaxis()->SetLabelFont(42);
     if(isSetXRange) CustomMultiGraph[2]->GetXaxis()->SetRangeUser(XRangeMin, XRangeMax);
     if(isSetCustomYRange[2]) CustomMultiGraph[2]->GetYaxis()->SetRangeUser(CustomYRangeMin[2], CustomYRangeMax[2]);

     if(isLogX) gPad->SetLogx();

     if(isDrawLegend2){
       legend2 = new TLegend(legend2xmin * (1 - LeftMargin - RightMargin) + LeftMargin, legend2ymin * (1 - TopMargin - BottomMargin) + BottomMargin, legend2xmax * (1 - LeftMargin - RightMargin) + LeftMargin, legend2ymax * (1 - TopMargin - BottomMargin) + BottomMargin);

       for(int i = 1; i < CustomFigureCount[2] + 1; i++){
         if(!((TString)myCustomLegendName[2][i] == "")) legend2->AddEntry(Custom[2][i], myCustomLegendName[2][i],"pe");
       }

       legend2->SetNColumns(1);
       legend2->Draw("same");
       legend2->SetFillColor(0);
       legend2->SetFillStyle(0);
       legend2->SetLineColor(0);
       legend2->SetLineWidth(0);
       legend2->SetTextSize(CustomLegendSize[2]);
       legend2->SetTextFont(42);
     }

   }
 }

 if(DoSave) MyN->SaveAs(myFigureName);
// delete MyN;
}

