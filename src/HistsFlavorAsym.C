#include "HistsFlavorAsym.h"

using namespace std;

void HistsFlavorAsym::bookHists(int TotalThread)
{
// int CMS_mass_bin = 14;
// double RangeCMSMass[15] = {40.0, 50.0, 60.0, 76.0, 86.0, 96.0, 106.0, 120.0, 133.0, 150.0, 171.0, 200.0, 320.0, 500.0, 2000.0};
// int CMS_mass_bin = 7;
// double RangeCMSMass[8] = {60.0, 76.0, 86.0, 96.0, 106.0, 120.0, 133.0, 150.0};
 int CMS_mass_bin = 35;
 double RangeCMSMass[36] = {60.0, 62.0, 64.0, 66.0, 68.0, 70.0, 72.0, 74.0, 76.0, 78.0, 80.0, 82.0, 84.0, 86.0, 88.0, 90.0,
                           92.0, 94.0, 96.0, 98.0, 100.0, 102.0, 104.0, 106.0, 108.0, 110.0, 112.0, 114.0, 116.0, 118.0, 120.0,
                           122.0, 124.0, 126.0, 128.0, 130.0};

 int CMS_ZPt_bin = 1;
 double RangeCMSZPt[2] = {0.0, 10000.0};
// int CMS_ZPt_Fine_bin = 17;
// double RangeCMSFineZPt[18] = {0.0, 5.0, 10.0, 15.0, 20.0, 25.0, 30.0, 35.0, 40.0, 45.0, 50.0, 60.0, 80.0, 100.0, 300.0, 500.0, 1000.0, 10000.0};
 int CMS_ZPt_Fine_bin = 1;
 double RangeCMSFineZPt[17] = {0.0, 10000.0};

// int CMS_ZY_bin = 4;
// double RangeCMSZY[5] = {0.0, 1.0, 1.25, 1.5, 2.4};
 int CMS_ZY_bin = 1;
 double RangeCMSZY[2] = {2.2, 2.5};

 this->TotalThread = TotalThread;
 TString tail = "";

 for(int i = 0; i < TotalThread; i++){
   BookHist(i, v_ZRapidity, ZRapidity[i], "ZRapidity", "ZRapidity", 20, -5, 5);
   BookHist(i, v_ChangePDF_ZRapidity, ChangePDF_ZRapidity[i], "ChangePDF_ZRapidity", "ChangePDF_ZRapidity", 20, -5, 5);

   Book3DHist1(i, v_FZMass_ZY_QT, FZMass_ZY_QT[i], "FZMass_ZY_QT", "FZMass_ZY_QT", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_bin, RangeCMSZPt);
   Book3DHist1(i, v_BZMass_ZY_QT, BZMass_ZY_QT[i], "BZMass_ZY_QT", "BZMass_ZY_QT", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_bin, RangeCMSZPt);

   Book3DHist1(i, v_ZMass_ZY_QT, ZMass_ZY_QT[i], "ZMass_ZY_QT", "ZMass_ZY_QT", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);
   Book3DHist1(i, v_ChangePDF_ZMass_ZY_QT, ChangePDF_ZMass_ZY_QT[i], "ChangePDF_ZMass_ZY_QT", "ChangePDF_ZMass_ZY_QT", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);

   Book3DHist1(i, v_FZMass_ZY_QT_uu, FZMass_ZY_QT_uu[i], "FZMass_ZY_QT_uu", "FZMass_ZY_QT_uu", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);
   Book3DHist1(i, v_BZMass_ZY_QT_uu, BZMass_ZY_QT_uu[i], "BZMass_ZY_QT_uu", "BZMass_ZY_QT_uu", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);
   Book3DHist1(i, v_FZMass_ZY_QT_dd, FZMass_ZY_QT_dd[i], "FZMass_ZY_QT_dd", "FZMass_ZY_QT_dd", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);
   Book3DHist1(i, v_BZMass_ZY_QT_dd, BZMass_ZY_QT_dd[i], "BZMass_ZY_QT_dd", "BZMass_ZY_QT_dd", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);

   Book3DHist1(i, v_ChangePDF_FZMass_ZY_QT_uu, ChangePDF_FZMass_ZY_QT_uu[i], "ChangePDF_FZMass_ZY_QT_uu", "ChangePDF_FZMass_ZY_QT_uu", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);
   Book3DHist1(i, v_ChangePDF_BZMass_ZY_QT_uu, ChangePDF_BZMass_ZY_QT_uu[i], "ChangePDF_BZMass_ZY_QT_uu", "ChangePDF_BZMass_ZY_QT_uu", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);
   Book3DHist1(i, v_ChangePDF_FZMass_ZY_QT_dd, ChangePDF_FZMass_ZY_QT_dd[i], "ChangePDF_FZMass_ZY_QT_dd", "ChangePDF_FZMass_ZY_QT_dd", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);
   Book3DHist1(i, v_ChangePDF_BZMass_ZY_QT_dd, ChangePDF_BZMass_ZY_QT_dd[i], "ChangePDF_BZMass_ZY_QT_dd", "ChangePDF_BZMass_ZY_QT_dd", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);


   Book3DHist1(i, v_ZMass_ZY_QT_uu, ZMass_ZY_QT_uu[i], "ZMass_ZY_QT_uu", "ZMass_ZY_QT_uu", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);
   Book3DHist1(i, v_ZMass_ZY_QT_dd, ZMass_ZY_QT_dd[i], "ZMass_ZY_QT_dd", "ZMass_ZY_QT_dd", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);
   Book3DHist1(i, v_ZMass_ZY_QT_ss, ZMass_ZY_QT_ss[i], "ZMass_ZY_QT_ss", "ZMass_ZY_QT_ss", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);
   Book3DHist1(i, v_ZMass_ZY_QT_cc, ZMass_ZY_QT_cc[i], "ZMass_ZY_QT_cc", "ZMass_ZY_QT_cc", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);
   Book3DHist1(i, v_ZMass_ZY_QT_bb, ZMass_ZY_QT_bb[i], "ZMass_ZY_QT_bb", "ZMass_ZY_QT_bb", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);

   Book3DHist1(i, v_ZMass_ZY_QT_uu_total, ZMass_ZY_QT_uu_total[i], "ZMass_ZY_QT_uu_total", "ZMass_ZY_QT_uu_total", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);
   Book3DHist1(i, v_ZMass_ZY_QT_uu_wrong, ZMass_ZY_QT_uu_wrong[i], "ZMass_ZY_QT_uu_wrong", "ZMass_ZY_QT_uu_wrong", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);
   Book3DHist1(i, v_ZMass_ZY_QT_dd_total, ZMass_ZY_QT_dd_total[i], "ZMass_ZY_QT_dd_total", "ZMass_ZY_QT_dd_total", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);
   Book3DHist1(i, v_ZMass_ZY_QT_dd_wrong, ZMass_ZY_QT_dd_wrong[i], "ZMass_ZY_QT_dd_wrong", "ZMass_ZY_QT_dd_wrong", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);

   Book3DHist1(i, v_ChangePDF_ZMass_ZY_QT_uu_total, ChangePDF_ZMass_ZY_QT_uu_total[i], "ChangePDF_ZMass_ZY_QT_uu_total", "ChangePDF_ZMass_ZY_QT_uu_total", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);
   Book3DHist1(i, v_ChangePDF_ZMass_ZY_QT_uu_wrong, ChangePDF_ZMass_ZY_QT_uu_wrong[i], "ChangePDF_ZMass_ZY_QT_uu_wrong", "ChangePDF_ZMass_ZY_QT_uu_wrong", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);
   Book3DHist1(i, v_ChangePDF_ZMass_ZY_QT_dd_total, ChangePDF_ZMass_ZY_QT_dd_total[i], "ChangePDF_ZMass_ZY_QT_dd_total", "ChangePDF_ZMass_ZY_QT_dd_total", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);
   Book3DHist1(i, v_ChangePDF_ZMass_ZY_QT_dd_wrong, ChangePDF_ZMass_ZY_QT_dd_wrong[i], "ChangePDF_ZMass_ZY_QT_dd_wrong", "ChangePDF_ZMass_ZY_QT_dd_wrong", CMS_mass_bin, RangeCMSMass, CMS_ZY_bin, RangeCMSZY, CMS_ZPt_Fine_bin, RangeCMSFineZPt);

   /*************************/
   /*  W related histogram  */
   /*************************/
   BookHist(i, v_WY_udbar, WY_udbar[i], "WY_udbar", "WY_udbar", 20, -5, 5);
   BookHist(i, v_WY_usbar, WY_usbar[i], "WY_usbar", "WY_usbar", 20, -5, 5);
   BookHist(i, v_WY_ubbar, WY_ubbar[i], "WY_ubbar", "WY_ubbar", 20, -5, 5);
   BookHist(i, v_WY_cdbar, WY_cdbar[i], "WY_cdbar", "WY_cdbar", 20, -5, 5);
   BookHist(i, v_WY_csbar, WY_csbar[i], "WY_csbar", "WY_csbar", 20, -5, 5);
   BookHist(i, v_WY_cbbar, WY_cbbar[i], "WY_cbbar", "WY_cbbar", 20, -5, 5);
   BookHist(i, v_WY_dbaru, WY_dbaru[i], "WY_dbaru", "WY_dbaru", 20, -5, 5);
   BookHist(i, v_WY_sbaru, WY_sbaru[i], "WY_sbaru", "WY_sbaru", 20, -5, 5);
   BookHist(i, v_WY_bbaru, WY_bbaru[i], "WY_bbaru", "WY_bbaru", 20, -5, 5);
   BookHist(i, v_WY_dbarc, WY_dbarc[i], "WY_dbarc", "WY_dbarc", 20, -5, 5);
   BookHist(i, v_WY_sbarc, WY_sbarc[i], "WY_sbarc", "WY_sbarc", 20, -5, 5);
   BookHist(i, v_WY_bbarc, WY_bbarc[i], "WY_bbarc", "WY_bbarc", 20, -5, 5);
   BookHist(i, v_WY_dubar, WY_dubar[i], "WY_dubar", "WY_dubar", 20, -5, 5);
   BookHist(i, v_WY_subar, WY_subar[i], "WY_subar", "WY_subar", 20, -5, 5);
   BookHist(i, v_WY_bubar, WY_bubar[i], "WY_bubar", "WY_bubar", 20, -5, 5);
   BookHist(i, v_WY_dcbar, WY_dcbar[i], "WY_dcbar", "WY_dcbar", 20, -5, 5);
   BookHist(i, v_WY_scbar, WY_scbar[i], "WY_scbar", "WY_scbar", 20, -5, 5);
   BookHist(i, v_WY_bcbar, WY_bcbar[i], "WY_bcbar", "WY_bcbar", 20, -5, 5);
   BookHist(i, v_WY_ubard, WY_ubard[i], "WY_ubard", "WY_ubard", 20, -5, 5);
   BookHist(i, v_WY_ubars, WY_ubars[i], "WY_ubars", "WY_ubars", 20, -5, 5);
   BookHist(i, v_WY_ubarb, WY_ubarb[i], "WY_ubarb", "WY_ubarb", 20, -5, 5);
   BookHist(i, v_WY_cbard, WY_cbard[i], "WY_cbard", "WY_cbard", 20, -5, 5);
   BookHist(i, v_WY_cbars, WY_cbars[i], "WY_cbars", "WY_cbars", 20, -5, 5);
   BookHist(i, v_WY_cbarb, WY_cbarb[i], "WY_cbarb", "WY_cbarb", 20, -5, 5);
   BookHist(i, v_WY_udbar_total, WY_udbar_total[i], "WY_udbar_total", "WY_udbar_total", 20, -5, 5);
   BookHist(i, v_WY_csbar_total, WY_csbar_total[i], "WY_csbar_total", "WY_csbar_total", 20, -5, 5);
   BookHist(i, v_WY_dubar_total, WY_dubar_total[i], "WY_dubar_total", "WY_dubar_total", 20, -5, 5);
   BookHist(i, v_WY_scbar_total, WY_scbar_total[i], "WY_scbar_total", "WY_scbar_total", 20, -5, 5);

   BookHist(i, v_WPlusY, WPlusY[i], "WPlusY", "WPlusY", 20, -5, 5);
   BookHist(i, v_WMinusY, WMinusY[i], "WMinusY", "WMinusY", 20, -5, 5);

   BookHist(i, v_LeptonEta, LeptonEta[i], "LeptonEta", "LeptonEta", 20, 0, 5);
   BookHist(i, v_AntiLeptonEta, AntiLeptonEta[i], "AntiLeptonEta", "AntiLeptonEta", 20, 0, 5);
   BookHist(i, v_ChangePDF_LeptonEta, ChangePDF_LeptonEta[i], "ChangePDF_LeptonEta", "ChangePDF_LeptonEta", 20, 0, 5);
   BookHist(i, v_ChangePDF_AntiLeptonEta, ChangePDF_AntiLeptonEta[i], "ChangePDF_AntiLeptonEta", "AntiLeptonEta", 20, 0, 5);

   BookHist(i, v_FWPlusY, FWPlusY[i], "FWPlusY", "FWPlusY", 20, -5, 5);
   BookHist(i, v_BWPlusY, BWPlusY[i], "BWPlusY", "BWPlusY", 20, -5, 5);
   BookHist(i, v_FWMinusY, FWMinusY[i], "FWMinusY", "FWMinusY", 20, -5, 5);
   BookHist(i, v_BWMinusY, BWMinusY[i], "BWMinusY", "BWMinusY", 20, -5, 5);
   BookHist(i, v_FWPlusY_reco_80385, FWPlusY_reco_80385[i], "FWPlusY_reco_80385", "FWPlusY_reco_80385", 20, -5, 5);
   BookHist(i, v_BWPlusY_reco_80385, BWPlusY_reco_80385[i], "BWPlusY_reco_80385", "BWPlusY_reco_80385", 20, -5, 5);
   BookHist(i, v_FWMinusY_reco_80385, FWMinusY_reco_80385[i], "FWMinusY_reco_80385", "FWMinusY_reco_80385", 20, -5, 5);
   BookHist(i, v_BWMinusY_reco_80385, BWMinusY_reco_80385[i], "BWMinusY_reco_80385", "BWMinusY_reco_80385", 20, -5, 5);
   BookHist(i, v_FWPlusY_reco_80395, FWPlusY_reco_80395[i], "FWPlusY_reco_80395", "FWPlusY_reco_80395", 20, -5, 5);
   BookHist(i, v_BWPlusY_reco_80395, BWPlusY_reco_80395[i], "BWPlusY_reco_80395", "BWPlusY_reco_80395", 20, -5, 5);
   BookHist(i, v_FWMinusY_reco_80395, FWMinusY_reco_80395[i], "FWMinusY_reco_80395", "FWMinusY_reco_80395", 20, -5, 5);
   BookHist(i, v_BWMinusY_reco_80395, BWMinusY_reco_80395[i], "BWMinusY_reco_80395", "BWMinusY_reco_80395", 20, -5, 5);
   BookHist(i, v_FWPlusY_reco_80375, FWPlusY_reco_80375[i], "FWPlusY_reco_80375", "FWPlusY_reco_80375", 20, -5, 5);
   BookHist(i, v_BWPlusY_reco_80375, BWPlusY_reco_80375[i], "BWPlusY_reco_80375", "BWPlusY_reco_80375", 20, -5, 5);
   BookHist(i, v_FWMinusY_reco_80375, FWMinusY_reco_80375[i], "FWMinusY_reco_80375", "FWMinusY_reco_80375", 20, -5, 5);
   BookHist(i, v_BWMinusY_reco_80375, BWMinusY_reco_80375[i], "BWMinusY_reco_80375", "BWMinusY_reco_80375", 20, -5, 5);
   BookHist(i, v_FWPlusMass, FWPlusMass[i], "FWPlusMass", "FWPlusMass", 30, 60, 120);
   BookHist(i, v_BWPlusMass, BWPlusMass[i], "BWPlusMass", "BWPlusMass", 30, 60, 120);
   BookHist(i, v_FWMinusMass, FWMinusMass[i], "FWMinusMass", "FWMinusMass", 30, 60, 120);
   BookHist(i, v_BWMinusMass, BWMinusMass[i], "BWMinusMass", "BWMinusMass", 30, 60, 120);

   BookHist(i, v_WPlusY_wrong, WPlusY_wrong[i], "WPlusY_wrong", "WPlusY_wrong", 20, -5, 5);
   BookHist(i, v_WPlusY_total, WPlusY_total[i], "WPlusY_total", "WPlusY_total", 20, -5, 5);
   BookHist(i, v_WMinusY_wrong, WMinusY_wrong[i], "WMinusY_wrong", "WMinusY_wrong", 20, -5, 5);
   BookHist(i, v_WMinusY_total, WMinusY_total[i], "WMinusY_total", "WMinusY_total", 20, -5, 5);

   tail = (TString)"_" + (int)i;
 }

 tail = "";

 AFB_Mass_ZY_ZPt = (TH3D *)v_FZMass_ZY_QT.at(0)->Clone("AFB_ZMass_ZY_QT");
 AFB_Mass_ZY_ZPt->Reset();

 AFB_Mass_ZY_ZPt_uu = (TH3D *)v_FZMass_ZY_QT_uu.at(0)->Clone("AFB_ZMass_ZY_QT_uu");
 AFB_Mass_ZY_ZPt_uu->Reset();
 AFB_Mass_ZY_ZPt_dd = (TH3D *)v_FZMass_ZY_QT_dd.at(0)->Clone("AFB_ZMass_ZY_QT_dd");
 AFB_Mass_ZY_ZPt_dd->Reset();

 AFB_ChangePDF_Mass_ZY_ZPt_uu = (TH3D *)v_ChangePDF_FZMass_ZY_QT_uu.at(0)->Clone("AFB_ChangePDF_ZMass_ZY_QT_uu");
 AFB_ChangePDF_Mass_ZY_ZPt_uu->Reset();
 AFB_ChangePDF_Mass_ZY_ZPt_dd = (TH3D *)v_ChangePDF_FZMass_ZY_QT_dd.at(0)->Clone("AFB_ChangePDF_ZMass_ZY_QT_dd");
 AFB_ChangePDF_Mass_ZY_ZPt_dd->Reset();

 Wasymmetry = (TH1D *)v_LeptonEta.at(0)->Clone("Wasymmetry");
 Wasymmetry->Reset();
 ChangePDF_Wasymmetry = (TH1D *)v_ChangePDF_LeptonEta.at(0)->Clone("ChangePDF_Wasymmetry");
 ChangePDF_Wasymmetry->Reset();

 AFB_WPlusY = (TH1D *)v_FWPlusY.at(0)->Clone("AFB_WPlusY");
 AFB_WPlusY->Reset();
 AFB_WMinusY = (TH1D *)v_FWMinusY.at(0)->Clone("AFB_WMinusY");
 AFB_WMinusY->Reset();
 AFB_WPlusY_reco_80385 = (TH1D *)v_FWPlusY_reco_80385.at(0)->Clone("AFB_WPlusY_reco_80385");
 AFB_WPlusY_reco_80385->Reset();
 AFB_WMinusY_reco_80385 = (TH1D *)v_FWMinusY_reco_80385.at(0)->Clone("AFB_WMinusY_reco_80385");
 AFB_WMinusY_reco_80385->Reset();
 AFB_WPlusY_reco_80395 = (TH1D *)v_FWPlusY_reco_80395.at(0)->Clone("AFB_WPlusY_reco_80395");
 AFB_WPlusY_reco_80395->Reset();
 AFB_WMinusY_reco_80395 = (TH1D *)v_FWMinusY_reco_80395.at(0)->Clone("AFB_WMinusY_reco_80395");
 AFB_WMinusY_reco_80395->Reset();
 AFB_WPlusY_reco_80375 = (TH1D *)v_FWPlusY_reco_80375.at(0)->Clone("AFB_WPlusY_reco_80375");
 AFB_WPlusY_reco_80375->Reset();
 AFB_WMinusY_reco_80375 = (TH1D *)v_FWMinusY_reco_80375.at(0)->Clone("AFB_WMinusY_reco_80375");
 AFB_WMinusY_reco_80375->Reset();

 AFB_WPlusMass = (TH1D *)v_FWPlusMass.at(0)->Clone("AFB_WPlusMass");
 AFB_WPlusMass->Reset();
 AFB_WMinusMass = (TH1D *)v_FWMinusMass.at(0)->Clone("AFB_WMinusMass");
 AFB_WMinusMass->Reset();

}

void HistsFlavorAsym::outputInformation()
{
 //AFB vs Mass ZY ZPt
 AFBFunction(v_FZMass_ZY_QT.at(0), v_BZMass_ZY_QT.at(0), AFB_Mass_ZY_ZPt);

 //W asymmetry
 GetWasymmetry(v_AntiLeptonEta.at(0), v_LeptonEta.at(0), Wasymmetry);
 GetWasymmetry(v_ChangePDF_AntiLeptonEta.at(0), v_ChangePDF_LeptonEta.at(0), ChangePDF_Wasymmetry); 

 if(RootType != "FittingCMSData"){
   //quark level AFB vs Mass ZY ZPt
   AFBFunction(v_FZMass_ZY_QT_uu.at(0), v_BZMass_ZY_QT_uu.at(0), AFB_Mass_ZY_ZPt_uu);
   AFBFunction(v_FZMass_ZY_QT_dd.at(0), v_BZMass_ZY_QT_dd.at(0), AFB_Mass_ZY_ZPt_dd);

   AFBFunction(v_ChangePDF_FZMass_ZY_QT_uu.at(0), v_ChangePDF_BZMass_ZY_QT_uu.at(0), AFB_ChangePDF_Mass_ZY_ZPt_uu);
   AFBFunction(v_ChangePDF_FZMass_ZY_QT_dd.at(0), v_ChangePDF_BZMass_ZY_QT_dd.at(0), AFB_ChangePDF_Mass_ZY_ZPt_dd);

   //Dilution
   GetCoefficientDilution(v_ZMass_ZY_QT_uu_wrong.at(0), v_ZMass_ZY_QT_uu_total.at(0), v_ZMass_ZY_QT.at(0), CoefficientDilution_ZMass_ZY_QT_uu);
   GetCoefficientDilution(v_ZMass_ZY_QT_dd_wrong.at(0), v_ZMass_ZY_QT_dd_total.at(0), v_ZMass_ZY_QT.at(0), CoefficientDilution_ZMass_ZY_QT_dd);

   GetDilutionAverage(CoefficientDilution_ZMass_ZY_QT_uu, DilutionAverage_ZMass_ZY_QT_uu, ResidualDilution_ZMass_ZY_QT_uu);
   GetDilutionAverage(CoefficientDilution_ZMass_ZY_QT_dd, DilutionAverage_ZMass_ZY_QT_dd, ResidualDilution_ZMass_ZY_QT_dd);

   GetCoefficientDilution(v_ChangePDF_ZMass_ZY_QT_uu_wrong.at(0), v_ChangePDF_ZMass_ZY_QT_uu_total.at(0), v_ChangePDF_ZMass_ZY_QT.at(0), CoefficientDilution_ChangePDF_ZMass_ZY_QT_uu);
   GetCoefficientDilution(v_ChangePDF_ZMass_ZY_QT_dd_wrong.at(0), v_ChangePDF_ZMass_ZY_QT_dd_total.at(0), v_ChangePDF_ZMass_ZY_QT.at(0), CoefficientDilution_ChangePDF_ZMass_ZY_QT_dd);

   GetDilutionAverage(CoefficientDilution_ChangePDF_ZMass_ZY_QT_uu, DilutionAverage_ChangePDF_ZMass_ZY_QT_uu, ResidualDilution_ChangePDF_ZMass_ZY_QT_uu);
   GetDilutionAverage(CoefficientDilution_ChangePDF_ZMass_ZY_QT_dd, DilutionAverage_ChangePDF_ZMass_ZY_QT_dd, ResidualDilution_ChangePDF_ZMass_ZY_QT_dd);

   //W+/W- A4/AFB and Dilution
   AFBFunction(v_FWPlusY.at(0), v_BWPlusY.at(0), AFB_WPlusY);
   AFBFunction(v_FWMinusY.at(0), v_BWMinusY.at(0), AFB_WMinusY);
   AFBFunction(v_FWPlusY_reco_80385.at(0), v_BWPlusY_reco_80385.at(0), AFB_WPlusY_reco_80385);
   AFBFunction(v_FWMinusY_reco_80385.at(0), v_BWMinusY_reco_80385.at(0), AFB_WMinusY_reco_80385);
   AFBFunction(v_FWPlusY_reco_80395.at(0), v_BWPlusY_reco_80395.at(0), AFB_WPlusY_reco_80395);
   AFBFunction(v_FWMinusY_reco_80395.at(0), v_BWMinusY_reco_80395.at(0), AFB_WMinusY_reco_80395);
   AFBFunction(v_FWPlusY_reco_80375.at(0), v_BWPlusY_reco_80375.at(0), AFB_WPlusY_reco_80375);
   AFBFunction(v_FWMinusY_reco_80375.at(0), v_BWMinusY_reco_80375.at(0), AFB_WMinusY_reco_80375);
   AFBFunction(v_FWPlusMass.at(0), v_BWPlusMass.at(0), AFB_WPlusMass);
   AFBFunction(v_FWMinusMass.at(0), v_BWMinusMass.at(0), AFB_WMinusMass);

   Dilution_WPlusY = (TH1D *)v_WPlusY_wrong.at(0)->Clone("Dilution_WPlusY");
   Dilution_WPlusY->Divide(v_WPlusY_total.at(0));
   Dilution_WMinusY = (TH1D *)v_WMinusY_wrong.at(0)->Clone("Dilution_WMinusY");
   Dilution_WMinusY->Divide(v_WMinusY_total.at(0));

   CoefficientDilution_WPlusY = (TH1D *)Dilution_WPlusY->Clone("CoefficientDilution_WPlusY");
   CoefficientDilution_WPlusY->Reset();
   for(int ibin = 1; ibin <= CoefficientDilution_WPlusY->GetNbinsX(); ibin++){
     CoefficientDilution_WPlusY->SetBinContent(ibin, 1.0 - 2.0 * Dilution_WPlusY->GetBinContent(ibin));
     CoefficientDilution_WPlusY->SetBinError(ibin, 2.0 * Dilution_WPlusY->GetBinError(ibin));
   }

   CoefficientDilution_WMinusY = (TH1D *)Dilution_WMinusY->Clone("CoefficientDilution_WMinusY");
   CoefficientDilution_WMinusY->Reset();
   for(int ibin = 1; ibin <= CoefficientDilution_WMinusY->GetNbinsX(); ibin++){
     CoefficientDilution_WMinusY->SetBinContent(ibin, 1.0 - 2.0 * Dilution_WMinusY->GetBinContent(ibin));
     CoefficientDilution_WMinusY->SetBinError(ibin, 2.0 * Dilution_WMinusY->GetBinError(ibin));
   }

   OriginAFB_WPlusY = (TH1D *)AFB_WPlusY->Clone("OriginAFB_WPlusY");
   OriginAFB_WPlusY->Divide(CoefficientDilution_WPlusY);
   OriginAFB_WMinusY = (TH1D *)AFB_WMinusY->Clone("OriginAFB_WMinusY");
   OriginAFB_WMinusY->Divide(CoefficientDilution_WMinusY);


 }

 if(RootType != "FittingCMSData") Save();



}

void HistsFlavorAsym::Save()
{
 hf->cd();

 AFB_Mass_ZY_ZPt->Write();

 if(RootType != "FittingCMSData"){
   AFB_Mass_ZY_ZPt_uu->Write();
   AFB_Mass_ZY_ZPt_dd->Write();

   AFB_ChangePDF_Mass_ZY_ZPt_uu->Write();
   AFB_ChangePDF_Mass_ZY_ZPt_dd->Write();

   CoefficientDilution_ZMass_ZY_QT_uu->Write();
   CoefficientDilution_ZMass_ZY_QT_dd->Write();

   DilutionAverage_ZMass_ZY_QT_uu->Write();
   DilutionAverage_ZMass_ZY_QT_dd->Write();

   ResidualDilution_ZMass_ZY_QT_uu->Write();
   ResidualDilution_ZMass_ZY_QT_dd->Write();

   CoefficientDilution_ChangePDF_ZMass_ZY_QT_uu->Write();
   CoefficientDilution_ChangePDF_ZMass_ZY_QT_dd->Write();

   DilutionAverage_ChangePDF_ZMass_ZY_QT_uu->Write();
   DilutionAverage_ChangePDF_ZMass_ZY_QT_dd->Write();

   ResidualDilution_ChangePDF_ZMass_ZY_QT_uu->Write();
   ResidualDilution_ChangePDF_ZMass_ZY_QT_dd->Write();

 }

 Wasymmetry->Write();
 ChangePDF_Wasymmetry->Write();

 AFB_WPlusY->Write();
 AFB_WMinusY->Write();
 AFB_WPlusY_reco_80385->Write();
 AFB_WMinusY_reco_80385->Write();
 AFB_WPlusY_reco_80395->Write();
 AFB_WMinusY_reco_80395->Write();
 AFB_WPlusY_reco_80375->Write();
 AFB_WMinusY_reco_80375->Write();
 AFB_WPlusMass->Write();
 AFB_WMinusMass->Write();
 Dilution_WPlusY->Write();
 Dilution_WMinusY->Write();
 OriginAFB_WPlusY->Write();
 OriginAFB_WMinusY->Write();

}

void HistsFlavorAsym::Reset()
{
 AFB_Mass_ZY_ZPt->Reset();

 if(RootType != "FittingCMSData"){
   AFB_Mass_ZY_ZPt_uu->Reset();
   AFB_Mass_ZY_ZPt_dd->Reset();

   AFB_ChangePDF_Mass_ZY_ZPt_uu->Reset();
   AFB_ChangePDF_Mass_ZY_ZPt_dd->Reset();

   CoefficientDilution_ZMass_ZY_QT_uu->Reset();
   CoefficientDilution_ZMass_ZY_QT_dd->Reset();

   DilutionAverage_ZMass_ZY_QT_uu->Reset();
   DilutionAverage_ZMass_ZY_QT_dd->Reset();

   ResidualDilution_ZMass_ZY_QT_uu->Reset();
   ResidualDilution_ZMass_ZY_QT_dd->Reset();

   CoefficientDilution_ChangePDF_ZMass_ZY_QT_uu->Reset();
   CoefficientDilution_ChangePDF_ZMass_ZY_QT_dd->Reset();

   DilutionAverage_ChangePDF_ZMass_ZY_QT_uu->Reset();
   DilutionAverage_ChangePDF_ZMass_ZY_QT_dd->Reset();

   ResidualDilution_ChangePDF_ZMass_ZY_QT_uu->Reset();
   ResidualDilution_ChangePDF_ZMass_ZY_QT_dd->Reset();
 }

 Wasymmetry->Reset();
 ChangePDF_Wasymmetry->Reset();

 AFB_WPlusY->Reset();
 AFB_WMinusY->Reset();
 AFB_WPlusY_reco_80385->Reset();
 AFB_WMinusY_reco_80385->Reset();
 AFB_WPlusY_reco_80395->Reset();
 AFB_WMinusY_reco_80395->Reset();
 AFB_WPlusY_reco_80375->Reset();
 AFB_WMinusY_reco_80375->Reset();
 AFB_WPlusMass->Reset();
 AFB_WMinusMass->Reset();
 Dilution_WPlusY->Reset();
 Dilution_WMinusY->Reset();
}

void HistsFlavorAsym::InitialSysName()
{

}
