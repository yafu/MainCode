#include "HistsResBosW.h"

using namespace std;

void HistsResBosW::bookHists(int TotalThread)
{
 double rangeZPt[24] = {0,2.5,5,8,11.4,14.9,18.5,22,25.5,29,32.6,36.4,40.4,44.9,50.2,56.4,63.9,73.4,85.4,105,132,173,253,600};
 double rangeEtaAbs[5] = {0, 1, 2, 3, 5};
 double rangeMET[6] = {25, 35, 45, 55, 65, 100};
 double rangeWPt[6] = {0, 2, 4, 6, 8, 10};
 double rangeWE[6] = {0, 200, 400, 600, 800, 1000};

 this->TotalThread = TotalThread;
 TString tail = "";

 for(int i = 0; i < TotalThread; i++){
   BookHist(i, v_LeptonEtaAbs, LeptonEtaAbs[i], "LeptonEtaAbs", "LeptonEtaAbs", 20, 0, 5);
   BookHist(i, v_AntiLeptonEtaAbs, AntiLeptonEtaAbs[i], "AntiLeptonEtaAbs", "AntiLeptonEtaAbs", 20, 0, 5);

   BookHist(i, v_LeptonEtaAbs_pt35, LeptonEtaAbs_pt35[i], "LeptonEtaAbs_pt35", "LeptonEtaAbs_pt35", 250, 0, 2.5);
   BookHist(i, v_AntiLeptonEtaAbs_pt35, AntiLeptonEtaAbs_pt35[i], "AntiLeptonEtaAbs_pt35", "AntiLeptonEtaAbs_pt35", 250, 0, 2.5);

   BookHist(i, v_LeptonEtaAbs_pt25_35, LeptonEtaAbs_pt25_35[i], "LeptonEtaAbs_pt25_35", "LeptonEtaAbs_pt25_35", 250, 0, 2.5);
   BookHist(i, v_AntiLeptonEtaAbs_pt25_35, AntiLeptonEtaAbs_pt25_35[i], "AntiLeptonEtaAbs_pt25_35", "AntiLeptonEtaAbs_pt25_35", 250, 0, 2.5);

   Book2DHist(i, v_LeptonEtaAbs2D, LeptonEtaAbs2D[i], "LeptonEtaAbs2D", "LeptonEtaAbs2D", 25, 0, 2.5, 20, 0, 100)
   Book2DHist(i, v_AntiLeptonEtaAbs2D, AntiLeptonEtaAbs2D[i], "AntiLeptonEtaAbs2D", "AntiLeptonEtaAbs2D", 25, 0, 2.5, 20, 0, 100)

   Book2DHist(i, v_LeptonEtaAbs_MT2D, LeptonEtaAbs_MT2D[i], "LeptonEtaAbs_MT2D", "LeptonEtaAbs_MT2D", 25, 0, 2.5, 1, 50, 80)
   Book2DHist(i, v_AntiLeptonEtaAbs_MT2D, AntiLeptonEtaAbs_MT2D[i], "AntiLeptonEtaAbs_MT2D", "AntiLeptonEtaAbs_MT2D", 25, 0, 2.5, 1, 50, 80)

   Book2DHist(i, v_LeptonEtaAbs_ut2D, LeptonEtaAbs_ut2D[i], "LeptonEtaAbs_ut2D", "LeptonEtaAbs_ut2D", 25, 0, 2.5, 1, 0, 5)
   Book2DHist(i, v_AntiLeptonEtaAbs_ut2D, AntiLeptonEtaAbs_ut2D[i], "AntiLeptonEtaAbs_ut2D", "AntiLeptonEtaAbs_ut2D", 25, 0, 2.5, 1, 0, 5)

   Book3DHist(i, v_LeptonEtaAbs3D_cut30, LeptonEtaAbs3D_cut30[i], "LeptonEtaAbs3D_cut30", "LeptonEtaAbs3D_cut30", 25, 0, 2.5, 1, 25, 30, 1, 25, 30)
   Book3DHist(i, v_AntiLeptonEtaAbs3D_cut30, AntiLeptonEtaAbs3D_cut30[i], "AntiLeptonEtaAbs3D_cut30", "AntiLeptonEtaAbs3D_cut30", 25, 0, 2.5, 1, 25, 30, 1, 25, 30)

   Book3DHist(i, v_LeptonEtaAbs3D_cut35, LeptonEtaAbs3D_cut35[i], "LeptonEtaAbs3D_cut35", "LeptonEtaAbs3D_cut35", 25, 0, 2.5, 1, 25, 35, 1, 25, 35)
   Book3DHist(i, v_AntiLeptonEtaAbs3D_cut35, AntiLeptonEtaAbs3D_cut35[i], "AntiLeptonEtaAbs3D_cut35", "AntiLeptonEtaAbs3D_cut35", 25, 0, 2.5, 1, 25, 35, 1, 25, 35)

   Book3DHist(i, v_LeptonEtaAbs3D_cut40, LeptonEtaAbs3D_cut40[i], "LeptonEtaAbs3D_cut40", "LeptonEtaAbs3D_cut40", 25, 0, 2.5, 1, 25, 40, 1, 25, 40)
   Book3DHist(i, v_AntiLeptonEtaAbs3D_cut40, AntiLeptonEtaAbs3D_cut40[i], "AntiLeptonEtaAbs3D_cut40", "AntiLeptonEtaAbs3D_cut40", 25, 0, 2.5, 1, 25, 40, 1, 25, 40)

   BookHist(i, v_LeptonEtaAbs_LHCb, LeptonEtaAbs_LHCb[i], "LeptonEtaAbs_LHCb", "LeptonEtaAbs_LHCb", 18, 2.0, 4.5);
   BookHist(i, v_AntiLeptonEtaAbs_LHCb, AntiLeptonEtaAbs_LHCb[i], "AntiLeptonEtaAbs_LHCb", "AntiLeptonEtaAbs_LHCb", 18, 2.0, 4.5);

   BookHist(i, v_WPlusRapidity, WPlusRapidity[i], "WPlusRapidity", "WPlusRapidity", 20, -5.0, 5.0)
   BookHist(i, v_WMinusRapidity, WMinusRapidity[i], "WMinusRapidity", "WMinusRapidity", 20, -5.0, 5.0)
   BookHist(i, v_WPlusPt, WPlusPt[i], "WPlusPt", "WPlusPt", 20, 0.0, 100.0)
   BookHist(i, v_WMinusPt, WMinusPt[i], "WMinusPt", "WMinusPt", 20, 0.0, 100.0)
   BookHist(i, v_WPlusE, WPlusE[i], "WPlusE", "WPlusE", 20, 0.0, 1000.0)
   BookHist(i, v_WMinusE, WMinusE[i], "WMinusE", "WMinusE", 20, 0.0, 1000.0)
   Book2DHist1(i, v_WPlusYPt, WPlusYPt[i], "WPlusYPt", "WPlusYPt", 4, rangeEtaAbs, 5, rangeWPt)
   Book2DHist1(i, v_WMinusYPt, WMinusYPt[i], "WMinusYPt", "WMinusYPt", 4, rangeEtaAbs, 5, rangeWPt)
   Book2DHist1(i, v_WPlusYE, WPlusYE[i], "WPlusYE", "WPlusYE", 4, rangeEtaAbs, 5, rangeWE)
   Book2DHist1(i, v_WMinusYE, WMinusYE[i], "WMinusYE", "WMinusYE", 4, rangeEtaAbs, 5, rangeWE)

   BookHist(i, v_LeptonEta, LeptonEta[i], "LeptonEta", "LeptonEta", 20, -5.0, 5.0);
   BookHist(i, v_AntiLeptonEta, AntiLeptonEta[i], "AntiLeptonEta", "AntiLeptonEta", 20, -5.0, 5.0);

   Book2DHist1(i, v_LeptonEtaMET, LeptonEtaMET[i], "LeptonEtaMET", "LeptonEtaMET", 4, rangeEtaAbs, 5, rangeMET)
   Book2DHist1(i, v_AntiLeptonEtaMET, AntiLeptonEtaMET[i], "AntiLeptonEtaMET", "AntiLeptonEtaMET", 4, rangeEtaAbs, 5, rangeMET)

   BookHist(i, v_WPlusMET, WPlusMET[i], "WPlusMET", "WPlusMET", 20, 25, 100);
   BookHist(i, v_WMinusMET, WMinusMET[i], "WMinusMET", "WMinusMET", 20, 25, 100);

   BookHist(i, v_WPlusMET_LowEta, WPlusMET_LowEta[i], "WPlusMET_LowEta", "WPlusMET_LowEta", 20, 25, 100);
   BookHist(i, v_WMinusMET_LowEta, WMinusMET_LowEta[i], "WMinusMET_LowEta", "WMinusMET_LowEta", 20, 25, 100);

   BookHist(i, v_WPlusMET_HighEta, WPlusMET_HighEta[i], "WPlusMET_HighEta", "WPlusMET_HighEta", 20, 25, 100);
   BookHist(i, v_WMinusMET_HighEta, WMinusMET_HighEta[i], "WMinusMET_HighEta", "WMinusMET_HighEta", 20, 25, 100);

   BookHist(i, v_LeptonPt, LeptonPt[i], "LeptonPt", "LeptonPt", 20, 25, 100);
   BookHist(i, v_AntiLeptonPt, AntiLeptonPt[i], "AntiLeptonPt", "AntiLeptonPt", 20, 25, 100);

   BookHist(i, v_LeptonPt_LowEta, LeptonPt_LowEta[i], "LeptonPt_LowEta", "LeptonPt_LowEta", 20, 25, 100);
   BookHist(i, v_AntiLeptonPt_LowEta, AntiLeptonPt_LowEta[i], "AntiLeptonPt_LowEta", "AntiLeptonPt_LowEta", 20, 25, 100);

   BookHist(i, v_LeptonPt_HighEta, LeptonPt_HighEta[i], "LeptonPt_HighEta", "LeptonPt_HighEta", 20, 25, 100);
   BookHist(i, v_AntiLeptonPt_HighEta, AntiLeptonPt_HighEta[i], "AntiLeptonPt_HighEta", "AntiLeptonPt_HighEta", 20, 25, 100);

   Book2DHist1(i, v_LeptonEtaPt, LeptonEtaPt[i], "LeptonEtaPt", "LeptonEtaPt", 4, rangeEtaAbs, 5, rangeMET)
   Book2DHist1(i, v_AntiLeptonEtaPt, AntiLeptonEtaPt[i], "AntiLeptonEtaPt", "AntiLeptonEtaPt", 4, rangeEtaAbs, 5, rangeMET)

   tail = (TString)"_" + (int)i;
 }

}
