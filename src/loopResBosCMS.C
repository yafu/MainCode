#define loopResBosCMS_cxx
#include <iostream>
#include "loopResBosCMS.h"

using namespace std;

loopResBosCMS::loopResBosCMS(TString RootType)
{
 this->RootType = RootType;

 Sta_TotalNumber = 0;

 if(RootType == "FittingCMSData"){
   myReweighting = new DilutionReweighting();
   //myReweighting->Initial();
 }

 isThread = false;
}

void loopResBosCMS::InputHist(HistsResBosCMS* &myhists)
{
 this->myhists = myhists;

}

void loopResBosCMS::InputTree(TreeForResBosCMS *ResBosCMSTree)
{
 this->ResBosCMSTree = ResBosCMSTree;
 this->RootNames = ResBosCMSTree->RootNames;
 this->isFSR = ResBosCMSTree->isFSR;
 this->isDilution = ResBosCMSTree->isDilution;

 this->FirstFile = ResBosCMSTree->FirstFile;
 this->EndFile = ResBosCMSTree->EndFile;

 this->DivideFile = ResBosCMSTree->DivideFile;

 for(int ifile = FirstFile; ifile < EndFile; ifile++){
   TotalEntries[ifile] = ResBosCMSTree->TotalEntries[ifile];
 }

}

void loopResBosCMS::ReadTree(int ifile)
{
 for(int iEvent = InitialEntry - 1; iEvent < Nentries; iEvent++){
   Sta_TotalNumber++;
   if(isTest) cout<<"Thread No."<<iThread<<": The No."<<Sta_TotalNumber<<" event."<<endl;

   if(Sta_TotalNumber % 1000000 == 0){
     if(isThread) cout<<"Thread No."<<iThread<<": "<<Sta_TotalNumber<<" events have finished."<<endl;
     if(!isThread) cout<<"Condor No."<<iThread<<": "<<Sta_TotalNumber<<" events have finished."<<endl;
   }

   ResBosCMSTree->GetEntry(ifile, iEvent);

   int ieta1=0;
   int ieta2=0;

   Electron.SetPxPyPzE(ResBosCMSTree->ElectronPx, ResBosCMSTree->ElectronPy, ResBosCMSTree->ElectronPz, ResBosCMSTree->ElectronE);
   Positron.SetPxPyPzE(ResBosCMSTree->PositronPx, ResBosCMSTree->PositronPy, ResBosCMSTree->PositronPz, ResBosCMSTree->PositronE);

   Weight = ResBosCMSTree->Weight;
   if(!isfinite(Weight)) continue;

   ZBoson = Electron + Positron;

   double ZRapidity=ZBoson.Rapidity();

   double LargeX = (sqrt(ZBoson.M() * ZBoson.M() + ZBoson.Pt() * ZBoson.Pt()) / ECM) * exp(fabs(ZBoson.Rapidity()));
   double SmallX = (sqrt(ZBoson.M() * ZBoson.M() + ZBoson.Pt() * ZBoson.Pt()) / ECM) * exp(fabs(ZBoson.Rapidity()) * (-1.0));

   cos_theta = calculate_theta(ZBoson, Electron, Positron) * ZRapidity / fabs(ZRapidity);
   phi = calculate_phi(Electron, Positron, (double)ECM / 2.0) * ZRapidity / fabs(ZRapidity);
   phi_eta = calculate_phi_eta(Electron, Positron);

   if(cos_theta > 1) cos_theta = 1.0;
   if(cos_theta < -1) cos_theta = -1.0;

   bool isCC = false;
   bool isCF = false;
   bool isLHCb = false;

   bool isATLAS8TeVZPt = false;
   bool isATLAS13TeVZPt = false;
   bool isCMS13TeVZPt = false;
   bool isLHCb13TeVZPt = false;

   bool isCMS = false;
   if(fabs(Electron.Eta()) > 2.5 && fabs(Electron.Eta()) < 4.9) ieta1=1;//1F
   if(fabs(Electron.Eta()) < 2.5) ieta1=2;                              //1C
   if(fabs(Positron.Eta()) > 2.5 && fabs(Positron.Eta()) < 4.9) ieta2=3;//2F
   if(fabs(Positron.Eta()) < 2.5) ieta2=4;                              //2C

   isCC = (ieta1 * ieta2 == 8) && (Electron.Pt() > 25 && Positron.Pt() > 25);
   isCF = (ieta1 * ieta2 == 4 || ieta1 * ieta2 == 6) && (Electron.Pt() > 25 && Positron.Pt() > 25);
   isLHCb = (Electron.Eta() > 2.0 && Electron.Eta() < 4.5)
          && (Positron.Eta() > 2.0 && Positron.Eta() < 4.5)
          && (Electron.Pt() > 20.0 && Positron.Pt() > 20.0)
          && (ZBoson.M() > 60.0 && ZBoson.M() < 120.0);

   isATLAS8TeVZPt = (fabs(ZBoson.Rapidity()) < 2.4)
                 && (Electron.Pt() > 20.0 && Positron.Pt() > 20.0)
                 && (fabs(Electron.Eta()) < 2.4 && fabs(Positron.Eta()) < 2.4);

   isATLAS13TeVZPt = (Electron.Pt() > 27.0 && Positron.Pt() > 27.0)
                  && (fabs(Electron.Eta()) < 2.5 && fabs(Positron.Eta()) < 2.5)
                  && (ZBoson.M() > 66.0 && ZBoson.M() < 116.0);

   isCMS13TeVZPt = (Electron.Pt() > 25.0 && Positron.Pt() > 25.0)
                && (fabs(Electron.Eta()) < 2.4 && fabs(Positron.Eta()) < 2.4)
                && (ZBoson.M() > 76.1876 && ZBoson.M() < 106.1876)
                && (fabs(ZBoson.Rapidity()) < 2.4);

   isLHCb13TeVZPt = (Electron.Eta() > 2.0 && Electron.Eta() < 4.5)
                 && (Positron.Eta() > 2.0 && Positron.Eta() < 4.5)
                 && (Electron.Pt() > 20.0 && Positron.Pt() > 20.0)
                 && (ZBoson.M() > 60.0 && ZBoson.M() < 120.0);

   isCMS = 1;

   isCMS = isCMS * (fabs(ZBoson.Rapidity()) > 1.5 && fabs(ZBoson.Rapidity()) < 2.4);

   if(!isCMS && RootType == "FittingCMSData") continue;

   bool isF = false;
   bool isB = false;
   isF = (cos_theta > 0);
   isB = (cos_theta < 0);

   if(RootType == "FittingCMSData"){
     ReweightingFactor = myReweighting->GetReweightingFactor(cos_theta, ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), StwValue, P0uValue, P0dValue);
     Weight = ResBosCMSTree->Weight * ReweightingFactor;
   }

   int isWrong;
   if(isDilution){
     bool isUUB = false;
     bool isDDB = false;
     bool isSSB = false;
     bool isCCB = false;
     bool isBBB = false;
     bool isGG = false;
     isUUB = (fabs(fabs(ResBosCMSTree->Type) - 1.0) < 1e-10);
     isDDB = (fabs(fabs(ResBosCMSTree->Type) - 2.0) < 1e-10);
     isSSB = (fabs(fabs(ResBosCMSTree->Type) - 3.0) < 1e-10);
     isCCB = (fabs(fabs(ResBosCMSTree->Type) - 4.0) < 1e-10);
     isBBB = (fabs(fabs(ResBosCMSTree->Type) - 5.0) < 1e-10);
     isGG = isSSB || isCCB || isBBB;

     if(isTest){
       if(isUUB) cout<<"Initial state: uub, Type = "<<ResBosCMSTree->Type<<endl;
       if(isDDB) cout<<"Initial state: ddb, Type = "<<ResBosCMSTree->Type<<endl;
       if(isSSB) cout<<"Initial state: ssb, Type = "<<ResBosCMSTree->Type<<endl;
       if(isCCB) cout<<"Initial state: ccb, Type = "<<ResBosCMSTree->Type<<endl;
       if(isBBB) cout<<"Initial state: bbb, Type = "<<ResBosCMSTree->Type<<endl;
     }

     double CosThetaQ;
     if(ResBosCMSTree->Type > 0) CosThetaQ = calculate_theta(ZBoson, Electron, Positron);
     if(ResBosCMSTree->Type < 0) CosThetaQ = calculate_theta(ZBoson, Electron, Positron) * (-1.0);
     double PhiQ;
     if(ResBosCMSTree->Type > 0) PhiQ = calculate_phi(Electron, Positron, (double)ECM / 2.0);
     if(ResBosCMSTree->Type < 0) PhiQ = calculate_phi(Electron, Positron, (double)ECM / 2.0) * (-1.0);

     if(ResBosCMSTree->Type > 0){
       if(ZBoson.Rapidity() / fabs(ZBoson.Rapidity()) > 0) isWrong = 0;
       if(ZBoson.Rapidity() / fabs(ZBoson.Rapidity()) < 0) isWrong = 1;
     }
     if(ResBosCMSTree->Type < 0){
       if(ZBoson.Rapidity() / fabs(ZBoson.Rapidity()) > 0) isWrong = 1;
       if(ZBoson.Rapidity() / fabs(ZBoson.Rapidity()) < 0) isWrong = 0;
     }

     bool isQuarkF = false;
     bool isQuarkB = false;
     isQuarkF = (CosThetaQ > 0);
     isQuarkB = (CosThetaQ < 0);

     if(RootType != "FittingCMSData"){
       myhists->v_A0_Mass_ZY_QT_uu.at(iThread - 1)->Initial(cos_theta, phi);
       myhists->v_A0_Mass_ZY_QT_uu.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isUUB);
       myhists->v_A0_Mass_ZY_QT_dd.at(iThread - 1)->Initial(cos_theta, phi);
       myhists->v_A0_Mass_ZY_QT_dd.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isDDB);
       myhists->v_A0_Mass_ZY_QT_ss.at(iThread - 1)->Initial(cos_theta, phi);
       myhists->v_A0_Mass_ZY_QT_ss.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isSSB);
       myhists->v_A0_Mass_ZY_QT_cc.at(iThread - 1)->Initial(cos_theta, phi);
       myhists->v_A0_Mass_ZY_QT_cc.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isCCB);
       myhists->v_A0_Mass_ZY_QT_bb.at(iThread - 1)->Initial(cos_theta, phi);
       myhists->v_A0_Mass_ZY_QT_bb.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isBBB);

       myhists->v_ZMass_ZY_QT_uu.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isUUB);
       myhists->v_ZMass_ZY_QT_dd.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isDDB);
       myhists->v_ZMass_ZY_QT_ss.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isSSB);
       myhists->v_ZMass_ZY_QT_cc.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isCCB);
       myhists->v_ZMass_ZY_QT_bb.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isBBB);

       myhists->v_FZMass_ZY_QT_uu.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isQuarkF * isUUB);
       myhists->v_BZMass_ZY_QT_uu.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isQuarkB * isUUB);
       myhists->v_FZMass_ZY_QT_dd.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isQuarkF * isDDB);
       myhists->v_BZMass_ZY_QT_dd.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isQuarkB * isDDB);

       myhists->v_ZMass_ZY_QT_uu_total.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isUUB);
       myhists->v_ZMass_ZY_QT_uu_wrong.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isUUB * isWrong);
       myhists->v_ZMass_ZY_QT_dd_total.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isDDB);
       myhists->v_ZMass_ZY_QT_dd_wrong.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isDDB * isWrong);

       myhists->v_ZMass_ZY_QT_uu_YP_wrong.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isUUB * (ZBoson.Rapidity() > 0.0) * isWrong);
       myhists->v_ZMass_ZY_QT_uu_YP_right.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isUUB * (ZBoson.Rapidity() > 0.0) * (!isWrong));
       myhists->v_ZMass_ZY_QT_uu_YM_wrong.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isUUB * (ZBoson.Rapidity() < 0.0) * isWrong);
       myhists->v_ZMass_ZY_QT_uu_YM_right.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isUUB * (ZBoson.Rapidity() < 0.0) * (!isWrong));
       myhists->v_ZMass_ZY_QT_dd_YP_wrong.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isDDB * (ZBoson.Rapidity() > 0.0) * isWrong);
       myhists->v_ZMass_ZY_QT_dd_YP_right.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isDDB * (ZBoson.Rapidity() > 0.0) * (!isWrong));
       myhists->v_ZMass_ZY_QT_dd_YM_wrong.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isDDB * (ZBoson.Rapidity() < 0.0) * isWrong);
       myhists->v_ZMass_ZY_QT_dd_YM_right.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isDDB * (ZBoson.Rapidity() < 0.0) * (!isWrong));

     }
   }

   if(RootType != "FittingCMSData"){
     myhists->v_ZMass_ZY_QT.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight);
     myhists->v_ZMass_ZY_QT_YP.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * (ZBoson.Rapidity() > 0.0));
     myhists->v_ZMass_ZY_QT_YM.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * (ZBoson.Rapidity() < 0.0));
     myhists->v_ZY_x1.at(iThread - 1)->Fill(ZBoson.Rapidity(), Weight * (fabs(LargeX - 0.05) < 0.01));
     myhists->v_ZY_x2.at(iThread - 1)->Fill(ZBoson.Rapidity(), Weight * (fabs(LargeX - 0.08) < 0.01));
     myhists->v_ZY_x3.at(iThread - 1)->Fill(ZBoson.Rapidity(), Weight * (fabs(LargeX - 0.11) < 0.01));

   }

   myhists->v_FZMass_ZY_QT.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isF);
   myhists->v_BZMass_ZY_QT.at(iThread - 1)->Fill(ZBoson.M(), fabs(ZBoson.Rapidity()), ZBoson.Pt(), Weight * isB);

   //////////////////////////////
   //  For NonPertFit pT Data  //
   //////////////////////////////

   myhists->v_ATLAS8TeV_pT_Mass12_20_Theory.at(iThread - 1)->Fill(ZBoson.Pt(), Weight * isATLAS8TeVZPt * (ZBoson.M() > 12 && ZBoson.M() < 20));
   myhists->v_ATLAS8TeV_pT_Mass20_30_Theory.at(iThread - 1)->Fill(ZBoson.Pt(), Weight * isATLAS8TeVZPt * (ZBoson.M() > 20 && ZBoson.M() < 30));
   myhists->v_ATLAS8TeV_pT_Mass30_46_Theory.at(iThread - 1)->Fill(ZBoson.Pt(), Weight * isATLAS8TeVZPt * (ZBoson.M() > 30 && ZBoson.M() < 46));
   myhists->v_ATLAS8TeV_pT_Mass46_66_Theory.at(iThread - 1)->Fill(ZBoson.Pt(), Weight * isATLAS8TeVZPt * (ZBoson.M() > 46 && ZBoson.M() < 66));
   myhists->v_ATLAS8TeV_pT_Mass66_116_Theory.at(iThread - 1)->Fill(ZBoson.Pt(), Weight * isATLAS8TeVZPt * (ZBoson.M() > 66 && ZBoson.M() < 116));
   myhists->v_ATLAS8TeV_pT_Mass116_150_Theory.at(iThread - 1)->Fill(ZBoson.Pt(), Weight * isATLAS8TeVZPt * (ZBoson.M() > 116 && ZBoson.M() < 150));
   myhists->v_ATLAS8TeV_Phi_Y1_Theory.at(iThread - 1)->Fill(phi_eta, Weight * isATLAS8TeVZPt * (ZBoson.M() > 66 && ZBoson.M() < 116) * (fabs(ZBoson.Rapidity()) > 0.0 && fabs(ZBoson.Rapidity()) < 0.4));
   myhists->v_ATLAS8TeV_Phi_Y2_Theory.at(iThread - 1)->Fill(phi_eta, Weight * isATLAS8TeVZPt * (ZBoson.M() > 66 && ZBoson.M() < 116) * (fabs(ZBoson.Rapidity()) > 0.4 && fabs(ZBoson.Rapidity()) < 0.8));
   myhists->v_ATLAS8TeV_Phi_Y3_Theory.at(iThread - 1)->Fill(phi_eta, Weight * isATLAS8TeVZPt * (ZBoson.M() > 66 && ZBoson.M() < 116) * (fabs(ZBoson.Rapidity()) > 0.8 && fabs(ZBoson.Rapidity()) < 1.2));
   myhists->v_ATLAS8TeV_Phi_Y4_Theory.at(iThread - 1)->Fill(phi_eta, Weight * isATLAS8TeVZPt * (ZBoson.M() > 66 && ZBoson.M() < 116) * (fabs(ZBoson.Rapidity()) > 1.2 && fabs(ZBoson.Rapidity()) < 1.6));
   myhists->v_ATLAS8TeV_Phi_Y5_Theory.at(iThread - 1)->Fill(phi_eta, Weight * isATLAS8TeVZPt * (ZBoson.M() > 66 && ZBoson.M() < 116) * (fabs(ZBoson.Rapidity()) > 1.6 && fabs(ZBoson.Rapidity()) < 2.0));
   myhists->v_ATLAS8TeV_Phi_Y6_Theory.at(iThread - 1)->Fill(phi_eta, Weight * isATLAS8TeVZPt * (ZBoson.M() > 66 && ZBoson.M() < 116) * (fabs(ZBoson.Rapidity()) > 2.0 && fabs(ZBoson.Rapidity()) < 2.4));
   myhists->v_ATLAS13TeV_pT_Theory.at(iThread - 1)->Fill(ZBoson.Pt(), Weight * isATLAS13TeVZPt);
   myhists->v_ATLAS13TeV_Phi_Theory.at(iThread - 1)->Fill(phi_eta, Weight * isATLAS13TeVZPt);
   myhists->v_CMS13TeV_pT_Theory.at(iThread - 1)->Fill(ZBoson.Pt(), Weight * isCMS13TeVZPt);
   myhists->v_CMS13TeV_Phi_Theory.at(iThread - 1)->Fill(phi_eta, Weight * isCMS13TeVZPt);
   myhists->v_LHCb13TeV_pT_Theory.at(iThread - 1)->Fill(ZBoson.Pt(), Weight * isLHCb13TeVZPt);
   myhists->v_LHCb13TeV_Phi_Theory.at(iThread - 1)->Fill(phi_eta, Weight * isLHCb13TeVZPt);

 }

}

void loopResBosCMS::End(int RootNumber)
{
 cout<<"Thread No."<<this->iThread<<": **Running: Free Rootfile: "<<RootNumber + 1<<endl;

 if(!ResBosCMSTree->fChain[RootNumber])
  {
   cout<<"XXXXX**Runing: BIG ERROR!!! No File loadead!"<<endl;
   return;
  }
 delete ResBosCMSTree->fChain[RootNumber]->GetCurrentFile();
}

void loopResBosCMS::Finish()
{
 cout<<"Thread No."<<this->iThread<<": **Total Event Number: "<<this->Sta_TotalNumber<<endl;

 if(EndFile == RootNames.size()) delete ResBosCMSTree;
 if(RootType == "FittingCMSData") myReweighting->Close();

}

loopResBosCMS::~loopResBosCMS()
{
}
