#include <BAT/BCLog.h>
#include <BAT/BCAux.h>
#include <BATHelper/BCSummaryPriorModel.h>
#include <BATHelper/BCModelOutput.h>
#include <BAT/BCH1D.h>

#include <BATHelper/BCMTFAnalysisFacility.h>
#include <BATHelper/BCMultiTemplateFitter.h>
#include <BATHelper/BCChannel.h>
#include <BATHelper/BCSummaryTool.h>

#include "ePumpHelper/ePumpHelper.h"

#include <TROOT.h>
#include <TCanvas.h>
#include <TH1D.h>
#include <TF1.h>
#include <TGraph.h>
#include <TDirectory.h>
#include <TFile.h>
#include <TTree.h>
#include <iostream>
#include <vector>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <stdlib.h>

using std::cerr;
using std::cout;
using std::endl;
using std::vector;
using std::map;
using std::string;

int main(int argc, char ** argv)
{
  TFile* file_ee = new TFile("Events.root");

  TH1D h_ee_bkg = *((TH1D*) file_ee->Get("mass_bkg_binned"));;
  TH1D h_mm_bkg = *((TH1D*) file_ee->Get("mass_bkg_binned"));;

  TH1D h_ee_data = *((TH1D*) file_ee->Get("mass_bkg_binned"));;
  TH1D h_mm_data = *((TH1D*) file_ee->Get("mass_bkg_binned"));;

  vector<TF1*>* ee_funct_vector=new vector<TF1*>(0);
  vector<TF1*>* mm_funct_vector=new vector<TF1*>(0);

  int ThetaBins = 500;
  double maxTheta = 0.01;

  bool doMCMC = true;
  int nEnsemble = 50;

  //################################################################################################################################
  // ----------------------------------------------------
  // configure BAT
  // ----------------------------------------------------
  // set nice style for drawing than the ROOT default
  BCAux::SetStyle();
  // open log file
  BCLog::OpenLog("log.txt");
  BCLog::SetLogLevel(BCLog::summary);

  // ----------------------------------------------------
  // Normalization
  // ----------------------------------------------------
  double Neebkg = h_ee_bkg.Integral();
  double Nmmbkg = h_mm_bkg.Integral();


  // ----------------------------------------------------
  // create new BCMultiTemplateFitter object
  // ----------------------------------------------------
  BCMultiTemplateFitter * m = new BCMultiTemplateFitter();

  m->SetPrecision(BCEngineMCMC::kMedium);

  BCLog::OutSummary("Test model created");

  // create a new summary tool object
  BCSummaryTool * summary = new BCSummaryTool(m);

  // ----------------------------------------------------
  // Define required input
  // ----------------------------------------------------

  std::cerr<<"maxTheta "<<maxTheta<<std::endl;

  m->AddChannel("ee");
  m->AddChannel("mm");

  m->AddProcess("eeBkg", Neebkg, Neebkg);
  m->SetPriorGauss("eeBkg", Neebkg, 0.1);

  m->AddProcess("mmBkg", Nmmbkg, Nmmbkg);
  m->SetPriorGauss("mmBkg", Nmmbkg, 0.1);

  m->AddProcess("Theta",   0., maxTheta);
  m->SetPriorConstant("Theta");

  m->SetData("ee", h_ee_data);
  m->SetData("mm", h_mm_data);

  TH1D h_ee_dummy = TH1D((h_ee_data));
  for(int ibin=0;ibin<=h_ee_dummy.GetNbinsX()+1;ibin++){
    h_ee_dummy.SetBinContent(ibin,0.0);
    h_ee_dummy.SetBinError(ibin,0.0);
  }

  TH1D h_mm_dummy = TH1D((h_mm_data));
  for(int ibin=0;ibin<=h_mm_dummy.GetNbinsX()+1;ibin++){
    h_mm_dummy.SetBinContent(ibin,0.0);
    h_mm_dummy.SetBinError(ibin,0.0);
  }

  m->SetTemplate("ee", "eeBkg", h_ee_bkg, 1.0);
  m->SetTemplate("mm", "mmBkg", h_mm_bkg, 1.0);

  m->SetTemplate("ee", "mmBkg", h_ee_dummy, 1.0);
  m->SetTemplate("mm", "eeBkg", h_mm_dummy, 1.0);

  m->SetTemplate("ee", "Theta", ee_funct_vector,ee_funct_vector->size(),1.0);
  m->SetTemplate("mm", "Theta", mm_funct_vector,mm_funct_vector->size(),1.0);

  //increase number of marginalization bins (higher precision) 
  std::cerr<<"ThetaBins "<<ThetaBins<<std::endl;
  m->SetNbins(ThetaBins);

  // ----------------------------------------------------
  // create prior model for pseudo-experiments
  // ----------------------------------------------------

  // create new prior model
  BCSummaryPriorModel* pm = new BCSummaryPriorModel();
  // set model (and make adjustment suitable for background only pseudo-experiments (PE))
  m->SetParameterRange(0,Neebkg,Neebkg);
  m->SetParameterRange(1,Nmmbkg,Nmmbkg);
  m->SetParameterRange(2,0.0,0.0);//set Theta to zero here (flat prior would incl Theta in PE)
  pm->SetModel(m);

  // ----------------------------------------------------
  // create output object
  // ----------------------------------------------------
  BCModelOutput* pmout = new BCModelOutput(pm, "prior.root");

  // switch writing of Markov Chains on
  pmout->WriteMarkovChain(true);

  // set precision
  pm->SetPrecision(BCEngineMCMC::kMedium);

 // perform marginalization
  pm->MarginalizeAll();

  // get tree
  TTree* priortree = (TTree*) pmout->GetFile()->Get("MarkovChainTree_0");

  //undo PE modifications
  m->SetParameterRange(0,Neebkg,Neebkg);
  m->SetParameterRange(1,Nmmbkg,Nmmbkg);
  m->SetParameterRange(2,0.0,maxTheta);

  // ----------------------------------------------------------------
  // Perform ensemble test
  // ----------------------------------------------------------------

  // create new analysis facility
  BCMTFAnalysisFacility* facility = new BCMTFAnalysisFacility(m);
  facility->SetFlagMCMC(doMCMC);

  // create ensembles
  TTree* tree = facility->BuildEnsembles( priortree, 10000 );

  std::vector<int> details;
  details.push_back(0);
  details.push_back(0);
  //details.push_back(0);
  details.push_back(1);

  // run ensemble test
  TTree* tree_out = facility->PerformEnsembleTest(tree, nEnsemble, details);

  // open new file
  TFile *file = new TFile("zprime_ensembles_mass.root", "RECREATE");
  file->cd();

  // write trees into file
  tree->Write();
  tree_out->Write();

  // close file
  file->Close();

  // free memory
  delete file;

  // close log file
  BCLog::CloseLog();

  // close output file
  pmout->Close();

  // free memory
  delete pm;
  delete pmout;
  delete facility;
  delete m;
  delete summary;

  BCLog::OutSummary("Test program ran successfully");
  BCLog::OutSummary("Exiting");

 return 1;
}

